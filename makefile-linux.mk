#
# Main makefile for Linux
#

# Enable ccache support
PATH:=/usr/lib/ccache:$(PATH)

# Parallel compilation
JOBSOPT=-j 8

build-debug: bin/Debug
	cd bin/Debug && make $(JOBSOPT) $*

bin/Debug:
	mkdir -p $@
	cd bin/Debug && cmake -DCMAKE_BUILD_TYPE=Debug ../..

build-release: bin/Release
	cd bin/Release && make $(JOBSOPT) $*

bin/Release:
	mkdir -p bin/Release
	cd bin/Release && cmake -DCMAKE_BUILD_TYPE=Release ../..

build-doc:
	mkdir -p doc
	doxygen doxygen.cfg

clean: clean-debug clean-release
	rm -rf bin

clean-debug:
	rm -rf bin/Debug

clean-release:
	rm -rf bin/Release

clean-doc:
	rm -rf doc
