/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT      OCTaedr Software
 * @{
 */

#ifndef _OCT__PACKAGE_H_
#define _OCT__PACKAGE_H_

/*---------------------------------------------------------------------------*/

/**
 * OCTaedr namespace.
 *
 * All OCTaedr software elements should be in that namespace.
 */
namespace Oct
{
    // other documentation goes here
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT__PACKAGE_H_*/

/**
 * @}
 */
