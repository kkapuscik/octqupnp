/*
 * JsonPair.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_UTIL__EVENT_THREAD_EXECUTOR_H_
#define _OCT_UTIL__EVENT_THREAD_EXECUTOR_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QObject>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Util
    {
        /**
         * Executor object to be used with EventThread.
         */
        class EventThreadExecutor : public QObject
        {
        Q_OBJECT

        protected:
            /**
             * Constructs executor.
             *
             * @param parent
             *      Owner of the executor.
             *      The EventThread will take ownership of executor
             *      on start of the execution.
             */
            EventThreadExecutor(QObject* parent = 0) :
                QObject(parent)
            {
                // nothing to do
            }

            /**
             * Starts processing.
             *
             * This method is called before starting event loop.
             * Inside this method executor shall create objects and
             * connect them to event loop.
             *
             * @return
             * True if execution start succeeded and thread shall continue,
             * false if execution start failed and thread shall be aborted.
             */
            virtual bool startExecution() = 0;

            /**
             * Finishes processing.
             *
             * This method is called before thread finish.
             * Inside this method executor shall release resources
             * and perform other cleanup tasks.
             */
            virtual void finishExecution() = 0;

            /**
             * Finishes execution.
             *
             * Method calls QThread::quit() on the thread it is actually running in.
             */
            void quit();

        signals:
            /**
             * This signal is emitted just before start of execution.
             */
            void started();

            /**
             * This signal is emitted just after end of execution.
             */
            void finished();

        private:
            /**
             * Starts processing.
             *
             * @return
             * True if execution start succeeded and thread shall continue,
             * false if execution start failed and thread shall be aborted.
             */
            bool start();

            /**
             * Finishes processing.
             */
            void finish();

            // EventThread is calling execution start/end methods
            friend class EventThread;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UTIL__EVENT_THREAD_EXECUTOR_H_ */
