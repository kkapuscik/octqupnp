/*
 * JsonPair.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "EventThreadExecutor.h"

#include <QtCore/QThread>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Util
    {
        bool EventThreadExecutor::start()
        {
            emit started();

            return startExecution();
        }

        void EventThreadExecutor::finish()
        {
            finishExecution();

            emit finished();
        }

        void EventThreadExecutor::quit()
        {
            QThread::currentThread()->quit();
        }
    }
}

/*---------------------------------------------------------------------------*/
