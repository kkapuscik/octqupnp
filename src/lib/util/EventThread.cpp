/*
 * JsonPair.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "EventThread.h"

/*---------------------------------------------------------------------------*/

#include <QtCore/QObject>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Util
    {
        EventThread::EventThread(EventThreadExecutor* executor, QObject* parent) :
            QThread(parent)
        {
            Q_ASSERT(executor != NULL);

            m_executor = executor;
            m_executor->moveToThread(this);
        }

        EventThread::~EventThread()
        {
            delete m_executor;
        }

        void EventThread::run()
        {
            Q_ASSERT(m_executor != NULL);

            if (m_executor->start()) {
                exec();
            }

            m_executor->finish();
        }
    }
}

/*---------------------------------------------------------------------------*/
