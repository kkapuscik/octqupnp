/*
 * JsonPair.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_UTIL__SHARED_DATA_H_
#define _OCT_UTIL__SHARED_DATA_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QAtomicInt>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Util
    {
        // forward
        template <class T>
        class SharedDataPointer;

        /**
         * Base class for shared data classes.
         *
         * Constructors are private as class must be inherited.
         */
        template<class T>
        class SharedData
        {
        private:
            /** References counter. */
            mutable QAtomicInt m_refCount;

        protected:
            /**
             * Constructs new shared data.
             */
            SharedData() :
                m_refCount(0)
            {
                // nothing to do
            }

            /**
             * Constructs new shared data from other object.
             */
            SharedData(const SharedData& data) :
                m_refCount(0)
            {
                // nothing to do
            }

            /**
             * Clones the object.
             *
             * @return
             * Create clone.
             */
            virtual T* clone() const = 0;

        private:
            // using the assignment operator would lead to corruption in the ref-counting
            SharedData &operator=(const SharedData &);

            // friends - classes that uses ref counts
            friend class SharedDataPointer<T>;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UTIL__SHARED_DATA_H_ */
