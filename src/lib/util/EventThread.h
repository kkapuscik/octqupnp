/*
 * JsonPair.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_UTIL__EVENT_THREAD_H_
#define _OCT_UTIL__EVENT_THREAD_H_

/*---------------------------------------------------------------------------*/

#include "EventThreadExecutor.h"

#include <QtCore/QThread>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Util
    {
        /**
         * Thread with event loop.
         */
        class EventThread : public QThread
        {
        Q_OBJECT

        private:
            /** Executor being executed by ths thread. */
            EventThreadExecutor* m_executor;

        public:
            /**
             * Constructs new thread with event loop.
             *
             * Thread is taking object ownership of given executor object.
             *
             * When started it will call EventThreadExecutor::startExecution() to
             * begin processing. If execution start succeeded it will start the
             * event loop and process the events until thread quit is requested.
             * Before finish it will call  EventThreadExecutor::finishExecution()
             * to cleanup the resources.
             *
             * @param executor
             *      Executor to be executed.
             * @param parent
             *      Parent of this thread.
             */
            EventThread(EventThreadExecutor* executor, QObject* parent = 0);

            /**
             * Destroys the thread.
             */
            virtual ~EventThread();

        protected:
            virtual void run();
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UTIL__EVENT_THREAD_H_ */
