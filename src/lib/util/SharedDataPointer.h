/*
 * JsonPair.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_UTIL__SHARED_DATA_POINTER_H_
#define _OCT_UTIL__SHARED_DATA_POINTER_H_

/*---------------------------------------------------------------------------*/

#include "SharedData.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Util
    {
        /**
         * Implicitly shared data pointer.
         */
        template<class T>
        class SharedDataPointer
        {
        private:
            typedef SharedData<T> SharedT;

        private:
            T* m_data;

        public:
            SharedDataPointer() :
                m_data(NULL)
            {
                // nothing to do
            }

            explicit SharedDataPointer(T* data) :
                m_data(data)
            {
                SharedT* d = shared();

                if (d != NULL) {
                    d->m_refCount.ref();
                }
            }

            SharedDataPointer(const SharedDataPointer<T>& other) :
                m_data(other.m_data)
            {
                SharedT* d = shared();

                if (d != NULL) {
                    d->m_refCount.ref();
                }
            }

            inline ~SharedDataPointer()
            {
                SharedT* d = shared();

                if (d != NULL && !d->m_refCount.deref()) {
                    delete d;
                }
            }

            T* data()
            {
                detach();
                return m_data;
            }

            const T* constData() const
            {
                return m_data;
            }

            SharedDataPointer<T>& operator=(const SharedDataPointer<T>& other)
            {
                /* check if it is really new data */
                if (other.m_data != m_data) {
                    /* add reference to new data */
                    if (other.m_data != NULL) {
                        other.shared()->m_refCount.ref();
                    }

                    /* remember old data */
                    SharedT* old = shared();

                    /* store new data */
                    m_data = other.m_data;

                    /* release old data */
                    if (old != NULL && !old->m_refCount.deref()) {
                        delete old;
                    }
                }

                return *this;
            }

            SharedDataPointer& operator=(T* other)
            {
                /* check if it is really new data */
                if (other != m_data) {
                    /* add reference to new data */
                    if (other != NULL) {
                        other->ref.ref();
                    }

                    /* remember old data */
                    SharedT* old = shared();

                    /* store new data */
                    m_data = other;

                    /* release old data */
                    if (old != NULL && !old->m_refCount.deref()) {
                        delete old;
                    }
                }

                return *this;
            }

            bool operator! () const
            {
                return !m_data;
            }

            bool operator==(const SharedDataPointer<T>& other) const
            {
                return m_data == other.m_data;
            }

            bool operator!=(const SharedDataPointer<T>& other) const
            {
                return m_data != other.m_data;
            }

        private:
            SharedT* shared()
            {
                return reinterpret_cast<SharedT*> (m_data);
            }

            const SharedT* shared() const
            {
                return reinterpret_cast<const SharedT*> (m_data);
            }

            void detach()
            {
                SharedT* d = shared();

                // TODO: Access not synchronized!
                if (d != NULL && d->m_refCount.load() != 1) {
                    doDetach();
                }
            }

            void doDetach()
            {
                /* get current data */
                SharedT* oldShared = shared();

                /* clone it */
                T* newData = oldShared->clone();

                /* add reference to new data */
                SharedT* newShared = reinterpret_cast<SharedT*> (newData);
                newShared->m_refCount.ref();

                /* release reference to old data */
                if (!oldShared->m_refCount.deref()) {
                    delete oldShared;
                }

                /* store new data */
                m_data = newData;
            }

#if 0
        public:
            typedef T Type;
            typedef T *pointer;

            inline void swap(QSharedDataPointer &other)
            {
                qSwap(d, other.d);
            }

        protected:

        private:

            T *d;
#endif
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UTIL__SHARED_DATA_POINTER_H_ */
