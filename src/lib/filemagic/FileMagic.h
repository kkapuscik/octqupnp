/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup FileMagic
 * @{
 */

#ifndef _OCT_FILE_MAGIC__FILE_MAGIC_H_
#define _OCT_FILE_MAGIC__FILE_MAGIC_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QObject>

namespace Oct
{
    namespace FileMagic
    {
        /**
         * File magic utilities.
         */
        class FileMagic : public QObject
        {
        Q_OBJECT

        private:
            // TODO: use some magic Qt macros instead?
            class FileMagicPrivate;

        private:
            FileMagicPrivate* m_private;

        public:
            /**
             * Constructs logger manager.
             */
            FileMagic(QObject* parent = 0);

            virtual ~FileMagic();

            bool isValid() const;

            QString processFile(const QString& fileName, int* error, QString* errstr) const;
            QString processBuffer(const void* buffer, int length, int* error, QString* errstr) const;

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_FILE_MAGIC__FILE_MAGIC_H_ */

/**
 * @}
 * @}
 * @}
 */
