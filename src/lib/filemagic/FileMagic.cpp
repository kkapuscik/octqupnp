/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "FileMagic.h"

#include <magic.h>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace FileMagic
    {
        class FileMagic::FileMagicPrivate
        {
        private:
            magic_t m_cookie;

        private:
            FileMagicPrivate()
            {
                do {
                    m_cookie = magic_open(MAGIC_SYMLINK | MAGIC_MIME_TYPE);
                    if (m_cookie == NULL) {
                        break;
                    }

                    if (magic_load(m_cookie, NULL) != 0) {
                        magic_close(m_cookie);
                        m_cookie = NULL;
                    }
                } while (0);
            }

            ~FileMagicPrivate()
            {
                if (m_cookie != NULL) {
                    magic_close(m_cookie);
                }
            }

            bool isValid() const
            {
                return m_cookie != NULL;
            }

            QString processFile(const QString& fileName, int* error, QString* errstr) const
            {
                // TODO: errors

                if (m_cookie != NULL) {
                    const char* mimeType = magic_file(m_cookie, fileName.toUtf8().data());
                    if (mimeType != NULL) {
                        return QString::fromUtf8(mimeType);
                    }
                }

                return QString();
            }

            QString processBuffer(const void* buffer, int length, int* error, QString* errstr) const
            {
                // TODO: errors

                if (m_cookie != NULL) {
                    const char* mimeType = magic_buffer(m_cookie, buffer, length);
                    if (mimeType != NULL) {
                        return QString::fromUtf8(mimeType);
                    }
                }

                return QString();
            }

            friend class FileMagic;
        };

        FileMagic::FileMagic(QObject* parent) :
            QObject(parent)
        {
            m_private = new FileMagicPrivate();
        }

        FileMagic::~FileMagic()
        {
            delete m_private;
        }

        bool FileMagic::isValid() const
        {
            return m_private->isValid();
        }

        QString FileMagic::processFile(const QString& fileName, int* error, QString* errstr) const
        {
            return m_private->processFile(fileName, error, errstr);
        }

        QString FileMagic::processBuffer(const void* buffer, int length, int* error, QString* errstr) const
        {
            return m_private->processBuffer(buffer, length, error, errstr);
        }

    }
}
