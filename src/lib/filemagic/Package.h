/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup FileMagic    File libmagic wrappers
 * @{
 */

#ifndef _OCT_FILE_MAGIC__PACKAGE_H_
#define _OCT_FILE_MAGIC__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * File libmagic wrappers.
     */
    namespace FileMagic
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_FILE_MAGIC__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
