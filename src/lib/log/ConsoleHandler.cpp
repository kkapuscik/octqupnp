/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ConsoleHandler.h"

#include <iostream>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        ConsoleHandler::ConsoleHandler(QObject* parent) :
            Handler(parent)
        {
            // nothing to do
        }

        void ConsoleHandler::processPathChange(PathEntry entry)
        {
            // nothing to do
        }

        void ConsoleHandler::processMessageReady(const QString& path, const QString& prefix, Level level,
                const QString& instance, const QString& message)
        {
            QByteArray levelUtf8 = levelString(level).toUtf8();
            QByteArray prefixUtf8 = normalizePrefix(prefix).toUtf8();
            QByteArray instanceUtf8 = instance.toUtf8();
            QByteArray messageUtf8 = message.toUtf8();

            std::cout << levelUtf8.data() << " [" << prefixUtf8.data() << "] [" << instanceUtf8.data() << "] "
                    << messageUtf8.data() << std::endl;
        }
    }
}

/*-----------------------------------------------------------------------------*/
