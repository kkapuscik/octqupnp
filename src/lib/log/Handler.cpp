/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Handler.h"
#include "Manager.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        const int NORMALIZED_PREFIX_LENGTH = 24;

        /** Debug message level prefixes - trace. */
        static const QString levelStringTrace("[TRACE]");
        /** Debug message level prefixes - info. */
        static const QString levelStringInfo("[INFO-]");
        /** Debug message level prefixes - warning. */
        static const QString levelStringWarn("[WARN-]");
        /** Debug message level prefixes - error. */
        static const QString levelStringError("[ERROR]");
        /** Debug message level prefixes - invalid level. */
        static const QString levelStringInvalid("[?????]");

        const QString& Handler::levelString(Level level)
        {
            switch (level) {
                case TRACE:
                    return levelStringTrace;
                case INFO:
                    return levelStringInfo;
                case WARNING:
                    return levelStringWarn;
                case ERROR:
                    return levelStringError;
                default:
                    return levelStringInvalid;
            }
        }

        QString Handler::normalizePrefix(const QString& prefix)
        {
            QString prefixNormalized;

            if (prefix.length() < NORMALIZED_PREFIX_LENGTH) {
                prefixNormalized = prefix + QString().fill(QChar('-'), NORMALIZED_PREFIX_LENGTH - prefix.length());
            } else if (prefix.length() == NORMALIZED_PREFIX_LENGTH) {
                prefixNormalized = prefix;
            } else {
                prefixNormalized = prefix.mid(0, NORMALIZED_PREFIX_LENGTH);
            }

            return prefixNormalized;
        }


        Handler::Handler(QObject* parent) :
            QObject(parent)
        {
            // nothing to do
        }

        void Handler::connectHandler()
        {
            Manager* manager = Manager::instance();

            connect(manager, SIGNAL( pathChanged(Oct::Log::PathEntry) ),
                    this, SLOT( onPathChange(Oct::Log::PathEntry) ));
            connect(manager, SIGNAL( messageReady(const QString&,const QString&,Oct::Log::Level,const QString&,const QString&) ),
                    this, SLOT( onMessageReady(const QString&,const QString&,Oct::Log::Level,const QString&,const QString&) ));
        }

        void Handler::disconnectHandler()
        {
            Manager* manager = Manager::instance();

            disconnect(manager, SIGNAL( pathChanged(Oct::Log::PathEntry) ),
                    this, SLOT( onPathChange(Oct::Log::PathEntry) ));
            disconnect(manager, SIGNAL( messageReady(const QString&,const QString&,Oct::Log::Level,const QString&,const QString&) ),
                    this, SLOT( onMessageReady(const QString&,const QString&,Oct::Log::Level,const QString&,const QString&) ));

        }

        void Handler::onPathChange(Oct::Log::PathEntry entry)
        {
            processPathChange(entry);
        }

        void Handler::onMessageReady(const QString& path, const QString& prefix, Oct::Log::Level level,
                const QString& instance, const QString& message)
        {
            processMessageReady(path, prefix, level, instance, message);
        }
    }
}

/*-----------------------------------------------------------------------------*/
