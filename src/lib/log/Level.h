/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#ifndef _OCT_LOG__LEVEL_H_
#define _OCT_LOG__LEVEL_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        /** Logging level. */
        enum Level
        {
            /**
             * Operations trace level.
             *
             * Trace messages shall be used to log start, progress and end of method execution.
             */
            TRACE,
            /**
             * General informations level.
             *
             * Informational messages shall be used to notify about important events or status
             * changes.
             */
            INFO,
            /**
             * Loss of functionality or incorrect behavior level.
             *
             * Warning messages shall be used when an error occurred during execution
             * but application can correct or handle the issue.
             */
            WARNING,
            /**
             * Error level.
             *
             * Warning messages shall be used when an error occurred during execution
             * and part of application may no longer be usable or stable.
             */
            ERROR
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_LOG__LEVEL_H_*/

/**
 * @}
 * @}
 * @}
 */
