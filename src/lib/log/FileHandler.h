/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#ifndef _OCT_LOG__FILE_HANDLER_H_
#define _OCT_LOG__FILE_HANDLER_H_

/*---------------------------------------------------------------------------*/

#include "Handler.h"

#include <QtCore/QFile>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        /**
         * Log messages handler printing message to file.
         */
        class FileHandler : public Handler
        {
        Q_OBJECT

        private:
            QFile m_logFile;

        public:
            /**
             * Constructs new handler.
             *
             * @param fileName
             *      Name of the log file.
             * @param parent
             *      Parent object.
             */
            FileHandler(const QString& fileName, QObject* parent = 0);

            /**
             * Destroys the handler.
             */
            virtual ~FileHandler(void);

        private slots:
            /**
             * Performs processing of loggers path change.
             *
             * @param entry
             *      Entry that changed.
             */
            virtual void processPathChange(PathEntry entry);

            /**
             * Performs processing of new message to log.
             *
             * @param path
             *      Logger path.
             * @param prefix
             *      Message prefix.
             * @param level
             *      Message level.
             * @param instance
             *      Instance pointer as string.
             * @param message
             *      The message.
             */
            virtual void processMessageReady(const QString& path, const QString& prefix, Level level,
                    const QString& instance, const QString& message);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_LOG__FILE_HANDLER_H_*/

/**
 * @}
 * @}
 * @}
 */
