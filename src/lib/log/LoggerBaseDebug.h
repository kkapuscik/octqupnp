/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#ifndef _OCT_LOG__LOGGER_BASE_DEBUG_H_
#define _OCT_LOG__LOGGER_BASE_DEBUG_H_

/*---------------------------------------------------------------------------*/

#include "Level.h"

#include <QtCore/QString>
#include <QtCore/QTextStream>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        /** Logger output stream context. */
        class OutputStreamDebugContext
        {
        private:
            /**
             * Logger path.
             */
            QString m_path;
            /**
             * Messages prefix.
             */
            QString m_prefix;
            /**
             * Debug message level.
             */
            Level m_level;
            /**
             * Object instance.
             *
             * If NULL it is a class message.
             */
            const void* m_instance;
            /**
             * Number of references.
             */
            int m_refCount;
            /**
             * Buffer to store the output.
             */
            QString m_buffer;
            /**
             * Text stream that will write to string.
             */
            QTextStream m_textStream;

        private:
            /**
             * Constructs new context.
             *
             * Contexts encapsulates all elements needed to produce log message.
             * It is shared between output streams used created to produce one output message.
             *
             * @param path
             *      Logger path.
             * @param prefix
             *      Message prefix.
             * @param level
             *      Message level.
             * @param instance
             *      Object instance (NULL for class messages).
             */
            OutputStreamDebugContext(const QString& path, const QString& prefix, Level level, const void* instance) :
                m_path(path), m_prefix(prefix), m_level(level), m_instance(instance), m_refCount(1)
            {
                m_textStream.setString(&m_buffer);
            }

            /**
             * Sends messages to manager.
             */
            void sendMessage();

            /**
             * Adds references to context object.
             *
             * @return
             * Pointer to referenced context (this).
             */
            OutputStreamDebugContext* addref()
            {
                Q_ASSERT(m_refCount > 0);
                ++m_refCount;
                return this;
            }

            /**
             * Releases reference to context object.
             *
             * When all references are release the object is freed and message is printed.
             */
            void release()
            {
                Q_ASSERT(m_refCount > 0);
                if (--m_refCount == 0) {
                    sendMessage();
                    delete this;
                }
            }

            /**
             * Writes \p data of type T to output stream.
             *
             * @param data
             *      Data to write.
             *
             * @return
             * Output stream instance.
             */
            template<typename T>
            void write(const T& data)
            {
                m_textStream << data;
            }

            friend class LoggerBaseDebug;
            friend class OutputStreamDebug;
        };

        /**
         * Logger output stream.
         */
        class OutputStreamDebug
        {
        private:
            /**
             * Output context.
             */
            OutputStreamDebugContext* m_context;

        private:
            /**
             * Constructs logger output stream.
             *
             * @param context
             *      Log output context.
             */
            OutputStreamDebug(OutputStreamDebugContext* context) :
                m_context(context)
            {
                // nothing to do
            }

        public:
            /**
             * Copies logger output stream.
             *
             * @param other
             *      Output stream to be copied.
             */
            OutputStreamDebug(const OutputStreamDebug& other)
            {
                m_context = other.m_context->addref();
            }

            /**
             * Destroys logger output stream.
             */
            ~OutputStreamDebug()
            {
                m_context->release();
            }

            /**
             * Writes \p data of type T to output stream.
             *
             * @param data
             *      Data to write.
             *
             * @return
             * Output stream instance.
             */
            template<typename T>
            inline OutputStreamDebug& operator <<(const T& data)
            {
                m_context->write(data);
                return *this;
            }

            friend class LoggerBaseDebug;
            friend class Manager;
        };

        /**
         * Logger base class.
         */
        class LoggerBaseDebug
        {
        private:
            /**
             * Logger path.
             */
            QString m_path;
            /**
             * Messages prefix.
             */
            QString m_prefix;

        protected:
            /**
             * Constructs logger base.
             *
             * @param path
             *      Logger path.
             */
            LoggerBaseDebug(const QString& path);

            /**
             * Returns output stream to print message at given \p level for specified \p instance of object.
             *
             * @param level
             *      Debug message level.
             * @param instance
             *      Object instance. If NULL it is a class message.
             *
             * @return
             * Logging output stream.
             */
            OutputStreamDebug message(Level level, const void* instance = NULL);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_LOG__LOGGER_BASE_DEBUG_H_*/

/**
 * @}
 * @}
 * @}
 */
