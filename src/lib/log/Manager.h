/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#ifndef _OCT_LOG__MANAGER_H_
#define _OCT_LOG__MANAGER_H_

/*---------------------------------------------------------------------------*/

#include "Level.h"
#include "PathEntry.h"

#include <QtCore/QString>
#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QMutex>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        /**
         * Logging manager.
         */
        class Manager : public QObject
        {
        Q_OBJECT

        private:
            /** Logger manager singleton. */
            static Manager* sm_instance;

        public:
            /**
             * Returns LoggerManager singleton.
             *
             * @return
             * Logger manager singleton.
             */
            static Manager* instance();

        private:
            /**
             * Mapping from paths to paths entries.
             */
            typedef QMap<QString, PathEntry> PathEntryMap;

        private:
            /**
             * Mutex for access synchronization.
             */
            mutable QMutex m_mutex;

            /**
             * Collection of currently known path entries.
             *
             * Maps paths to path entries to speedup 'enable' lookup.
             */
            PathEntryMap m_pathEntries;

        private:
            /**
             * Constructs logger manager.
             */
            Manager();

            /**
             * Registers logger.
             *
             * @param path
             *      Path of logger to register.
             */
            void registerLogger(const QString& path);

            /**
             * Writes message.
             *
             * @param path
             *      Logger path.
             * @param prefix
             *      Message prefix.
             * @param level
             *      Message level.
             * @param instance
             *      Object instance (NULL for class messages).
             * @param message
             *      The message.
             */
            void writeMessage(const QString& path, const QString& prefix, Level level, const void* instance,
                    const QString& message);

        public:
            /**
             * Loads manager settings.
             */
            void loadSettings();

            /**
             * Saves manager settings.
             */
            void saveSettings() const;

            /**
             * Returns known path entries.
             *
             * @return
             * Collection of path entries currently known.
             */
            QList<PathEntry> pathEntries() const;

            /**
             * Sets logging status.
             *
             * @param path
             *      Logger path.
             * @param enabled
             *      Logging enabled flag.
             */
            void setEnabled(QString path, bool enabled);

        private:
            /**
             * Sets logging status (unlocked).
             *
             * @param path
             *      Logger path.
             * @param enabled
             *      Logging enabled flag.
             */
            void setEnabledUnlocked(QString path, bool enabled);

        signals:
            /**
             * Signal is emitted when logger path status was changed.
             *
             * It is currently emitted in two scenarios:
             * - When path is added.
             * - When logging on path is enabled/disabled.
             *
             * @param entry
             *      Entry that changes.
             */
            void pathChanged(Oct::Log::PathEntry entry);

            /**
             * Signal is emitted when there is new message to log.
             *
             * @param path
             *      Logger path.
             * @param prefix
             *      Message prefix.
             * @param level
             *      Message level.
             * @param instance
             *      Instance pointer as string (null string for class messages).
             * @param message
             *      The message.
             */
            void messageReady(const QString& path, const QString& prefix, Oct::Log::Level level,
                    const QString& instance, const QString& message);

            friend class LoggerBaseDebug;
            friend class OutputStreamDebugContext;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_LOG__MANAGER_H_ */

/**
 * @}
 * @}
 * @}
 */
