/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#ifndef _OCT_LOG__HANDLER_H_
#define _OCT_LOG__HANDLER_H_

/*---------------------------------------------------------------------------*/

#include "Level.h"
#include "PathEntry.h"

#include <QtCore/QObject>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        /**
         * Log messages handlers base class.
         */
        class Handler : public QObject
        {
        Q_OBJECT

        public:
            /**
             * Returns debug message level prefix string.
             *
             * @param level
             *      Debug message level.
             *
             * @return
             * Level prefix string.
             */
            static const QString& levelString(Level level);

            /**
             * Returns normalized prefix string.
             *
             * @param prefix
             *      Input prefix string.
             *
             * @return
             * Normalized prefix string.
             */
            static QString normalizePrefix(const QString& prefix);

        protected:
            /**
             * Constructs new handler.
             *
             * @param parent
             *      Parent object.
             */
            Handler(QObject* parent = 0);

        public:
            /**
             * Connects handler to logging manager.
             */
            void connectHandler();

            /**
             * Disconnects handler from logging manager.
             */
            void disconnectHandler();

        protected:
            /**
             * Performs processing of loggers path change.
             *
             * @param entry
             *      Entry that changed.
             */
            virtual void processPathChange(PathEntry entry) = 0;

            /**
             * Performs processing of new message to log.
             *
             * @param path
             *      Logger path.
             * @param prefix
             *      Message prefix.
             * @param level
             *      Message level.
             * @param instance
             *      Instance pointer as string.
             * @param message
             *      The message.
             */
            virtual void processMessageReady(const QString& path, const QString& prefix, Level level,
                    const QString& instance, const QString& message) = 0;

        private slots:
            /**
             * Slot listening for logging manager path change signal.
             *
             * @param entry
             *      Entry that changed.
             */
            virtual void onPathChange(Oct::Log::PathEntry entry);

            /**
             * Slot listening for logging manager messages.
             *
             * @param path
             *      Logger path.
             * @param prefix
             *      Message prefix.
             * @param level
             *      Message level.
             * @param instance
             *      Instance pointer as string.
             * @param message
             *      The message.
             */
            void onMessageReady(const QString& path, const QString& prefix, Oct::Log::Level level,
                    const QString& instance, const QString& message);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_LOG__HANDLER_H_*/

/**
 * @}
 * @}
 * @}
 */
