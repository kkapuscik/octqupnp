/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log      Logging utilities
 * @{
 */

#ifndef _OCT_LOG__PACKAGE_H_
#define _OCT_LOG__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * Logging utilities.
     */
    namespace Log
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_LOG__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
