/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#ifndef _OCT_LOG__LOGGER_H_
#define _OCT_LOG__LOGGER_H_

/*-----------------------------------------------------------------------------*/

#include <QtCore/QString>

/*-----------------------------------------------------------------------------*/

#include "Level.h"

#include "LoggerBaseDebug.h"
#include "LoggerBaseNoDebug.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
#ifdef OCT_CONFIG_LOGGING_ENABLED
typedef LoggerBaseDebug     LoggerBase;
typedef OutputStreamDebug   OutputStream;
#else /*OCT_CONFIG_LOGGING_ENABLED*/
typedef LoggerBaseNoDebug   LoggerBase;
typedef OutputStreamNoDebug OutputStream;
#endif /*OCT_CONFIG_LOGGING_ENABLED*/

        /**
         * Logging class.
         */
        template<typename T>
        class Logger : public LoggerBase
        {
        public:
            /**
             * Constructs the logger.
             *
             * @param path
             *      Logger path. Must be absolute (begin with '/').
             *      Example: "/oct/upnp/SsdpSocket".
             */
            Logger(const QString& path) :
                LoggerBase(path)
            {
                // nothing to do
            }

        public:
            /**
             * Requests object related trace debug message output stream.
             *
             * @param instance
             *      Logged object instance.
             *
             * @return
             * Debug message output stream.
             */
            OutputStream trace(const T* instance)
            {
                Q_ASSERT(instance != NULL);
                return message(TRACE, instance);
            }

            /**
             * Requests object related info debug message output stream.
             *
             * @param instance
             *      Logged object instance.
             *
             * @return
             * Debug message output stream.
             */
            OutputStream info(const T* instance)
            {
                Q_ASSERT(instance != NULL);
                return message(INFO, instance);
            }

            /**
             * Requests object related warning debug message output stream.
             *
             * @param instance
             *      Logged object instance.
             *
             * @return
             * Debug message output stream.
             */
            OutputStream warn(const T* instance)
            {
                Q_ASSERT(instance != NULL);
                return message(WARNING, instance);
            }

            /**
             * Requests object related error debug message output stream.
             *
             * @param instance
             *      Logged object instance.
             *
             * @return
             * Debug message output stream.
             */
            OutputStream error(const T* instance)
            {
                Q_ASSERT(instance != NULL);
                return message(ERROR, instance);
            }

            /**
             * Requests type related trace debug message output stream.
             *
             * @return
             * Debug message output stream.
             */
            OutputStream strace()
            {
                return message(TRACE, NULL);
            }

            /**
             * Requests type related info debug message output stream.
             *
             * @return
             * Debug message output stream.
             */
            OutputStream sinfo()
            {
                return message(INFO, NULL);
            }

            /**
             * Requests type related warning debug message output stream.
             *
             * @return
             * Debug message output stream.
             */
            OutputStream swarn()
            {
                return message(WARNING, NULL);
            }

            /**
             * Requests type related error debug message output stream.
             *
             * @return
             * Debug message output stream.
             */
            OutputStream serror()
            {
                return message(ERROR, NULL);
            }
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_LOG__LOGGER_H_ */

/**
 * @}
 * @}
 * @}
 */
