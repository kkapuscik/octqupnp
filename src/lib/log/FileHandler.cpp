/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "FileHandler.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        FileHandler::FileHandler(const QString& fileName, QObject* parent) :
            Handler(parent), m_logFile(fileName)
        {
            m_logFile.open(QFile::WriteOnly | QFile::Truncate);
        }

        FileHandler::~FileHandler()
        {
            if (m_logFile.isOpen()) {
                m_logFile.close();
            }
        }

        void FileHandler::processPathChange(PathEntry entry)
        {
            // nothing to do
        }

        void FileHandler::processMessageReady(const QString& path, const QString& prefix, Level level,
                const QString& instance, const QString& message)
        {
            if (!m_logFile.isOpen()) {
                return;
            }

            QString line = QString("%1 [%2] [%3] %4").
                    arg(levelString(level)).
                    arg(normalizePrefix(prefix)).
                    arg(instance).
                    arg(message);

            // TODO; repeat on partial write etc.
            m_logFile.write(line.toUtf8());
        }
    }
}

/*-----------------------------------------------------------------------------*/
