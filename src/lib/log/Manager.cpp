/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Manager.h"

#include <QtCore/QMutexLocker>
#include <QtCore/QSettings>
#include <QtCore/QStringList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        Manager* Manager::sm_instance = NULL;

        Manager* Manager::instance()
        {
            if (sm_instance == NULL) {
                sm_instance = new Manager();
            }

            return sm_instance;
        }

        Manager::Manager() :
            m_mutex(QMutex::NonRecursive)
        {
            // do nothing
        }

        void Manager::loadSettings()
        {
            QMutexLocker locker(&m_mutex);

            QSettings settings;

            settings.beginGroup("log");
            QStringList keys = settings.childKeys();
            for (int i = 0; i < keys.length(); ++i) {
                QVariant value = settings.value(keys[i]);
                bool boolValue = value.toBool();

                QString path = keys[i];

                path.replace('#', '/');

                setEnabledUnlocked(path, boolValue);
            }
            settings.endGroup();
        }

        void Manager::saveSettings() const
        {
            QMutexLocker locker(&m_mutex);

            PathEntryMap::const_iterator curIter = m_pathEntries.begin();
            PathEntryMap::const_iterator endIter = m_pathEntries.end();

            QSettings settings;

            settings.beginGroup("log");
            for (; curIter != endIter; ++curIter) {
                QString key = curIter.key();
                QVariant value = curIter.value().isEnabled();

                key.replace('/', '#');

                settings.setValue(key, value);
            }
            settings.endGroup();
        }

        void Manager::registerLogger(const QString& path)
        {
            QMutexLocker locker(&m_mutex);

            PathEntryMap::const_iterator iter = m_pathEntries.find(path);
            if (iter != m_pathEntries.end()) {
                /* edit entry */
                m_pathEntries.insert(path, PathEntry(path, iter.value().isEnabled(), true));
            } else {
                /* add new entry */
                m_pathEntries.insert(path, PathEntry(path, true, true));
            }

            emit pathChanged(m_pathEntries.find(path).value());
        }

        void Manager::writeMessage(const QString& path, const QString& prefix, Level level, const void* instance,
                const QString& message)
        {
            QString instanceStr;

            PathEntryMap::const_iterator iter = m_pathEntries.find(path);
            if (iter != m_pathEntries.end()) {
                if (!iter.value().isEnabled()) {
                    /* logging disabled */
                    return;
                }
            } else {
                /* path not registered */
                Q_ASSERT(0);
                return;
            }

#if QT_POINTER_SIZE == 4
            if (instance != NULL) {
                quint32 instValue = (quint32) instance;
                instanceStr = QString("%1").arg(instValue, 8, 16, QChar('0'));
            } else {
                instanceStr = QString("%1").fill(QChar('-'), 8);
            }
#else
            if (instance != NULL) {
                quint64 instValue = (quint64)instance;
                instanceStr = QString("%1").arg(instValue, 16, 16, QChar('0'));
            } else {
                instanceStr = QString("%1").fill(QChar('-'), 16);
            }
#endif

            emit messageReady(path, prefix, level, instanceStr, message);
        }

        QList<PathEntry> Manager::pathEntries() const
        {
            QMutexLocker locker(&m_mutex);

            return m_pathEntries.values();
        }

        void Manager::setEnabled(QString path, bool enabled)
        {
            QMutexLocker locker(&m_mutex);

            setEnabledUnlocked(path, enabled);
        }

        void Manager::setEnabledUnlocked(QString path, bool enabled)
        {
            PathEntryMap::iterator iter;

            iter = m_pathEntries.find(path);
            if (iter != m_pathEntries.end()) {
                /* edit entry */
                m_pathEntries.insert(path, PathEntry(path, enabled, iter.value().isRegistered()));
            } else {
                /* add new entry */
                m_pathEntries.insert(path, PathEntry(path, enabled, false));
            }

            emit pathChanged(m_pathEntries.find(path).value());
        }
    }
}
