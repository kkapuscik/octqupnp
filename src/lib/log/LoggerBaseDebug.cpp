/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#include "LoggerBaseDebug.h"
#include "Manager.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        void OutputStreamDebugContext::sendMessage()
        {
            m_textStream.flush();

            Manager::instance()->writeMessage(m_path, m_prefix, m_level, m_instance, m_buffer);
        }

        LoggerBaseDebug::LoggerBaseDebug(const QString& path) :
            m_path(path)
        {
            int idx = path.lastIndexOf('/');
            if (idx >= 0) {
                m_prefix = path.mid(idx + 1);
            } else {
                m_prefix = path;
            }

            Manager::instance()->registerLogger(m_path);
        }

        OutputStreamDebug LoggerBaseDebug::message(Level level, const void* instance)
        {
            OutputStreamDebugContext* context;

            context = new OutputStreamDebugContext(m_path, m_prefix, level, instance);

            return OutputStreamDebug(context);
        }
    }
}

/*---------------------------------------------------------------------------*/
