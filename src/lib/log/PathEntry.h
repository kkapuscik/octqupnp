/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#ifndef _OCT_LOG__PATH_ENTRY_H_
#define _OCT_LOG__PATH_ENTRY_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QString>
#include <QtCore/QStringList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        /** Path entry data. */
        class PathEntry
        {
        private:
            /**
             * Path string.
             */
            QString m_path;

            /**
             * Logging enabled flag.
             */
            bool m_enabled;

            /**
             * Path was registered flag.
             *
             * If true the path was registered by the logger.
             * If false the path is coming from other source (e.g. deserialization).
             */
            bool m_registered;

        public:
            /**
             * Constructs empty (invalid) entry.
             */
            PathEntry() :
                m_path(QString::null), m_enabled(false), m_registered(false)
            {
                // do nothing
            }

            /**
             * Constructs entry.
             *
             * @param[in] path
             *      Path string.
             * @param[in] enabled
             *      Logging enabled flag.
             * @param[in] registered
             *      Path was registered by logger flag.
             */
            PathEntry(const QString& path, bool enabled = false, bool registered = false) :
                m_path(path), m_enabled(enabled), m_registered(registered)
            {
                verifyPath();
            }

            /**
             * Checks if entry is valid.
             *
             * @return
             * True if entry is valid, false otherwise.
             */
            bool isValid() const
            {
                return !m_path.isEmpty();
            }

            /**
             * Returns entry path.
             *
             * @return
             * Logger path string.
             */
            const QString& path() const
            {
                Q_ASSERT(isValid());
                return m_path;
            }

            /**
             * Checks if logging is enabled.
             *
             * @return
             * True if logging is enabled, false otherwise.
             */
            bool isEnabled() const
            {
                Q_ASSERT(isValid());
                return m_enabled;
            }

            /**
             * Checks if path was registered by a logger.
             *
             * @return
             * True if path was registered, false otherwise.
             */
            bool isRegistered() const
            {
                Q_ASSERT(isValid());
                return m_registered;
            }

            /**
             * Returns path segments.
             *
             * @return
             * Path segments (first segment is always empty).
             */
            QStringList segments() const;

        private:
            /**
             * Verifies if path is valid.
             */
            void verifyPath();

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_LOG__PATH_ENTRY_H_*/

/**
 * @}
 * @}
 * @}
 */
