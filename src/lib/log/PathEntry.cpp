/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "PathEntry.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        void PathEntry::verifyPath()
        {
            QStringList pathSegments = m_path.split('/', QString::KeepEmptyParts);

            /* there must be at least 2 parts (first empty and then name) */
            Q_ASSERT(pathSegments.size() >= 2);

            /* first segment must be empty (path must be absolute) */
            Q_ASSERT(pathSegments[0].isEmpty());

            /* other segments must be non-empty */
            for (int i = 1; i < pathSegments.size(); ++i) {
                Q_ASSERT(!pathSegments[i].isEmpty());
            }

            Q_ASSERT(isValid());
        }

        QStringList PathEntry::segments() const
        {
            return m_path.split('/', QString::KeepEmptyParts);
        }

    }
}
