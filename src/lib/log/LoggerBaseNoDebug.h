/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Log
 * @{
 */

#ifndef _OCT_LOG__LOGGER_BASE_NO_DEBUG_H_
#define _OCT_LOG__LOGGER_BASE_NO_DEBUG_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Log
    {
        /**
         * Logger output stream.
         */
        class OutputStreamNoDebug
        {
        private:
            /**
             * Constructs logger output stream.
             */
            inline OutputStreamNoDebug()
            {
                // nothing to do
            }

        public:
            /**
             * Copies logger output stream.
             *
             * @param other
             *      Output stream to be copied.
             */
            inline OutputStreamNoDebug(const OutputStreamNoDebug &other)
            {
                // nothing to do
            }

            /**
             * Destroys logger output stream.
             */
            inline ~OutputStreamNoDebug()
            {
                // nothing to do
            }

            /**
             * Writes \p data of type T to output stream.
             *
             * @param data
             *      Data to write.
             *
             * @return
             * Output stream instance.
             */
            template<typename T>
            inline OutputStreamNoDebug& operator <<(const T &data)
            {
                return *this;
            }

            friend class LoggerBaseNoDebug;
        };

        /**
         * Logger base class.
         */
        class LoggerBaseNoDebug
        {
        protected:
            /**
             * Constructs logger base.
             *
             * @param path
             *      Logger path.
             */
            LoggerBaseNoDebug(const QString& path)
            {
                // nothing to do
            }

            /**
             * Returns output stream to print message at given \p level for specified \p instance of object.
             *
             * @param level
             *      Debug message level.
             * @param instance
             *      Object instance. If NULL it is a class message.
             *
             * @return
             * Logging output stream.
             */
            OutputStreamNoDebug message(Level level, const void* instance = NULL)
            {
                return OutputStreamNoDebug();
            }
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_LOG__LOGGER_BASE_NO_DEBUG_H_*/

/**
 * @}
 * @}
 * @}
 */
