/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpUdp
 * @{
 */

#ifndef _OCT_HTTP_UDP__MULTICAST_SOCKET_H_
#define _OCT_HTTP_UDP__MULTICAST_SOCKET_H_

/*-----------------------------------------------------------------------------*/

#include "Message.h"

#include <log/Logger.h>
#include <net/MulticastSocket.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpUdp
    {
        class MulticastSocket : public Oct::Net::MulticastSocket
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<MulticastSocket> Logger;

        public:
            MulticastSocket(QObject* parent = 0);
            virtual ~MulticastSocket();

            Message readMessage(QHostAddress* address = 0, quint16 * port = 0);
            bool writeMessage(const Message& message, const QHostAddress& address, quint16 port);

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_UDP__MULTICAST_SOCKET_H_ */

/**
 * @}
 * @}
 * @}
 */
