/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Message.h"

#include <httpcore/Header.h>
#include <httpcore/HeaderSet.h>
#include <httpcore/StartLine.h>

#include <QtCore/QString>
#include <QtCore/QStringList>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpUdp
    {
        Oct::Log::Logger<Message> Message::Logger("/oct/httpudp/Message");

        class Buffer
        {
        private:
            const QByteArray* m_array;
            int m_index;

        public:
            Buffer(const QByteArray* array) :
                m_array(array), m_index(0)
            {
            }

            QString readLine()
            {
                int startIndex = m_index;
                int endIndex = m_array->indexOf("\r\n", m_index);

                if (endIndex >= 0) {
                    m_index = endIndex + 2;
                    return QString::fromUtf8(m_array->data() + startIndex, endIndex - startIndex);
                }

                return QString::null;
            }

            int bytesLeft()
            {
                return m_array->size() - m_index;
            }
        };

        Message::Message()
        {
            // nothing to do
        }

        Message::Message(Oct::HttpCore::HttpVersion version, int code, const QString& message, const Oct::HttpCore::HeaderSet& headerSet) :
            m_startLine(version, code, message), m_headerSet(headerSet)
        {
            // nothing to do
        }

        Message::Message(const QString& method, const QString& uri, Oct::HttpCore::HttpVersion version, const Oct::HttpCore::HeaderSet& headerSet) :
            m_startLine(method, uri, version), m_headerSet(headerSet)
        {
            // nothing to do
        }

        Message::Message(const Oct::HttpCore::StartLine& startLine, const Oct::HttpCore::HeaderSet& headerSet) :
            m_startLine(startLine), m_headerSet(headerSet)
        {
            // nothing to do
        }

        Message::~Message()
        {
            // nothing to do
        }

        Message Message::parse(const QByteArray& array)
        {
            // generic-message = start-line
            //                   *(message-header CRLF)
            //                   CRLF
            //                   [ message-body ]
            // start-line      = Request-Line | Status-Line

            // Request-Line    = Method SP Request-URI SP HTTP-Version CRLF
            // Status-Line     = HTTP-Version SP Status-Code SP Reason-Phrase CRLF

            // message-header  = field-name ":" [ field-value ]
            // field-name      = token
            // field-value     = *( field-content | LWS )
            // field-content   = <the OCTETs making up the field-value
            //                   and consisting of either *TEXT or combinations
            //                   of token, separators, and quoted-string>

            // HTTP/1.1 header field values can be folded onto multiple lines if the
            // continuation line begins with a space or horizontal tab. All linear
            // white space, including folding, has the same semantics as SP. A
            // recipient MAY replace any linear white space with a single SP before
            // interpreting the field value or forwarding the message downstream.
            //
            // LWS            = [CRLF] 1*( SP | HT )

            // quoted-string  = ( <"> *(qdtext | quoted-pair ) <"> )
            // qdtext         = <any TEXT except <">>

            Buffer buffer(&array);
            QString startLine;
            QStringList headerLines;

            startLine = buffer.readLine();
            if (startLine.isNull()) {
                Logger.strace() << "Null status line - ignoring  datagram";
                return Message();
            }

            /* read header lines, merging on LWS */
            for (;;) {
                QString line = buffer.readLine();
                if (line.isNull()) {
                    Logger.strace() << "Null header line - ignoring datagram";
                    return Message();
                }

                if (line.length() == 0) {
                    Logger.strace() << "Empty headers line - headers reading finished";
                    break;
                }

                if (line[0] == ' ' || line[0] == '\t') {
                    if (headerLines.size() > 0) {
                        headerLines.last() += line;
                    } else {
                        Logger.strace() << "Header continuation to nonexisting header";
                        return Message();
                    }
                } else {
                    headerLines.append(line);
                }
            }

            /* process start line */
            Oct::HttpCore::StartLine messageStart = Oct::HttpCore::StartLine::parse(startLine);
            if (!messageStart.isValid()) {
                Logger.strace() << "Invalid start line: " << startLine;
                return Message();
            }

            Oct::HttpCore::HeaderSet headerSet;

            /* process headers */
            for (int i = 0; i < headerLines.size(); ++i) {
                Oct::HttpCore::Header header = Oct::HttpCore::Header::parse(headerLines[i]);
                if (!header.isValid()) {
                    Logger.strace() << "Header parsing error: " << headerLines[i];
                    return Message();
                }

                headerSet.appendOrSet(header);
            }

#if 0
            // TODO: print
            if (messageStart.isRequestLine()) {
                Logger.strace() << "Request: " << messageStart.requestLine().method() << messageStart.requestLine().uri() << messageStart.requestLine().version();
            } else if (messageStart.isStatusLine()) {
                Logger.strace() << "Status: " << messageStart.statusLine().version() << messageStart.statusLine().code() << messageStart.statusLine().message();
            } else {
                Logger.strace() << "Invalid start line";
                return OctHttpUdpMessage();
            }

            QList<OctHttpHeader> allHeaders = headerSet.allHeaders();
            for (int i = 0; i < allHeaders.size(); ++i) {
                Logger.strace() << "Header: " << allHeaders[i].name() << " : " << allHeaders[i].value();
            }

            Logger.strace() << "Body bytes: " << buffer.bytesLeft();
#endif

            return Message(messageStart, headerSet);
        }

        bool Message::write(QBuffer& buffer) const
        {
            if (!isValid()) {
                return false;
            }

            if (!m_startLine.write(buffer)) {
                return false;
            }
            if (!m_headerSet.write(buffer)) {
                return false;
            }

            return true;
        }
    }
}
