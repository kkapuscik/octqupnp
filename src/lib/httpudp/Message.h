/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpUdp
 * @{
 */

#ifndef _OCT_HTTP_UDP__MESSAGE_H_
#define _OCT_HTTP_UDP__MESSAGE_H_

/*-----------------------------------------------------------------------------*/

#include <httpcore/StartLine.h>
#include <httpcore/HeaderSet.h>
#include <log/Logger.h>

#include <QtCore/QByteArray>
#include <QtCore/QBuffer>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpUdp
    {
        class Message
        {
        private:
            static Oct::Log::Logger<Message> Logger;

        private:
            Oct::HttpCore::StartLine m_startLine;
            Oct::HttpCore::HeaderSet m_headerSet;

        public:
            Message();
            Message(Oct::HttpCore::HttpVersion version, int code, const QString& message,
                    const Oct::HttpCore::HeaderSet& headerSet = Oct::HttpCore::HeaderSet());
            Message(const QString& method, const QString& uri, Oct::HttpCore::HttpVersion version,
                    const Oct::HttpCore::HeaderSet& headerSet = Oct::HttpCore::HeaderSet());
            Message(const Oct::HttpCore::StartLine& startLine, const Oct::HttpCore::HeaderSet& headerSet =
                    Oct::HttpCore::HeaderSet());
            virtual ~Message();

            bool isValid() const
            {
                return m_startLine.isValid();
            }

            const Oct::HttpCore::StartLine& startLine() const
            {
                return m_startLine;
            }

            Oct::HttpCore::HeaderSet& headers()
            {
                return m_headerSet;
            }

            const Oct::HttpCore::HeaderSet& headers() const
            {
                return m_headerSet;
            }

            bool write(QBuffer& buffer) const;

        public:
            static Message parse(const QByteArray& array);

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_UDP__MESSAGE_H_ */

/**
 * @}
 * @}
 * @}
 */
