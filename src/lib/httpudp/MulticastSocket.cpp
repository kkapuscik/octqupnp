/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "MulticastSocket.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpUdp
    {
        Oct::Log::Logger<MulticastSocket> MulticastSocket::Logger("/oct/httpudp/MulticastSocket");

        MulticastSocket::MulticastSocket(QObject* parent) :
            Oct::Net::MulticastSocket(parent)
        {
            // nothing to do
        }

        MulticastSocket::~MulticastSocket()
        {
            // nothing to do
        }

        Message MulticastSocket::readMessage(QHostAddress* address, quint16* port)
        {
            qint64 pendingSize = pendingDatagramSize();

            if (pendingSize <= 0) {
                return Message();
            }

            // TODO: one array per socket?
            QByteArray array;
            array.reserve(pendingSize);

            qint64 bytesRead = readDatagram(array.data(), array.capacity(), address, port);
            Logger.trace(this) << "OctHttpUdpMulticastSocket - Datagram bytes read: " << bytesRead;

            if (bytesRead <= 0) {
                return Message();
            }

            array.resize(bytesRead);

            return Message::parse(array);
        }

        bool MulticastSocket::writeMessage(const Message& message, const QHostAddress& address, quint16 port)
        {
            bool rv = false;

            QBuffer messageBuffer;

            messageBuffer.open(QBuffer::WriteOnly);

            if (message.write(messageBuffer)) {
                if (writeDatagram(messageBuffer.buffer(), address, port) == messageBuffer.size()) {
                    rv = true;
                }
            }

            messageBuffer.close();

            return rv;
        }
    }
}
