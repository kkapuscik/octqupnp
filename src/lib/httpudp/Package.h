/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpUdp    HTTP protocol over UDP
 * @{
 */

#ifndef _OCT_HTTP_UDP__PACKAGE_H_
#define _OCT_HTTP_UDP__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * HTTP protocol over UDP.
     */
    namespace HttpUdp
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_HTTP_UDP__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
