/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpSsdp     UPnP protocol SSDP elements
 * @{
 */

#ifndef _OCT_UPNP_SSDP__PACKAGE_H_
#define _OCT_UPNP_SSDP__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * UPnP protocol SSDP elements.
     */
    namespace UpnpSsdp
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_UPNP_SSDP__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
