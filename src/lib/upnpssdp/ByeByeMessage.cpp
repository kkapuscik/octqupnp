/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ByeByeMessage.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpSsdp
    {

        /*
         NOTIFY * HTTP/1.1
         HOST: 239.255.255.250:1900
         NT: search target
         NTS: ssdp:byebye
         USN: uuid:advertisement UUID
         */

        ByeByeMessage::ByeByeMessage(const Oct::HttpUdp::Message& message) :
            Oct::HttpUdp::Message(message)
        {
            // nothing to do
        }

        ByeByeMessage::~ByeByeMessage()
        {
            // nothing to do
        }

        bool ByeByeMessage::isByeByeMessage(const Oct::HttpUdp::Message& message)
        {
            if (!message.isValid()) {
                return false;
            }

            const Oct::HttpCore::StartLine& startLine = message.startLine();
            if (!startLine.isRequestLine()) {
                return false;
            }

            const Oct::HttpCore::RequestLine& requestLine = startLine.requestLine();
            if (requestLine.method() != "NOTIFY" || requestLine.uri() != "*" ||
                    requestLine.version() != Oct::HttpCore::HTTP_VERSION_1_1) {
                return false;
            }

            const Oct::HttpCore::HeaderSet& headers = message.headers();
            if (!headers.contains("NT") || !headers.contains("NTS") || !headers.contains("USN")) {

                return false;
            }

            if (headers.value("NTS") != "ssdp:byebye") {
                return false;
            }

            return true;
        }
    }
}
