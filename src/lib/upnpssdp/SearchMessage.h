/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpSsdp
 * @{
 */

#ifndef _OCT_UPNP_SSDP__SEARCH_MESSAGE_H_
#define _OCT_UPNP_SSDP__SEARCH_MESSAGE_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QString>

#include <httpudp/Message.h>

/*---------------------------------------------------------------------------*/

#if 0
ST
Required. Search Target. Must be one of the following. (cf. NT header in NOTIFY with ssdp:alive above.) Single URI.
ssdp:all
Search for all devices and services.
upnp:rootdevice
Search for root devices only.
uuid:device-UUID
Search for a particular device. Device UUID specified by UPnP vendor.
urn:schemas-upnp-org:device:deviceType:v
Search for any device of this type. Device type and version defined by UPnP Forum working committee.
urn:schemas-upnp-org:service:serviceType:v
Search for any service of this type. Service type and version defined by UPnP Forum working committee.
urn:domain-name:device:deviceType:v
Search for any device of this type. Domain name, device type and version defined by UPnP vendor. Period
characters in the domain name must be replaced with hyphens in accordance with RFC 2141.
urn:domain-name:service:serviceType:v
Search for any service of this type. Domain name, service type and version defined by UPnP vendor. Period
characters in the domain name must be replaced with hyphens in accordance with RFC 2141.
#endif

namespace Oct
{
    namespace UpnpSsdp
    {
        class SearchMessage : public Oct::HttpUdp::Message
        {
        public:
            SearchMessage(const Oct::HttpUdp::Message& message);
            SearchMessage(const QString& searchTarget, int delay = 3);
            virtual ~SearchMessage();

            QString searchTarget() const
            {
                return headers().value("ST");
            }

        public:
            static bool isSearchMessage(const Oct::HttpUdp::Message& message);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_SSDP__SEARCH_MESSAGE_H_ */

/**
 * @}
 * @}
 * @}
 */
