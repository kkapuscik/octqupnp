/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DiscoverySocket.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpSsdp
    {
        Oct::Log::Logger<DiscoverySocket> DiscoverySocket::Logger("/oct/upnp/SsdpSocket");

        DiscoverySocket::DiscoverySocket(QObject* parent) :
            Oct::HttpUdp::MulticastSocket(parent)
        {
            connect(this, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
        }

        DiscoverySocket::~DiscoverySocket()
        {
            // nothing to do
        }

        bool DiscoverySocket::joinSsdpGroup()
        {
            bool rv = false;

            if (bind(1900, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint)) {
                Logger.trace(this) << "Socket bound to port 1900.";

                if (joinGroup(QHostAddress("239.255.255.250"))) {
                    Logger.trace(this) << "Socket joined multicast group 239.255.255.250.";
                    if (setMulticastLoop(true)) {
                        Logger.trace(this) << "Socket enabled multicast loop.";
                        rv = true;
                    }
                } else {
                    Logger.trace(this) << "Cannot join multicast group 239.255.255.250.";
                }
            } else {
                Logger.trace(this) << "Cannot bind socket to port 1900.";
            }

            return rv;
        }

        bool DiscoverySocket::sendSearch(const SearchMessage& message)
        {
            return writeMessage(message, QHostAddress("239.255.255.250"), 1900);
        }

        bool DiscoverySocket::sendMessage(const Oct::HttpUdp::Message& message)
        {
            return writeMessage(message, QHostAddress("239.255.255.250"), 1900);
        }

        void DiscoverySocket::readPendingDatagrams()
        {
            while (hasPendingDatagrams()) {
                QHostAddress sender;
                quint16 senderPort;

                Oct::HttpUdp::Message message = readMessage(&sender, &senderPort);

                if (message.isValid()) {
                    Logger.trace(this)
                            << QString("Received message from %1:%2.").arg(sender.toString()).arg(senderPort);

                    if (SearchResponseMessage::isSearchResponseMessage(message)) {
                        SearchResponseMessage srMessage(message);

                        emit searchResponseReceived(sender, senderPort, srMessage);
                    } else if (AliveMessage::isAliveMessage(message)) {
                        AliveMessage aliveMessage(message);

                        emit aliveMessageReceived(sender, senderPort, aliveMessage);
                    } else if (ByeByeMessage::isByeByeMessage(message)) {
                        ByeByeMessage byeByeMessage(message);

                        emit byeByeMessageReceived(sender, senderPort, byeByeMessage);
                    } else if (SearchMessage::isSearchMessage(message)) {
                        SearchMessage searchMessage(message);

                        emit searchMessageReceived(sender, senderPort, searchMessage);
                    }
                }
            }
        }
    }
}
