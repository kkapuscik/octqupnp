/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SearchResponseMessage.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpSsdp
    {
        /*
         HTTP/1.1 200 OK
         CACHE-CONTROL: max-age = seconds until advertisement expires
         DATE: when response was generated
         EXT:
         LOCATION: URL for UPnP description for root device
         SERVER: OS/version UPnP/1.0 product/version
         ST: search target
         USN: advertisement UUID
         */

        SearchResponseMessage::SearchResponseMessage(const Oct::HttpUdp::Message& message) :
            Oct::HttpUdp::Message(message)
        {
            // do nothing
        }

        SearchResponseMessage::~SearchResponseMessage()
        {
            // do nothing
        }

        bool SearchResponseMessage::isSearchResponseMessage(const Oct::HttpUdp::Message& message)
        {
            if (!message.isValid()) {
                return false;
            }

            const Oct::HttpCore::StartLine& startLine = message.startLine();
            if (!startLine.isStatusLine()) {
                return false;
            }

            const Oct::HttpCore::StatusLine& statusLine = startLine.statusLine();
            // TODO: remove magic number
            if (statusLine.code() != 200 || statusLine.version() != Oct::HttpCore::HTTP_VERSION_1_1) {
                return false;
            }

            const Oct::HttpCore::HeaderSet& headers = message.headers();
            if (!headers.contains("CACHE-CONTROL") || !headers.contains("EXT") || !headers.contains("Location")
                    || !headers.contains("Server") || !headers.contains("ST") || !headers.contains("USN")) {

                return false;
            }

            return true;
        }
    }
}
