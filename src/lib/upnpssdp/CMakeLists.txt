cmake_minimum_required(VERSION 2.8.11)

project(liboctupnpssdp)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

include_directories(${Qt5Core_INCLUDE_DIRS})

file(GLOB liboctupnpssdp_SOURCES *.cpp)
file(GLOB liboctupnpssdp_HEADERS *.h)

QT5_WRAP_CPP(liboctupnpssdp_MOCSOURCES ${liboctupnpssdp_HEADERS})
#macro QT5_WRAP_UI(outfiles inputfile ... OPTIONS ...)
#macro QT5_ADD_RESOURCES(outfiles inputfile ... OPTIONS ...)

add_library(octupnpssdp STATIC ${liboctupnpssdp_SOURCES} ${liboctupnpssdp_HEADERS} ${liboctupnpssdp_MOCSOURCES})
target_link_libraries(octupnpssdp octlog octupnp octhttpudp Qt5::Core)
