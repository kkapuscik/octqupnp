/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpSsdp
 * @{
 */

#ifndef _OCT_UPNP_SSDP__BYE_BYE_MESSAGE_H_
#define _OCT_UPNP_SSDP__BYE_BYE_MESSAGE_H_

/*---------------------------------------------------------------------------*/

#include <upnp/USN.h>
#include <httpudp/Message.h>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpSsdp
    {
        class ByeByeMessage : public Oct::HttpUdp::Message
        {
        public:
            ByeByeMessage(const Oct::HttpUdp::Message& message);
            virtual ~ByeByeMessage();

            Oct::Upnp::USN usn() const
            {
                return Oct::Upnp::USN(headers().value("USN"));
            }

        public:
            static bool isByeByeMessage(const Oct::HttpUdp::Message& message);

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_SSDP__BYE_BYE_MESSAGE_H_ */

/**
 * @}
 * @}
 * @}
 */
