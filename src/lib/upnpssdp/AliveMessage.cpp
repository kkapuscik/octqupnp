/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "AliveMessage.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpSsdp
    {

        /*
         NOTIFY * HTTP/1.1
         HOST: 239.255.255.250:1900
         CACHE-CONTROL: max-age = seconds until advertisement expires
         LOCATION: URL for UPnP description for root device
         NT: search target
         NTS: ssdp:alive
         SERVER: OS/version UPnP/1.0 product/version
         USN: advertisement UUID
         */

        AliveMessage::AliveMessage(const Oct::HttpUdp::Message& message) :
            Oct::HttpUdp::Message(message)
        {
            // nothing to do
        }

        AliveMessage::~AliveMessage()
        {
            // nothing to do
        }

        bool AliveMessage::isAliveMessage(const Oct::HttpUdp::Message& message)
        {
            if (!message.isValid()) {
                return false;
            }

            const Oct::HttpCore::StartLine& startLine = message.startLine();
            if (!startLine.isRequestLine()) {
                return false;
            }

            const Oct::HttpCore::RequestLine& requestLine = startLine.requestLine();
            if (requestLine.method() != "NOTIFY" || requestLine.uri() != "*" ||
                    requestLine.version() != Oct::HttpCore::HTTP_VERSION_1_1) {
                return false;
            }

            const Oct::HttpCore::HeaderSet& headers = message.headers();
            if (!headers.contains("CACHE-CONTROL") || !headers.contains("LOCATION") || !headers.contains("NT")
                    || !headers.contains("NTS") || !headers.contains("SERVER") || !headers.contains("USN")) {

                return false;
            }

            if (headers.value("NTS") != "ssdp:alive") {
                return false;
            }

            if (Oct::Upnp::USN(headers.value("USN")).type() == Oct::Upnp::USN::USN_TYPE_INVALID) {
                return false;
            }

            return true;
        }
    }
}
