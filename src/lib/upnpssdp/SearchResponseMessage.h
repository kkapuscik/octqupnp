/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpSsdp
 * @{
 */

#ifndef _OCT_UPNP_SSDP__SEARCH_RESPONSE_MESSAGE_H_
#define _OCT_UPNP_SSDP__SEARCH_RESPONSE_MESSAGE_H_

/*---------------------------------------------------------------------------*/

#include <upnp/USN.h>
#include <httpudp/Message.h>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpSsdp
    {
        class SearchResponseMessage : public Oct::HttpUdp::Message
        {
        public:
            SearchResponseMessage(const Oct::HttpUdp::Message& message);
            virtual ~SearchResponseMessage();

            Oct::Upnp::USN usn() const
            {
                return Oct::Upnp::USN(headers().value("USN"));
            }

            QString location() const
            {
                return headers().value("LOCATION");
            }

            int cacheControl() const
            {
                QString val = headers().value("CACHE-CONTROL");
                if (val.startsWith("max-age=")) {
                    bool ok;

                    int rv = val.mid(8).toInt(&ok, 10);
                    if (ok) {
                        return rv;
                    }
                }
                return -1;
            }

        public:
            static bool isSearchResponseMessage(const Oct::HttpUdp::Message& message);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_SSDP__SEARCH_RESPONSE_MESSAGE_H_ */

/**
 * @}
 * @}
 * @}
 */
