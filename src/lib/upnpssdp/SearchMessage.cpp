/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SearchMessage.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpSsdp
    {
        /*
         M-SEARCH * HTTP/1.1
         HOST: 239.255.255.250:1900
         MAN: "ssdp:discover"
         MX: seconds to delay response
         ST: search target
         */

        SearchMessage::SearchMessage(const Oct::HttpUdp::Message& message) :
            Oct::HttpUdp::Message(message)
        {
            // do nothing
        }

        SearchMessage::SearchMessage(const QString& searchTarget, int delay) :
            Oct::HttpUdp::Message("M-SEARCH", "*", Oct::HttpCore::HTTP_VERSION_1_1)
        {
            headers().set("HOST", "239.255.255.250:1900");
            headers().set("MAN", "\"ssdp:discover\"");
            headers().set("MX", QString::number(delay));
            headers().set("ST", searchTarget);
        }

        SearchMessage::~SearchMessage()
        {
            // nothing to do
        }

        bool SearchMessage::isSearchMessage(const Oct::HttpUdp::Message& message)
        {
            if (!message.isValid()) {
                return false;
            }

            const Oct::HttpCore::StartLine& startLine = message.startLine();
            if (!startLine.isRequestLine()) {
                return false;
            }

            const Oct::HttpCore::RequestLine& requestLine = startLine.requestLine();
            if (requestLine.method() != "M-SEARCH" || requestLine.uri() != "*" ||
                    requestLine.version() != Oct::HttpCore::HTTP_VERSION_1_1) {
                return false;
            }

            const Oct::HttpCore::HeaderSet& headers = message.headers();
            if (!headers.contains("MAN") || !headers.contains("MX") || !headers.contains("ST")) {

                return false;
            }

            if (headers.value("MAN") != "\"ssdp:discover\"") {
                return false;
            }

            return true;
        }
    }
}
