/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpSsdp
 * @{
 */

#ifndef _OCT_UPNP_SSDP__DISCOVERY_SOCKET_H_
#define _OCT_UPNP_SSDP__DISCOVERY_SOCKET_H_

/*---------------------------------------------------------------------------*/

#include "SearchMessage.h"
#include "AliveMessage.h"
#include "ByeByeMessage.h"
#include "SearchResponseMessage.h"

#include <log/Logger.h>
#include <httpudp/MulticastSocket.h>

#include <QtNetwork/QHostAddress>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpSsdp
    {
        class DiscoverySocket : public Oct::HttpUdp::MulticastSocket
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<DiscoverySocket> Logger;

        public:
            DiscoverySocket(QObject* parent = 0);
            virtual ~DiscoverySocket();

            bool joinSsdpGroup();

            bool sendSearch(const SearchMessage& message = SearchMessage("upnp:rootdevice"));
            bool sendMessage(const Oct::HttpUdp::Message& message);

        private slots:
            void readPendingDatagrams();

        signals:
            void aliveMessageReceived(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::AliveMessage& message);

            void byeByeMessageReceived(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::ByeByeMessage& message);

            void searchMessageReceived(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchMessage& message);

            void searchResponseReceived(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchResponseMessage& message);

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_SSDP__DISCOVERY_SOCKET_H_ */

/**
 * @}
 * @}
 * @}
 */
