/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__MANAGER_H_
#define _OCT_UPNP_DEV__MANAGER_H_

/*---------------------------------------------------------------------------*/

#include "Device.h"
#include "RegisteredDevice.h"
#include "ManagerVfs.h"
#include "ManagerURLFactory.h"
#include "ManagerSoapProvider.h"

#include <log/Logger.h>
#include <httpserver/Server.h>
#include <upnpssdp/DiscoverySocket.h>
#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QQueue>
#include <QtCore/QMap>
#include <QtNetwork/QHostAddress>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class Manager : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<Manager> Logger;
            static const int UDP_MESSAGES_DELAY = 20;
            static const int UDP_MESSAGES_REPEAT = 2;

        private:
            class MessageEntry
            {
            private:
                Oct::HttpUdp::Message* m_message;
                bool m_searchResponse;
                QHostAddress m_targetHost;
                quint16 m_targetPort;

            private:
                Q_DISABLE_COPY(MessageEntry)

            public:
                MessageEntry(Oct::HttpUdp::Message* message) :
                    m_message(message), m_searchResponse(false)
                {
                    // nothing to do
                }

                MessageEntry(Oct::HttpUdp::Message* message, const QHostAddress& targetHost, quint16 targetPort) :
                    m_message(message), m_searchResponse(true), m_targetHost(targetHost), m_targetPort(targetPort)
                {
                    // nothing to do
                }

                ~MessageEntry()
                {
                    delete m_message;
                }

                bool isSearchResponse() const
                {
                    return m_searchResponse;
                }

                const QHostAddress& targetHost() const
                {
                    return m_targetHost;
                }

                const quint16 targetPort() const
                {
                    return m_targetPort;
                }

                const Oct::HttpUdp::Message& message() const
                {
                    return *m_message;
                }
            };

            typedef QMap<Device*, RegisteredDevice*> RegDeviceMap;
            typedef QQueue<MessageEntry*> MessageQueue;

        private:
            Oct::HttpServer::Server* m_httpServer;
            Oct::UpnpSsdp::DiscoverySocket* m_discoverySocket;

            QTimer* m_messageQueueTimer;
            MessageQueue m_messageQueue;
            RegDeviceMap m_registeredDevices;
            QHostAddress m_address; // TODO: remove
            ManagerURLFactory m_urlFactory;

            ManagerVfs* m_documentVfs;
            ManagerSoapProvider* m_soapProvider;

        public:
            Manager(Oct::HttpServer::Server* server = NULL, QObject* parent = NULL);
            virtual ~Manager();

            bool isRegistered(Device* device) const;
            bool registerDevice(Device* device);
            bool unregisterDevice(Device* device);

        private:
            void saveDeviceState(Device* device);

            Service* serviceForControlURL(const QString& controlPathRemainder);

            void addMessage(Oct::HttpUdp::Message* message);
            void addMessage(Oct::HttpUdp::Message* message, const QHostAddress& sender, quint16 senderPort);
            MessageEntry* getMessage();
            bool hasMoreMessages();

            void registerDocuments(RegisteredDevice* regDevice);
            void unregisterDocuments(RegisteredDevice* regDevice);

            QString createDeviceLocationURL(const RegisteredDevice* regDevice);

            Oct::HttpUdp::Message* createMessage(bool alive, const RegisteredDevice* regDevice, const Device* device,
                    const QString& nt);

            Oct::HttpUdp::Message* createSearchMessage(bool searchAll, const RegisteredDevice* regDevice,
                    const Device* device, const QString& st, const QString& nt);

            void sendRootDeviceMessages(bool alive, RegisteredDevice* regDevice, const Device* device);

            void sendDeviceMessages(bool alive, RegisteredDevice* regDevice, const Device* device);

            void sendServiceMessages(bool alive, RegisteredDevice* regDevice, const Device* device,
                    const Service* service);

            void sendMessages(bool alive, RegisteredDevice* regDevice);

            void processSearchAll(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchMessage& message);

            void processSearchRootDevices(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchMessage& message);

            void processSearchDeviceByUUID(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchMessage& message, const QString& deviceUUID);

            void processSearchDeviceByType(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchMessage& message, const QString& deviceType);

            void processSearchServiceByType(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchMessage& message, const QString& serviceType);

            void sendQueueMessage();

        private slots:
            void onAppQuit();

            void processSearchRequest(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchMessage& message);

            void sendDeviceDiscovery();

            void messageQueueTimeout();

            friend class ManagerSoapHandler;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__MANAGER_H_ */

/**
 * @}
 * @}
 * @}
 */
