/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ActionRequest.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        Oct::Log::Logger<ActionRequest> ActionRequest::Logger("/oct/upnpdev/ActionRequest");

        ActionRequest::ActionRequest(const Action& action) :
            m_action(action), m_errorCode(0)
        {
            // nothing to do
        }

        QString ActionRequest::actionName() const
        {
            return m_action.name();
        }

        Action ActionRequest::action() const
        {
            return m_action;
        }

        bool ActionRequest::setInputArgumentValue(const QString& name, const QVariant& value)
        {
            if (!m_action.hasArgument(name)) {
                Logger.info(this) << "Action does not have argument: " << name;
                return false;
            }

            if (m_arguments.contains(name)) {
                Logger.info(this) << "Action argument already set: " << name;
                return false;
            }

            ActionArgument arg = m_action.argument(name);
            if (!arg.isInput()) {
                Logger.info(this) << "Action argument is not an input argument: " << name;
                return false;
            }

            m_arguments.insert(name, value);

            return true;
        }

        bool ActionRequest::setOutputArgumentValue(const QString& name, const QVariant& value)
        {
            if (!m_action.hasArgument(name)) {
                Logger.info(this) << "Action does not have argument: " << name;
                return false;
            }

            if (m_arguments.contains(name)) {
                Logger.info(this) << "Action argument already set: " << name;
                return false;
            }

            ActionArgument arg = m_action.argument(name);
            if (!arg.isOutput()) {
                Logger.info(this) << "Action argument is not an output argument: " << name;
                return false;
            }

            m_arguments.insert(name, value);

            return true;
        }

        bool ActionRequest::hasArgumentValue(const QString& name) const
        {
            return m_arguments.contains(name);
        }

        QVariant ActionRequest::argumentValue(const QString& name) const
        {
            if (!m_arguments.contains(name)) {
                return QVariant();
            }

            return m_arguments.value(name);
        }

        QVariant ActionRequest::argumentValueBoolean(const QString& name) const
        {
            if (m_arguments.contains(name)) {
                QString value = m_arguments.value(name).toString().toLower();
                if (value == "1" || value == "true" || value == "yes") {
                    return QVariant(true);
                } else if (value == "0" || value == "false" || value == "no") {
                    return QVariant(false);
                }
            }

            Logger.info(this) << "Argument value is not boolean: " << name;

            return QVariant();
        }

        bool ActionRequest::isRequestValid() const
        {
            QList<ActionArgument> args = m_action.arguments();
            for (int i = 0; i < args.size(); ++i) {
                if (args[i].isInput()) {
                    if (!hasArgumentValue(args[i].name())) {
                        Logger.info(this) << "Request invalid. Missing argument: " << args[i].name();

                        return false;
                    }
                }
            }

            return true;
        }

        bool ActionRequest::isResponseValid() const
        {
            if (m_errorCode > 0) {
                return true;
            }

            QList<ActionArgument> args = m_action.arguments();
            for (int i = 0; i < args.size(); ++i) {
                if (args[i].isOutput()) {
                    if (!hasArgumentValue(args[i].name())) {
                        Logger.info(this) << "Response invalid. Missing argument: " << args[i].name();

                        return false;
                    }
                }
            }

            return true;
        }

        void ActionRequest::setError(int errorCode, const QString& reason)
        {
            Q_ASSERT(errorCode > 0);

            m_errorCode = errorCode;
            m_errorReason = reason;
        }

        void ActionRequest::setStandardError(int errorCode)
        {
            switch (errorCode) {
                case ERROR_CODE__INVALID_ACTION:
                    setError(errorCode, "Invalid Action");
                    break;

                case ERROR_CODE__INVALID_ARGS:
                    setError(errorCode, "Invalid Args");
                    break;

                case ERROR_CODE__OUT_OF_SYNCH:
                    setError(errorCode, "Out of Synch");
                    break;

                case ERROR_CODE__ACTION_FAILED:
                    setError(errorCode, "Action Failed");
                    break;

                default:
                    Q_ASSERT(0);
                    setError(ERROR_CODE__ACTION_FAILED, "Internal Error");
                    break;
            }
        }

        int ActionRequest::errorCode() const
        {
            return m_errorCode;
        }

        QString ActionRequest::errorReason() const
        {
            return m_errorReason;
        }

    }
}
