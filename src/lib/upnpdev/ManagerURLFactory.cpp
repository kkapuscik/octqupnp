/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ManagerURLFactory.h"

#include "Device.h"
#include "Service.h"
#include "RegisteredDevice.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        ManagerURLFactory::ManagerURLFactory()
        {
            m_uuid = UUID::generate();
        }

        QString ManagerURLFactory::filesBaseURL() const
        {
            return QString("/%1/files/").arg(m_uuid.toString(false));
        }

        QString ManagerURLFactory::controlBaseURL() const
        {
            return QString("/%1/control/").arg(m_uuid.toString(false));
        }

        QString ManagerURLFactory::eventingBaseURL() const
        {
            return QString("/%1/eventing/").arg(m_uuid.toString(false));
        }

        QString ManagerURLFactory::deviceLocationURL(const QString& serverBase, const RegisteredDevice* regDevice) const
        {
            return serverBase + deviceDescriptionURL(regDevice).mid(1);
        }

        QString ManagerURLFactory::deviceDescriptionURL(const RegisteredDevice* regDevice, bool filesDirRelative) const
        {
            if (filesDirRelative) {
                return regDevice->rootDevice()->savedUDN().toString(false) + ".xml";
            } else {
                return filesBaseURL() + regDevice->rootDevice()->savedUDN().toString(false) + ".xml";
            }
        }

        QString ManagerURLFactory::serviceDescriptionURL(const Service* service, bool filesDirRelative) const
        {
            if (filesDirRelative) {
                return service->uuid().toString(false) + ".xml";
            } else {
                return filesBaseURL() + service->uuid().toString(false) + ".xml";
            }
        }

        QString ManagerURLFactory::serviceControlURL(const Service* service) const
        {
            return controlBaseURL() + service->uuid().toString(false) + "/";
        }

        QString ManagerURLFactory::serviceEventingURL(const Service* service) const
        {
            return eventingBaseURL() + service->uuid().toString(false) + "/";
        }

        UUID ManagerURLFactory::serviceUUID(const QString& controlPathRemainder) const
        {
            if (controlPathRemainder.endsWith('/')) {
                QString uuidStr = controlPathRemainder.mid(0, controlPathRemainder.length() - 1);
                return UUID("uuid:" + uuidStr);
            }
            return UUID();
        }

    }
}
