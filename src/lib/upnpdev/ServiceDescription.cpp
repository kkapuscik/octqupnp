/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ServiceDescription.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        const QString ServiceDescription::DOCUMENT_HEADER("<?xml version=\"1.0\"?>\n"
            "<scpd xmlns=\"urn:schemas-upnp-org:service-1-0\">\n"
            "<specVersion>\n"
            "<major>1</major>\n"
            "<minor>0</minor>\n"
            "</specVersion>\n");

        const QString ServiceDescription::DOCUMENT_FOOTER("</scpd>\n");

        ServiceDescription::ServiceDescription(const Service* service) :
            ManagerDocument()
        {
            QString buffer;

            buffer.append(DOCUMENT_HEADER);
            appendActions(service, buffer);
            appendStateVariables(service, buffer);
            buffer.append(DOCUMENT_FOOTER);

            m_buffer = buffer.toUtf8();
        }

        ServiceDescription::~ServiceDescription()
        {
            // do nothing
        }

        void ServiceDescription::appendActions(const Service* service, QString& buffer)
        {
            QList<Action> actions = service->actions();
            if (actions.size() == 0) {
                return;
            }

            buffer.append("<actionList>\n");
            for (int i = 0; i < actions.size(); ++i) {
                buffer.append("<action>\n");

                buffer.append("<name>");
                buffer.append(actions[i].name());
                buffer.append("</name>\n");

                QList<ActionArgument> arguments = actions[i].arguments();
                if (arguments.size() > 0) {
                    buffer.append("<argumentList>\n");

                    for (int j = 0; j < arguments.size(); ++j) {
                        buffer.append("<argument>\n");

                        buffer.append("<name>");
                        buffer.append(arguments[j].name());
                        buffer.append("</name>\n");

                        switch (arguments[j].type()) {
                            case ActionArgument::ACTION_ARGUMENT_TYPE_INPUT:
                                buffer.append("<direction>in</direction>\n");
                                break;
                            case ActionArgument::ACTION_ARGUMENT_TYPE_OUTPUT:
                                buffer.append("<direction>out</direction>\n");
                                break;
                            case ActionArgument::ACTION_ARGUMENT_TYPE_RETVAL:
                                buffer.append("<direction>out</direction>\n");
                                buffer.append("<retval />\n");
                                break;
                        }

                        buffer.append("<relatedStateVariable>");
                        buffer.append(arguments[j].relatedStateVariable());
                        buffer.append("</relatedStateVariable>\n");

                        buffer.append("</argument>\n");
                    }

                    buffer.append("</argumentList>\n");
                }
                buffer.append("</action>\n");
            }
            buffer.append("</actionList>\n");
        }

        void ServiceDescription::appendStateVariables(const Service* service, QString& buffer)
        {
            QList<StateVariable> stateVariables = service->stateVariables();

            if (stateVariables.size() == 0) {
                return;
            }

            buffer.append("<serviceStateTable>\n");

            for (int i = 0; i < stateVariables.size(); ++i) {
                if (stateVariables[i].sendEvents()) {
                    buffer.append("<stateVariable sendEvents=\"yes\">\n");
                } else {
                    buffer.append("<stateVariable sendEvents=\"no\">\n");
                }

                buffer.append("<name>");
                buffer.append(stateVariables[i].name());
                buffer.append("</name>\n");

                buffer.append("<dataType>");
                buffer.append(stateVariables[i].dataType());
                buffer.append("</dataType>\n");

                if (!stateVariables[i].defaultValue().isNull()) {
                    buffer.append("<defaultValue>");
                    buffer.append(stateVariables[i].defaultValue().toString());
                    buffer.append("</defaultValue>\n");
                }

                if (stateVariables[i].allowedValues().size() > 0) {
                    buffer.append("<allowedValueList>\n");
                    QStringList allowedValues = stateVariables[i].allowedValues();
                    for (int j = 0; j < allowedValues.size(); ++j) {
                        buffer.append("<allowedValue>");
                        buffer.append(allowedValues[j]);
                        buffer.append("</allowedValue>\n");
                    }
                    buffer.append("</allowedValueList>\n");
                }

                if (!stateVariables[i].minimumValue().isNull() && !stateVariables[i].maximumValue().isNull()) {
                    buffer.append("<allowedValueRange>\n");

                    buffer.append("<minimum>");
                    buffer.append(stateVariables[i].minimumValue().toString());
                    buffer.append("</minimum>\n");

                    buffer.append("<maximum>");
                    buffer.append(stateVariables[i].maximumValue().toString());
                    buffer.append("</maximum>\n");

                    if (!stateVariables[i].stepValue().isNull()) {
                        buffer.append("<step>");
                        buffer.append(stateVariables[i].stepValue().toString());
                        buffer.append("</step>\n");
                    }

                    buffer.append("</allowedValueRange>\n");
                }

                buffer.append("</stateVariable>\n");
            }

            buffer.append("</serviceStateTable>\n");
        }

        QByteArray ServiceDescription::buffer() const
        {
            return m_buffer;
        }

        QString ServiceDescription::contentType() const
        {
            return "text/xml";
        }

    }
}
