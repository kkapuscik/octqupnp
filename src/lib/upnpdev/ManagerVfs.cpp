/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ManagerVfs.h"

#include "Manager.h"

#include <httpserver/ByteArrayVfsFile.h>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        Oct::Log::Logger<ManagerVfs> ManagerVfs::Logger("/oct/upnpdev/ManagerVfs");

        ManagerVfs::ManagerVfs(QObject* parent) :
                Oct::HttpServer::Vfs(parent)
        {
            // nothing to do
        }

        ManagerVfs::~ManagerVfs()
        {
            // nothing to do
        }

        Oct::HttpServer::VfsFile* ManagerVfs::open(const QString& path)
        {
            Logger.trace(this) << "open() path=" << path;

            Oct::HttpServer::VfsFile* file = NULL;

            DocumentMap::const_iterator iter = m_documents.find(path);
            if (iter != m_documents.end()) {
                file = new Oct::HttpServer::ByteArrayVfsFile(iter.value().m_contentType, iter.value().m_data);
                Logger.trace(this) << "open() path=" << path << " - file opened: " << file;
            } else {
                Logger.trace(this) << "open() path=" << path << " - file not found";
            }

            return file;
        }

        void ManagerVfs::registerDocument(const QString& path, const ManagerDocument& document)
        {
            Q_ASSERT(!m_documents.contains(path));

            Logger.trace(this) << "registerDocument() path=" << path;

            m_documents.insert(path, Entry(document));
        }

        void ManagerVfs::unregisterDocument(const QString& path)
        {
            Q_ASSERT(m_documents.contains(path));

            Logger.trace(this) << "unregisterDocument() path=" << path;

            m_documents.remove(path);
        }

    }
}
