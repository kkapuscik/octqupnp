/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "RegisteredDevice.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        RegisteredDevice::RegisteredDevice(Device* device, const ManagerURLFactory& urlFactory, QObject* parent) :
            QObject(parent), m_device(device), m_deviceDescription(device, urlFactory), m_cacheControl(DEFAULT_CACHE_CONTROL)
        {
            Q_ASSERT(device != NULL);

            m_device = device;
            m_discoveryTimer = new QTimer(this);

            m_discoveryTimer->setSingleShot(false);
            m_discoveryTimer->setInterval(cacheControl() / 2 * 1000);

            fillObjectLists(device);

            connect(m_discoveryTimer, SIGNAL( timeout() ), this, SIGNAL( discoveryTimeout() ));
        }

        RegisteredDevice::~RegisteredDevice()
        {
            // nothing do to
        }

        void RegisteredDevice::start()
        {
            return m_discoveryTimer->start();
        }

        int RegisteredDevice::cacheControl() const
        {
            return m_cacheControl;
        }

        void RegisteredDevice::setCacheControl(int seconds)
        {
            if (m_discoveryTimer->isActive()) {
                Q_ASSERT(0);
            }

            m_cacheControl = seconds;
            m_discoveryTimer->setInterval(cacheControl() / 2 * 1000);
        }

        void RegisteredDevice::fillObjectLists(Device* device)
        {
            m_devices.append(device);

            /* add all services */
            QList<Service*> services = device->savedServices();
            for (int i = 0; i < services.size(); ++i) {
                m_services.append(services[i]);
                m_servicesDescriptions.insert(services[i], ServiceDescription(services[i]));
                m_servicesDevices.insert(services[i], device);
            }

            /* add all devices */
            QList<Device*> devices = device->savedDevices();
            for (int i = 0; i < devices.size(); ++i) {
                fillObjectLists(devices[i]);
            }
        }

        Device* RegisteredDevice::rootDevice() const
        {
            return m_device;
        }

        QList<Device*> RegisteredDevice::allDevices() const
        {
            return m_devices;
        }

        QList<Service*> RegisteredDevice::allServices() const
        {
            return m_services;
        }

    }
}
