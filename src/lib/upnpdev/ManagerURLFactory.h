/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__MANAGER_URL_FACTORY_H_
#define _OCT_UPNP_DEV__MANAGER_URL_FACTORY_H_

/*---------------------------------------------------------------------------*/

#include "Types.h"

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class RegisteredDevice;
        class Device;
        class Service;

        class ManagerURLFactory
        {
        private:
            Oct::Upnp::UUID m_uuid;

        public:
            ManagerURLFactory();

            QString filesBaseURL() const;
            QString controlBaseURL() const;
            QString eventingBaseURL() const;

            QString deviceDescriptionURL(const RegisteredDevice* regDevice, bool filesDirRelative = false) const;

            QString serviceDescriptionURL(const Service* service, bool filesDirRelative = false) const;
            QString serviceControlURL(const Service* service) const;
            QString serviceEventingURL(const Service* service) const;

            QString deviceLocationURL(const QString& serverBase, const RegisteredDevice* regDevice) const;

            UUID serviceUUID(const QString& controlPathRemainder) const;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__MANAGER_URL_FACTORY_H_ */

/**
 * @}
 * @}
 * @}
 */
