/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__ACTION_REQUEST_H_
#define _OCT_UPNP_DEV__ACTION_REQUEST_H_

/*---------------------------------------------------------------------------*/

#include "Types.h"

#include <log/Logger.h>

#include <QtCore/QMap>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class ActionRequest
        {
        private:
            static Oct::Log::Logger<ActionRequest> Logger;

        public:
            enum
            {
                ERROR_CODE__INVALID_ACTION = 401,
                ERROR_CODE__INVALID_ARGS = 402,
                ERROR_CODE__OUT_OF_SYNCH = 403,
                ERROR_CODE__ACTION_FAILED = 501
            };

        private:
            typedef QMap<QString, QVariant> ArgumentMap;

        private:
            Action m_action;
            ArgumentMap m_arguments;
            int m_errorCode;
            QString m_errorReason;

        public:
            ActionRequest(const Action& action);

            QString actionName() const;
            Action action() const;

            bool setInputArgumentValue(const QString& name, const QVariant& value);
            bool setOutputArgumentValue(const QString& name, const QVariant& value);
            bool hasArgumentValue(const QString& name) const;
            QVariant argumentValue(const QString& name) const;

            /**
             * Returns argument value as boolean wrapped by QVariant.
             *
             * @param name
             *      Name of the argument.
             *
             * @return
             * QVariant(true) if the string representation of value was: "true". "1" or "yes".
             * QVariant(false) if the string representation of value was: "false". "0" or "no".
             * Null QVariant if argument was not present or the string representation was not boolean compliant.
             */
            QVariant argumentValueBoolean(const QString& name) const;

            bool isRequestValid() const;
            bool isResponseValid() const;

            void setStandardError(int errorCode);

            void setError(int errorCode, const QString& reason);
            int errorCode() const;
            QString errorReason() const;

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__ACTION_REQUEST_H_ */

/**
 * @}
 * @}
 * @}
 */
