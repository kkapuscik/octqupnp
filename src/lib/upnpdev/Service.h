/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__SERVICE_H_
#define _OCT_UPNP_DEV__SERVICE_H_

/*---------------------------------------------------------------------------*/

#include "Types.h"
#include "ActionRequest.h"

#include <upnp/Service.h>

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtXml/QDomDocument>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class Service : public QObject, public Oct::Upnp::Service
        {
        private:
            QString m_serviceType;
            QString m_serviceId;
            QList<StateVariable> m_stateVariables;
            QList<Action> m_actions;
            UUID m_uuid;

        public:
            Service(QObject* parent = 0);
            virtual ~Service();

            UUID uuid() const
            {
                return m_uuid;
            }

            bool isValid() const
            {
                // TODO
                return true;
            }

            void setServiceType(const QString& serviceType);
            void setServiceId(const QString& serviceId);

            void addStateVariable(const StateVariable& variable);
            void removeStateVariable(const QString& name);
            void addAction(const Action& action);
            void removeAction(const QString& name);

            bool addFromSCPD(QString fileName);
            bool addFromSCPD(QByteArray buffer);
            bool addFromSCPD(QDomDocument document);

            virtual const QString& serviceType() const;
            virtual const QString& serviceId() const;

            virtual bool hasStateVariable(const QString& name) const;
            virtual QList<StateVariable> stateVariables() const;
            virtual StateVariable stateVariable(const QString& name) const;

            virtual bool hasAction(const QString& name) const;
            virtual QList<Action> actions() const;
            virtual Action action(const QString& name) const;

            virtual bool processActionRequest(ActionRequest& actionRequest) = 0;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__SERVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
