/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__REGISTERED_DEVICE_H_
#define _OCT_UPNP_DEV__REGISTERED_DEVICE_H_

/*---------------------------------------------------------------------------*/

#include "Device.h"
#include "Service.h"
#include "DeviceDescription.h"
#include "ServiceDescription.h"
#include "ManagerURLFactory.h"

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QList>
#include <QtCore/QMap>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class RegisteredDevice : public QObject
        {
        Q_OBJECT

        private:
            static const int DEFAULT_CACHE_CONTROL = 60; /* s */

        private:
            typedef QList<Device*> DeviceList;
            typedef QList<Service*> ServiceList;
            typedef QMap<const Service*, ServiceDescription> ServiceDocMap;
            typedef QMap<const Service*, Device*> ServiceDeviceMap;

        private:
            Device* m_device;
            DeviceDescription m_deviceDescription;
            int m_cacheControl;

            QTimer* m_discoveryTimer;

            DeviceList m_devices;
            ServiceList m_services;
            ServiceDocMap m_servicesDescriptions;
            ServiceDeviceMap m_servicesDevices;

        public:
            RegisteredDevice(Device* device, const ManagerURLFactory& urlFactory, QObject* parent = 0);
            virtual ~RegisteredDevice();

            void start();

            int cacheControl() const;
            void setCacheControl(int seconds);

            Device* rootDevice() const;
            QList<Device*> allDevices() const;
            QList<Service*> allServices() const;

            const ManagerDocument& deviceDescription() const
            {
                return m_deviceDescription;
            }

            const ManagerDocument& serviceDescription(const Service* service) const
            {
                ServiceDocMap::const_iterator iter = m_servicesDescriptions.find(service);
                if (iter != m_servicesDescriptions.end()) {
                    return iter.value();
                } else {
                    Q_ASSERT(0);
                    // TODO: return value (empty document)
                }
            }

            Device* serviceDevice(Service* service) const
            {
                ServiceDeviceMap::const_iterator iter = m_servicesDevices.find(service);
                if (iter != m_servicesDevices.end()) {
                    return iter.value();
                } else {
                    Q_ASSERT(0);
                    return NULL;
                }
            }

        private:
            void fillObjectLists(Device* device);

        signals:
            void discoveryTimeout();
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__REGISTERED_DEVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
