/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev     UPnP protocol Device elements
 * @{
 */

#ifndef _OCT_UPNP_DEV__PACKAGE_H_
#define _OCT_UPNP_DEV__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * UPnP protocol Device elements.
     */
    namespace UpnpDev
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_UPNP_DEV__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
