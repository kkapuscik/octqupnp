/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ManagerSoapProvider.h"

#include "ManagerSoapHandler.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        Oct::Log::Logger<ManagerSoapProvider> ManagerSoapProvider::Logger(
                "/oct/upnpdev/ManagerSoapProvider");

        ManagerSoapProvider::ManagerSoapProvider(Manager* manager, QObject* parent) :
            Oct::HttpServer::RequestHandlerProvider(parent),
            m_manager(manager)
        {
            // nothing to do
        }

        ManagerSoapProvider::~ManagerSoapProvider()
        {
            // nothing to do
        }

        Oct::HttpServer::RequestHandler* ManagerSoapProvider::createHandler()
        {
            return new ManagerSoapHandler(m_manager, this);
        }
    }
}
