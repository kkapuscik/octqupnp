/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__MANAGER_SOAP_PROVIDER_H_
#define _OCT_UPNP_DEV__MANAGER_SOAP_PROVIDER_H_

/*---------------------------------------------------------------------------*/

#include <log/Logger.h>
#include <httpserver/RequestHandlerProvider.h>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class Manager;

        class ManagerSoapProvider : public Oct::HttpServer::RequestHandlerProvider
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<ManagerSoapProvider> Logger;

        private:
            Manager* m_manager;

        public:
            ManagerSoapProvider(Manager* manager, QObject* parent = 0);
            virtual ~ManagerSoapProvider();

            virtual Oct::HttpServer::RequestHandler* createHandler();
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__MANAGER_SOAP_PROVIDER_H_ */

/**
 * @}
 * @}
 * @}
 */
