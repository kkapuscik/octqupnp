/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__MANAGER_SOAP_HANDLER_H_
#define _OCT_UPNP_DEV__MANAGER_SOAP_HANDLER_H_

/*---------------------------------------------------------------------------*/

#include "ActionRequest.h"
#include "SoapActionElement.h"

#include <log/Logger.h>
#include <soapserver/SoapRequestHandler.h>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class Manager;

        class ManagerSoapHandler : public Oct::SoapServer::SoapRequestHandler
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<ManagerSoapHandler> Logger;

        private:
            Manager* m_manager;

        public:
            ManagerSoapHandler(Manager* manager, QObject* parent = 0);
            virtual ~ManagerSoapHandler();

            virtual Oct::Soap::SoapMessage processMessage(const QString& pathRemainder,
                    Oct::Soap::SoapMessage& requestMessage);

        private:
            Oct::Soap::SoapMessage createSuccessResponse(SoapActionElement actionElement,
                    const ActionRequest& actionRequest);
            Oct::Soap::SoapMessage createFailureResponse(int code, const QString& reason);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__MANAGER_SOAP_HANDLER_H_ */

/**
 * @}
 * @}
 * @}
 */
