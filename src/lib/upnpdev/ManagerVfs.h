/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__MANAGER_VFS_H_
#define _OCT_UPNP_DEV__MANAGER_VFS_H_

/*---------------------------------------------------------------------------*/

#include "ManagerDocument.h"

#include <log/Logger.h>
#include <httpserver/Vfs.h>

#include <QtCore/QByteArray>
#include <QtCore/QMap>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class ManagerVfs : public Oct::HttpServer::Vfs
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<ManagerVfs> Logger;

        private:
            struct Entry
            {
                QString m_contentType;
                QByteArray m_data;

                Entry(const ManagerDocument& document) :
                    m_contentType(document.contentType()), m_data(document.buffer())
                {
                    // do nothing
                }
            };

            typedef QMap<QString, Entry> DocumentMap;

        private:
            DocumentMap m_documents;

        public:
            ManagerVfs(QObject* parent = 0);
            virtual ~ManagerVfs();

            void registerDocument(const QString& path, const ManagerDocument& document);
            void unregisterDocument(const QString& path);

            virtual Oct::HttpServer::VfsFile* open(const QString& path);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__MANAGER_VFS_H_ */

/**
 * @}
 * @}
 * @}
 */
