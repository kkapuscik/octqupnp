/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ManagerSoapHandler.h"

#include "SoapActionElement.h"
#include "Manager.h"
#include "ActionRequest.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        Oct::Log::Logger<ManagerSoapHandler> ManagerSoapHandler::Logger("/oct/upnpdev/ManagerSoapHandler");

        ManagerSoapHandler::ManagerSoapHandler(Manager* manager, QObject* parent) :
            Oct::SoapServer::SoapRequestHandler(parent), m_manager(manager)
        {
            // nothing to do
        }

        ManagerSoapHandler::~ManagerSoapHandler()
        {
            // nothing to do
        }

        Oct::Soap::SoapMessage ManagerSoapHandler::processMessage(const QString& pathRemainder,
                Oct::Soap::SoapMessage& requestMessage)
        {
#if 0
            POST path of control URL HTTP/1.1
            HOST: host of control URL:port of control URL
            CONTENT-LENGTH: bytes in body
            CONTENT-TYPE: text/xml; charset="utf-8"
            SOAPACTION: "urn:schemas-upnp-org:service:serviceType:v#actionName"
            <?xml version="1.0"?>
            <s:Envelope
            xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
            s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            <s:Body>
            <u:actionName xmlns:u="urn:schemas-upnp-org:service:serviceType:v">
            <argumentName>in arg value</argumentName>
            other in args and their values go here, if any
            </u:actionName>
            </s:Body>
            </s:Envelope>
#endif

            Q_ASSERT(requestMessage.isValid());
            Q_ASSERT(!requestMessage.isFault());

            Oct::Soap::SoapEnvelope envElem = requestMessage.document().envelope();
            Q_ASSERT(!envElem.isNull());

            Oct::Soap::SoapBody bodyElem = envElem.body();
            Q_ASSERT(!bodyElem.isNull());

            QList<Oct::Soap::SoapBodyElement> elements = bodyElem.bodyElements();
            if (elements.size() != 1) {
                Logger.trace(this) << "No action body element found";
                return Oct::Soap::SoapMessage();
            }

            SoapActionElement actionElement = SoapActionElement(elements[0]);
            if (!actionElement.isValid()) {
                Logger.trace(this) << "Action body element is invalid";
                return Oct::Soap::SoapMessage();
            }

            if (actionElement.soapAction() != requestMessage.soapAction()) {
                Logger.trace(this) << "Action service and action does not match SOAPACTION";
                return Oct::Soap::SoapMessage();
            }

            Logger.trace(this) << "Action element: service=" << actionElement.serviceType() << " action="
                    << actionElement.actionName() << " path=" << pathRemainder;

            Service* service = m_manager->serviceForControlURL(pathRemainder);
            if (service == NULL) {
                Logger.trace(this) << "Cannot find service for given URL: " << pathRemainder;
                return Oct::Soap::SoapMessage();
            }

            Logger.trace(this) << "Service found: service=" << service << " type=" << service->serviceType();

            if (!service->isCompatible(actionElement.serviceType())) {
                Logger.trace(this) << "Service type is not compatible with request";
                return Oct::Soap::SoapMessage();
            }

            if (!service->hasAction(actionElement.actionName())) {
                Logger.trace(this) << "Service does not have action: " << actionElement.actionName();
                return createFailureResponse(ActionRequest::ERROR_CODE__INVALID_ACTION, "Invalid Action");
            }

            Action action = service->action(actionElement.actionName());

            Logger.trace(this) << "Processing arguments for action: " << action.name();

            ActionRequest actionRequest(action);

            /* TODO: invalid XML handling */
            for (QDomElement argElement = actionElement.firstChildElement(); !argElement.isNull(); argElement
                    = argElement.nextSiblingElement()) {

                // TODO: check namespace, type, etc.
                if (!actionRequest.setInputArgumentValue(argElement.localName(), argElement.text())) {

                    Logger.trace(this) << "Invalid input argument: " << argElement.localName();
                    return Oct::Soap::SoapMessage();
                }
            }

            if (!actionRequest.isRequestValid()) {
                Logger.trace(this) << "Action request is not valid";
                return Oct::Soap::SoapMessage();
            }

            if (!service->processActionRequest(actionRequest)) {
                Logger.trace(this) << "Action processing failed";
                return Oct::Soap::SoapMessage();
            }

            if (!actionRequest.isResponseValid()) {
                Logger.trace(this) << "Action response is not valid";
                return Oct::Soap::SoapMessage();
            }

            if (actionRequest.errorCode() == 0) {
                /* success */

                Logger.trace(this) << "Action succeeded. Preparing response.";

                return createSuccessResponse(actionElement, actionRequest);
            } else {
                Logger.trace(this) << "Action failed. Preparing response.";

                /* error */
                return createFailureResponse(actionRequest.errorCode(), actionRequest.errorReason());
            }
        }

        Oct::Soap::SoapMessage ManagerSoapHandler::createSuccessResponse(SoapActionElement actionElement,
                const ActionRequest& actionRequest)
        {
#if 0
            HTTP/1.1 200 OK
            CONTENT-LENGTH: bytes in body
            CONTENT-TYPE: text/xml; charset="utf-8"
            DATE: when response was generated
            EXT:
            SERVER: OS/version UPnP/1.0 product/version
            <?xml version="1.0"?>
            <s:Envelope
            xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
            s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            <s:Body>
            <u:actionNameResponse xmlns:u="urn:schemas-upnp-org:service:serviceType:v">
            <argumentName>out arg value</argumentName>
            other out args and their values go here, if any
            </u:actionNameResponse>
            </s:Body>
            </s:Envelope>
#endif

            Oct::Soap::SoapMessage responseMessage = Oct::Soap::SoapMessage::createResponse("s");

            Oct::Soap::SoapEnvelope envelope = responseMessage.document().envelope();
            Oct::Soap::SoapBody body = envelope.body(true);
            Oct::Soap::SoapBodyElement responseElement = body.addBodyElementNS(actionElement.serviceType(), "u:"
                    + actionElement.actionName() + "Response");

            QList<ActionArgument> args = actionRequest.action().arguments();
            for (int i = 0; i < args.size(); ++i) {
                if (args[i].isOutput()) {
                    Oct::Soap::SoapBodyElement argElement = responseElement.addBodyElement(args[i].name());

                    argElement.appendTextEntry(actionRequest.argumentValue(args[i].name()).toString());
                }
            }

            Logger.trace(this) << "Prepared response:" << responseMessage.document().toString();

            return responseMessage;
        }

        Oct::Soap::SoapMessage ManagerSoapHandler::createFailureResponse(int code, const QString& reason)
        {
#if 0
            HTTP/1.1 500 Internal Server Error
            CONTENT-LENGTH: bytes in body
            CONTENT-TYPE: text/xml; charset="utf-8"
            DATE: when response was generated
            EXT:
            SERVER: OS/version UPnP/1.0 product/version
            <?xml version="1.0"?>
            <s:Envelope
            xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
            s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            <s:Body>
            <s:Fault>
            <faultcode>s:Client</faultcode>
            <faultstring>UPnPError</faultstring>
            <detail>
            <UPnPError xmlns="urn:schemas-upnp-org:control-1-0">
            <errorCode>error code</errorCode>
            <errorDescription>error string</errorDescription>
            </UPnPError>
            </detail>
            </s:Fault>
            </s:Body>
            </s:Envelope>
#endif

            Oct::Soap::SoapMessage responseMessage = Oct::Soap::SoapMessage::createResponse("s");

            Oct::Soap::SoapEnvelope envelope = responseMessage.document().envelope();
            Oct::Soap::SoapFault fault = envelope.body(true).fault(true);
            fault.setFaultCode("Client");
            fault.setFaultString("UPnPError");

            Oct::Soap::SoapFaultDetail faultDetail = fault.detail(true);
            Oct::Soap::SoapFaultDetailEntry faultUpnpError = faultDetail.addEntryNS(
                    "urn:schemas-upnp-org:control-1-0", "UPnPError");
            Oct::Soap::SoapFaultDetailEntry faultErrorCode = faultUpnpError.addEntryNS(
                    "urn:schemas-upnp-org:control-1-0", "errorCode");
            Oct::Soap::SoapFaultDetailEntry faultErrorDescription = faultUpnpError.addEntryNS(
                    "urn:schemas-upnp-org:control-1-0", "errorDescription");

            faultErrorCode.appendTextEntry(QString("%1").arg(code));
            faultErrorDescription.appendTextEntry(reason);

            Logger.trace(this) << "Prepared response:" << responseMessage.document().toString();

            return responseMessage;
        }

    }
}
