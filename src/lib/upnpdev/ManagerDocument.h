/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__MANAGER_DOCUMENT_H_
#define _OCT_UPNP_DEV__MANAGER_DOCUMENT_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QByteArray>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class ManagerDocument
        {
        public:
            ManagerDocument();
            virtual ~ManagerDocument();

            virtual QByteArray buffer() const = 0;
            virtual QString contentType() const = 0;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__MANAGER_DOCUMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
