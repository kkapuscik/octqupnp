/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__DESCRIPTION_PARSER_H_
#define _OCT_UPNP_DEV__DESCRIPTION_PARSER_H_

/*---------------------------------------------------------------------------*/

#include "Device.h"
#include "Service.h"

#include <upnp/DescriptionParser.h>

#include <log/Logger.h>

#include <QtCore/QUrl>
#include <QtXml/QDomElement>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class DescriptionParser : protected Oct::Upnp::DescriptionParser
        {
        private:
            static Oct::Log::Logger<DescriptionParser> Logger;

        public:
            static bool initService(Service& service, QString fileName);
            static bool initService(Service& service, QByteArray buffer);
            static bool initService(Service& service, QDomDocument document);

        private:
            static bool initService(Service& service, QDomElement& documentElement);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__DESCRIPTION_PARSER_H_ */

/**
 * @}
 * @}
 * @}
 */
