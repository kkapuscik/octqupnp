/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__DEVICE_H_
#define _OCT_UPNP_DEV__DEVICE_H_

/*---------------------------------------------------------------------------*/

#include "Types.h"
#include "Service.h"

#include <upnp/Device.h>
#include <QtCore/QObject>

/*---------------------------------------------------------------------------*/

// TODO: copying should done somehow. engine should hold its own, const copy

namespace Oct
{
    namespace UpnpDev
    {
        /* forwards */
        class Manager;
        class ManagerURLFactory;
        class DeviceDescription;
        class RegisteredDevice;

        class Device : public QObject, public Oct::Upnp::Device
        {
        private:
            typedef QMap<QString, QString> PropertyMap;
            typedef QList<Device*> DeviceList;
            typedef QList<Service*> ServiceList;
            // typedef QList<Icon> IconList;

        private:
            UUID m_udn;
            PropertyMap m_properties;
            DeviceList m_devices;
            ServiceList m_services;
            // IconList m_icons;

            UUID m_savedUDN;
            DeviceList m_savedDevices;
            ServiceList m_savedServices;

        public:
            Device(QObject* parent = 0);
            virtual ~Device();

            bool isValid() const
            {
                return !(property(PROPERTY_DEVICE_TYPE).isEmpty() || property(PROPERTY_FRIENDLY_NAME).isEmpty()
                        || property(PROPERTY_MANUFACTURER).isEmpty() || property(PROPERTY_MODEL_NAME).isEmpty())
                        && udn().isValid();
            }

            bool hasService(Service* service);
            void addService(Service* service);
            void removeService(Service* service);
            QList<Service*> services() const;

            bool hasDevice(Device* device);
            void addDevice(Device* device);
            void removeDevice(Device* device);
            QList<Device*> devices() const;

            void setProperty(const QString& key, const QString& value);
            void removeProperty(const QString& key);
            void setUDN(UUID udn);

            virtual UUID udn() const;
            virtual QString property(const QString& name) const;
            virtual QList<QString> propertyKeys() const;

        private:
            /**
             * Used by manager to save state at moment of registration.
             */
            void saveState();

            UUID savedUDN() const;
            QList<Service*> savedServices() const;
            QList<Device*> savedDevices() const;

            friend class Manager;
            friend class ManagerURLFactory;
            friend class DeviceDescription;
            friend class RegisteredDevice;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__DEVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
