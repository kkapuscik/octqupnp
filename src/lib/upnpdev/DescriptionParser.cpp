/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DescriptionParser.h"

#include <QtCore/QFile>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        Oct::Log::Logger<DescriptionParser> DescriptionParser::Logger("/oct/upnpdev/DescriptionParser");

        bool DescriptionParser::initService(Service& service, QString fileName)
        {
            QFile xmlFile(fileName);
            bool result;

            result = xmlFile.open(QFile::ReadOnly);

            if (result) {
                /* parse the XML */
                QDomDocument document;
                int errorLine, errorColumn;
                QString error;
                result = document.setContent(&xmlFile, true, &error, &errorLine, &errorColumn);

                if (result) {
                    result = initService(service, document);
                } else {
                    Logger.strace() << "Cannot parse document from file: " << fileName;
                }
            } else {
                Logger.strace() << "Cannot open file: " << fileName;
            }

            return result;
        }

        bool DescriptionParser::initService(Service& service, QByteArray buffer)
        {
            bool result = false;

            /* parse the XML */
            QDomDocument document;
            int errorLine, errorColumn;
            QString error;
            result = document.setContent(buffer, true, &error, &errorLine, &errorColumn);

            if (result) {
                result = initService(service, document);
            } else {
                Logger.strace() << "Cannot parse document from buffer.";
            }

            return result;
        }

        bool DescriptionParser::initService(Service& service, QDomDocument document)
        {
            bool result = false;

            if (!document.isNull()) {
                QDomElement documentElement = document.documentElement();
                if (!documentElement.isNull()) {
                    result = initService(service, documentElement);
                } else {
                    Logger.swarn() << "Document element is null.";
                }
            } else {
                Logger.swarn() << "Given document is null.";
            }

            return result;
        }

        bool DescriptionParser::initService(Service& service, QDomElement& documentElement)
        {
            bool result = false;

            do {
                QList<StateVariable> stateVarList;
                QList<Action> actionList;

                /* parse the XML */
                if (!Oct::Upnp::DescriptionParser::parseServiceXML(documentElement, stateVarList, actionList)) {
                    Logger.swarn() << "Cannot parse service XML.";
                    break;
                }

                /* verify and add state variables */
                bool stateVarsCorrect = true;
                for (int i = 0; stateVarsCorrect && i < stateVarList.size(); ++i) {
                    if (!service.hasStateVariable(stateVarList[i].name())) {
                        service.addStateVariable(stateVarList[i]);
                    } else {
                        Logger.strace() << "Duplicated state variable: " << stateVarList[i].name();
                        stateVarsCorrect = false;
                    }
                }
                if (!stateVarsCorrect) {
                    Logger.swarn() << "Incorrect state variables.";
                    break;
                }

                /* verify and add actions */
                bool actionsCorrect = true;
                for (int i = 0; actionsCorrect && i < actionList.size(); ++i) {
                    if (!service.hasAction(actionList[i].name())) {
                        QList<ActionArgument> arguments = actionList[i].arguments();
                        for (int j = 0; actionsCorrect && j < arguments.size(); ++j) {
                            if (!service.hasStateVariable(arguments[j].relatedStateVariable())) {
                                Logger.strace() << "Missing related state variable: " << arguments[j].relatedStateVariable();
                                actionsCorrect = false;
                            }
                        }

                        if (actionsCorrect) {
                            service.addAction(actionList[i]);
                        }
                    } else {
                        Logger.strace() << "Duplicated action: " << actionList[i].name();
                        actionsCorrect = false;
                    }
                }
                if (!actionsCorrect) {
                    Logger.swarn() << "Incorrect actions.";
                    break;
                }

                /* success */
                result = true;
            } while (0);

            return result;
        }

    }
}
