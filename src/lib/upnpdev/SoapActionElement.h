/*
 * $Id: SoapActionElement.h 126 2011-03-28 21:28:59Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__SOAP_ACTION_ELEMENT_H_
#define _OCT_UPNP_DEV__SOAP_ACTION_ELEMENT_H_

/*---------------------------------------------------------------------------*/

#include <soap/SoapBodyElement.h>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class SoapActionElement : public Oct::Soap::SoapBodyElement
        {
        public:
            SoapActionElement();
            SoapActionElement(Oct::Soap::SoapBodyElement& element);

            bool isValid();

            QString soapAction() const;
            QString serviceType() const;;
            QString actionName() const;;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__SOAP_ACTION_ELEMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
