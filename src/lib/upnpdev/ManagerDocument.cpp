/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ManagerDocument.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        ManagerDocument::ManagerDocument()
        {
            // do nothing
        }

        ManagerDocument::~ManagerDocument()
        {
            // do nothing
        }
    }
}
