/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__SERVICE_DESCRIPTION_H_
#define _OCT_UPNP_DEV__SERVICE_DESCRIPTION_H_

/*---------------------------------------------------------------------------*/

#include "Service.h"
#include "ManagerDocument.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class ServiceDescription : public ManagerDocument
        {
        private:
            static const QString DOCUMENT_HEADER;
            static const QString DOCUMENT_FOOTER;

        private:
            QByteArray m_buffer;

        public:
            ServiceDescription(const Service* service);
            virtual ~ServiceDescription();

            virtual QByteArray buffer() const;
            virtual QString contentType() const;

        private:
            void appendActions(const Service* service, QString& buffer);
            void appendStateVariables(const Service* service, QString& buffer);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__SERVICE_DESCRIPTION_H_ */

/**
 * @}
 * @}
 * @}
 */
