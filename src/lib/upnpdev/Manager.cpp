/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Manager.h"

#include "RegisteredDevice.h"
#include "ManagerVfs.h"

#include <httpserver/VfsRequestHandlerProvider.h>

/*---------------------------------------------------------------------------*/

#if 0
friendlyName
Required. Short description for end user. Should be localized (cf. ACCEPT-LANGUAGE and CONTENT-
        LANGUAGE headers). Specified by UPnP vendor. String. Should be < 64 characters.

manufacturer
Required. Manufacturers name. May be localized (cf. ACCEPT-LANGUAGE and CONTENT-LANGUAGE headers).
Specified by UPnP vendor. String. Should be < 64 characters.

manufacturerURL
Optional. Web site for Manufacturer. May be localized (cf. ACCEPT-LANGUAGE and CONTENT-LANGUAGE
        headers). May be relative to base URL. Specified by UPnP vendor. Single URL.

modelDescription
Recommended. Long description for end user. Should be localized (cf. ACCEPT-LANGUAGE and CONTENT-
        LANGUAGE headers). Specified by UPnP vendor. String. Should be < 128 characters.

modelName
Required. Model name. May be localized (cf. ACCEPT-LANGUAGE and CONTENT-LANGUAGE headers).
Specified by UPnP vendor. String. Should be < 32 characters.

modelNumber
Recommended. Model number. May be localized (cf. ACCEPT-LANGUAGE and CONTENT-LANGUAGE headers).
Specified by UPnP vendor. String. Should be < 32 characters.

modelURL
Optional. Web site for model. May be localized (cf. ACCEPT-LANGUAGE and CONTENT-LANGUAGE headers).
May be relative to base URL. Specified by UPnP vendor. Single URL.

serialNumber
Recommended. Serial number. May be localized (cf. ACCEPT-LANGUAGE and CONTENT-LANGUAGE headers).
Specified by UPnP vendor. String. Should be < 64 characters.

UDN
Required. Unique Device Name. Universally-unique identifier for the device, whether root or embedded. Must
be the same over time for a specific device instance (i.e., must survive reboots). Must match the value of the
27
#endif

#include <QtCore/QCoreApplication>

namespace Oct
{
    namespace UpnpDev
    {
        Oct::Log::Logger<Manager> Manager::Logger("/oct/upnpdev/Manager");

        Manager::Manager(Oct::HttpServer::Server* server, QObject* parent) :
            QObject(parent)
        {
            QCoreApplication* app = QCoreApplication::instance();
            if (app != NULL) {
                connect(app, SIGNAL( aboutToQuit() ), this, SLOT( onAppQuit() ));
            }

            /* create required stuff */
            if (server != NULL) {
                m_httpServer = server;
            } else {
                m_httpServer = new Oct::HttpServer::Server(this);
            }
            m_discoverySocket = new Oct::UpnpSsdp::DiscoverySocket(this);
            m_messageQueueTimer = new QTimer(this);

            m_documentVfs = new ManagerVfs(this);
            m_soapProvider = new ManagerSoapProvider(this, this);

            m_httpServer->requestHandlerManager().registerHandlerProvider(m_urlFactory.filesBaseURL(),
                    new Oct::HttpServer::VfsRequestHandlerProvider(m_documentVfs, this));

            m_httpServer->requestHandlerManager().registerHandlerProvider(m_urlFactory.controlBaseURL(), m_soapProvider);

            /* connect signals */
            connect(
                    m_discoverySocket,
                    SIGNAL( searchMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchMessage&) ),
                    this,
                    SLOT( processSearchRequest(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchMessage&) ));

            connect(m_messageQueueTimer, SIGNAL( timeout() ), this, SLOT( messageQueueTimeout() ));

            /* setup elements */
            m_messageQueueTimer->setSingleShot(true);

            /* start elements */
            // TODO: separate this. What about server start?
            m_httpServer->start();
            m_discoverySocket->joinSsdpGroup();
        }

        Manager::~Manager()
        {
            // nothing to do
        }

        void Manager::onAppQuit()
        {
            // TODO: This is risky (deadlocks, uses event queue etc.). Should be checked.
            while (!m_registeredDevices.isEmpty()) {
                Oct::UpnpDev::Device* device = m_registeredDevices.begin().key();
                m_registeredDevices.take(device);
                unregisterDevice(device);
            }

            while (hasMoreMessages()) {
                sendQueueMessage();
            }
        }

        bool Manager::isRegistered(Device* device) const
        {
            return m_registeredDevices.contains(device);
        }

        void Manager::saveDeviceState(Device* device)
        {
            device->saveState();

            QList<Device*> childDevices = device->savedDevices();
            for (int i = 0; i < childDevices.size(); ++i) {
                saveDeviceState(childDevices[i]);
            }
        }

        bool Manager::registerDevice(Device* device)
        {
            /* check if it is not already registered */
            if (m_registeredDevices.contains(device)) {
                return false;
            }

            /* save device state */
            saveDeviceState(device);

            /* created new device */
            RegisteredDevice* regDevice = new RegisteredDevice(device, m_urlFactory, this);

            /* connect signals */
            connect(regDevice, SIGNAL( discoveryTimeout() ), this, SLOT( sendDeviceDiscovery() ));

            /* put into registered devices collection */
            m_registeredDevices.insert(device, regDevice);

            /* register documents */
            registerDocuments(regDevice);

            /* starts discovery */
            regDevice->start();

            /* send byebye messages (to inform about device restart) */
            sendMessages(false, regDevice);

            /* send initial alive messages */
            sendMessages(true, regDevice);

            return true;
        }

        void Manager::registerDocuments(RegisteredDevice* regDevice)
        {
            m_documentVfs->registerDocument(m_urlFactory.deviceDescriptionURL(regDevice, true),
                    regDevice->deviceDescription());

            QList<Service*> services = regDevice->allServices();
            for (int i = 0; i < services.size(); ++i) {
                m_documentVfs->registerDocument(m_urlFactory.serviceDescriptionURL(services[i], true),
                        regDevice->serviceDescription(services[i]));
            }
        }

        void Manager::unregisterDocuments(RegisteredDevice* regDevice)
        {
            m_documentVfs->unregisterDocument(m_urlFactory.deviceDescriptionURL(regDevice, true));

            QList<Service*> services = regDevice->allServices();
            for (int i = 0; i < services.size(); ++i) {
                m_documentVfs->unregisterDocument(m_urlFactory.serviceDescriptionURL(services[i], true));
            }
        }

        bool Manager::unregisterDevice(Device* device)
        {
            /* check if it is registered */
            if (!m_registeredDevices.contains(device)) {
                return false;
            }

            /* take (remove) from collection */
            RegisteredDevice* regDevice = m_registeredDevices.take(device);

            unregisterDocuments(regDevice);

            /* send byebye messages */
            sendMessages(false, regDevice);

            /* destroy registered device */
            delete regDevice;

            return true;
        }

        void Manager::processSearchAll(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::SearchMessage& message)
        {
            RegDeviceMap::const_iterator regDevIter = m_registeredDevices.begin();
            RegDeviceMap::const_iterator regDevEnd = m_registeredDevices.end();

            for (; regDevIter != regDevEnd; ++regDevIter) {
                const RegisteredDevice* regDevice = regDevIter.value();

                const Device* rootDevice = regDevice->rootDevice();
                QList<Device*> devices = regDevice->allDevices();
                QList<Service*> services = regDevice->allServices();

                for (int r = 0; r < UDP_MESSAGES_REPEAT; ++r) {
                    addMessage(createSearchMessage(true, regDevice, rootDevice, "ssdp:all", "upnp:rootdevice"), sender, senderPort);

                    for (int i = 0; i < devices.size(); ++i) {
                        Device* device = devices[i];

                        addMessage(createSearchMessage(true, regDevice, device, "ssdp:all", QString::null), sender, senderPort);
                        addMessage(createSearchMessage(true, regDevice, device, "ssdp:all", device->deviceType()), sender, senderPort);
                    }
                    for (int i = 0; i < services.size(); ++i) {
                        Service* service = services[i];
                        Device* device = regDevice->serviceDevice(service);

                        addMessage(createSearchMessage(true, regDevice, device, "ssdp:all", service->serviceType()), sender, senderPort);
                    }
                }
            }
        }

        void Manager::processSearchRootDevices(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::SearchMessage& message)
        {
            RegDeviceMap::const_iterator regDevIter = m_registeredDevices.begin();
            RegDeviceMap::const_iterator regDevEnd = m_registeredDevices.end();

            for (; regDevIter != regDevEnd; ++regDevIter) {
                const RegisteredDevice* regDevice = regDevIter.value();
                Device* rootDevice = regDevice->rootDevice();

                addMessage(createSearchMessage(false, regDevice, rootDevice, "upnp:rootdevice", "upnp:rootdevice"),
                        sender, senderPort);
            }
        }

        void Manager::processSearchDeviceByUUID(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::SearchMessage& message, const QString& deviceUUID)
        {
            UUID udn(deviceUUID);

            if (! udn.isValid()) {
                Logger.trace(this) << "UUID is not valid: " << deviceUUID;
                return;
            }

            RegDeviceMap::const_iterator regDevIter = m_registeredDevices.begin();
            RegDeviceMap::const_iterator regDevEnd = m_registeredDevices.end();

            for (; regDevIter != regDevEnd; ++regDevIter) {
                const RegisteredDevice* regDevice = regDevIter.value();

                QList<Device*> devices = regDevice->allDevices();
                for (int i = 0; i < devices.size(); ++i) {
                    Device* device = devices[i];

                    if (device->savedUDN() == udn) {
                        addMessage(createSearchMessage(false, regDevice, device, deviceUUID, QString::null),
                                sender, senderPort);

                        /* UDN is unique, we could abort enumeration */
                        return;
                    }
                }
            }
        }

        void Manager::processSearchDeviceByType(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::SearchMessage& message, const QString& deviceType)
        {
            RegDeviceMap::const_iterator regDevIter = m_registeredDevices.begin();
            RegDeviceMap::const_iterator regDevEnd = m_registeredDevices.end();

            for (; regDevIter != regDevEnd; ++regDevIter) {
                const RegisteredDevice* regDevice = regDevIter.value();

                QList<Device*> devices = regDevice->allDevices();
                for (int i = 0; i < devices.size(); ++i) {
                    Device* device = devices[i];

                    if (device->isCompatible(deviceType)) {
                        addMessage(createSearchMessage(false, regDevice, device, deviceType, device->deviceType()),
                                sender, senderPort);
                    }
                }
            }
        }

        void Manager::processSearchServiceByType(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::SearchMessage& message, const QString& serviceType)
        {
            RegDeviceMap::const_iterator regDevIter = m_registeredDevices.begin();
            RegDeviceMap::const_iterator regDevEnd = m_registeredDevices.end();

            for (; regDevIter != regDevEnd; ++regDevIter) {
                const RegisteredDevice* regDevice = regDevIter.value();

                QList<Service*> services = regDevice->allServices();
                for (int i = 0; i < services.size(); ++i) {
                    Service* service = services[i];

                    if (service->isCompatible(serviceType)) {
                        Device* device = regDevice->serviceDevice(service);

                        addMessage(createSearchMessage(false, regDevice, device, serviceType, service->serviceType()),
                                sender, senderPort);
                    }
                }
            }
        }

        void Manager::processSearchRequest(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::SearchMessage& message)
        {
            QString searchTarget = message.searchTarget();

            Logger.trace(this) << "Search request: " << searchTarget;

            QList<Oct::HttpCore::Header> headers = message.headers().allHeaders();
            for (int i = 0; i < headers.size(); ++i) {
                Logger.trace(this) << "- Header: " << headers[i].name() << " = " << headers[i].value();
            }

            // TODO: put search request into timer. re-trigger it later.

            if (searchTarget == "ssdp:all") {
                /*
                 * ssdp:all
                 * Search for all devices and services.
                 */
                processSearchAll(sender, senderPort, message);
            } else if (searchTarget == "upnp:rootdevice") {
                /*
                 * upnp:rootdevice
                 * Search for root devices only.
                 */
                processSearchRootDevices(sender, senderPort, message);
            } else if (searchTarget.startsWith("uuid:")) {
                /*
                 * uuid:device-UUID
                 * Search for a particular device. Device UUID specified by UPnP vendor.
                 */
                processSearchDeviceByUUID(sender, senderPort, message, searchTarget);
            } else if (searchTarget.contains(":device:")) {
                /*
                 * urn:schemas-upnp-org:device:deviceType:v
                 * Search for any device of this type. Device type and version defined by UPnP Forum working committee.
                 *
                 * urn:domain-name:device:deviceType:v
                 * Search for any device of this type. Domain name, device type and version defined by UPnP vendor.
                 * Period characters in the domain name must be replaced with hyphens in accordance with RFC 2141.
                 */
                processSearchDeviceByType(sender, senderPort, message, searchTarget);
            } else if (searchTarget.contains(":service:")) {
                /*
                 * urn:schemas-upnp-org:service:serviceType:v
                 * Search for any service of this type. Service type and version defined by UPnP Forum working committee.
                 *
                 * urn:domain-name:service:serviceType:v
                 * Search for any service of this type. Domain name, service type and version defined by UPnP vendor.
                 * Period characters in the domain name must be replaced with hyphens in accordance with RFC 2141.
                 */
                processSearchServiceByType(sender, senderPort, message, searchTarget);
            }

#if 0
            // TODO: workaround. Remove later
            RegDeviceMap::iterator iter = m_registeredDevices.begin();
            for (; iter != m_registeredDevices.end(); ++iter) {
                sendMessages(true, iter.value());
            }
#endif
        }

        QString Manager::createDeviceLocationURL(const RegisteredDevice* regDevice)
        {
            return m_urlFactory.deviceLocationURL(m_httpServer->urlBase(), regDevice);
        }

        /*
         * === Alive message ===
         * NOTIFY * HTTP/1.1
         * HOST: 239.255.255.250:1900
         * NT: search target
         * NTS: ssdp:alive
         * USN: advertisement UUID
         * CACHE-CONTROL: max-age = seconds until advertisement expires
         * LOCATION: URL for UPnP description for root device
         * SERVER: OS/version UPnP/1.0 product/version
         *
         * === ByeBye message ===
         * NOTIFY * HTTP/1.1
         * HOST: 239.255.255.250:1900
         * NT: search target
         * NTS: ssdp:byebye
         * USN: uuid:advertisement UUID
         */
        Oct::HttpUdp::Message* Manager::createMessage(bool alive, const RegisteredDevice* regDevice, const Device* device,
                const QString& nt)
        {
            Oct::HttpUdp::Message* ssdpMessage;
            ssdpMessage = new Oct::HttpUdp::Message("NOTIFY", "*", Oct::HttpCore::HTTP_VERSION_1_1);
            ssdpMessage->headers().set("HOST", "239.255.255.250:1900");
            if (alive) {
                ssdpMessage->headers().set("NTS", "ssdp:alive");
                ssdpMessage->headers().set("CACHE-CONTROL", QString("max-age=%1").arg(regDevice->cacheControl()));
                ssdpMessage->headers().set("LOCATION", createDeviceLocationURL(regDevice));
                ssdpMessage->headers().set("SERVER", "linux/1.0 UPnP/1.0 OctUpnpDev/1.0"); // TODO platform, version
            } else {
                ssdpMessage->headers().set("NTS", "ssdp:byebye");
            }

            if (nt.isNull()) {
                ssdpMessage->headers().set("NT", device->savedUDN().toString());
                ssdpMessage->headers().set("USN", device->savedUDN().toString());
            } else {
                ssdpMessage->headers().set("NT", nt);
                ssdpMessage->headers().set("USN", device->savedUDN().toString() + "::" + nt);
            }

            return ssdpMessage;
        }

        /*
         * === Search response message ==
         * HTTP/1.1 200 OK
         * ST: search target
         * USN: advertisement UUID
         * CACHE-CONTROL: max-age = seconds until advertisement expires
         * LOCATION: URL for UPnP description for root device
         * SERVER: OS/version UPnP/1.0 product/version
         * DATE: when response was generated
         * EXT:
         */
        Oct::HttpUdp::Message* Manager::createSearchMessage(bool searchAll, const RegisteredDevice* regDevice, const Device* device,
                const QString& st, const QString& nt)
        {
            Oct::HttpUdp::Message* ssdpMessage;
            ssdpMessage = new Oct::HttpUdp::Message(Oct::HttpCore::HTTP_VERSION_1_1, 200, "OK"); // TODO: use consts
            ssdpMessage->headers().set("CACHE-CONTROL", QString("max-age=%1").arg(regDevice->cacheControl()));
            ssdpMessage->headers().set("LOCATION", createDeviceLocationURL(regDevice));
            ssdpMessage->headers().set("SERVER", "linux/1.0 UPnP/1.0 OctUpnpDev/1.0"); // TODO platform, version
            // TODO: DATE header
            ssdpMessage->headers().set("EXT", "");

            if (nt.isNull()) {
                if (searchAll) {
                    /* Value for ST header must be the same as for the NT header in NOTIFY messages with ssdp:alive. */
                    ssdpMessage->headers().set("ST", device->savedUDN().toString());
                } else {
                    /* Same as in request */
                    ssdpMessage->headers().set("ST", st);
                }
                ssdpMessage->headers().set("USN", device->savedUDN().toString());
            } else {
                if (searchAll) {
                    /* Value for ST header must be the same as for the NT header in NOTIFY messages with ssdp:alive. */
                    ssdpMessage->headers().set("ST", nt);
                } else {
                    /* Same as in request */
                    ssdpMessage->headers().set("ST", st);
                }
                ssdpMessage->headers().set("USN", device->savedUDN().toString() + "::" + nt);
            }

            return ssdpMessage;
        }

        void Manager::sendRootDeviceMessages(bool alive, RegisteredDevice* regDevice, const Device* device)
        {
            addMessage(createMessage(alive, regDevice, device, "upnp:rootdevice"));
        }

        void Manager::sendDeviceMessages(bool alive, RegisteredDevice* regDevice, const Device* device)
        {
            addMessage(createMessage(alive, regDevice, device, QString::null));
            addMessage(createMessage(alive, regDevice, device, device->deviceType()));
        }

        void Manager::sendServiceMessages(bool alive, RegisteredDevice* regDevice, const Device* device,
                const Service* service)
        {
            addMessage(createMessage(alive, regDevice, device, service->serviceType()));
        }

        void Manager::sendMessages(bool alive, RegisteredDevice* regDevice)
        {
            Logger.trace(this) << "Send messages: device=" << regDevice << " alive=" << alive;

            const Device* rootDevice = regDevice->rootDevice();
            QList<Device*> devices = regDevice->allDevices();
            QList<Service*> services = regDevice->allServices();

            for (int r = 0; r < UDP_MESSAGES_REPEAT; ++r) {
                sendRootDeviceMessages(alive, regDevice, rootDevice);
                for (int i = 0; i < devices.size(); ++i) {
                    sendDeviceMessages(alive, regDevice, devices.at(i));
                }
                for (int i = 0; i < services.size(); ++i) {
                    sendServiceMessages(alive, regDevice, regDevice->serviceDevice(services.at(i)), services.at(i));
                }
            }
        }

        void Manager::sendDeviceDiscovery()
        {
            RegisteredDevice* regDevice = qobject_cast<RegisteredDevice*> (sender());

            sendMessages(true, regDevice);
        }

        void Manager::addMessage(Oct::HttpUdp::Message* message)
        {
            Q_ASSERT(message != NULL);

            /* first packet will be added - start timer */
            if (m_messageQueue.isEmpty()) {
                m_messageQueueTimer->start(UDP_MESSAGES_DELAY);
            }

            m_messageQueue.enqueue(new MessageEntry(message));
        }

        void Manager::addMessage(Oct::HttpUdp::Message* message, const QHostAddress& sender, quint16 senderPort)
        {
            Q_ASSERT(message != NULL);

            /* first packet will be added - start timer */
            if (m_messageQueue.isEmpty()) {
                m_messageQueueTimer->start(UDP_MESSAGES_DELAY);
            }

            m_messageQueue.enqueue(new MessageEntry(message, sender, senderPort));
        }

        Manager::MessageEntry* Manager::getMessage()
        {
            return m_messageQueue.dequeue();
        }

        bool Manager::hasMoreMessages()
        {
            return !m_messageQueue.isEmpty();
        }

        void Manager::sendQueueMessage()
        {
            MessageEntry* messageEntry = getMessage();

            if (messageEntry != NULL) {
                if (messageEntry->isSearchResponse()) {
                    m_discoverySocket->writeMessage(
                            messageEntry->message(), messageEntry->targetHost(), messageEntry->targetPort());
                } else {
                    m_discoverySocket->sendMessage(messageEntry->message());
                }
                delete messageEntry;
            }
        }

        void Manager::messageQueueTimeout()
        {
            sendQueueMessage();

            /* some packets are still there - restart timer */
            if (hasMoreMessages()) {
                m_messageQueueTimer->start(UDP_MESSAGES_DELAY);
            }
        }

        Service* Manager::serviceForControlURL(const QString& controlPathRemainder)
        {
            Service* service = NULL;

            // TODO refactoring needed

            UUID serviceUUID = m_urlFactory.serviceUUID(controlPathRemainder);
            if (serviceUUID.isValid()) {
                RegDeviceMap::const_iterator regDevIter;

                for (regDevIter = m_registeredDevices.begin(); regDevIter != m_registeredDevices.end(); ++regDevIter) {

                    QList<Service*> services = regDevIter.value()->allServices();
                    for (int i = 0; i < services.size(); ++i) {
                        Logger.trace(this) << "Service: uuid=" << services[i]->uuid().toString() << " lookup="
                                << serviceUUID.toString();

                        if (services[i]->uuid() == serviceUUID) {
                            service = services[i];
                            Logger.trace(this) << "Service found: " << service;
                            break;
                        }
                    }
                }
            } else {
                Logger.trace(this) << "Invalid UUID in path: " << controlPathRemainder;
            }

            return service;
        }

    }
}
