/*
 * $Id: SoapActionElement.cpp 83 2011-02-21 21:47:56Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapActionElement.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        SoapActionElement::SoapActionElement() :
            Oct::Soap::SoapBodyElement()
        {
            // do nothing
        }

        SoapActionElement::SoapActionElement(SoapBodyElement& element) :
            Oct::Soap::SoapBodyElement(element)
        {

        }

        bool SoapActionElement::isValid()
        {
            // TODO: more tests?

            return !serviceType().isEmpty() && !actionName().isEmpty();
        }

        QString SoapActionElement::serviceType() const
        {
            return namespaceURI();
        }

        QString SoapActionElement::actionName() const
        {
            return localName();
        }

        QString SoapActionElement::soapAction() const
        {
            return serviceType() + '#' + actionName();
        }
    }
}

/*---------------------------------------------------------------------------*/
