/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Device.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        Device::Device(QObject* parent) :
            QObject(parent), Oct::Upnp::Device()
        {
            m_udn = UUID::generate();
        }

        Device::~Device()
        {
            // nothing do to
        }

        void Device::setProperty(const QString& key, const QString& value)
        {
            QString tmpKey = key.trimmed();
            QString tmpValue = value.trimmed();

            Q_ASSERT(tmpKey.length() > 0 && tmpValue.length() > 0);

            m_properties[tmpKey] = tmpValue;
        }

        void Device::removeProperty(const QString& key)
        {
            QString tmpKey = key.trimmed();

            Q_ASSERT(tmpKey.length() > 0);

            m_properties.remove(tmpKey);
        }

        void Device::setUDN(UUID udn)
        {
            m_udn = udn;
        }

        UUID Device::udn() const
        {
            return m_udn;
        }

        QString Device::property(const QString& name) const
        {
            if (m_properties.contains(name)) {
                return m_properties[name];
            } else {
                return QString::null;
            }
        }

        QList<QString> Device::propertyKeys() const
        {
            return m_properties.keys();
        }

        bool Device::hasService(Service* service)
        {
            return m_services.contains(service);
        }

        void Device::addService(Service* service)
        {
            Q_ASSERT(!hasService(service));

            m_services.append(service);
        }

        void Device::removeService(Service* service)
        {
            Q_ASSERT(hasService(service));

            m_services.removeOne(service);
        }

        QList<Service*> Device::services() const
        {
            return m_services;
        }

        bool Device::hasDevice(Device* device)
        {
            return m_devices.contains(device);
        }

        void Device::addDevice(Device* device)
        {
            Q_ASSERT(!hasDevice(device));

            m_devices.append(device);
        }

        void Device::removeDevice(Device* device)
        {
            Q_ASSERT(hasDevice(device));

            m_devices.removeOne(device);
        }

        QList<Device*> Device::devices() const
        {
            return m_devices;
        }

        void Device::saveState()
        {
            m_savedUDN = m_udn;
            m_savedServices = m_services;
            m_savedDevices = m_devices;
        }

        UUID Device::savedUDN() const
        {
            return m_savedUDN;
        }

        QList<Service*> Device::savedServices() const
        {
            return m_savedServices;
        }

        QList<Device*> Device::savedDevices() const
        {
            return m_savedDevices;
        }

    }
}
