/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Service.h"

#include "DescriptionParser.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        Service::Service(QObject* parent) :
            QObject(parent), Oct::Upnp::Service()
        {
            /* the UUID will uniquely identify the service */
            /* it may be used for example to generate URIs */
            m_uuid = UUID::generate();
        }

        Service::~Service()
        {
            // do nothing
        }

        void Service::setServiceType(const QString& serviceType)
        {
            m_serviceType = serviceType;
        }

        void Service::setServiceId(const QString& serviceId)
        {
            m_serviceId = serviceId;
        }

        void Service::addStateVariable(const StateVariable& variable)
        {
            Q_ASSERT(!hasStateVariable(variable.name()));

            m_stateVariables.append(variable);
        }

        void Service::removeStateVariable(const QString& name)
        {
            Q_ASSERT(hasStateVariable(name));

            for (int i = 0; i < m_stateVariables.size(); ++i) {
                if (m_stateVariables.at(i).name() == name) {
                    m_stateVariables.removeAt(i);
                    break;
                }
            }
        }

        void Service::addAction(const Action& action)
        {
            Q_ASSERT(!hasAction(action.name()));

            m_actions.append(action);
        }

        void Service::removeAction(const QString& name)
        {
            Q_ASSERT(hasAction(name));

            for (int i = 0; i < m_actions.size(); ++i) {
                if (m_actions.at(i).name() == name) {
                    m_actions.removeAt(i);
                    break;
                }
            }
        }

        const QString& Service::serviceType() const
        {
            return m_serviceType;
        }

        const QString& Service::serviceId() const
        {
            return m_serviceId;
        }

        bool Service::hasStateVariable(const QString& name) const
        {
            for (int i = 0; i < m_stateVariables.size(); ++i) {
                if (m_stateVariables.at(i).name() == name) {
                    return true;
                }
            }

            return false;
        }

        QList<StateVariable> Service::stateVariables() const
        {
            return m_stateVariables;
        }

        StateVariable Service::stateVariable(const QString& name) const
        {
            for (int i = 0; i < m_stateVariables.size(); ++i) {
                if (m_stateVariables.at(i).name() == name) {
                    return m_stateVariables.at(i);
                }
            }

            Q_ASSERT(0);

            // TODO: what should be returned?
        }

        bool Service::hasAction(const QString& name) const
        {
            for (int i = 0; i < m_actions.size(); ++i) {
                if (m_actions.at(i).name() == name) {
                    return true;
                }
            }

            return false;
        }

        QList<Action> Service::actions() const
        {
            return m_actions;
        }

        Action Service::action(const QString& name) const
        {
            for (int i = 0; i < m_actions.size(); ++i) {
                if (m_actions.at(i).name() == name) {
                    return m_actions.at(i);
                }
            }

            Q_ASSERT(0);

            // TODO: what should be returned?
        }

        bool Service::addFromSCPD(QString fileName)
        {
            return DescriptionParser::initService(*this, fileName);
        }

        bool Service::addFromSCPD(QByteArray buffer)
        {
            return DescriptionParser::initService(*this, buffer);
        }

        bool Service::addFromSCPD(QDomDocument document)
        {
            return DescriptionParser::initService(*this, document);
        }

    }
}
