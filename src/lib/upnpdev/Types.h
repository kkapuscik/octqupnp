/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP_DEV__TYPES_H_
#define _OCT_UPNP_DEV__TYPES_H_

/*---------------------------------------------------------------------------*/

#include <upnp/Action.h>
#include <upnp/ActionArgument.h>
#include <upnp/StateVariable.h>
#include <upnp/USN.h>
#include <upnp/UUID.h>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        typedef Oct::Upnp::Action Action;
        typedef Oct::Upnp::ActionArgument ActionArgument;
        typedef Oct::Upnp::StateVariable StateVariable;
        typedef Oct::Upnp::USN USN;
        typedef Oct::Upnp::UUID UUID;

    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__TYPES_H_ */

/**
 * @}
 * @}
 * @}
 */
