/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DeviceDescription.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        const QString DeviceDescription::DOCUMENT_HEADER("<?xml version=\"1.0\"?>\n"
            "<root xmlns=\"urn:schemas-upnp-org:device-1-0\">\n"
            "<specVersion>\n"
            "<major>1</major>\n"
            "<minor>0</minor>\n"
            "</specVersion>\n");

        const QString DeviceDescription::DOCUMENT_FOOTER("</root>\n");

        DeviceDescription::DeviceDescription(const Device* device, const ManagerURLFactory& urlFactory) :
            ManagerDocument()
        {
            QString buffer;

            buffer.append(DOCUMENT_HEADER);
            appendDevice(device, urlFactory, buffer);
            buffer.append(DOCUMENT_FOOTER);

            m_buffer = buffer.toUtf8();
#if 0
            <?xml version="1.0"?>
            <root xmlns="urn:schemas-upnp-org:device-1-0">
            <specVersion>
            <major>1</major>
            <minor>0</minor>
            </specVersion>
            <URLBase>base URL for all relative URLs</URLBase>

            <device>
            <deviceType>urn:schemas-upnp-org:device:deviceType:v</deviceType>
            <friendlyName>short user-friendly title</friendlyName>
            <manufacturer>manufacturer name</manufacturer>
            <manufacturerURL>URL to manufacturer site</manufacturerURL>
            <modelDescription>long user-friendly title</modelDescription>
            <modelName>model name</modelName>
            <modelNumber>model number</modelNumber>
            <modelURL>URL to model site</modelURL>
            <serialNumber>manufacturers serial number</serialNumber>
            <UDN>uuid:UUID</UDN>
            <UPC>Universal Product Code</UPC>
            <iconList>
            <icon>
            <mimetype>image/format</mimetype>
            <width>horizontal pixels</width>
            <height>vertical pixels</height>
            <depth>color depth</depth>
            <url>URL to icon</url>
            </icon>
            XML to declare other icons, if any, go here
            </iconList>
            <serviceList>
            <service>
            <serviceType>urn:schemas-upnp-org:service:serviceType:v</serviceType>
            <serviceId>urn:upnp-org:serviceId:serviceID</serviceId>

#endif
        }

        DeviceDescription::~DeviceDescription()
        {
            // do nothing
        }

        void DeviceDescription::appendDevice(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer)
        {
            buffer.append("<device>\n");

            appendProperties(device, urlFactory, buffer);
            appendIconList(device, urlFactory, buffer);
            appendServiceList(device, urlFactory, buffer);
            appendDeviceList(device, urlFactory, buffer);

            buffer.append("</device>\n");
        }

        void DeviceDescription::appendProperties(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer)
        {
            QList<QString> propertyKeys = device->propertyKeys();
            for (int i = 0; i < propertyKeys.size(); ++i) {
                QString key = propertyKeys[i];
                QString value = device->property(key);

                buffer.append('<');
                buffer.append(key);
                buffer.append('>');

                buffer.append(value);

                buffer.append("</");
                buffer.append(key);
                buffer.append(">\n");
            }

            buffer.append("<UDN>");
            buffer.append(device->savedUDN().toString(true));
            buffer.append("</UDN>\n");
        }

        void DeviceDescription::appendIconList(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer)
        {
            // TODO
        }

        void DeviceDescription::appendServiceList(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer)
        {
            QList<Service*> services = device->savedServices();
            if (services.size() == 0) {
                return;
            }

            buffer.append("<serviceList>\n");

            for (int i = 0; i < services.size(); ++i) {
                buffer.append("<service>\n");

                buffer.append("<serviceType>");
                buffer.append(services[i]->serviceType());
                buffer.append("</serviceType>\n");

                buffer.append("<serviceId>");
                buffer.append(services[i]->serviceId());
                buffer.append("</serviceId>\n");

                buffer.append("<SCPDURL>");
                buffer.append(urlFactory.serviceDescriptionURL(services[i]));
                buffer.append("</SCPDURL>\n");

                buffer.append("<controlURL>");
                buffer.append(urlFactory.serviceControlURL(services[i]));
                buffer.append("</controlURL>\n");

                if (services[i]->sendEvents()) {
                    buffer.append("<eventSubURL>");
                    buffer.append(urlFactory.serviceEventingURL(services[i]));
                    buffer.append("</eventSubURL>\n");
                }

                buffer.append("</service>\n");
            }

            buffer.append("</serviceList>\n");
        }

        void DeviceDescription::appendDeviceList(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer)
        {
            QList<Device*> devices = device->savedDevices();

            if (devices.size() == 0) {
                return;
            }

            buffer.append("<deviceList>\n");

            for (int i = 0; i < devices.size(); ++i) {
                appendDevice(device, urlFactory, buffer);
            }

            buffer.append("</deviceList>\n");
        }

        QByteArray DeviceDescription::buffer() const
        {
            return m_buffer;
        }

        QString DeviceDescription::contentType() const
        {
            return "text/xml";
        }

    }
}
