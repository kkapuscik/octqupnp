/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpDev
 * @{
 */

#ifndef _OCT_UPNP_DEV__DEVICE_DESCRIPTION_H_
#define _OCT_UPNP_DEV__DEVICE_DESCRIPTION_H_

/*---------------------------------------------------------------------------*/

#include "Device.h"
#include "ManagerDocument.h"
#include "ManagerURLFactory.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpDev
    {
        class DeviceDescription : public ManagerDocument
        {
        private:
            static const QString DOCUMENT_HEADER;
            static const QString DOCUMENT_FOOTER;

        private:
            QByteArray m_buffer;

        public:
            DeviceDescription(const Device* device, const ManagerURLFactory& urlFactory);
            virtual ~DeviceDescription();

            virtual QByteArray buffer() const;
            virtual QString contentType() const;

        private:
            void appendDevice(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer);
            void appendProperties(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer);
            void appendIconList(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer);
            void appendServiceList(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer);
            void appendDeviceList(const Device* device, const ManagerURLFactory& urlFactory, QString& buffer);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_DEV__DEVICE_DESCRIPTION_H_ */

/**
 * @}
 * @}
 * @}
 */
