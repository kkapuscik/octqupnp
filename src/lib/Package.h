/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs     Collection of OCTaedr libraries
 * @{
 */

#ifndef _OCT_LIB__PACKAGE_H_
#define _OCT_LIB__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    // other documentation goes here
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_LIB__PACKAGE_H_*/

/**
 * @}
 * @}
 */
