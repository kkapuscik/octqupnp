/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__ACTION_H_
#define _OCT_UPNP__ACTION_H_

/*---------------------------------------------------------------------------*/

#include "ActionArgument.h"

#include <QtCore/QList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class Action
        {
        private:
            typedef QList<ActionArgument> ArgumentList;

        private:
            QString m_name;
            ArgumentList m_arguments;

        public:
            Action(const QString& name, QList<ActionArgument> args = QList<ActionArgument>());

            const QString& name() const
            {
                return m_name;
            }

            bool hasArgument(const QString& name)
            {
                for (int i = 0; i < m_arguments.size(); ++i) {
                    if (m_arguments[i].name() == name) {
                        return true;
                    }
                }
                return false;
            }

            ActionArgument argument(const QString& name)
            {
                for (int i = 0; i < m_arguments.size(); ++i) {
                    if (m_arguments[i].name() == name) {
                        return m_arguments[i];
                    }
                }

                Q_ASSERT(0);

                // fake argument
                return ActionArgument(QString::null, ActionArgument::ACTION_ARGUMENT_TYPE_INVALID, QString::null);
            }

            QList<ActionArgument> arguments() const
            {
                return m_arguments;
            }
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__ACTION_H_ */

/**
 * @}
 * @}
 * @}
 */
