/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Action.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        Action::Action(const QString& name, QList<ActionArgument> args) :
            m_name(name), m_arguments(args)
        {
            // TODO Auto-generated constructor stub
        }
    }
}
