/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__XML_TOOLS_H_
#define _OCT_UPNP__XML_TOOLS_H_

/*---------------------------------------------------------------------------*/

#include <log/Logger.h>

#include <QtXml/QDomElement>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class XmlTools
        {
        private:
            static Oct::Log::Logger<XmlTools> Logger;

        protected:
            static const QString DEVICE_NAMESPACE;
            static const QString DEVICE_ELEMENT_ROOT;
            static const QString DEVICE_ELEMENT_SPEC_VERSION;
            static const QString DEVICE_ELEMENT_MAJOR;
            static const QString DEVICE_ELEMENT_MINOR;
            static const QString DEVICE_ELEMENT_DEVICE;
            static const QString DEVICE_ELEMENT_UDN;
            static const QString DEVICE_ELEMENT_DEVICE_LIST;
            static const QString DEVICE_ELEMENT_SERVICE_LIST;
            static const QString DEVICE_ELEMENT_ICON_LIST;
            static const QString DEVICE_ELEMENT_SERVICE;
            static const QString DEVICE_ELEMENT_ICON;
            static const QString DEVICE_ELEMENT_URL_BASE;
            static const QString DEVICE_ELEMENT_SERVICE_TYPE;
            static const QString DEVICE_ELEMENT_SERVICE_ID;
            static const QString DEVICE_ELEMENT_SCPDURL;
            static const QString DEVICE_ELEMENT_CONTROL_URL;
            static const QString DEVICE_ELEMENT_EVENT_SUB_URL;
            static const QString DEVICE_ELEMENT_MIME_TYPE;
            static const QString DEVICE_ELEMENT_WIDTH;
            static const QString DEVICE_ELEMENT_HEIGHT;
            static const QString DEVICE_ELEMENT_DEPTH;
            static const QString DEVICE_ELEMENT_URL;

            static const QString SERVICE_NAMESPACE;
            static const QString SERVICE_ELEMENT_SCPD;
            static const QString SERVICE_ELEMENT_SPEC_VERSION;
            static const QString SERVICE_ELEMENT_MAJOR;
            static const QString SERVICE_ELEMENT_MINOR;
            static const QString SERVICE_ELEMENT_SERVICE_STATE_TABLE;
            static const QString SERVICE_ELEMENT_STATE_VARIABLE;
            static const QString SERVICE_ELEMENT_ACTION_LIST;
            static const QString SERVICE_ELEMENT_ACTION;
            static const QString SERVICE_ELEMENT_NAME;
            static const QString SERVICE_ELEMENT_ARGUMENT_LIST;
            static const QString SERVICE_ELEMENT_ARGUMENT;
            static const QString SERVICE_ELEMENT_DIRECTION;
            static const QString SERVICE_ELEMENT_RETVAL;
            static const QString SERVICE_ELEMENT_RELATED_STATE_VARIABLE;
            static const QString SERVICE_ELEMENT_DATA_TYPE;
            static const QString SERVICE_ELEMENT_DEFAULT_VALUE;
            static const QString SERVICE_ELEMENT_ALLOWED_VALUE_LIST;
            static const QString SERVICE_ELEMENT_ALLOWED_VALUE;
            static const QString SERVICE_ELEMENT_ALLOWED_VALUE_RANGE;
            static const QString SERVICE_ELEMENT_MINIMUM;
            static const QString SERVICE_ELEMENT_MAXIMUM;
            static const QString SERVICE_ELEMENT_STEP;
            static const QString SERVICE_ATTRIBUTES_SEND_EVENTS;
            static const QString SERVICE_VALUE_YES;

        private:
            XmlTools()
            {
            }

        public:
            static QDomElement findChildElement(QDomElement element, const QString& namespaceURI,
                    const QString& localName);
            static QString extractValue(QDomElement element);

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__XML_TOOLS_H_ */

/**
 * @}
 * @}
 * @}
 */
