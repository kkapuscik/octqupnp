/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__ICON_H_
#define _OCT_UPNP__ICON_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class Icon
        {
        private:
            QString m_mimeType;
            int m_width;
            int m_height;
            int m_depth;

        protected:
            Icon();
            Icon(const QString& mimeType, int width, int height, int depth);

        public:
            virtual ~Icon();

            virtual bool isValid() const
            {
                return m_width > 0 && m_height > 0 && m_depth > 0;
            }

            const QString& mimeType() const
            {
                return m_mimeType;
            }

            int width() const
            {
                return m_width;
            }

            int height() const
            {
                return m_height;
            }

            int depth() const
            {
                return m_depth;
            }

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__ICON_H_ */

/**
 * @}
 * @}
 * @}
 */
