/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "UUID.h"

#include <QtCore/QUuid>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {

        Oct::Log::Logger<UUID> UUID::Logger("/oct/upnp/UUID");

        UUID::UUID()
        {
            // nothing to do
        }

        UUID::UUID(const QString& uuidString)
        {
#if 0
            Creates a QUuid object from the string text, which must be formatted as five hex fields
            separated by '-', e.g., "{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}" where 'x' is a hex digit.
            The curly braces shown here are optional, but it is normal to include them. If the conversion
            fails, a null UUID is created. See toString() for an explanation of how the five hex fields
            map to the public data members in QUuid.
            See also toString() and QUuid().
#endif

            // TODO: polish the solution
            QString tmpUuid = QString(uuidString).remove('-').toLower();
            if (tmpUuid.startsWith("uuid:")) {
                tmpUuid = tmpUuid.mid(5);
            } else {
                tmpUuid.clear();
            }

            for (int i = 0; !tmpUuid.isNull() && i < tmpUuid.length(); ++i) {
                switch (tmpUuid.at(i).toLatin1()) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                        // char OK
                        break;
                    default:
                        Logger.trace(this) << "Invalid UDN character: " << tmpUuid.at(i);
                        tmpUuid.clear();
                        break;
                }
            }

            if (!tmpUuid.isNull()) {
                tmpUuid = QString("uuid:%1-%2-%3-%4-%5"). arg(tmpUuid.mid(0, 8)). arg(tmpUuid.mid(8, 4)).arg(
                        tmpUuid.mid(12, 4)). arg(tmpUuid.mid(16, 4)).arg(tmpUuid.mid(20));
                m_uuidString = tmpUuid;
            } else {
                m_uuidString = uuidString;
            }
        }

        QString UUID::toString(bool withPrefix) const
        {
            if (withPrefix) {
                return m_uuidString;
            } else {
                return m_uuidString.mid(5); // skip uuid:
            }
        }

        bool UUID::operator !=(const UUID& other) const
        {
            return m_uuidString != other.m_uuidString;
        }

        bool UUID::operator <(const UUID& other) const
        {
            return m_uuidString < other.m_uuidString;
        }

        bool UUID::operator ==(const UUID& other) const
        {
            return m_uuidString == other.m_uuidString;
        }

        bool UUID::operator >(const UUID& other) const
        {
            return m_uuidString > other.m_uuidString;
        }

        UUID UUID::generate()
        {
            QString uidStr = QUuid::createUuid().toString();

            UUID uuid = UUID(QString("uuid:") + uidStr.remove('{').remove('}'));
            Q_ASSERT(uuid.isValid());
            return uuid;
        }

    }
}
