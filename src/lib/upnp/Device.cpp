/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Device.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        /* OctUpnpCpDevice consts */
        const QString Device::PROPERTY_DEVICE_TYPE("deviceType");
        const QString Device::PROPERTY_FRIENDLY_NAME("friendlyName");
        const QString Device::PROPERTY_MANUFACTURER("manufacturer");
        const QString Device::PROPERTY_MANUFACTURER_URL("manufacturerURL");
        const QString Device::PROPERTY_MODEL_DESCRIPTION("modelDescription");
        const QString Device::PROPERTY_MODEL_NAME("modelName");
        const QString Device::PROPERTY_MODEL_NUMBER("modelNumber");
        const QString Device::PROPERTY_MODEL_URL("modelURL");
        const QString Device::PROPERTY_SERIAL_NUMBER("serialNumber");
        const QString Device::PROPERTY_UPC("UPC");
    }
}
