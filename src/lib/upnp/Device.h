/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__DEVICE_H_
#define _OCT_UPNP__DEVICE_H_

/*---------------------------------------------------------------------------*/

#include "UUID.h"
#include "Icon.h"
//#include "Service.h"
//#include "Types.h"

#include <QtCore/QMap>
#include <QtXml/QDomElement>
#include <QtCore/QUrl>
#include <QtCore/QSharedPointer>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class Device
        {
        public:
            static const QString PROPERTY_DEVICE_TYPE;
            static const QString PROPERTY_FRIENDLY_NAME;
            static const QString PROPERTY_MANUFACTURER;
            static const QString PROPERTY_MANUFACTURER_URL;
            static const QString PROPERTY_MODEL_DESCRIPTION;
            static const QString PROPERTY_MODEL_NAME;
            static const QString PROPERTY_MODEL_NUMBER;
            static const QString PROPERTY_MODEL_URL;
            static const QString PROPERTY_SERIAL_NUMBER;
            static const QString PROPERTY_UPC;

        protected:
            Device()
            {
                // do nothing
            }

        public:
            virtual ~Device()
            {
                // do nothing
            }

            virtual UUID udn() const = 0;

            virtual QString property(const QString& name) const = 0;
            virtual QList<QString> propertyKeys() const = 0;

            bool isCompatible(const QString& otherType) const
            {
                QString thisType = deviceType();

                int thisIndex = thisType.lastIndexOf(':');
                int otherIndex = otherType.lastIndexOf(':');

                if (thisIndex > 0 && otherIndex > 0 && thisIndex == otherIndex) {
                    if (thisType.mid(0, thisIndex) == otherType.mid(0, otherIndex)) {
                        QString thisVersionStr = thisType.mid(thisIndex + 1);
                        QString otherVersionStr = otherType.mid(otherIndex + 1);

                        bool thisOk;
                        bool otherOk;

                        int thisVersion = thisVersionStr.toInt(&thisOk, 10);
                        int otherVersion = otherVersionStr.toInt(&otherOk, 10);
                        if (thisOk && otherOk) {
                            if (thisVersion >= otherVersion) {
                                return true;
                            }
                        }
                    }
                }

                return thisType == otherType;
            }

            QString deviceType() const
            {
                return property(PROPERTY_DEVICE_TYPE);
            }

            QString friendlyName() const
            {
                return property(PROPERTY_FRIENDLY_NAME);
            }

            //            virtual QList<Service> services() const = 0;
            //            virtual QList<Device> subdevices() const = 0;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__DEVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
