/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__UUID_H_
#define _OCT_UPNP__UUID_H_

/*---------------------------------------------------------------------------*/

#include <log/Logger.h>

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class UUID
        {
        private:
            static Oct::Log::Logger<UUID> Logger;

        private:
            QString m_uuidString;

        public:
            UUID();
            UUID(const QString& uuidString);

            bool isValid() const
            {
                return !m_uuidString.isNull();
            }

            QString toString(bool withPrefix = true) const;

            // operator QString() const;
            bool operator!=(const UUID& other) const;
            bool operator<(const UUID& other) const;
            bool operator==(const UUID& other) const;
            bool operator>(const UUID& other) const;

        public:
            static UUID generate();
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__UUID_H_ */

/**
 * @}
 * @}
 * @}
 */
