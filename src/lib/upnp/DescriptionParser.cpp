/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DescriptionParser.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        Oct::Log::Logger<DescriptionParser> DescriptionParser::Logger("/oct/upnp/DescriptionParser");

        bool DescriptionParser::parseServiceXML(QDomElement documentElement, QList<StateVariable>& stateVarList, QList<Action>& actionList)
        {
            bool result = false;

            do {
                /* check root element */
                if (documentElement.namespaceURI() != SERVICE_NAMESPACE || documentElement.localName()
                        != SERVICE_ELEMENT_SCPD) {

                    Logger.strace() << "Invalid root element: " << documentElement.namespaceURI() << documentElement.localName();
                    break;
                }

                /* find and check spec version */
                QDomElement specVersionElem = findChildElement(documentElement, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_SPEC_VERSION);
                if (specVersionElem.isNull()) {
                    Logger.strace() << "Missing spec version element";
                    break;
                }
                QDomElement versionMajorElem = findChildElement(specVersionElem, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_MAJOR);
                QDomElement versionMinorElem = findChildElement(specVersionElem, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_MINOR);
                if (versionMajorElem.isNull() || versionMinorElem.isNull()) {
                    Logger.strace() << "Missing spec version subelements";
                    break;
                }
                QString versionMajor = extractValue(versionMajorElem);
                QString versionMinor = extractValue(versionMajorElem);

                if (versionMajor != "1" /*|| versionMinor != "0"*/) {
                    Logger.strace() << "Invalid spec version: " << versionMajor << versionMinor;
                    break;
                }

                QDomElement tableElement = findChildElement(documentElement, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_SERVICE_STATE_TABLE);
                if (!tableElement.isNull()) {
                    if (!parseStateVarTable(tableElement, stateVarList)) {
                        Logger.strace() << "Parsing of service state table failed";
                        break;
                    }
                } else {
                    Logger.strace() << "State variables table is null";
                }

                QDomElement actionListElement = findChildElement(documentElement, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_ACTION_LIST);
                if (!actionListElement.isNull()) {
                    if (!parseActionList(actionListElement, actionList)) {
                        Logger.strace() << "Parsing of action list failed";
                        break;
                    }
                } else {
                    Logger.strace() << "Action list is null";
                }

                result = true;
            } while (0);

            return result;
        }

        bool DescriptionParser::parseStateVarTable(QDomElement tableElement, QList<StateVariable>& stateVarList)
        {
            /*
             <serviceStateTable>
             <stateVariable sendEvents="yes">
             <name>variableName</name>
             <dataType>variable data type</dataType>
             <defaultValue>default value</defaultValue>
             <allowedValueList>
             <allowedValue>enumerated value</allowedValue>
             Other allowed values defined by UPnP Forum working committee (if any) go here
             </allowedValueList>
             </stateVariable>
             <stateVariable sendEvents="yes">
             <name>variableName</name>
             <dataType>variable data type</dataType>
             <defaultValue>default value</defaultValue>
             <allowedValueRange>
             <minimum>minimum value</minimum>
             <maximum>maximum value</maximum>
             <step>increment value</step>
             </allowedValueRange>
             </stateVariable>
             Declarations for other state variables defined by UPnP Forum working committee
             (if any) go here
             Declarations for other state variables added by UPnP vendor (if any) go here
             </serviceStateTable>
             */

            bool result = true;
            for (QDomElement stateVarElement = tableElement.firstChildElement(); result && !stateVarElement.isNull(); stateVarElement
                    = stateVarElement.nextSiblingElement()) {

                /* check if it is an action */
                if (stateVarElement.namespaceURI() != SERVICE_NAMESPACE || stateVarElement.localName()
                        != SERVICE_ELEMENT_STATE_VARIABLE) {
                    continue;
                }

                QDomElement stateVarNameElement = findChildElement(stateVarElement, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_NAME);
                QDomElement dataTypeElement = findChildElement(stateVarElement, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_DATA_TYPE);
                QDomElement defaultValueElement = findChildElement(stateVarElement, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_DEFAULT_VALUE);
                QDomElement allowedValueListElement = findChildElement(stateVarElement,
                        SERVICE_NAMESPACE, SERVICE_ELEMENT_ALLOWED_VALUE_LIST);
                QDomElement allowedValueRangeElement = findChildElement(stateVarElement,
                        SERVICE_NAMESPACE, SERVICE_ELEMENT_ALLOWED_VALUE_RANGE);

                /* check required elements */
                if (stateVarNameElement.isNull()) {
                    Logger.strace() << "State variable name not available";
                    result = false;
                    break;
                }
                if (dataTypeElement.isNull()) {
                    Logger.strace() << "State variable data type not available";
                    result = false;
                    break;
                }
                if (!allowedValueListElement.isNull() && !allowedValueRangeElement.isNull()) {
                    Logger.strace() << "Allowed value list and range present for one state variable";
                    result = false;
                    break;
                }

                /* parse element values */
                QString stateVarName = extractValue(stateVarNameElement);
                if (stateVarName.isNull()) {
                    Logger.strace() << "State variable name is empty";
                    result = false;
                    break;
                }
                QString dataType = extractValue(dataTypeElement);
                if (dataType.isNull()) {
                    Logger.strace() << "Data type is empty";
                    result = false;
                    break;
                }

                // TODO: namespace?
                QString sendEventsStr = stateVarElement.attribute(SERVICE_ATTRIBUTES_SEND_EVENTS,
                        SERVICE_VALUE_YES);
                bool sendEvents = (sendEventsStr == SERVICE_VALUE_YES);

                QVariant defaultValue;
                if (!defaultValueElement.isNull()) {
                    QString defaultValueString = extractValue(defaultValueElement);
                    if (defaultValueString.isNull()) {
                        Logger.strace() << "Default value is empty";
                        result = false;
                        break;
                    } else {
                        defaultValue.setValue(defaultValueString);
                    }
                }

                QVariant minimumValue;
                QVariant maximumValue;
                QVariant stepValue;
                QStringList allowedValueList;

                /* process allowed value list */
                if (!allowedValueListElement.isNull()) {
                    for (QDomElement allowedValueElement = allowedValueListElement.firstChildElement(); result
                            && !allowedValueElement.isNull(); allowedValueElement
                            = allowedValueElement.nextSiblingElement()) {

                        /* check if it is an action */
                        if (allowedValueElement.namespaceURI() != SERVICE_NAMESPACE
                                || allowedValueElement.localName() != SERVICE_ELEMENT_ALLOWED_VALUE) {
                            continue;
                        }

                        QString allowedValue = extractValue(allowedValueElement);
                        if (allowedValue.isNull()) {
                            Logger.strace() << "Allowed value is empty";
                            result = false;
                            break;
                        }

                        allowedValueList.append(allowedValue);
                    }

                    if (!result) {
                        break;
                    }
                }

                /* process allowed value range */
                if (!allowedValueRangeElement.isNull()) {
                    QDomElement maximumValueElement = findChildElement(allowedValueRangeElement,
                            SERVICE_NAMESPACE, SERVICE_ELEMENT_MINIMUM);
                    QDomElement minimumValueElement = findChildElement(allowedValueRangeElement,
                            SERVICE_NAMESPACE, SERVICE_ELEMENT_MAXIMUM);
                    QDomElement stepValueElement = findChildElement(allowedValueRangeElement,
                            SERVICE_NAMESPACE, SERVICE_ELEMENT_STEP);

                    if (maximumValueElement.isNull()) {
                        Logger.strace() << "Maximum value not found";
                        result = false;
                        break;
                    }
                    if (minimumValueElement.isNull()) {
                        Logger.strace() << "Minimum value not found";
                        result = false;
                        break;
                    }

                    QString maximumString = extractValue(maximumValueElement);
                    if (maximumString.isNull()) {
                        Logger.strace() << "Maximum value is empty";
                        result = false;
                        break;
                    } else {
                        maximumValue.setValue(maximumString);
                    }

                    QString minimumString = extractValue(minimumValueElement);
                    if (minimumString.isNull()) {
                        Logger.strace() << "Minimum value is empty";
                        result = false;
                        break;
                    } else {
                        maximumValue.setValue(minimumString);
                    }

                    if (!stepValueElement.isNull()) {
                        QString stepString = extractValue(stepValueElement);
                        if (stepString.isNull()) {
                            Logger.strace() << "Step value is empty";
                            result = false;
                            break;
                        } else {
                            stepValue.setValue(stepString);
                        }
                    }
                }

                Logger.strace() << "State variable: " << stateVarName;

                stateVarList.append(StateVariable(stateVarName, sendEvents, dataType, defaultValue, minimumValue,
                        maximumValue, stepValue, allowedValueList));

                // TODO verify values against data type
            }

            return result;
        }

        bool DescriptionParser::parseActionList(QDomElement actionListElement, QList<Action>& actionList)
        {
            bool result = true;

#if 0
            <actionList>
            <action>
            <name>actionName</name>
            <argumentList>
            <argument>
            <name>formalParameterName</name>
            <direction>in xor out</direction>
            <retval />
            <relatedStateVariable>stateVariableName</relatedStateVariable>
            </argument>
            Declarations for other arguments defined by UPnP Forum working committee (if any)
            </argumentList>
            </action>
            Declarations for other actions defined by UPnP Forum working committee (if any) go here
            Declarations for other actions added by UPnP vendor (if any) go here
            </actionList>
#endif
            for (QDomElement actionElement = actionListElement.firstChildElement(); result && !actionElement.isNull(); actionElement
                    = actionElement.nextSiblingElement()) {

                /* check if it is an action */
                if (actionElement.namespaceURI() != SERVICE_NAMESPACE || actionElement.localName()
                        != SERVICE_ELEMENT_ACTION) {
                    continue;
                }

                QDomElement actionNameElement = findChildElement(actionElement, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_NAME);
                if (actionNameElement.isNull()) {
                    Logger.strace() << "Action name missing";
                    result = false;
                    break;
                }

                QString actionName = extractValue(actionNameElement);
                if (actionName.isNull()) {
                    Logger.strace() << "Action name is empty";
                    result = false;
                    break;
                }

                QList<ActionArgument> argumentList;

                QDomElement argumentListElement = findChildElement(actionElement, SERVICE_NAMESPACE,
                        SERVICE_ELEMENT_ARGUMENT_LIST);
                if (!argumentListElement.isNull()) {

                    for (QDomElement argumentElement = argumentListElement.firstChildElement(); result
                            && !argumentElement.isNull(); argumentElement = argumentElement.nextSiblingElement()) {

                        /* check if it is an action */
                        if (argumentElement.namespaceURI() != SERVICE_NAMESPACE || argumentElement.localName()
                                != SERVICE_ELEMENT_ARGUMENT) {
                            continue;
                        }

                        QDomElement argumentNameElement = findChildElement(argumentElement,
                                SERVICE_NAMESPACE, SERVICE_ELEMENT_NAME);
                        QDomElement directionElement = findChildElement(argumentElement,
                                SERVICE_NAMESPACE, SERVICE_ELEMENT_DIRECTION);
                        QDomElement retvalElement = findChildElement(argumentElement, SERVICE_NAMESPACE,
                                SERVICE_ELEMENT_RETVAL);
                        QDomElement relatedStateVarElement = findChildElement(argumentElement,
                                SERVICE_NAMESPACE, SERVICE_ELEMENT_RELATED_STATE_VARIABLE);

                        if (argumentNameElement.isNull()) {
                            Logger.strace() << "Argument name missing";
                            result = false;
                            break;
                        }
                        if (directionElement.isNull()) {
                            Logger.strace() << "Argument direction missing";
                            result = false;
                            break;
                        }
                        if (relatedStateVarElement.isNull()) {
                            Logger.strace() << "Argument related state variable missing";
                            result = false;
                            break;
                        }

                        QString argumentName = extractValue(argumentNameElement);
                        QString direction = extractValue(directionElement);
                        QString relatedStateVar = extractValue(relatedStateVarElement);

                        if (argumentName.isNull()) {
                            Logger.strace() << "Empty argument name";
                            result = false;
                            break;
                        }
                        if (direction.isNull()) {
                            Logger.strace() << "Empty direction";
                            result = false;
                            break;
                        }
                        if (relatedStateVar.isNull()) {
                            Logger.strace() << "Empty related state variable";
                            result = false;
                            break;
                        }

                        bool isRetVal = !retvalElement.isNull();

                        ActionArgument::Type argumentType = ActionArgument::parseType(direction, isRetVal);
                        if (argumentType == ActionArgument::ACTION_ARGUMENT_TYPE_INVALID) {
                            Logger.strace() << "Invalid argument type: " << direction << isRetVal;
                            result = false;
                            break;
                        }

                        // TODO: check argument name for duplicates

                        argumentList.append(ActionArgument(argumentName, argumentType, relatedStateVar));

                        Logger.strace() << "Argument: " << argumentName;
                    }

                    if (!result) {
                        break;
                    }
                }

                Logger.strace() << "Action: " << actionName;

                actionList.append(Action(actionName, argumentList));
            }

            return result;
        }

    }
}
