/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__ACTION_ARGUMENT_H_
#define _OCT_UPNP__ACTION_ARGUMENT_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class ActionArgument
        {
        public:
            enum Type
            {
                ACTION_ARGUMENT_TYPE_INVALID,
                ACTION_ARGUMENT_TYPE_INPUT,
                ACTION_ARGUMENT_TYPE_OUTPUT,
                ACTION_ARGUMENT_TYPE_RETVAL
            };

        private:
            QString m_name;
            Type m_type;
            QString m_relStateVar;

        public:
            ActionArgument(const QString& name, Type type, const QString& relStateVar);

            const QString& name() const
            {
                return m_name;
            }

            bool isInput() const
            {
                return m_type == ACTION_ARGUMENT_TYPE_INPUT;
            }

            bool isOutput() const
            {
                return m_type == ACTION_ARGUMENT_TYPE_OUTPUT ||
                        m_type == ACTION_ARGUMENT_TYPE_RETVAL;
            }

            Type type() const
            {
                return m_type;
            }

            const QString& relatedStateVariable() const
            {
                return m_relStateVar;
            }

        public:
            static Type parseType(const QString& direction, bool isRetVal);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__ACTION_ARGUMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
