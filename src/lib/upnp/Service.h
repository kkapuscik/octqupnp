/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__SERVICE_H_
#define _OCT_UPNP__SERVICE_H_

/*---------------------------------------------------------------------------*/

#include "Action.h"
#include "StateVariable.h"

#include <QtCore/QString>
#include <QtCore/QList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class Service
        {
        protected:
            Service()
            {
                // do nothing
            }

        public:
            virtual ~Service()
            {
                // do nothing
            }

            bool sendEvents() const
            {
                QList<StateVariable> vars = stateVariables();
                for (int i = 0; vars.size(); ++i) {
                    if (vars[i].sendEvents()) {
                        return true;
                    }
                }
                return false;
            }

            bool isCompatible(const QString& otherType) const
            {
                QString thisType = serviceType();

                int thisIndex = thisType.lastIndexOf(':');
                int otherIndex = otherType.lastIndexOf(':');

                if (thisIndex > 0 && otherIndex > 0 && thisIndex == otherIndex) {
                    if (thisType.mid(0, thisIndex) == otherType.mid(0, otherIndex)) {
                        QString thisVersionStr = thisType.mid(thisIndex + 1);
                        QString otherVersionStr = otherType.mid(otherIndex + 1);

                        bool thisOk;
                        bool otherOk;

                        int thisVersion = thisVersionStr.toInt(&thisOk, 10);
                        int otherVersion = otherVersionStr.toInt(&otherOk, 10);
                        if (thisOk && otherOk) {
                            if (thisVersion >= otherVersion) {
                                return true;
                            }
                        }
                    }
                }

                return thisType == otherType;
            }

            virtual const QString& serviceType() const = 0;
            virtual const QString& serviceId() const = 0;

            virtual bool hasStateVariable(const QString& name) const = 0;
            virtual QList<StateVariable> stateVariables() const = 0;
            virtual StateVariable stateVariable(const QString& name) const = 0;

            virtual bool hasAction(const QString& name) const = 0;
            virtual QList<Action> actions() const = 0;
            virtual Action action(const QString& name) const = 0;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__SERVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
