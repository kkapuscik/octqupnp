/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__STATE_VARIABLE_H_
#define _OCT_UPNP__STATE_VARIABLE_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtCore/QStringList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class StateVariable
        {
        private:
            QString m_name;
            bool m_sendEvents;
            QString m_dataType;
            QVariant m_defaultValue;
            QVariant m_minimumValue;
            QVariant m_maximumValue;
            QVariant m_stepValue;
            QStringList m_allowedValues;

        public:
            StateVariable(const QString& name, bool sendEvents, const QString& dataType, const QVariant& defaultValue =
                    QVariant(), const QVariant& minimumValue = QVariant(), const QVariant& maximumValue = QVariant(),
                    const QVariant& stepValue = QVariant(), const QStringList allowedValues = QStringList());

            const QString& name() const
            {
                return m_name;
            }

            bool sendEvents() const
            {
                return m_sendEvents;
            }

            const QString& dataType() const
            {
                return m_dataType;
            }

            const QVariant& defaultValue() const
            {
                return m_defaultValue;
            }

            const QVariant& minimumValue() const
            {
                return m_minimumValue;
            }

            const QVariant& maximumValue() const
            {
                return m_maximumValue;
            }

            const QVariant& stepValue() const
            {
                return m_stepValue;
            }

            QStringList allowedValues() const
            {
                return m_allowedValues;
            }
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__STATE_VARIABLE_H_ */

/**
 * @}
 * @}
 * @}
 */
