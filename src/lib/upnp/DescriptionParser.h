/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__DESCRIPTION_PARSER_H_
#define _OCT_UPNP__DESCRIPTION_PARSER_H_

/*---------------------------------------------------------------------------*/

#include "StateVariable.h"
#include "Action.h"
#include "XmlTools.h"

#include <log/Logger.h>

#include <QtCore/QList>
#include <QtXml/QDomElement>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class DescriptionParser : protected XmlTools
        {
        private:
            static Oct::Log::Logger<DescriptionParser> Logger;

        protected:
            static bool parseServiceXML(QDomElement documentElement, QList<StateVariable>& stateVarList, QList<Action>& actionList);
            static bool parseStateVarTable(QDomElement tableElement, QList<StateVariable>& stateVarList);
            static bool parseActionList(QDomElement actionListElement, QList<Action>& actionList);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__DESCRIPTION_PARSER_H_ */

/**
 * @}
 * @}
 * @}
 */
