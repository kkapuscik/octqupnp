/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "USN.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        USN::USN(const QString& usnString) :
            m_usn(usnString), m_type(USN_TYPE_INVALID)
        {
            if (usnString.startsWith("uuid:")) {
                QString uuidString;

                int uuidEnd = usnString.indexOf("::", 5);
                if (uuidEnd >= 0) {
                    uuidString = usnString.mid(0, uuidEnd);
                    m_elementInfo = usnString.mid(uuidEnd + 2);
                } else {
                    uuidString = usnString;
                }

                m_deviceUUID = UUID(uuidString);

                if (m_elementInfo == "upnp:rootdevice") {
                    m_type = USN_TYPE_ROOT_DEVICE;
                } else if (m_elementInfo.contains(":device:")) {
                    m_type = USN_TYPE_DEVICE_TYPE;
                } else if (m_elementInfo.contains(":service:")) {
                    m_type = USN_TYPE_SERVICE_TYPE;
                } else if (m_elementInfo.length() == 0) {
                    m_type = USN_TYPE_DEVICE;
                }
            }
        }
    }
}
