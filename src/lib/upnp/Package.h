/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp     UPnP protocol common core elements
 * @{
 */

#ifndef _OCT_UPNP__PACKAGE_H_
#define _OCT_UPNP__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * UPnP protocol common core elements.
     */
    namespace Upnp
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_UPNP__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
