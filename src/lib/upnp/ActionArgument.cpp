/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ActionArgument.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        ActionArgument::ActionArgument(const QString& name, Type type, const QString& relStateVar) :
            m_name(name), m_type(type), m_relStateVar(relStateVar)
        {
            // nothing to do
        }

        ActionArgument::Type ActionArgument::parseType(const QString& direction, bool isRetVal)
        {
            if (direction == "in") {
                return isRetVal ? ACTION_ARGUMENT_TYPE_INVALID : ACTION_ARGUMENT_TYPE_INPUT;
            } else if (direction == "out") {
                return isRetVal ? ACTION_ARGUMENT_TYPE_RETVAL : ACTION_ARGUMENT_TYPE_OUTPUT;
            } else {
                return ACTION_ARGUMENT_TYPE_INVALID;
            }
        }
    }
}
