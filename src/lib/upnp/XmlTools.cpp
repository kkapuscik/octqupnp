/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "XmlTools.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        Oct::Log::Logger<XmlTools> XmlTools::Logger("/oct/upnp/UpnpXmlTools");

        const QString XmlTools::DEVICE_NAMESPACE("urn:schemas-upnp-org:device-1-0");
        const QString XmlTools::DEVICE_ELEMENT_ROOT("root");
        const QString XmlTools::DEVICE_ELEMENT_SPEC_VERSION("specVersion");
        const QString XmlTools::DEVICE_ELEMENT_MAJOR("major");
        const QString XmlTools::DEVICE_ELEMENT_MINOR("minor");
        const QString XmlTools::DEVICE_ELEMENT_DEVICE("device");
        const QString XmlTools::DEVICE_ELEMENT_UDN("UDN");
        const QString XmlTools::DEVICE_ELEMENT_DEVICE_LIST("deviceList");
        const QString XmlTools::DEVICE_ELEMENT_SERVICE_LIST("serviceList");
        const QString XmlTools::DEVICE_ELEMENT_ICON_LIST("iconList");
        const QString XmlTools::DEVICE_ELEMENT_SERVICE("service");
        const QString XmlTools::DEVICE_ELEMENT_ICON("icon");
        const QString XmlTools::DEVICE_ELEMENT_URL_BASE("URLBase");
        const QString XmlTools::DEVICE_ELEMENT_SERVICE_TYPE("serviceType");
        const QString XmlTools::DEVICE_ELEMENT_SERVICE_ID("serviceId");
        const QString XmlTools::DEVICE_ELEMENT_SCPDURL("SCPDURL");
        const QString XmlTools::DEVICE_ELEMENT_CONTROL_URL("controlURL");
        const QString XmlTools::DEVICE_ELEMENT_EVENT_SUB_URL("eventSubURL");
        const QString XmlTools::DEVICE_ELEMENT_MIME_TYPE("mimetype");
        const QString XmlTools::DEVICE_ELEMENT_WIDTH("width");
        const QString XmlTools::DEVICE_ELEMENT_HEIGHT("height");
        const QString XmlTools::DEVICE_ELEMENT_DEPTH("depth");
        const QString XmlTools::DEVICE_ELEMENT_URL("url");

        const QString XmlTools::SERVICE_NAMESPACE("urn:schemas-upnp-org:service-1-0");
        const QString XmlTools::SERVICE_ELEMENT_SCPD("scpd");
        const QString XmlTools::SERVICE_ELEMENT_SPEC_VERSION("specVersion");
        const QString XmlTools::SERVICE_ELEMENT_MAJOR("major");
        const QString XmlTools::SERVICE_ELEMENT_MINOR("minor");
        const QString XmlTools::SERVICE_ELEMENT_SERVICE_STATE_TABLE("serviceStateTable");
        const QString XmlTools::SERVICE_ELEMENT_STATE_VARIABLE("stateVariable");
        const QString XmlTools::SERVICE_ELEMENT_ACTION_LIST("actionList");
        const QString XmlTools::SERVICE_ELEMENT_ACTION("action");
        const QString XmlTools::SERVICE_ELEMENT_NAME("name");
        const QString XmlTools::SERVICE_ELEMENT_ARGUMENT_LIST("argumentList");
        const QString XmlTools::SERVICE_ELEMENT_ARGUMENT("argument");
        const QString XmlTools::SERVICE_ELEMENT_DIRECTION("direction");
        const QString XmlTools::SERVICE_ELEMENT_RETVAL("retVal");
        const QString XmlTools::SERVICE_ELEMENT_RELATED_STATE_VARIABLE("relatedStateVariable");
        const QString XmlTools::SERVICE_ELEMENT_DATA_TYPE("dataType");
        const QString XmlTools::SERVICE_ELEMENT_DEFAULT_VALUE("defaultValue");
        const QString XmlTools::SERVICE_ELEMENT_ALLOWED_VALUE_LIST("allowedValueList");
        const QString XmlTools::SERVICE_ELEMENT_ALLOWED_VALUE("allowedValue");
        const QString XmlTools::SERVICE_ELEMENT_ALLOWED_VALUE_RANGE("allowedValueRange");
        const QString XmlTools::SERVICE_ELEMENT_MINIMUM("minimum");
        const QString XmlTools::SERVICE_ELEMENT_MAXIMUM("maximum");
        const QString XmlTools::SERVICE_ELEMENT_STEP("step");
        const QString XmlTools::SERVICE_ATTRIBUTES_SEND_EVENTS("sendEvents");
        const QString XmlTools::SERVICE_VALUE_YES("yes");

        QDomElement XmlTools::findChildElement(QDomElement element, const QString& namespaceURI,
                const QString& localName)
        {
            QDomElement theChild;

            for (QDomElement childElement = element.firstChildElement(); !childElement.isNull(); childElement
                    = childElement.nextSiblingElement()) {

                if (childElement.namespaceURI() == namespaceURI && childElement.localName() == localName) {
                    if (theChild.isNull()) {
                        theChild = childElement;
                    } else {
                        Logger.strace() << "Child element duplicated: " << namespaceURI << localName << "in"
                                << element.namespaceURI() << element.localName();
                        return QDomElement();
                    }
                }
            }

            return theChild;
        }

        QString XmlTools::extractValue(QDomElement element)
        {
            QString value;

            for (QDomNode childNode = element.firstChild(); !childNode.isNull(); childNode = childNode.nextSibling()) {

                if (childNode.isText()) {
                    QDomText text = childNode.toText();
                    value.append(text.data());
                } else if (childNode.isElement()) {
                    return QString();
                } else {
                    // TODO? skip? error
                }
            }

            return value;
        }
    }
}
