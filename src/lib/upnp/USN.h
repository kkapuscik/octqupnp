/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Upnp
 * @{
 */

#ifndef _OCT_UPNP__USN_H_
#define _OCT_UPNP__USN_H_

/*---------------------------------------------------------------------------*/

#include "UUID.h"

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        class USN
        {
        public:
            enum Type
            {
                USN_TYPE_ROOT_DEVICE,
                USN_TYPE_DEVICE,
                USN_TYPE_DEVICE_TYPE,
                USN_TYPE_SERVICE_TYPE,
                USN_TYPE_INVALID
            };

        private:
            QString m_usn;
            Type m_type;
            UUID m_deviceUUID;
            QString m_elementInfo;

        public:
            USN(const QString& usnString);

            QString usnString() const
            {
                return m_usn;
            }

            Type type() const
            {
                return m_type;
            }

            UUID deviceUUID() const
            {
                return m_deviceUUID;
            }

            QString elementInfo() const
            {
                return m_elementInfo;
            }

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP__USN_H_ */

/**
 * @}
 * @}
 * @}
 */
