/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "StateVariable.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        StateVariable::StateVariable(const QString& name, bool sendEvents, const QString& dataType,
                const QVariant& defaultValue, const QVariant& minimumValue, const QVariant& maximumValue,
                const QVariant& stepValue, const QStringList allowedValues) :
            m_name(name), m_sendEvents(sendEvents), m_dataType(dataType), m_defaultValue(defaultValue), m_minimumValue(
                    minimumValue), m_maximumValue(maximumValue), m_stepValue(stepValue), m_allowedValues(allowedValues)
        {
        }
    }
}
