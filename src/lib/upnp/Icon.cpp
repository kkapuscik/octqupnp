/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Icon.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Upnp
    {
        Icon::Icon() :
            m_width(-1), m_height(-1), m_depth(-1)
        {
        }

        Icon::Icon(const QString& mimeType, int width, int height, int depth) :
            m_mimeType(mimeType), m_width(width), m_height(height), m_depth(depth)
        {
            // nothing to do
        }

        Icon::~Icon()
        {
            // nothing to do
        }
    }
}
