cmake_minimum_required(VERSION 2.8.11)

project(liboctupnp)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Xml REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

include_directories(${Qt5Core_INCLUDE_DIRS})
include_directories(${Qt5Xml_INCLUDE_DIRS})

file(GLOB liboctupnp_SOURCES *.cpp)
file(GLOB liboctupnp_HEADERS *.h)

QT5_WRAP_CPP(liboctupnp_MOCSOURCES ${liboctupnp_HEADERS})
#macro QT5_WRAP_UI(outfiles inputfile ... OPTIONS ...)
#macro QT5_ADD_RESOURCES(outfiles inputfile ... OPTIONS ...)

add_library(octupnp STATIC ${liboctupnp_SOURCES} ${liboctupnp_HEADERS} ${liboctupnp_MOCSOURCES})
target_link_libraries(octupnp octlog Qt5::Core Qt5::Xml)
