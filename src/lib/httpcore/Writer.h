/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__WRITER_H_
#define _OCT_HTTP_CORE__WRITER_H_

/*-----------------------------------------------------------------------------*/

#include <QtCore/QObject>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class Writer : public QObject
        {
        public:
            Writer(QObject* parent = 0);
            virtual ~Writer();

            virtual qint64 write(const char* data, qint64 maxSize) = 0;
            virtual qint64 write(const QByteArray& byteArray) = 0;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__WRITER_H_ */

/**
 * @}
 * @}
 * @}
 */
