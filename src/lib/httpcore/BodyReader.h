/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__BODY_READER_H_
#define _OCT_HTTP_CORE__BODY_READER_H_

/*-----------------------------------------------------------------------------*/

#include "Reader.h"

#include <QtCore/QObject>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class BodyReader : public QObject
        {
        Q_OBJECT

        protected:
            BodyReader(QObject* parent = 0);
        public:
            virtual ~BodyReader();

            /**
             * Returns body length.
             *
             * @return
             * Length of the body in bytes or -1 if body is unbound.
             */
            virtual qint64 length() const = 0;

            /**
             * Reads bytes to given buffer.
             *
             * @param data
             *      Buffer to store the data.
             * @param maxSize
             *      Maximum number of bytes to read.
             *
             * @return
             * Returned values have different meanings:
             * <ul>
             *      <li>N &gt; 0 means that N bytes were read to buffer data.</li>
             *      <li>N = 0 means that there is no more data <strong>currently</strong> available.</li>
             *      <li>N = -1 means that an error occurred.</li>
             *      <li>N = -2 means that an there is no more data available (EOF was reached).</li>
             * </ul>
             */
            virtual qint64 read(char* data, qint64 maxSize) = 0;

        signals:
            void readyRead();
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__BODY_READER_H_ */

/**
 * @}
 * @}
 * @}
 */
