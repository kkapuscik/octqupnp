/*
 * ByteRange.cpp
 *
 *  Created on: 28-03-2011
 *      Author: saveman
 */

#include "ByteRange.h"

namespace Oct
{
    namespace HttpCore
    {
        ByteRange ByteRange::parse(Mode mode, QString value)
        {
            if (mode == MODE_REQUEST) {
                int sep = value.indexOf('-');
                if (sep < 0) {
                    return ByteRange();
                }

                QString firstByteStr = value.mid(0, sep);
                QString lastByteStr = value.mid(sep + 1);

                if (firstByteStr.isEmpty() && lastByteStr.isEmpty()) {
                    return ByteRange();
                }

                bool ok;

                qint64 firstByte;
                if (firstByteStr.isEmpty()) {
                    firstByte = -1;
                } else {
                    firstByte = firstByteStr.toLong(&ok, 10);
                    if (!ok) {
                        return ByteRange();
                    }
                }

                qint64 lastByte;
                if (lastByteStr.isEmpty()) {
                    lastByte = -1;
                } else {
                    lastByte = lastByteStr.toLong(&ok, 10);
                    if (!ok) {
                        return ByteRange();
                    }
                }

                return ByteRange(mode, firstByte, lastByte);
            } else {
                // TODO
                Q_ASSERT(0);
                return ByteRange();
            }
        }
    }
}
