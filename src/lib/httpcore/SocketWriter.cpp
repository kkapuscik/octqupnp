/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SocketWriter.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        Oct::Log::Logger<SocketWriter> SocketWriter::Logger("/oct/httpcore/HttpSocketWriter");

        SocketWriter::SocketWriter(QTcpSocket* tcpSocket, QObject* parent) :
            Writer(parent), m_socket(tcpSocket)
        {
            connect(tcpSocket, SIGNAL( bytesWritten(qint64) ), this, SIGNAL( readyWrite() ));
        }

        SocketWriter::~SocketWriter()
        {
            // nothing to do
        }
    }
}
