/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__START_LINE_H_
#define _OCT_HTTP_CORE__START_LINE_H_

/*-----------------------------------------------------------------------------*/

#include "StatusLine.h"
#include "RequestLine.h"

#include <QtCore/QString>
#include <QtCore/QBuffer>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class StartLine
        {
        private:
            StatusLine m_statusLine;
            RequestLine m_requestLine;

        public:
            StartLine();
            StartLine(const StatusLine& statusLine);
            StartLine(const RequestLine& requestLine);
            StartLine(HttpVersion version, int code, const QString& message);
            StartLine(const QString& method, const QString& uri, HttpVersion version);
            ~StartLine();

            bool isStatusLine() const
            {
                return m_statusLine.isValid();
            }

            bool isRequestLine() const
            {
                return m_requestLine.isValid();
            }

            bool isValid() const
            {
                return isStatusLine() || isRequestLine();
            }

            const StatusLine& statusLine() const
            {
                return m_statusLine;
            }

            const RequestLine& requestLine() const
            {
                return m_requestLine;
            }

            bool write(QBuffer& buffer) const;

        public:
            static StartLine parse(const QString& value);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__START_LINE_H_ */

/**
 * @}
 * @}
 * @}
 */
