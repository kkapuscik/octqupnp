/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "StatusLineWriter.h"

#include "Tools.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        StatusLineWriter::StatusLineWriter() :
            m_ioStatus(IO_STATUS_WAIT)
        {
            // nothing to do
        }

        IOStatus StatusLineWriter::writeStatusLine(Writer* writer)
        {
            // TODO: that way?
            if (m_ioStatus != IO_STATUS_WAIT) {
                Q_ASSERT(0);
                return IO_STATUS_ERROR;
            }

            Q_ASSERT(m_statusLine.isValid());

            /* check if we need to prepare buffer */
            if (m_buffer.size() == 0) {
                QString line = QString("%1 %2 %3\r\n"). arg(Tools::versionString(m_statusLine.version())). arg(
                        m_statusLine.code()). arg(m_statusLine.reasonPhrase());

                m_buffer.append(line.toUtf8());
            }

            return m_ioStatus = m_buffer.write(writer);
        }
    }
}
