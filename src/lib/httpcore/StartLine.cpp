/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "StartLine.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        StartLine::StartLine()
        {
            // nothing to do
        }

        StartLine::StartLine(const StatusLine& statusLine) :
            m_statusLine(statusLine)
        {
            // nothing to do
        }

        StartLine::StartLine(const RequestLine& requestLine) :
            m_requestLine(requestLine)
        {
            // nothing to do
        }

        StartLine::StartLine(HttpVersion version, int code, const QString& message) :
            m_statusLine(version, code, message)
        {
            // nothing to do
        }

        StartLine::StartLine(const QString& method, const QString& uri, HttpVersion version) :
            m_requestLine(method, uri, version)
        {
            // nothing to do
        }

        StartLine::~StartLine()
        {
            // nothing to do
        }

        bool StartLine::write(QBuffer& buffer) const
        {
            if (!isValid()) {
                return false;
            }

            if (isRequestLine()) {
                return m_requestLine.write(buffer);
            } else if (isStatusLine()) {
                return m_statusLine.write(buffer);
            } else {
                return false;
            }
        }

        StartLine StartLine::parse(const QString& value)
        {
            StatusLine statusLine = StatusLine::parse(value);
            if (statusLine.isValid()) {
                return StartLine(statusLine);
            }

            RequestLine requestLine = RequestLine::parse(value);
            if (requestLine.isValid()) {
                return StartLine(requestLine);
            }

            return StartLine();
        }
    }
}
