/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__IO_STATUS_H_
#define _OCT_HTTP_CORE__IO_STATUS_H_

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        enum IOStatus
        {
            IO_STATUS_READY = 0,
            IO_STATUS_WAIT = -1,
            IO_STATUS_ERROR = -2,
            IO_STATUS_END_OF_DATA = -3
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__IO_STATUS_H_ */

/**
 * @}
 * @}
 * @}
 */
