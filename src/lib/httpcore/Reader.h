/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__READER_H_
#define _OCT_HTTP_CORE__READER_H_

/*-----------------------------------------------------------------------------*/

#include <QtCore/QObject>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class Reader : public QObject
        {
        Q_OBJECT

        public:
            Reader(QObject* parent = 0);
            virtual ~Reader();

            /**
             * Reads bytes to given buffer.
             *
             * @param data
             *      Buffer to store the data.
             * @param maxSize
             *      Maximum number of bytes to read.
             *
             * @return
             * Returned values have different meanings:
             * <ul>
             *      <li>N &gt; 0 means that N bytes were read to buffer data.</li>
             *      <li>N = 0 means that there is no more data <strong>currently</strong> available.</li>
             *      <li>N = -1 means that an error occured.</li>
             *      <li>N = -2 means that an there is no more data available (EOF was reached).</li>
             * </ul>
             */
            virtual qint64 read(char* data, qint64 maxSize) = 0;

            /**
             * Reads line to given buffer.
             *
             * <p>The methods reads one line finished with CR+LF characters pair. Zero is stored after
             * the CR+LF pair. So the maxSize must be &gt;= 3 (CR+LF+ZERO + space for line characters).</p>
             *
             * @param data
             *      Buffer to store the data.
             * @param maxSize
             *      Maximum number of bytes to read.
             *
             * @return
             * Returned values have different meanings:
             * <ul>
             *      <li>N &gt; 0 means that N bytes were read to buffer data.</li>
             *      <li>N = 0 means that there is no more data <strong>currently</strong> available.</li>
             *      <li>N = -1 means that an error occured.</li>
             *      <li>N = -2 means that an there is no more data available (EOF was reached).</li>
             * </ul>
             */
            virtual qint64 readLine(char* data, qint64 maxSize) = 0;

        signals:
            void readyRead();
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__READER_H_ */

/**
 * @}
 * @}
 * @}
 */
