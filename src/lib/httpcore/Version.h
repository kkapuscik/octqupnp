/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__VERSION_H_
#define _OCT_HTTP_CORE__VERSION_H_

/*-----------------------------------------------------------------------------*/

#include <log/Logger.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        enum HttpVersion
        {
            HTTP_VERSION_INVALID,
            HTTP_VERSION_1_0,
            HTTP_VERSION_1_1
        };

        Oct::Log::OutputStream operator <<(Oct::Log::OutputStream dbg, const HttpVersion& version);
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__VERSION_H_ */

/**
 * @}
 * @}
 * @}
 */
