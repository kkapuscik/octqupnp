/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__BYTE_RANGE_H_
#define _OCT_HTTP_CORE__BYTE_RANGE_H_

/*-----------------------------------------------------------------------------*/

#include <QtCore/QString>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class ByteRange
        {
        public:
            static const qint64 LENGTH_UNKNOWN = -1;
            static const qint64 LENGTH_UNBOUND = -2;

            enum Mode
            {
                MODE_REQUEST,
                MODE_RESPONSE
            };

        private:
            Mode m_mode;
            qint64 m_firstBytePos;
            qint64 m_lastBytePos;
            qint64 m_byteLength;

        public:
            ByteRange(Mode mode = MODE_REQUEST, qint64 firstBytePos = -1, qint64 lastBytePos = -1, qint64 byteLength =
                    LENGTH_UNKNOWN) :
                m_mode(mode), m_firstBytePos(firstBytePos), m_lastBytePos(lastBytePos), m_byteLength(byteLength)
            {
                // nothing to do
            }

            bool isValid() const
            {
                if (m_byteLength > 0) {
                    if (m_firstBytePos >= 0 && m_lastBytePos >= 0 && m_firstBytePos <= m_lastBytePos) {
                        if (m_firstBytePos < m_byteLength && m_lastBytePos < m_byteLength) {
                            return true;
                        }
                    }
                } else if (m_byteLength == LENGTH_UNBOUND) {
                    if (m_firstBytePos >= 0 && m_lastBytePos >= 0 && m_firstBytePos <= m_lastBytePos) {
                        return true;
                    }
                } else if (m_byteLength == LENGTH_UNKNOWN && m_mode == MODE_REQUEST) {
                    if (m_firstBytePos >= 0 || m_lastBytePos >= 0) {
                        return true;
                    }
                }

                return false;
            }

            Mode mode() const
            {
                return m_mode;
            }

            qint64 firstByte() const
            {
                return m_firstBytePos;
            }

            qint64 lastByte() const
            {
                return m_lastBytePos;
            }

            qint64 length() const
            {
                return m_byteLength;
            }

            QString toString() const
            {
                if (isValid()) {
                    if (m_mode == MODE_REQUEST) {
                        if (m_firstBytePos >= 0) {
                            return QString("%1-%2").arg(m_firstBytePos).arg(m_lastBytePos);
                        } else {
                            return QString("-%2").arg(m_lastBytePos);
                        }
                    } else {
                        return QString("%1-%2/%3").arg(m_firstBytePos).arg(m_lastBytePos).arg(m_byteLength);
                    }
                }

                return QString();
            }

        public:
            static ByteRange parse(Mode mode, QString value);
        };
    }

}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__BYTE_RANGE_H_ */

/**
 * @}
 * @}
 * @}
 */
