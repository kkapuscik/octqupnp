/*
 * $Id: OctHttpHeader.h 49 2011-01-10 20:00:26Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__HTTP_HEADER_H_
#define _OCT_HTTP_CORE__HTTP_HEADER_H_

/*-----------------------------------------------------------------------------*/

#include <log/Logger.h>

#include <QtCore/QString>
#include <QtCore/QBuffer>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class Header
        {
        private:
            static Oct::Log::Logger<Header> Logger;

        private:
            QString m_name;
            QString m_value;

        public:
            Header();
            Header(const Header& header);
            Header(const QString& name, const QString& value);
            virtual ~Header();

            bool isValid() const;

            const QString& name() const
            {
                return m_name;
            }

            const QString& value() const
            {
                return m_value;
            }

            Header append(const QString& value) const;

            bool write(QBuffer& buffer) const;

        public:
            static Header parse(const QString& value);
        };

        Oct::Log::OutputStream operator <<(Oct::Log::OutputStream dbg, const Header& header);
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__HTTP_HEADER_H_ */

/**
 * @}
 * @}
 * @}
 */
