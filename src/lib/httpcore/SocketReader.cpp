/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SocketReader.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        SocketReader::SocketReader(QTcpSocket* tcpSocket, QObject* parent) :
            Reader(parent), m_socket(tcpSocket), m_channelFinished(false)
        {
            connect(m_socket, SIGNAL( readyRead() ), this, SIGNAL( readyRead() ));
            connect(m_socket, SIGNAL( readChannelFinished() ), this, SLOT( readChannelFinished() ));

            if (!m_socket->isReadable()) {
                m_channelFinished = true;
            }
        }

        SocketReader::~SocketReader()
        {
            // nothing to do
        }
    }
}
