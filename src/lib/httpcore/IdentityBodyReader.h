/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__IDENTITY_BODY_READER_H_
#define _OCT_HTTP_CORE__IDENTITY_BODY_READER_H_

/*-----------------------------------------------------------------------------*/

#include "BodyReader.h"

#include <QtCore/QObject>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class IdentityBodyReader : public BodyReader
        {
        Q_OBJECT

        private:
            Reader* m_reader;
            quint64 m_length;
            quint64 m_position;

        public:
            IdentityBodyReader(Reader* reader, quint64 length, QObject* parent = 0);
            virtual ~IdentityBodyReader();

            virtual qint64 length() const;
            virtual qint64 read(char* data, qint64 maxSize);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__IDENTITY_BODY_READER_H_ */

/**
 * @}
 * @}
 * @}
 */
