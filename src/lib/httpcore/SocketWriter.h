/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__SOCKET_WRITER_H_
#define _OCT_HTTP_CORE__SOCKET_WRITER_H_

/*-----------------------------------------------------------------------------*/

#include "Writer.h"

#include <log/Logger.h>
#include <QtNetwork/QTcpSocket>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
#define BYTES_TO_WRITE_LIMIT    (64 * 1024)

        class SocketWriter : public Writer
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<SocketWriter> Logger;

        private:
            QTcpSocket* m_socket;

        public:
            SocketWriter(QTcpSocket* tcpSocket, QObject* parent = 0);
            virtual ~SocketWriter();

            virtual qint64 write(const char* data, qint64 maxSize)
            {
                if (m_socket->bytesToWrite() >= BYTES_TO_WRITE_LIMIT) {
                    Logger.trace(this) << "Limiting write";
                    return 0;
                }

                qint64 rv = m_socket->write(data, maxSize);

                Logger.trace(this) << "Socket write: maxSize=" << maxSize << " rv=" << rv;

                return rv;
            }

            virtual qint64 write(const QByteArray& byteArray)
            {
                if (m_socket->bytesToWrite() >= BYTES_TO_WRITE_LIMIT) {
                    Logger.trace(this) << "Limiting write";
                    return 0;
                }

                return m_socket->write(byteArray);
            }

        signals:
            void readyWrite();

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__SOCKET_WRITER_H_ */

/**
 * @}
 * @}
 * @}
 */
