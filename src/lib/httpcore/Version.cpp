/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Version.h"

#include "Tools.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        Oct::Log::OutputStream operator <<(Oct::Log::OutputStream dbg, const HttpVersion& version)
        {
            return dbg << "[HttpVersion version=" << Tools::versionString(version);
        }
    }
}
