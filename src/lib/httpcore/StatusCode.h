/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__STATUS_CODE_H_
#define _OCT_HTTP_CORE__STATUS_CODE_H_

/*-----------------------------------------------------------------------------*/

#include <QtCore/QString>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        enum
        {
            HTTP_STATUS_CODE_100_CONTINUE = 100,
            HTTP_STATUS_CODE_101_SWITCHING_PROTOCOLS = 101,
            HTTP_STATUS_CODE_200_OK = 200,
            HTTP_STATUS_CODE_201_CREATED = 201,
            HTTP_STATUS_CODE_202_ACCEPTED = 202,
            HTTP_STATUS_CODE_203_NON_AUTHORITATIVE_INFORMATION = 203,
            HTTP_STATUS_CODE_204_NO_CONTENT = 204,
            HTTP_STATUS_CODE_205_RESET_CONTENT = 205,
            HTTP_STATUS_CODE_206_PARTIAL_CONTENT = 206,
            HTTP_STATUS_CODE_300_MULTIPLE_CHOICES = 300,
            HTTP_STATUS_CODE_301_MOVED_PERMANENTLY = 301,
            HTTP_STATUS_CODE_302_FOUND = 302,
            HTTP_STATUS_CODE_303_SEE_OTHER = 303,
            HTTP_STATUS_CODE_304_NOT_MODIFIED = 304,
            HTTP_STATUS_CODE_305_USE_PROXY = 305,
            HTTP_STATUS_CODE_307_TEMPORARY_REDIRECT = 307,
            HTTP_STATUS_CODE_400_BAD_REQUEST = 400,
            HTTP_STATUS_CODE_401_UNAUTHORIZED = 401,
            HTTP_STATUS_CODE_402_PAYMENT_REQUIRED = 402,
            HTTP_STATUS_CODE_403_FORBIDDEN = 403,
            HTTP_STATUS_CODE_404_NOT_FOUND = 404,
            HTTP_STATUS_CODE_405_METHOD_NOT_ALLOWED = 405,
            HTTP_STATUS_CODE_406_NOT_ACCEPTABLE = 406,
            HTTP_STATUS_CODE_407_PROXY_AUTHENTICATION_REQUIRED = 407,
            HTTP_STATUS_CODE_408_REQUEST_TIME_OUT = 408,
            HTTP_STATUS_CODE_409_CONFLICT = 409,
            HTTP_STATUS_CODE_410_GONE = 410,
            HTTP_STATUS_CODE_411_LENGTH_REQUIRED = 411,
            HTTP_STATUS_CODE_412_PRECONDITION_FAILED = 412,
            HTTP_STATUS_CODE_413_REQUEST_ENTITY_TOO_LARGE = 413,
            HTTP_STATUS_CODE_414_REQUEST_URI_TOO_LARGE = 414,
            HTTP_STATUS_CODE_415_UNSUPPORTED_MEDIA_TYPE = 415,
            HTTP_STATUS_CODE_416_REQUESTED_RANGE_NOT_SATISFIABLE = 416,
            HTTP_STATUS_CODE_417_EXPECTATION_FAILED = 417,
            HTTP_STATUS_CODE_500_INTERNAL_SERVER_ERROR = 500,
            HTTP_STATUS_CODE_501_NOT_IMPLEMENTED = 501,
            HTTP_STATUS_CODE_502_BAD_GATEWAY = 502,
            HTTP_STATUS_CODE_503_SERVICE_UNAVAILABLE = 503,
            HTTP_STATUS_CODE_504_GATEWAY_TIME_OUT = 504,
            HTTP_STATUS_CODE_505_HTTP_VERSION_NOT_SUPPORTED = 505
        };

        class StatusCode
        {
        private:
            StatusCode()
            {
                // nothing to do, forbids creation of objects
            }

        public:
            static const QString& reasonPhrase(int code);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__STATUS_CODE_H_ */

/**
 * @}
 * @}
 * @}
 */
