/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__HEADERS_READER_H_
#define _OCT_HTTP_CORE__HEADERS_READER_H_

/*-----------------------------------------------------------------------------*/

#include "HeaderSet.h"
#include "IOStatus.h"
#include "Reader.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class HeadersReader
        {
        private:
            static Oct::Log::Logger<HeadersReader> Logger;

        private:
            IOStatus m_status;
            HeaderSet m_headerSet;
            QString m_currentLine;

        public:
            HeadersReader();

            const HeaderSet& headers() const
            {
                return m_headerSet;
            }

            IOStatus readHeaders(Reader* reader);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__HEADERS_READER_H_ */

/**
 * @}
 * @}
 * @}
 */
