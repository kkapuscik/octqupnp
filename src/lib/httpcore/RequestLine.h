/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__REQUEST_LINE_H_
#define _OCT_HTTP_CORE__REQUEST_LINE_H_

/*-----------------------------------------------------------------------------*/

#include "Version.h"

#include <log/Logger.h>

#include <QtCore/QString>
#include <QtCore/QBuffer>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class RequestLine
        {
        private:
            static Oct::Log::Logger<RequestLine> Logger;

        private:
            QString m_method;
            QString m_uri;
            HttpVersion m_version;

        public:
            RequestLine();
            RequestLine(const QString& method, const QString& uri, HttpVersion version);

            ~RequestLine();

            bool isValid() const
            {
                return m_version != HTTP_VERSION_INVALID && m_method.length() > 0 && m_uri.length() > 0;
            }

            const QString& method() const
            {
                return m_method;
            }

            const QString& uri() const
            {
                return m_uri;
            }

            HttpVersion version() const
            {
                return m_version;
            }

            bool write(QBuffer& buffer) const;

        public:
            static RequestLine parse(const QString& value);
        };

        Oct::Log::OutputStream operator <<(Oct::Log::OutputStream dbg, const RequestLine& requestLine);
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__REQUEST_LINE_H_ */

/**
 * @}
 * @}
 * @}
 */
