/*
 * $Id: OctHttpHeaderSet.h 49 2011-01-10 20:00:26Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__HTTP_HEADER_SET_H_
#define _OCT_HTTP_CORE__HTTP_HEADER_SET_H_

/*-----------------------------------------------------------------------------*/

#include "Header.h"

#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtCore/QBuffer>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class HeaderSet
        {
        private:
            typedef QMap<QString, Header> NameToHeaderMap;

        public:

        private:
            NameToHeaderMap m_headersMap;

        public:
            HeaderSet();

            ~HeaderSet();

            bool contains(const QString& name) const;

            QString value(const QString& name) const;

            Header header(const QString& name) const;

            QList<Header> allHeaders() const;

            void set(const Header& header);

            void set(const QString& name, const QString& value)
            {
                set(Header(name, value));
            }

            void appendOrSet(const Header& header);

            void appendOrSet(const QString& name, const QString& value)
            {
                appendOrSet(Header(name, value));
            }

            void remove(const QString& name);

            bool write(QBuffer& buffer) const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__HTTP_HEADER_SET_H_ */

/**
 * @}
 * @}
 * @}
 */
