/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__STATUS_LINE_WRITER_H_
#define _OCT_HTTP_CORE__STATUS_LINE_WRITER_H_

/*-----------------------------------------------------------------------------*/

#include "IOStatus.h"
#include "StatusLine.h"
#include "Writer.h"
#include "IOBuffer.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class StatusLineWriter
        {
        private:
            StatusLine m_statusLine;
            IOStatus m_ioStatus;
            IOBuffer m_buffer;

        public:
            StatusLineWriter();

            void setStatusLine(const StatusLine& statusLine)
            {
                Q_ASSERT(!m_statusLine.isValid());
                Q_ASSERT(statusLine.isValid());
                m_statusLine = statusLine;
            }

            const StatusLine& statusLine() const
            {
                return m_statusLine;
            }

            IOStatus writeStatusLine(Writer* writer);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__STATUS_LINE_WRITER_H_ */

/**
 * @}
 * @}
 * @}
 */
