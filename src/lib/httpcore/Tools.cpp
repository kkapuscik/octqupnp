/*
 * $Id: OctHttpTools.cpp 49 2011-01-10 20:00:26Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Tools.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        Oct::Log::Logger<Tools> Tools::Logger("/oct/httpcore/Tools");

        QString Tools::removeLWS(const QString& value)
        {
            bool needFix = false;
            bool inQuote;
            QChar prevChar;

            inQuote = false;
            prevChar = 'A'; // any non space

            for (int j = 0; j < value.length(); ++j) {
                QChar currentChar = value[j];

                if (!inQuote) {
                    if (prevChar.isSpace() && currentChar.isSpace()) {
                        needFix = true;
                    }
                }

                if (currentChar == '"') {
                    inQuote = !inQuote;
                }

                prevChar = currentChar;
            }

            if (inQuote) {
                Logger.strace() << "Quoted string not closed";
                return QString::null;
            }

            if (needFix) {
                QString result;

                inQuote = false;
                prevChar = 'A'; // any non space

                for (int j = 0; j < value.length(); ++j) {
                    QChar currentChar = value[j];

                    if (!inQuote) {
                        if (prevChar.isSpace() && currentChar.isSpace()) {
                            // skip
                        } else {
                            result.append(' ');
                        }
                    } else {
                        result.append(currentChar);
                    }

                    if (currentChar == '"') {
                        inQuote = !inQuote;
                    }

                    prevChar = currentChar;
                }

                return result;
            } else {
                return value;
            }
        }

        /*
         * token = 1*<any CHAR except CTLs or separators>
         *
         * separators     = "(" | ")" | "<" | ">" | "@"
         *                | "," | ";" | ":" | "\" | <">
         *                | "/" | "[" | "]" | "?" | "="
         *                | "{" | "}" | SP | HT
         */
        bool Tools::isToken(const QString& value)
        {
            if (value.length() == 0) {
                return false;
            }

            for (int i = 0; i < value.length(); ++i) {
                // TODO: FIXME
                if (value[i].isLetterOrNumber()) {
                    // nothing
                } else if (value[i] == '-') {
                    // nothing
                } else if (value[i] == '.') {
                    // nothing
                } else {
                    return false;
                }
            }

            return true;
        }

        QString Tools::versionString(HttpVersion version)
        {
            switch (version) {
                case HTTP_VERSION_1_0:
                    return "HTTP/1.0";
                case HTTP_VERSION_1_1:
                    return "HTTP/1.1";
                default:
                    return QString::null;
            }
        }

        HttpVersion Tools::parseVersion(const QString& value)
        {
            if (value == "HTTP/1.0") {
                return HTTP_VERSION_1_0;
            } else if (value == "HTTP/1.1") {
                return HTTP_VERSION_1_1;
            } else {
                return HTTP_VERSION_INVALID;
            }
        }
    }
}
