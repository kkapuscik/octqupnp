/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__BYTE_RANGE_SET_H_
#define _OCT_HTTP_CORE__BYTE_RANGE_SET_H_

/*-----------------------------------------------------------------------------*/

#include "ByteRange.h"

#include <QtCore/QString>
#include <QtCore/QList>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class ByteRangeSet
        {
        private:
            QList<ByteRange> m_ranges;

        private:
            ByteRangeSet(QList<ByteRange> ranges);

        public:
            ByteRangeSet();

            int size() const
            {
                return m_ranges.size();
            }

            ByteRange range(int index) const
            {
                Q_ASSERT(index >= 0 && index < size());

                return m_ranges[index];
            }

        public:
            static Oct::HttpCore::ByteRangeSet parse(QString value);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__BYTE_RANGE_SET_H_ */

/**
 * @}
 * @}
 * @}
 */
