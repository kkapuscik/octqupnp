/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__STATUS_LINE_H_
#define _OCT_HTTP_CORE__STATUS_LINE_H_

/*-----------------------------------------------------------------------------*/

#include "Version.h"
#include "StatusCode.h"

#include <log/Logger.h>

#include <QtCore/QString>
#include <QtCore/QBuffer>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class StatusLine
        {
        private:
            static Oct::Log::Logger<StatusLine> Logger;

        private:
            HttpVersion m_version;
            int m_code;
            QString m_reasonPhrase;

        public:
            StatusLine();
            StatusLine(HttpVersion version, int code);
            StatusLine(HttpVersion version, int code, const QString& reasonPhrase);

            ~StatusLine();

            bool isValid() const
            {
                return m_version != HTTP_VERSION_INVALID && m_code >= 100 && m_code <= 999 && m_reasonPhrase.length() > 0;
            }

            HttpVersion version() const
            {
                return m_version;
            }

            int code() const
            {
                return m_code;
            }

            const QString& reasonPhrase() const
            {
                return m_reasonPhrase;
            }

            bool write(QBuffer& buffer) const;

        public:
            static StatusLine parse(const QString& value);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__STATUS_LINE_H_ */

/**
 * @}
 * @}
 * @}
 */
