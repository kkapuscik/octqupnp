/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__REQUEST_LINE_READER_H_
#define _OCT_HTTP_CORE__REQUEST_LINE_READER_H_

/*-----------------------------------------------------------------------------*/

#include "SocketReader.h"
#include "IOStatus.h"
#include "RequestLine.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class RequestLineReader
        {
        private:
            RequestLine m_requestLine;
            IOStatus m_status;

        public:
            RequestLineReader();

            const RequestLine& requestLine() const
            {
                return m_requestLine;
            }

            IOStatus readRequestLine(Reader* reader);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__REQUEST_LINE_READER_H_ */

/**
 * @}
 * @}
 * @}
 */
