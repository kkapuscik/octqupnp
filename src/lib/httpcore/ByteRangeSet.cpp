/*
 * ByteRangeSet.cpp
 *
 *  Created on: 28-03-2011
 *      Author: saveman
 */

#include "ByteRangeSet.h"

#include <QtCore/QStringList>

namespace Oct
{
    namespace HttpCore
    {
        ByteRangeSet::ByteRangeSet()
        {
            // nothing to do
        }

        ByteRangeSet::ByteRangeSet(QList<ByteRange> ranges) :
            m_ranges(ranges)
        {
            // nothing to do
        }

        Oct::HttpCore::ByteRangeSet ByteRangeSet::parse(QString value)
        {
            QList<ByteRange> ranges;

            if (value.startsWith("bytes=")) {
                value = value.mid(6);
            } else {
                return ByteRangeSet();
            }

            QStringList segments = value.split(',', QString::KeepEmptyParts);
            for (int i = 0; i < segments.size(); ++i) {
                ByteRange range = ByteRange::parse(ByteRange::MODE_REQUEST, segments[i]);

                if (!range.isValid()) {
                    return ByteRangeSet();
                }

                ranges.append(range);
            }

            return ByteRangeSet(ranges);
        }

    }
}
