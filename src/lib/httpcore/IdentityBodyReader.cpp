/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "IdentityBodyReader.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        IdentityBodyReader::IdentityBodyReader(Reader* reader, quint64 length, QObject* parent) :
            BodyReader(parent), m_reader(reader), m_length(length), m_position(0)
        {
            connect(m_reader, SIGNAL( readyRead() ), this, SIGNAL( readyRead() ));
        }

        IdentityBodyReader::~IdentityBodyReader()
        {
            // nothing to do
        }

        qint64 IdentityBodyReader::length() const
        {
            return m_length;
        }

        qint64 IdentityBodyReader::read(char* data, qint64 maxSize)
        {
            if (m_length >= 0) {
                qint64 remaining = m_length - m_position;
                if (remaining == 0) {
                    return -2;
                } else if (remaining < 0) {
                    return -1;
                }

                if (maxSize > remaining) {
                    maxSize = remaining;
                }
            }

            qint64 res = m_reader->read(data, maxSize);

            if (res > 0) {
                m_position += res;
            } else if (res == -2) {
                if (m_length >= 0 && m_position != m_length) {
                    // TODO: message
                    return -1; /* error */
                }
            }

            return res;
        }

    }
}
