/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "StatusCode.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        static const QString ReasonPhrase_100("Continue");
        static const QString ReasonPhrase_101("Switching Protocols");
        static const QString ReasonPhrase_200("OK");
        static const QString ReasonPhrase_201("Created");
        static const QString ReasonPhrase_202("Accepted");
        static const QString ReasonPhrase_203("Non-Authoritative Information");
        static const QString ReasonPhrase_204("No Content");
        static const QString ReasonPhrase_205("Reset Content");
        static const QString ReasonPhrase_206("Partial Content");
        static const QString ReasonPhrase_300("Multiple Choices");
        static const QString ReasonPhrase_301("Moved Permanently");
        static const QString ReasonPhrase_302("Found");
        static const QString ReasonPhrase_303("See Other");
        static const QString ReasonPhrase_304("Not Modified");
        static const QString ReasonPhrase_305("Use Proxy");
        static const QString ReasonPhrase_307("Temporary Redirect");
        static const QString ReasonPhrase_400("Bad Request");
        static const QString ReasonPhrase_401("Unauthorized");
        static const QString ReasonPhrase_402("Payment Required");
        static const QString ReasonPhrase_403("Forbidden");
        static const QString ReasonPhrase_404("Not Found");
        static const QString ReasonPhrase_405("Method Not Allowed");
        static const QString ReasonPhrase_406("Not Acceptable");
        static const QString ReasonPhrase_407("Proxy Authentication Required");
        static const QString ReasonPhrase_408("Request Time-out");
        static const QString ReasonPhrase_409("Conflict");
        static const QString ReasonPhrase_410("Gone");
        static const QString ReasonPhrase_411("Length Required");
        static const QString ReasonPhrase_412("Precondition Failed");
        static const QString ReasonPhrase_413("Request Entity Too Large");
        static const QString ReasonPhrase_414("Request-URI Too Large");
        static const QString ReasonPhrase_415("Unsupported Media Type");
        static const QString ReasonPhrase_416("Requested range not satisfiable");
        static const QString ReasonPhrase_417("Expectation Failed");
        static const QString ReasonPhrase_500("Internal Server Error");
        static const QString ReasonPhrase_501("Not Implemented");
        static const QString ReasonPhrase_502("Bad Gateway");
        static const QString ReasonPhrase_503("Service Unavailable");
        static const QString ReasonPhrase_504("Gateway Time-out");
        static const QString ReasonPhrase_505("HTTP Version not supported");
        static const QString ReasonPhrase_Unknown("Unknown Status Code");

        const QString& StatusCode::reasonPhrase(int code)
        {
            switch (code) {
                case 100:   return ReasonPhrase_100;
                case 101:   return ReasonPhrase_101;
                case 200:   return ReasonPhrase_200;
                case 201:   return ReasonPhrase_201;
                case 202:   return ReasonPhrase_202;
                case 203:   return ReasonPhrase_203;
                case 204:   return ReasonPhrase_204;
                case 205:   return ReasonPhrase_205;
                case 206:   return ReasonPhrase_206;
                case 300:   return ReasonPhrase_300;
                case 301:   return ReasonPhrase_301;
                case 302:   return ReasonPhrase_302;
                case 303:   return ReasonPhrase_303;
                case 304:   return ReasonPhrase_304;
                case 305:   return ReasonPhrase_305;
                case 307:   return ReasonPhrase_307;
                case 400:   return ReasonPhrase_400;
                case 401:   return ReasonPhrase_401;
                case 402:   return ReasonPhrase_402;
                case 403:   return ReasonPhrase_403;
                case 404:   return ReasonPhrase_404;
                case 405:   return ReasonPhrase_405;
                case 406:   return ReasonPhrase_406;
                case 407:   return ReasonPhrase_407;
                case 408:   return ReasonPhrase_408;
                case 409:   return ReasonPhrase_409;
                case 410:   return ReasonPhrase_410;
                case 411:   return ReasonPhrase_411;
                case 412:   return ReasonPhrase_412;
                case 413:   return ReasonPhrase_413;
                case 414:   return ReasonPhrase_414;
                case 415:   return ReasonPhrase_415;
                case 416:   return ReasonPhrase_416;
                case 417:   return ReasonPhrase_417;
                case 500:   return ReasonPhrase_500;
                case 501:   return ReasonPhrase_501;
                case 502:   return ReasonPhrase_502;
                case 503:   return ReasonPhrase_503;
                case 504:   return ReasonPhrase_504;
                case 505:   return ReasonPhrase_505;
                default:    return ReasonPhrase_Unknown;
            }
        }
    }
}
