/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "RequestLineReader.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        static const int MAX_LINE_LENGTH = 1024;

        RequestLineReader::RequestLineReader() :
            m_status(IO_STATUS_WAIT)
        {
            // nothing to do
        }

        IOStatus RequestLineReader::readRequestLine(Reader* reader)
        {
            // TODO: that way?
            if (m_status != IO_STATUS_WAIT) {
                Q_ASSERT(0);
                return IO_STATUS_ERROR;
            }

            char lineBuffer[MAX_LINE_LENGTH];

            int size = reader->readLine(lineBuffer, sizeof(lineBuffer));
            if (size > 0) {
                QString line = QString::fromUtf8(lineBuffer, size - 2);

                m_requestLine = RequestLine::parse(line);

                if (m_requestLine.isValid()) {
                    return m_status = IO_STATUS_READY;
                } else {
                    return m_status = IO_STATUS_WAIT;
                }
            } else if (size == 0) {
                return m_status = IO_STATUS_WAIT;
            } else if (size == -1) {
                return m_status = IO_STATUS_ERROR;
            } else if (size == -2) {
                return m_status = IO_STATUS_END_OF_DATA;
            } else {
                Q_ASSERT(0);
                return m_status = IO_STATUS_ERROR;
            }
        }
    }
}
