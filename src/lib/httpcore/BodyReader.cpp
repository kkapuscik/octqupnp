/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "BodyReader.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        BodyReader::BodyReader(QObject* parent) :
            QObject(parent)
        {
            // nothing to do
        }

        BodyReader::~BodyReader()
        {
            // nothing to do
        }
    }
}
