/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "HeadersReader.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        Oct::Log::Logger<HeadersReader> HeadersReader::Logger("/oct/httpcore/HeadersReader");

        static const int MAX_LINE_LENGTH = 1024;

        HeadersReader::HeadersReader() :
            m_status(IO_STATUS_WAIT)
        {
            // nothing to do
        }

        IOStatus HeadersReader::readHeaders(Reader* reader)
        {
            // TODO: that way?
            if (m_status != IO_STATUS_WAIT) {
                Q_ASSERT(0);
                return IO_STATUS_ERROR;
            }

            char lineBuffer[MAX_LINE_LENGTH];

            for (;;) {
                int size = reader->readLine(lineBuffer, sizeof(lineBuffer));

                Logger.trace(this) << "Read line returned: " << size;

                if (size > 2) {
                    QString line = QString::fromUtf8(lineBuffer, size - 2);

                    if (line.startsWith(' ')) { /* LWS */
                        if (m_currentLine.length() > 0) {
                            /* append to current line */
                            m_currentLine.append(line);
                        } else {
                            /* header line must not start with LWS */
                            return m_status = IO_STATUS_ERROR;
                        }
                    } else {
                        /* parse the current line */
                        if (m_currentLine.length() > 0) {
                            Header header = Header::parse(m_currentLine);
                            if (header.isValid()) {
                                m_headerSet.appendOrSet(header);
                            } else {
                                /* invalid header line */
                                return m_status = IO_STATUS_ERROR;
                            }
                        }

                        /* set new current line */
                        m_currentLine = line;
                    }

                } else if (size == 2) {
                    /* there is only CR+LF */

                    if (m_currentLine.length() > 0) {
                        Header header = Header::parse(m_currentLine);
                        if (header.isValid()) {
                            m_headerSet.appendOrSet(header);
                        } else {
                            /* invalid header line */
                            return m_status = IO_STATUS_ERROR;
                        }

                        m_currentLine.clear();
                    }

                    return m_status = IO_STATUS_READY;
                } else if (size == 0) {
                    return m_status = IO_STATUS_WAIT;
                } else if (size == -1) {
                    return m_status = IO_STATUS_ERROR;
                } else if (size == -2) {
                    return m_status = IO_STATUS_END_OF_DATA;
                } else {
                    Q_ASSERT(0);
                    return m_status = IO_STATUS_ERROR;
                }
            }
        }

    /*-----------------------------------------------------------------------------*/
    }
}
