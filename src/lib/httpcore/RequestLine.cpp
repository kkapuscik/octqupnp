/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "RequestLine.h"

#include "Tools.h"

#include <log/Logger.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        Oct::Log::Logger<RequestLine> RequestLine::Logger("/oct/httpcore/RequestLine");

        RequestLine::RequestLine() :
            m_method(QString::null), m_uri(QString::null), m_version(HTTP_VERSION_INVALID)
        {
            // nothing to do
        }

        RequestLine::RequestLine(const QString& method, const QString& uri, HttpVersion version) :
            m_method(method), m_uri(uri), m_version(version)
        {
            // nothing to do
        }

        RequestLine::~RequestLine()
        {
            // nothing to do
        }

        bool RequestLine::write(QBuffer& buffer) const
        {
            if (!isValid()) {
                return false;
            }

            buffer.write(
                    QString("%1 %2 %3\r\n") .arg(m_method) .arg(m_uri) .arg(Tools::versionString(m_version)) .toLatin1());

            return true;
        }

        RequestLine RequestLine::parse(const QString& value)
        {
            // Request-Line    = Method SP Request-URI SP HTTP-Version CRLF

            int space1 = value.indexOf(' ');
            if (space1 < 0) {
                Logger.strace() << "Could not find first separator of request line";
                return RequestLine();
            }

            int space2 = value.indexOf(' ', space1 + 1);
            if (space2 < 0) {
                Logger.strace() << "Could not find second separator of request line";
                return RequestLine();
            }

            QString methodString = value.mid(0, space1);
            QString uriString = value.mid(space1 + 1, space2 - (space1 + 1));
            QString versionString = value.mid(space2 + 1);

            HttpVersion version = Tools::parseVersion(versionString);
            if (version == HTTP_VERSION_INVALID) {
                Logger.strace() << "Invalid request line http version";
                return RequestLine();
            }

            if (!Tools::isToken(methodString)) {
                Logger.strace() << "Request line method is not a token";
                return RequestLine();
            }
            if (uriString.trimmed() != uriString) {
                Logger.strace() << "Invalid URI in request line: " << uriString;
                return RequestLine();
            }

            return RequestLine(methodString, uriString, version);
        }

        Oct::Log::OutputStream operator <<(Oct::Log::OutputStream dbg, const RequestLine& requestLine)
        {
            if (requestLine.isValid()) {
                return dbg << "[HttpRequestLine" << " method=" << requestLine.method() << " uri="
                        << requestLine.uri() << " version=" << requestLine.version() << "]";
            } else {
                return dbg << "[HttpRequestLine -invalid-]";
            }
        }
    }
}
