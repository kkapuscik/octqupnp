/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "HeadersWriter.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        HeadersWriter::HeadersWriter() :
            m_ioStatus(IO_STATUS_WAIT)
        {
            // nothing to do
        }

        IOStatus HeadersWriter::writeHeaders(Writer* writer)
        {
            // TODO: that way?
            if (m_ioStatus != IO_STATUS_WAIT) {
                Q_ASSERT(0);
                return IO_STATUS_ERROR;
            }

            /* check if we need to prepare buffer */
            if (m_buffer.size() == 0) {
                QList<Header> headers = m_headerSet.allHeaders();
                for (int i = 0; i < headers.size(); ++i) {
                    QString line = QString("%1: %2\r\n"). arg(headers[i].name()). arg(headers[i].value());

                    m_buffer.append(line.toUtf8());
                }

                m_buffer.append("\r\n");
            }

            return m_ioStatus = m_buffer.write(writer);
        }

    }
}
