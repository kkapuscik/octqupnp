/*
 * $Id: OctHttpHeaderSet.cpp 49 2011-01-10 20:00:26Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "HeaderSet.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        // TODO: optimize, remove toLower

        HeaderSet::HeaderSet()
        {
            // nothing to do
        }

        HeaderSet::~HeaderSet()
        {
            // nothing to do
        }

        bool HeaderSet::contains(const QString& name) const
        {
            return m_headersMap.contains(name.toLower());
        }

        QString HeaderSet::value(const QString& name) const
        {
            NameToHeaderMap::ConstIterator iter = m_headersMap.find(name.toLower());
            if (iter != m_headersMap.end()) {
                return iter.value().value();
            } else {
                return QString::null;
            }
        }

        Header HeaderSet::header(const QString& name) const
        {
            NameToHeaderMap::ConstIterator iter = m_headersMap.find(name.toLower());
            if (iter != m_headersMap.end()) {
                return iter.value();
            } else {
                Q_ASSERT(0);
                return Header();
            }
        }

        QList<Header> HeaderSet::allHeaders() const
        {
            return m_headersMap.values();
        }

        void HeaderSet::set(const Header& header)
        {
            m_headersMap.insert(header.name().toLower(), header);
        }

        void HeaderSet::appendOrSet(const Header& header)
        {
            NameToHeaderMap::ConstIterator iter = m_headersMap.find(header.name().toLower());
            if (iter != m_headersMap.end()) {
                set(iter.value().append(header.value()));
            } else {
                set(header);
            }
        }

        void HeaderSet::remove(const QString& name)
        {
            m_headersMap.remove(name.toLower());
        }

        bool HeaderSet::write(QBuffer& buffer) const
        {
            NameToHeaderMap::ConstIterator cur = m_headersMap.begin();
            NameToHeaderMap::ConstIterator end = m_headersMap.end();

            for (; cur != end; ++cur) {
                if (!cur.value().write(buffer)) {
                    return false;
                }
            }

            buffer.write(QString("\r\n").toLatin1());

            return true;
        }
    }
}
