/*
 * $Id: OctHttpHeader.cpp 49 2011-01-10 20:00:26Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Header.h"
#include "Tools.h"

namespace Oct
{
    namespace HttpCore
    {
        Oct::Log::Logger<Header> Header::Logger("/oct/httpcore/Header");

        Header::Header() :
            m_name(QString::null), m_value(QString::null)
        {
        }

        Header::Header(const Header& header) :
            m_name(header.m_name), m_value(header.m_value)
        {
        }

        Header::Header(const QString& name, const QString& value) :
            m_name(name), m_value(value)
        {
            // TODO: checks
        }

        Header::~Header()
        {
        }

        bool Header::isValid() const
        {
            return !(m_name.isNull() || m_value.isNull());
        }

        Header Header::append(const QString& value) const
        {
            return Header(m_name, m_value + "," + value);
        }

        bool Header::write(QBuffer& buffer) const
        {
            if (!isValid()) {
                return false;
            }

            buffer.write(QString("%1: %2\r\n") .arg(m_name) .arg(m_value) .toLatin1());

            return true;
        }

        Header Header::parse(const QString& header)
        {
            int index = header.indexOf(':');
            if (index <= 0) {
                Logger.strace() << "Header parse error - separator not found: " << header;
                return Header();
            }

            QString name = header.mid(0, index).trimmed();
            QString value = header.mid(index + 1).trimmed();

            if (!Tools::isToken(name)) {
                Logger.strace() << "Header parse error - name is not a token: " << header;
                return Header();
            }

            if (!value.isEmpty()) {
                value = Tools::removeLWS(value);
            } else {
                value = QString("");
            }
            if (value.isNull()) {
                Logger.strace() << "Header parse error - failed to remove LWS from value: " << header;
                return Header();
            }

            return Header(name, value);
        }

        Oct::Log::OutputStream operator <<(Oct::Log::OutputStream dbg, const Header& header)
        {
            return dbg << "[HttpHeader name=" << header.name() << " value=" << header.value() << "]";
        }

    }
}
