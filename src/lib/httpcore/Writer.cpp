/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Writer.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        Writer::Writer(QObject* parent) :
            QObject(parent)
        {
            // nothing to do
        }

        Writer::~Writer()
        {
            // nothing to do
        }
    }
}
