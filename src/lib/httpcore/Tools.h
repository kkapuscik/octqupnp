/*
 * $Id: OctHttpTools.h 49 2011-01-10 20:00:26Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__TOOLS_H_
#define _OCT_HTTP_CORE__TOOLS_H_

/*-----------------------------------------------------------------------------*/

#include "Version.h"

#include <log/Logger.h>

#include <QtCore/QString>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class Tools
        {
        private:
            static Oct::Log::Logger<Tools> Logger;

        private:
            Tools()
            {
            }

        public:
            static QString removeLWS(const QString& value);
            static bool isToken(const QString& value);

            static HttpVersion parseVersion(const QString& value);
            static QString versionString(HttpVersion version);

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__TOOLS_H_ */

/**
 * @}
 * @}
 * @}
 */
