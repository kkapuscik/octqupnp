/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__IO_BUFFER_H_
#define _OCT_HTTP_CORE__IO_BUFFER_H_

/*-----------------------------------------------------------------------------*/

#include "Writer.h"
#include "IOStatus.h"

#include <QtCore/QByteArray>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class IOBuffer
        {
        private:
            QByteArray m_data;
            int m_position;

        public:
            IOBuffer() :
                m_position(0)
            {
                // nothing to do
            }

            IOBuffer(const QByteArray& data) :
                m_data(data), m_position(0)
            {
                // nothing to do
            }

            int size() const
            {
                return m_data.size();
            }

            void append(const QByteArray& ba)
            {
                m_data.append(ba);
            }

            void append(const QString& str)
            {
                m_data.append(str);
            }

            void append(const char* str)
            {
                m_data.append(str);
            }

            void append(const char* str, int len)
            {
                m_data.append(str, len);
            }

            void append(char ch)
            {
                m_data.append(ch);
            }

            IOStatus write(Writer* writer)
            {
                for (;;) {
                    Q_ASSERT(m_position <= m_data.size());

                    if (m_position == m_data.size()) {
                        return IO_STATUS_READY;
                    } else {
                        qint64 written = writer->write(m_data.data() + m_position, m_data.size() - m_position);
                        if (written > 0) {
                            m_position += written;
                        } else if (written == 0) {
                            return IO_STATUS_WAIT;
                        } else {
                            return IO_STATUS_ERROR;
                        }
                    }
                }
            }
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__IO_BUFFER_H_ */

/**
 * @}
 * @}
 * @}
 */
