/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "StatusLine.h"
#include "Tools.h"
#include "StatusCode.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        Oct::Log::Logger<StatusLine> StatusLine::Logger("/oct/httpcore/HttpStatusLine");

        StatusLine::StatusLine() :
            m_version(HTTP_VERSION_INVALID), m_code(0), m_reasonPhrase(QString::null)
        {

        }

        StatusLine::StatusLine(HttpVersion version, int code) :
            m_version(version), m_code(code), m_reasonPhrase(StatusCode::reasonPhrase(code))
        {

        }

        StatusLine::StatusLine(HttpVersion version, int code, const QString& reasonPhrase) :
            m_version(version), m_code(code), m_reasonPhrase(reasonPhrase)
        {
        }

        StatusLine::~StatusLine()
        {
            // nothing to do
        }

        bool StatusLine::write(QBuffer& buffer) const
        {
            if (!isValid()) {
                return false;
            }

            buffer.write(QString("%1 %2 %3\r\n") .arg(Tools::versionString(m_version)) .arg(m_code) .arg(
                    m_reasonPhrase) .toLatin1());

            return true;
        }

        StatusLine StatusLine::parse(const QString& value)
        {
            int space1 = value.indexOf(' ');
            if (space1 < 0) {
                Logger.strace() << "Could not find first separator of status line";
                return StatusLine();
            }

            int space2 = value.indexOf(' ', space1 + 1);
            if (space2 < 0) {
                Logger.strace() << "Could not find second separator of status line";
                return StatusLine();
            }

            QString versionString = value.mid(0, space1);
            QString codeString = value.mid(space1 + 1, space2 - (space1 + 1));
            QString messageString = value.mid(space2 + 1);

            HttpVersion version = Tools::parseVersion(versionString);
            if (version == HTTP_VERSION_INVALID) {
                Logger.strace() << "Invalid status line http version: " << versionString;
                return StatusLine();
            }

            bool codeOK = false;
            int code = codeString.toInt(&codeOK, 10);
            if (!codeOK || code < 100 || code > 999) {
                Logger.strace() << "Invalid status line code: " << code << ' ' << codeOK;
                return StatusLine();
            }

            QString message = messageString.trimmed();
            if (message.length() == 0) {
                Logger.strace() << "Invalid status line message string: " << message;
                return StatusLine();
            }

            return StatusLine(version, code, message);
        }
    }
}
