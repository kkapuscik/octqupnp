/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__HEADERS_WRITER_H_
#define _OCT_HTTP_CORE__HEADERS_WRITER_H_

/*-----------------------------------------------------------------------------*/

#include "IOBuffer.h"
#include "Writer.h"
#include "HeaderSet.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class HeadersWriter
        {
        private:
            IOStatus m_ioStatus;
            HeaderSet m_headerSet;
            IOBuffer m_buffer;

        public:
            HeadersWriter();

            const HeaderSet& headers() const
            {
                return m_headerSet;
            }

            HeaderSet& headers()
            {
                return m_headerSet;
            }

            IOStatus writeHeaders(Writer* writer);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__HEADERS_WRITER_H_ */

/**
 * @}
 * @}
 * @}
 */
