/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Reader.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        Reader::Reader(QObject* parent) :
            QObject(parent)
        {
            // nothing to do
        }

        Reader::~Reader()
        {
            // nothing to do
        }
    }
}
