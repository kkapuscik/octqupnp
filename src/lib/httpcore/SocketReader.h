/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore
 * @{
 */

#ifndef _OCT_HTTP_CORE__SOCKET_READER_H_
#define _OCT_HTTP_CORE__SOCKET_READER_H_

/*-----------------------------------------------------------------------------*/

#include "Reader.h"

#include <QtNetwork/QTcpSocket>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpCore
    {
        class SocketReader : public Reader
        {
        Q_OBJECT

        private:
            QTcpSocket* m_socket;
            bool m_channelFinished;

        public:
            SocketReader(QTcpSocket* tcpSocket, QObject* parent = 0);
            virtual ~SocketReader();

            virtual qint64 bytesAvailable() const
            {
                return m_socket->bytesAvailable();
            }

            virtual qint64 read(char* data, qint64 maxSize)
            {
                qint64 bytesRead = m_socket->read(data, maxSize);
                if (bytesRead == 0) {
                    if (m_channelFinished) {
                        bytesRead = -2; /* EOF */
                    }
                }
                return bytesRead;
            }

            virtual qint64 readLine(char* data, qint64 maxSize)
            {
                qint64 bytesRead = m_socket->readLine(data, maxSize);
                if (bytesRead > 0) {
                    /* there must be CR LF in valid line */
                    if (bytesRead >= 2 && data[bytesRead - 2] == '\r' && data[bytesRead - 1] == '\n') {
                        // do nothing
                    } else {
                        bytesRead = -1; /* ERROR */
                    }
                } else if (bytesRead == 0) {
                    if (m_channelFinished) {
                        bytesRead = -2; /* EOF */
                    }
                }
                return bytesRead;
            }

        signals:
            void readyRead();

        private slots:
            void readChannelFinished()
            {
                m_channelFinished = true;
            }
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_CORE__SOCKET_READER_H_ */

/**
 * @}
 * @}
 * @}
 */
