/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpCore    HTTP protocol common core classes
 * @{
 */

#ifndef _OCT_HTTP_CORE__PACKAGE_H_
#define _OCT_HTTP_CORE__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * HTTP protocol common core classes.
     */
    namespace HttpCore
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_HTTP_CORE__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
