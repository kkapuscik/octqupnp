/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_HEADER_ELEMENT_H_
#define _OCT_SOAP__SOAP_HEADER_ELEMENT_H_

/*-----------------------------------------------------------------------------*/

#include "SoapElement.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapHeaderElement : public SoapElement
        {
        public:
            SoapHeaderElement();
            SoapHeaderElement(QDomElement& element);

            /* Returns the uri of the actor attribute of this SOAPHeaderElement. */
            QString actor() const;
            /* Returns the boolean value of the mustUnderstand attribute for this SOAPHeaderElement. */
            bool mustUnderstand() const;

#if 0
            /* Returns the boolean value of the relay attribute for this SOAPHeaderElement. */
            bool relay() const;
            /* Returns the value of the Role attribute of this SOAPHeaderElement. */
            QString role() const;
#endif

            /* Sets the actor associated with this SOAPHeaderElement object to the specified actor. */
            void setActor(const QString& actorURI);
            /* Sets the mustUnderstand attribute for this SOAPHeaderElement object to be either true or false. */
            void setMustUnderstand(bool mustUnderstand);

#if 0
            /* Sets the relay attribute for this SOAPHeaderElement to be either true or false. */
            void setRelay(bool relay);
            /* Sets the Role associated with this SOAPHeaderElement object to the specified Role. */
            void setRole(const QString& uri);
#endif
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_HEADER_ELEMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
