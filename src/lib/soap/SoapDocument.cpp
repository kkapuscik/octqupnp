/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapDocument.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapDocument::SoapDocument() :
            QDomDocument()
        {
            // nothing to do
        }

        SoapDocument::SoapDocument(const QString& prefix) :
            QDomDocument()
        {
            /* add envelope */
            QDomElement element = createElementNS(SoapConst::NAMESPACE_URI, prefix.trimmed() + ":"
                    + SoapConst::ENVELOPE_LOCAL_NAME);
            Q_ASSERT(! element.isNull());

            appendChild(element);
        }

        SoapDocument::SoapDocument(const QDomDocument& document) :
            QDomDocument(document)
        {
            // nothing to do
        }

        QString SoapDocument::soapPrefix() const
        {
            SoapEnvelope envelopeElement = envelope();
            if (envelopeElement.isNull()) {
                return QString::null;
            }

            return envelopeElement.prefix();
        }

        bool SoapDocument::isValid() const
        {
            SoapEnvelope envelopeElement = envelope();
            if (envelopeElement.isNull()) {
                return false;
            }

            return envelopeElement.isValid();
        }

        SoapEnvelope SoapDocument::envelope() const
        {
            QDomElement element = firstChildElement();
            if (element.isNull()) {
                return SoapEnvelope();
            }

            if (element.localName() != SoapConst::ENVELOPE_LOCAL_NAME || element.namespaceURI()
                    != SoapConst::NAMESPACE_URI) {
                return SoapEnvelope();
            }

            return SoapEnvelope(element);
        }
    }
}
