/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_FAULT_DETAIL_H_
#define _OCT_SOAP__SOAP_FAULT_DETAIL_H_

/*-----------------------------------------------------------------------------*/

#include "SoapFaultElement.h"
#include "SoapFaultDetailEntry.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapFaultDetail : public SoapFaultElement
        {
        public:
            SoapFaultDetail();
            SoapFaultDetail(QDomElement& element);

            SoapFaultDetailEntry addEntryNS(const QString& namespaceURI, const QString& qName);
            SoapFaultDetailEntry addEntry(const QString& tagName);

            QList<SoapFaultDetailEntry> entries() const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_FAULT_DETAIL_H_ */

/**
 * @}
 * @}
 * @}
 */
