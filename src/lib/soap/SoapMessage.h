/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_MESSAGE_H_
#define _OCT_SOAP__SOAP_MESSAGE_H_

/*-----------------------------------------------------------------------------*/

#include "SoapDocument.h"
#include "SoapConst.h"

#include <QtCore/QString>
#include <QtCore/QByteArray>

#include <log/Logger.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapMessage
        {
        private:
            static Oct::Log::Logger<SoapMessage> Logger;

        private:
            enum Type {
                TYPE_NULL,
                TYPE_REQUEST,
                TYPE_RESPONSE
            };

        private:
            Type m_type;
            QString m_soapAction;
            SoapDocument m_document;

        private:
            SoapMessage(Type type, const QString& soapAction, const SoapDocument document);

        public:
            SoapMessage();
#if 0
            SoapMessage(const QString& soapAction, const QString& prefix = SoapConst::DEFAULT_PREFIX);
            SoapMessage(const QString& soapAction, const QByteArray& data);
#endif

            QString soapAction() const;
            SoapDocument document() const;

            bool isValid() const;

            bool isFault() const;

            QByteArray toUtf8() const;

        public:
            static SoapMessage parseRequest(const QString& soapAction, const QByteArray& data);
            static SoapMessage createRequest(const QString& soapAction, const QString& prefix = SoapConst::DEFAULT_PREFIX);
            static SoapMessage createResponse(const QString& prefix = SoapConst::DEFAULT_PREFIX);

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_MESSAGE_H_ */

/**
 * @}
 * @}
 * @}
 */
