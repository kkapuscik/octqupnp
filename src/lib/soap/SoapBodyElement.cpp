/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapBodyElement.h"

#include "SoapConst.h"
#include "SoapDocument.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapBodyElement::SoapBodyElement() :
            SoapElement()
        {
            // nothing to do
        }

        SoapBodyElement::SoapBodyElement(QDomElement& element) :
            SoapElement(element)
        {
            // nothing to do
        }

        QList<SoapBodyElement> SoapBodyElement::bodyElements() const
        {
            QList<SoapBodyElement> list;

            if (!isNull()) {
                for (QDomElement elem = firstChildElement(); !elem.isNull(); elem = elem.nextSiblingElement()) {
                    list.append(elem);
                }
            }

            return list;
        }

        SoapBodyElement SoapBodyElement::addBodyElementNS(const QString& namespaceURI, const QString& qName)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomElement element = document.createElementNS(namespaceURI, qName);
            appendChild(element);

            return element;
        }

        SoapBodyElement SoapBodyElement::addBodyElement(const QString& tagName)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomElement element = document.createElement(tagName);
            appendChild(element);

            return element;
        }
    }
}
