/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapFaultDetailEntry.h"

#include "SoapConst.h"
#include "SoapDocument.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapFaultDetailEntry::SoapFaultDetailEntry() :
            SoapElement()
        {
            // nothing to do
        }

        SoapFaultDetailEntry::SoapFaultDetailEntry(QDomElement& element) :
            SoapElement(element)
        {
            // nothing to do
        }

        QList<SoapFaultDetailEntry> SoapFaultDetailEntry::entries() const
        {
            QList<SoapFaultDetailEntry> list;

            if (!isNull()) {
                for (QDomElement elem = firstChildElement(); !elem.isNull(); elem = elem.nextSiblingElement()) {
                    list.append(elem);
                }
            }

            return list;
        }

        SoapFaultDetailEntry SoapFaultDetailEntry::addEntryNS(const QString& namespaceURI, const QString& qName)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomElement element = document.createElementNS(namespaceURI, qName);
            appendChild(element);

            return element;
        }

        SoapFaultDetailEntry SoapFaultDetailEntry::addEntry(const QString& tagName)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomElement element = document.createElement(tagName);
            appendChild(element);

            return element;
        }
    }
}
