/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapFaultDetail.h"

#include "SoapConst.h"
#include "SoapDocument.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapFaultDetail::SoapFaultDetail() :
            SoapFaultElement()
        {
            // nothing to do
        }

        SoapFaultDetail::SoapFaultDetail(QDomElement& element) :
            SoapFaultElement(element)
        {
            Q_ASSERT(namespaceURI().isEmpty());
            Q_ASSERT(tagName() == SoapConst::DETAIL_TAG_NAME);
        }

        QList<SoapFaultDetailEntry> SoapFaultDetail::entries() const
        {
            QList<SoapFaultDetailEntry> list;

            if (!isNull()) {
                for (QDomElement elem = firstChildElement(); !elem.isNull(); elem = elem.nextSiblingElement()) {
                    list.append(elem);
                }
            }

            return list;
        }

        SoapFaultDetailEntry SoapFaultDetail::addEntryNS(const QString& namespaceURI, const QString& qName)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomElement element = document.createElementNS(namespaceURI, qName);
            appendChild(element);

            return element;
        }

        SoapFaultDetailEntry SoapFaultDetail::addEntry(const QString& tagName)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomElement element = document.createElement(tagName);
            appendChild(element);

            return element;
        }

    }
}
