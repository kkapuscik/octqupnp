/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_BODY_H_
#define _OCT_SOAP__SOAP_BODY_H_

/*-----------------------------------------------------------------------------*/

#include "SoapElement.h"
#include "SoapFault.h"
#include "SoapBodyElement.h"

#include <QtCore/QList>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapBody : public SoapElement
        {
        public:
            SoapBody();
            SoapBody(QDomElement& element);

            bool hasFault() const;

            SoapFault fault(bool create = false);

            SoapBodyElement addBodyElementNS(const QString& namespaceURI, const QString& qName);
            SoapBodyElement addBodyElement(const QString& tagName);

            QList<SoapBodyElement> bodyElements() const;

        private:
            SoapFault addFault();
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_BODY_H_ */

/**
 * @}
 * @}
 * @}
 */
