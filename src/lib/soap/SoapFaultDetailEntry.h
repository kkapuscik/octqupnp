/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_FAULT_DETAIL_ENTRY_H_
#define _OCT_SOAP__SOAP_FAULT_DETAIL_ENTRY_H_

/*-----------------------------------------------------------------------------*/

#include "SoapElement.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapFaultDetailEntry : public SoapElement
        {
        public:
            SoapFaultDetailEntry();
            SoapFaultDetailEntry(QDomElement& element);

            SoapFaultDetailEntry addEntryNS(const QString& namespaceURI, const QString& qName);
            SoapFaultDetailEntry addEntry(const QString& tagName);

            QList<SoapFaultDetailEntry> entries() const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_FAULT_DETAIL_ENTRY_H_ */

/**
 * @}
 * @}
 * @}
 */
