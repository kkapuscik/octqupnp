/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapHeaderElement.h"

#include "SoapConst.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapHeaderElement::SoapHeaderElement() :
            SoapElement()
        {
            // nothing to do
        }

        SoapHeaderElement::SoapHeaderElement(QDomElement& element) :
            SoapElement(element)
        {
            Q_ASSERT(!namespaceURI().isEmpty());
        }

        QString SoapHeaderElement::actor() const
        {
            return attributeNS(SoapConst::NAMESPACE_URI, SoapConst::ACTOR_LOCAL_NAME);
        }

        bool SoapHeaderElement::mustUnderstand() const
        {
            return attributeNS(SoapConst::NAMESPACE_URI, SoapConst::MUST_UNDERSTAND_LOCAL_NAME) == "1";
        }

#if 0
        bool SoapHeaderElement::relay() const
        {

        }

        QString SoapHeaderElement::role() const
        {

        }
#endif

        void SoapHeaderElement::setActor(const QString& actorURI)
        {
            Q_ASSERT(!isNull());

            if (actorURI.isEmpty()) {
                removeAttributeNS(SoapConst::NAMESPACE_URI, SoapConst::ACTOR_LOCAL_NAME);
            } else {
                setAttributeNS(SoapConst::NAMESPACE_URI, SoapConst::ACTOR_LOCAL_NAME, actorURI);
            }
        }

        void SoapHeaderElement::setMustUnderstand(bool mustUnderstand)
        {
            Q_ASSERT(!isNull());

            if (!mustUnderstand) {
                removeAttributeNS(SoapConst::NAMESPACE_URI, SoapConst::MUST_UNDERSTAND_LOCAL_NAME);
            } else {
                setAttributeNS(SoapConst::NAMESPACE_URI, SoapConst::MUST_UNDERSTAND_LOCAL_NAME, "1");
            }
        }

#if 0
    void SoapHeaderElement::setRelay(bool relay)
    {

    }

    void SoapHeaderElement::setRole(const QString& uri)
    {

    }
#endif
    }
}
