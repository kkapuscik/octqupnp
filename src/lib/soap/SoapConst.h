/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_CONST_H_
#define _OCT_SOAP__SOAP_CONST_H_

/*-----------------------------------------------------------------------------*/

#include <QtCore/QString>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapConst
        {
        private:
            SoapConst()
            {
            }

        public:
            static const QString NAMESPACE_URI;

            static const QString DEFAULT_PREFIX;

            static const QString ENVELOPE_LOCAL_NAME;
            static const QString HEADER_LOCAL_NAME;
            static const QString BODY_LOCAL_NAME;
            static const QString FAULT_LOCAL_NAME;

            static const QString ENCODING_STYLE_LOCAL_NAME;
            static const QString ACTOR_LOCAL_NAME;
            static const QString MUST_UNDERSTAND_LOCAL_NAME;

            static const QString FAULT_CODE_TAG_NAME;
            static const QString FAULT_ACTOR_TAG_NAME;
            static const QString FAULT_STRING_TAG_NAME;
            static const QString DETAIL_TAG_NAME;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_CONST_H_ */

/**
 * @}
 * @}
 * @}
 */
