/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapFault.h"
#include "SoapConst.h"
#include "SoapDocument.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapFault::SoapFault() :
            SoapBodyElement()
        {
            // nothing to do
        }

        SoapFault::SoapFault(QDomElement& element) :
            SoapBodyElement(element)
        {
            Q_ASSERT(namespaceURI() == SoapConst::NAMESPACE_URI);
            Q_ASSERT(localName() == SoapConst::FAULT_LOCAL_NAME);
        }

#if 0
        <s:Fault>
        <faultcode>s:Client</faultcode>
        <faultstring>UPnPError</faultstring>
        <detail>
        <UPnPError xmlns="urn:schemas-upnp-org:control-1-0">
        <errorCode>error code</errorCode>
        <errorDescription>error string</errorDescription>
        </UPnPError>
        </detail>
        </s:Fault>
#endif

        SoapFaultDetail SoapFault::detail(bool create)
        {
            bool found;
            QDomElement detailElement = findElement(SoapConst::DETAIL_TAG_NAME, &found);
            if (found) {
                return detailElement;
            } else if (create) {
                return addDetail();
            } else {
                return SoapFaultDetail();
            }
        }

        QString SoapFault::faultActor()
        {
            bool found;
            QDomElement element = findElement(SoapConst::FAULT_ACTOR_TAG_NAME, &found);
            if (found) {
                // TODO: is this correct?
                return element.text();
            } else {
                return QString::null;
            }
        }

        QString SoapFault::faultCode()
        {
            bool found;
            QDomElement element = findElement(SoapConst::FAULT_CODE_TAG_NAME, &found);
            if (found) {
                // TODO: is this correct?
                return element.text();
            } else {
                return QString::null;
            }
        }

        QString SoapFault::faultString()
        {
            bool found;
            QDomElement element = findElement(SoapConst::FAULT_STRING_TAG_NAME, &found);
            if (found) {
                // TODO: is this correct?
                return element.text();
            } else {
                return QString::null;
            }
        }

        SoapFaultDetail SoapFault::addDetail()
        {
            /* create element */
            QDomElement element = createElement(SoapConst::DETAIL_TAG_NAME);

            /* add element */
            appendChild(element);

            return element;
        }

        void SoapFault::setFaultActor(const QString& faultActor)
        {
            // TODO: find and remove the old one first

            SoapDocument document = ownerDocument();

            QDomElement faultActorElement = document.createElement(SoapConst::FAULT_ACTOR_TAG_NAME);
            QDomText faultCodeText = document.createTextNode(faultActor);

            appendChild(faultActorElement);
            faultActorElement.appendChild(faultCodeText);
        }

        void SoapFault::setFaultCode(const QString& faultCode)
        {
            // TODO: find and remove the old one first

            SoapDocument document = ownerDocument();

            QDomElement faultCodeElement = document.createElement(SoapConst::FAULT_CODE_TAG_NAME);
            QDomText faultCodeText = document.createTextNode(document.soapPrefix() + ":" + faultCode);

            appendChild(faultCodeElement);
            faultCodeElement.appendChild(faultCodeText);
        }

        void SoapFault::setFaultString(const QString& faultString)
        {
            // TODO: find and remove the old one first

            SoapDocument document = ownerDocument();

            QDomElement faultStringElement = document.createElement(SoapConst::FAULT_STRING_TAG_NAME);
            QDomText faultStringText = document.createTextNode(faultString);

            appendChild(faultStringElement);
            faultStringElement.appendChild(faultStringText);
        }
    }
}
