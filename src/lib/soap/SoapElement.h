/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_ELEMENT_H_
#define _OCT_SOAP__SOAP_ELEMENT_H_

/*-----------------------------------------------------------------------------*/

#include <QtXml/QDomElement>
#include <QtXml/QDomText>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapElement : public QDomElement
        {
        public:
            SoapElement();
            SoapElement(QDomElement& element);

            QString encodingStyle() const;
            void setEncodingStyle(const QString& encodingStyle);

            QDomText appendTextEntry(const QString& text);

        protected:
            bool hasElement(const QString& tagName) const;
            bool hasElement(const QString& namespaceURI, const QString& localName) const;

            QDomElement findElement(const QString& tagName, bool* found);
            QDomElement findElementNS(const QString& namespaceURI, const QString& localName, bool* found);

            QDomElement createElement(const QString& tagName);
            QDomElement createElementNS(const QString& namespaceURI, const QString& prefix, const QString& localName);

            QDomElement createSOAPElement(const QString& localName);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_ELEMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
