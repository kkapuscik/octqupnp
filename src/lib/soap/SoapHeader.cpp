/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapHeader.h"

#include "SoapConst.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapHeader::SoapHeader() :
            SoapElement()
        {
            // nothing to do
        }

        SoapHeader::SoapHeader(QDomElement& element) :
            SoapElement(element)
        {
            Q_ASSERT(namespaceURI() == SoapConst::NAMESPACE_URI);
            Q_ASSERT(localName() == SoapConst::HEADER_LOCAL_NAME);
        }
    }
}
