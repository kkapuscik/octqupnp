/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_DOCUMENT_H_
#define _OCT_SOAP__SOAP_DOCUMENT_H_

/*-----------------------------------------------------------------------------*/

#include "SoapEnvelope.h"
#include "SoapConst.h"

#include <QtXml/QDomDocument>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapDocument : public QDomDocument
        {
        public:
            SoapDocument();
            SoapDocument(const QString& prefix);
            SoapDocument(const QDomDocument& document);

            QString soapPrefix() const;

            bool isValid() const;

            SoapEnvelope envelope() const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_DOCUMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
