/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap     SOAP protocol common core elements
 * @{
 */

#ifndef _OCT_SOAP__PACKAGE_H_
#define _OCT_SOAP__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * SOAP protocol common core elements.
     */
    namespace Soap
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_SOAP__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
