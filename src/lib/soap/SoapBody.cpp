/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapBody.h"

#include "SoapConst.h"
#include "SoapDocument.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapBody::SoapBody() :
            SoapElement()
        {
            // nothing to do
        }

        SoapBody::SoapBody(QDomElement& element) :
            SoapElement(element)
        {
            Q_ASSERT(namespaceURI() == SoapConst::NAMESPACE_URI);
            Q_ASSERT(localName() == SoapConst::BODY_LOCAL_NAME);
        }

        bool SoapBody::hasFault() const
        {
            return hasElement(SoapConst::NAMESPACE_URI, SoapConst::FAULT_LOCAL_NAME);
        }

        SoapFault SoapBody::fault(bool create)
        {
            bool found;

            QDomElement element = findElementNS(SoapConst::NAMESPACE_URI, SoapConst::FAULT_LOCAL_NAME, &found);
            if (found) {
                return SoapFault(element);
            } else if (create) {
                return addFault();
            } else {
                return SoapFault();
            }
        }

        SoapFault SoapBody::addFault()
        {
            /* create element */
            QDomElement faultElement = createSOAPElement(SoapConst::FAULT_LOCAL_NAME);

            /* add element */
            appendChild(faultElement);

            return faultElement;
        }

        QList<SoapBodyElement> SoapBody::bodyElements() const
        {
            QList < SoapBodyElement > list;

            if (!isNull()) {
                for (QDomElement elem = firstChildElement(); !elem.isNull(); elem = elem.nextSiblingElement()) {
                    list.append(elem);
                }
            }

            return list;
        }

        SoapBodyElement SoapBody::addBodyElementNS(const QString& namespaceURI, const QString& qName)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomElement element = document.createElementNS(namespaceURI, qName);
            appendChild(element);

            return element;
        }

        SoapBodyElement SoapBody::addBodyElement(const QString& tagName)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomElement element = document.createElement(tagName);
            appendChild(element);

            return element;
        }
    }
}
