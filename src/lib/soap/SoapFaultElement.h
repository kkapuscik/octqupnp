/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_FAULT_ELEMENT_H_
#define _OCT_SOAP__SOAP_FAULT_ELEMENT_H_

/*-----------------------------------------------------------------------------*/

#include "SoapElement.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapFaultElement : public SoapElement
        {
        public:
            SoapFaultElement();
            SoapFaultElement(QDomElement& element);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_FAULT_ELEMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
