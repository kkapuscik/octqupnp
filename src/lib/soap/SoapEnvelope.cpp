/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapEnvelope.h"

#include "SoapConst.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapEnvelope::SoapEnvelope() :
            SoapElement()
        {
            // nothing to do
        }

        SoapEnvelope::SoapEnvelope(QDomElement& element) :
            SoapElement(element)
        {
            Q_ASSERT(namespaceURI() == SoapConst::NAMESPACE_URI);
            Q_ASSERT(localName() == SoapConst::ENVELOPE_LOCAL_NAME);
        }

        SoapBody SoapEnvelope::addBody()
        {
            /* create element */
            QDomElement bodyElement = createSOAPElement(SoapConst::BODY_LOCAL_NAME);

            /* add element */
            appendChild(bodyElement).toElement();

            return bodyElement;
        }

        SoapHeader SoapEnvelope::addHeader()
        {
            /* create element */
            QDomElement headerElement = createSOAPElement(SoapConst::HEADER_LOCAL_NAME);

            SoapBody soapBody = body(true);
            Q_ASSERT(!soapBody.isNull());

            insertBefore(headerElement, soapBody);

            return headerElement;
        }

        SoapBody SoapEnvelope::body(bool create)
        {
            bool found;

            QDomElement element = findElementNS(SoapConst::NAMESPACE_URI, SoapConst::BODY_LOCAL_NAME, &found);
            if (found) {
                return element;
            } else if (create) {
                return addBody();
            } else {
                return SoapBody();
            }
        }

        SoapHeader SoapEnvelope::header(bool create)
        {
            bool found;

            QDomElement element = findElementNS(SoapConst::NAMESPACE_URI, SoapConst::HEADER_LOCAL_NAME, &found);
            if (found) {
                return element;
            } else if (create) {
                return addHeader();
            } else {
                return SoapHeader();
            }
        }

        bool SoapEnvelope::isValid()
        {
            if (isNull()) {
                return false;
            }

            // TODO

            return true;
        }
    }
}
