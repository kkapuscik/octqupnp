/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_FAULT_H_
#define _OCT_SOAP__SOAP_FAULT_H_

/*-----------------------------------------------------------------------------*/

#include "SoapBodyElement.h"
#include "SoapFaultDetail.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapFault : public SoapBodyElement
        {
        public:
            SoapFault();
            SoapFault(QDomElement& element);

            SoapFaultDetail detail(bool create = false);
            QString faultActor();
            QString faultCode();
            QString faultString(); // TODO: locale

            void setFaultActor(const QString& faultActor);
            void setFaultCode(const QString& faultCode); // TODO: it is namespace prefixed, another method?
            void setFaultString(const QString& faultString); // TODO: locale)

        private:
            SoapFaultDetail addDetail();
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_FAULT_H_ */

/**
 * @}
 * @}
 * @}
 */
