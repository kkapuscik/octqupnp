/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapMessage.h"
#include "SoapConst.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        Oct::Log::Logger<SoapMessage> SoapMessage::Logger("/oct/soap/SoapMessage");

        SoapMessage::SoapMessage() :
            m_type(TYPE_NULL), m_soapAction(QString::null), m_document()
        {
            // nothing to do
        }

        SoapMessage::SoapMessage(Type type, const QString& soapAction, const SoapDocument document) :
            m_type(type), m_soapAction(soapAction), m_document(document)
        {
            // nothing to do
        }

        QString SoapMessage::soapAction() const
        {
            return m_soapAction;
        }

        SoapDocument SoapMessage::document() const
        {
            return m_document;
        }

        bool SoapMessage::isValid() const
        {
            if (m_type == TYPE_NULL) {
                return false;
            }

            if (m_type == TYPE_REQUEST) {
                if (m_soapAction.isEmpty()) {
                    return false;
                }
            }

            return m_document.isValid();
        }

        QByteArray SoapMessage::toUtf8() const
        {
            return m_document.toString().toUtf8();
        }

        bool SoapMessage::isFault() const
        {
            SoapEnvelope envElem = m_document.envelope();
            if (envElem.isNull()) {
                return false;
            }

            SoapBody bodyElem = envElem.body();
            if (bodyElem.isNull()) {
                return false;
            }

            return bodyElem.hasFault();
        }

        SoapMessage SoapMessage::parseRequest(const QString& soapAction, const QByteArray& data)
        {
            QString errorMsg;
            int errorLine;
            int errorColumn;
            SoapDocument document;

            if (document.setContent(data, true, &errorMsg, &errorLine, &errorColumn)) {
                Logger.strace() << "Document successfully parsed.";
                return SoapMessage(TYPE_REQUEST, soapAction, document);
            } else {
                Logger.swarn() << "Document parsing failed: error=" << errorMsg << " line=" << errorLine << " column="
                        << errorColumn;
                return SoapMessage();
            }
        }

        SoapMessage SoapMessage::createRequest(const QString& soapAction, const QString& prefix)
        {
            return SoapMessage(TYPE_REQUEST, soapAction, SoapDocument(prefix));
        }

        SoapMessage SoapMessage::createResponse(const QString& prefix)
        {
            return SoapMessage(TYPE_RESPONSE, QString(), SoapDocument(prefix));
        }

    }
}
