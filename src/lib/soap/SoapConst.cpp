/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapConst.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        const QString SoapConst::NAMESPACE_URI("http://schemas.xmlsoap.org/soap/envelope/");

        const QString SoapConst::DEFAULT_PREFIX("SOAP-Env");

        const QString SoapConst::ENVELOPE_LOCAL_NAME("Envelope");
        const QString SoapConst::HEADER_LOCAL_NAME("Header");
        const QString SoapConst::BODY_LOCAL_NAME("Body");
        const QString SoapConst::FAULT_LOCAL_NAME("Fault");

        const QString SoapConst::ENCODING_STYLE_LOCAL_NAME("encodingStyle");
        const QString SoapConst::ACTOR_LOCAL_NAME("actor");
        const QString SoapConst::MUST_UNDERSTAND_LOCAL_NAME("mustUnderstand");

        const QString SoapConst::FAULT_CODE_TAG_NAME("faultcode");
        const QString SoapConst::FAULT_ACTOR_TAG_NAME("faultactor");
        const QString SoapConst::FAULT_STRING_TAG_NAME("faultstring");
        const QString SoapConst::DETAIL_TAG_NAME("detail");
    }
}
