/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_BODY_ELEMENT_H_
#define _OCT_SOAP__SOAP_BODY_ELEMENT_H_

/*-----------------------------------------------------------------------------*/

#include "SoapElement.h"

#include <QtCore/QList>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapBodyElement : public SoapElement
        {
        public:
            SoapBodyElement();
            SoapBodyElement(QDomElement& element);

            SoapBodyElement addBodyElementNS(const QString& namespaceURI, const QString& qName);
            SoapBodyElement addBodyElement(const QString& tagName);

            QList<SoapBodyElement> bodyElements() const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_BODY_ELEMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
