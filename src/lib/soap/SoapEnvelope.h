/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Soap
 * @{
 */

#ifndef _OCT_SOAP__SOAP_ENVELOPE_H_
#define _OCT_SOAP__SOAP_ENVELOPE_H_

/*-----------------------------------------------------------------------------*/

#include "SoapBody.h"
#include "SoapHeader.h"
#include "SoapElement.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        class SoapEnvelope : public SoapElement
        {
        public:
            SoapEnvelope();
            SoapEnvelope(QDomElement& element);

            /* Returns the SOAPBody object associated with this SOAPEnvelope object. */
            SoapBody body(bool create = false);
            /* Returns the SOAPHeader object for this SOAPEnvelope object. */
            SoapHeader header(bool create = false);

            bool isValid();

        private:
            /* Creates a SOAPBody object and sets it as the SOAPBody object for this SOAPEnvelope object. */
            SoapBody addBody();
            /* Creates a SOAPHeader object and sets it as the SOAPHeader object for this SOAPEnvelope object. */
            SoapHeader addHeader();

#if 0
            Name createName(java.lang.String localName)
            Creates a new Name object initialized with the given local name.
            Name createName(java.lang.String localName, java.lang.String prefix, java.lang.String uri)
            Creates a new Name object initialized with the given local name, namespace prefix, and namespace URI.
#endif

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP__SOAP_ENVELOPE_H_ */

/**
 * @}
 * @}
 * @}
 */
