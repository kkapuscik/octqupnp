/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapFaultElement.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapFaultElement::SoapFaultElement() :
            SoapElement()
        {
            // nothing to do
        }

        SoapFaultElement::SoapFaultElement(QDomElement& element) :
            SoapElement(element)
        {
            // nothing to do
        }
    }
}
