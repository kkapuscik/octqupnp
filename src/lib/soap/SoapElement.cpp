/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapElement.h"

#include "SoapConst.h"
#include "SoapDocument.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Soap
    {
        SoapElement::SoapElement() :
            QDomElement()
        {
            // nothing to do
        }

        SoapElement::SoapElement(QDomElement& element) :
            QDomElement(element)
        {
            // nothing to do
        }

        QString SoapElement::encodingStyle() const
        {
            return attributeNS(SoapConst::NAMESPACE_URI, SoapConst::ENCODING_STYLE_LOCAL_NAME);
        }

        void SoapElement::setEncodingStyle(const QString& encodingStyle)
        {
            Q_ASSERT(!isNull());

            if (encodingStyle.isEmpty()) {
                removeAttributeNS(SoapConst::NAMESPACE_URI, SoapConst::ENCODING_STYLE_LOCAL_NAME);
            } else {
                setAttributeNS(SoapConst::NAMESPACE_URI, SoapConst::ENCODING_STYLE_LOCAL_NAME, encodingStyle);
            }
        }

        QDomElement SoapElement::findElementNS(const QString& namespaceURI, const QString& localName, bool* found)
        {
            for (QDomElement childElement = firstChildElement(); !childElement.isNull(); childElement
                    = childElement.nextSiblingElement()) {

                if (childElement.namespaceURI() == namespaceURI && childElement.localName() == localName) {

                    if (found != NULL) {
                        *found = true;
                    }

                    return childElement;
                }
            }

            if (found != NULL) {
                *found = false;
            }

            return QDomElement();
        }

        QDomElement SoapElement::findElement(const QString& tagName, bool* found)
        {
            for (QDomElement childElement = firstChildElement(); !childElement.isNull(); childElement
                    = childElement.nextSiblingElement()) {

                if (childElement.namespaceURI().isEmpty() && childElement.tagName() == tagName) {

                    if (found != NULL) {
                        *found = true;
                    }

                    return childElement;
                }
            }

            if (found != NULL) {
                *found = false;
            }

            return QDomElement();
        }

        QDomElement SoapElement::createElement(const QString& tagName)
        {
            /* get and check document */
            SoapDocument document(ownerDocument());
            Q_ASSERT(!document.isNull());

            /* create element */
            QDomElement element = document.createElement(tagName);
            Q_ASSERT(!element.isNull());

            return element;
        }

        QDomElement SoapElement::createSOAPElement(const QString& localName)
        {
            /* get and check document */
            SoapDocument document(ownerDocument());
            Q_ASSERT(!document.isNull());

            /* create element */
            QDomElement element = document.createElementNS(SoapConst::NAMESPACE_URI, document.soapPrefix() + ":"
                    + localName);
            Q_ASSERT(!element.isNull());

            return element;
        }

        QDomElement SoapElement::createElementNS(const QString& namespaceURI, const QString& prefix,
                const QString& localName)
        {
            /* get and check document */
            SoapDocument document(ownerDocument());
            Q_ASSERT(!document.isNull());

            /* create element */
            QDomElement element = document.createElementNS(namespaceURI, prefix + ":" + localName);
            Q_ASSERT(!element.isNull());

            return element;
        }

        bool SoapElement::hasElement(const QString& namespaceURI, const QString& localName) const
        {
            for (QDomElement childElement = firstChildElement(); !childElement.isNull(); childElement
                    = childElement.nextSiblingElement()) {

                if (childElement.namespaceURI() == namespaceURI && childElement.localName() == localName) {

                    return true;
                }
            }

            return false;
        }

        bool SoapElement::hasElement(const QString& tagName) const
        {
            return hasElement("", tagName);
        }

        QDomText SoapElement::appendTextEntry(const QString& text)
        {
            SoapDocument document = ownerDocument();
            Q_ASSERT(!document.isNull());

            QDomText textNode = document.createTextNode(text);

            appendChild(textNode);

            return textNode;
        }

    }
}
