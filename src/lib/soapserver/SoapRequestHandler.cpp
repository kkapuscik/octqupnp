/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SoapRequestHandler.h"

#include <httpserver/ByteArrayBodyPart.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace SoapServer
    {
        Oct::Log::Logger<SoapRequestHandler> SoapRequestHandler::Logger("/oct/soapserver/SoapRequestHandler");

        SoapRequestHandler::SoapRequestHandler(QObject* parent) :
            Oct::HttpServer::SimpleRequestHandler(parent)
        {
            // nothing to do
        }

        SoapRequestHandler::~SoapRequestHandler()
        {
            // nothing to do
        }

        bool SoapRequestHandler::processRequest(const Oct::HttpServer::Request& request,
                Oct::HttpServer::Response& response, const QByteArray& requestBody)
        {
            Logger.trace(this) << "Process request: " << request.requestLine();

            /* check for SOAP ACTION header */
            if (!request.headers().contains("SOAPACTION")) {
                Logger.trace(this) << "Missing SOAPACTION header";
                return false;
            }

            QString soapAction = request.headers().value("SOAPACTION");
            // TODO: polish this
            if (soapAction.length() > 2 && soapAction.startsWith('"') && soapAction.endsWith('"')) {
                soapAction = soapAction.mid(1, soapAction.length() - 2);
            } else {
                Logger.trace(this) << "Invalid SOAPACTION header: " << soapAction;
                return false;
            }

            Oct::Soap::SoapMessage requestMessage = Oct::Soap::SoapMessage::parseRequest(soapAction, requestBody);

            if (!requestMessage.isValid()) {
                Logger.trace(this) << "Invalid request SOAP message";
                return false;
            }

            if (requestMessage.isFault()) {
                Logger.trace(this) << "Fault request SOAP message";
                return false;
            }

            Oct::Soap::SoapMessage responseMessage = processMessage(request.pathRemainder(), requestMessage);
            if (!responseMessage.isValid()) {
                Logger.trace(this) << "Invalid response SOAP message";
                return false;
            }

#if 0
            HTTP/1.1 200 OK
            CONTENT-LENGTH: bytes in body
            CONTENT-TYPE: text/xml; charset="utf-8"
            DATE: when response was generated
            EXT:
            SERVER: OS/version UPnP/1.0 product/version
            <?xml version="1.0"?>
            <s:Envelope
            xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
            s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            <s:Body>
            <u:actionNameResponse xmlns:u="urn:schemas-upnp-org:service:serviceType:v">
            <argumentName>out arg value</argumentName>
            other out args and their values go here, if any
            </u:actionNameResponse>
            </s:Body>
            </s:Envelope>
#endif

            QByteArray responseBody = responseMessage.toUtf8();

            if (!responseMessage.isFault()) {
                response.setStatusLine(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_200_OK);
            } else {
                response.setStatusLine(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_500_INTERNAL_SERVER_ERROR);
            }

            response.headers().set("EXT", "");
            response.appendBodyPart(new Oct::HttpServer::ByteArrayBodyPart("text/xml; charset=\"utf-8\"", responseBody));

            // TODO: Date header

#if 0
            DATE: when response was generated
            SERVER: OS/version UPnP/1.0 product/version
#endif

            return true;
        }
    }
}
