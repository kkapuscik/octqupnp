/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup SoapServer
 * @{
 */

#ifndef _OCT_SOAP_SERVER__SOAP_REQUEST_HANDLER_H_
#define _OCT_SOAP_SERVER__SOAP_REQUEST_HANDLER_H_

/*-----------------------------------------------------------------------------*/

#include <log/Logger.h>
#include <httpserver/SimpleRequestHandler.h>

#include <soap/SoapMessage.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace SoapServer
    {
        class SoapRequestHandler : public Oct::HttpServer::SimpleRequestHandler
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<SoapRequestHandler> Logger;

        public:
            SoapRequestHandler(QObject* parent = 0);
            virtual ~SoapRequestHandler();

            virtual Oct::Soap::SoapMessage processMessage(const QString& pathRemainder, Oct::Soap::SoapMessage& requestMessage) = 0;

        protected:
            virtual bool processRequest(const Oct::HttpServer::Request& request, Oct::HttpServer::Response& response,
                    const QByteArray& requestBody);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_SOAP_SERVER__SOAP_REQUEST_HANDLER_H_ */

/**
 * @}
 * @}
 * @}
 */
