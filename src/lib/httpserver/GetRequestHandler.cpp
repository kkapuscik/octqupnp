/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "GetRequestHandler.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        Oct::Log::Logger<GetRequestHandler> GetRequestHandler::Logger("/oct/httpserver/GetRequestHandler");

        GetRequestHandler::GetRequestHandler(QObject* parent) :
            RequestHandler(parent)
        {
            // nothing to do
        }

        GetRequestHandler::~GetRequestHandler()
        {
            // nothing to do
        }
    }
}
