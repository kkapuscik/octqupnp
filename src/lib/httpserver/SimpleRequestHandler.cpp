/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SimpleRequestHandler.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        SimpleRequestHandler::SimpleRequestHandler(QObject* parent) :
            RequestHandler(parent)
        {
            // nothing to do
        }

        SimpleRequestHandler::~SimpleRequestHandler()
        {
            // nothing to do
        }

        //virtual bool processRequest(const Request& request, Response& response, const QByteArray& requestBody) = 0;


        RequestHandler::ProcessingResult SimpleRequestHandler::processRequest(const Request& request, Response& response)
        {
            Oct::HttpCore::BodyReader* bodyReader = request.bodyReader();
            if (bodyReader == NULL || bodyReader->length() < 0) {
                // TODO
                return PROCESSING_ERROR;
            }

            // TODO: optimize

            for (;;) {
                QByteArray tmpArray(4 * 1024, Qt::Uninitialized);
                int bytesRead = bodyReader->read(tmpArray.data(), tmpArray.capacity());
                if (bytesRead > 0) {
                    tmpArray.resize(bytesRead);
                    m_requestBody.append(tmpArray);
                } else if (bytesRead == 0) {
                    return PROCESSING_NOT_FINISHED;
                } else if (bytesRead == -1) {
                    /* error */
                    // TODO
                    return PROCESSING_ERROR;
                } else if (bytesRead == -2) {
                    if (processRequest(request, response, m_requestBody)) {
                        return PROCESSING_FINISHED;
                    } else {
                        return PROCESSING_ERROR;
                    }
                } else {
                    // TODO
                    return PROCESSING_ERROR;
                }
            }
        }
    }
}
