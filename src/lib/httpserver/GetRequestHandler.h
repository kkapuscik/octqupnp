/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__GET_HANDLER_H_
#define _OCT_HTTP_SERVER__GET_HANDLER_H_

/*-----------------------------------------------------------------------------*/

#include "RequestHandler.h"
#include "ByteArrayBodyPart.h"
#include "FileBodyPart.h"

#include <log/Logger.h>

#include <QtCore/QFileInfo>
#include <QtCore/QDir>
#include <QtCore/QUrl>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class GetRequestHandler : public RequestHandler
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<GetRequestHandler> Logger;

        private:
            QFileInfo m_fileInfo;

        public:
            GetRequestHandler(QObject* parent = 0);
            virtual ~GetRequestHandler();

            virtual bool analyzeRequestLine(const Oct::HttpCore::RequestLine& requestLine)
            {
                QString fixed = QUrl::fromPercentEncoding(requestLine.uri().toUtf8());
                QUrl url = QUrl::fromEncoded(requestLine.uri().toUtf8());

                Logger.trace(this) << "Get handler - url:" << url.toString() << "path:" << url.path() << "fixed:"
                        << fixed;

                if (!url.isValid() || !url.host().isEmpty() || !url.path().startsWith('/')) {
                    return false;
                }

                QString path = QDir::homePath() + url.path();

                Logger.trace(this) << "Get handler - path: " << path;
                // TODO: fixme

                m_fileInfo = QFileInfo(path);

                Logger.trace(this) << "Get handler - selected file:" << m_fileInfo.absoluteFilePath();

                // TODO

                return true;
            }

            virtual bool analyzeHeaders(const Oct::HttpCore::HeaderSet& /*headers*/)
            {
                // TODO
                return true;
            }

            virtual bool need100Continue()
            {
                // TODO
                return false;
            }

            virtual ProcessingResult processRequest(Request& request, Response& response)
            {
                // TODO:

                response.headers().set("Server", "OctHttpServer/1.0");
                response.headers().set("Connection", "close");

#if 0
                if (request.requestLine().method() != "GET") {
                    response.setStatusLine(StatusLine(request.requestLine().version(), 405, "Method Not Allowed"));
                    response.headers().set("Allow", "GET,HEAD");
                    return PROCESSING_FINISHED;
                }
#endif

                if (!m_fileInfo.exists()) {
                    response.setStatusLine(Oct::HttpCore::StatusLine(request.requestLine().version(), 404, "Not Found"));
                    return PROCESSING_FINISHED;
                }

                if (request.headers().contains("Range")) {
                    response.setStatusLine(Oct::HttpCore::StatusLine(request.requestLine().version(), 406, "Not Acceptable"));
                    return PROCESSING_FINISHED;
                }

                if (m_fileInfo.isDir()) {
                    response.setStatusLine(Oct::HttpCore::StatusLine(request.requestLine().version(), 200, "OK"));

                    QDir dir = QDir(m_fileInfo.absoluteFilePath());
                    QStringList entries = dir.entryList(QDir::AllEntries, QDir::DirsFirst);

                    QString str;

                    str.append("<html>\n");
                    str.append("<head>\n");
                    str.append("<title>OCT Http Server</title>\n");
                    str.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n");
                    str.append("</head>\n");
                    str.append("<body>\n");

                    str.append("<h1>Currently in: ");
                    str.append(m_fileInfo.absolutePath());
                    str.append("</h1>\n");

                    str.append("<ul>\n");

                    QUrl url = QUrl::fromEncoded(request.requestLine().uri().toUtf8());

                    for (int i = 0; i < entries.size(); ++i) {
                        QString entryUrlStr = url.path();
                        if (!entryUrlStr.endsWith('/')) {
                            entryUrlStr.append('/');
                        }
                        entryUrlStr.append(entries[i]);

                        QUrl entryUrl;

                        entryUrl.setPath(entryUrlStr);

                        Logger.trace(this) << "Entry URL" << entryUrl.toString();

                        str.append("<li>");
                        str.append("<a href=\"");
                        str.append(entryUrl.toEncoded());
                        str.append("\">");
                        str.append(entries[i]);
                        str.append("</li>\n");
                    }

                    str.append("</ul>\n");

                    str.append("</body>\n");
                    str.append("</html>\n");

                    response.appendBodyPart(new ByteArrayBodyPart("\"text/html; charset=utf-8\"", str.toUtf8()));
                } else if (m_fileInfo.isFile()) {
                    response.setStatusLine(Oct::HttpCore::StatusLine(request.requestLine().version(), 200, "OK"));
                    QString extension = m_fileInfo.suffix().toLower();
                    QString contentType;

                    if (extension == "jpg" || extension == "jpeg") {
                        contentType = "image/jpeg";
                    } else if (extension == "png") {
                        contentType = "image/png";
                    } else {
                        contentType = "application/octet-stream";
                    }

                    response.appendBodyPart(new FileBodyPart(contentType, m_fileInfo.absoluteFilePath()));
                } else {
                    response.setStatusLine(Oct::HttpCore::StatusLine(request.requestLine().version(), 500, "Internal Server Error"));
                }

                // TODO

                return PROCESSING_FINISHED;
            }

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__GET_HANDLER_H_ */

/**
 * @}
 * @}
 * @}
 */
