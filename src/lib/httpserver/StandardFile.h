/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__STANDARD_FILE_H_
#define _OCT_HTTP_SERVER__STANDARD_FILE_H_

/*-----------------------------------------------------------------------------*/

#include "VfsFile.h"

#include <QtCore/QFile>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class StandardFile : public VfsFile
        {
        private:
            QFile m_file;
            QString m_mimeType;

        public:
            StandardFile(const QString& fileName, const QString& mimeType = QString());
            virtual ~StandardFile();

            virtual bool isValid() const;

            virtual qint64 read(char* buffer, qint64 size);
            virtual bool seek(qint64 pos);
            virtual QString contentType() const;
            virtual qint64 length() const;
       };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__STANDARD_FILE_H_ */

/**
 * @}
 * @}
 * @}
 */
