/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__VFS_FILE_H_
#define _OCT_HTTP_SERVER__VFS_FILE_H_

/*-----------------------------------------------------------------------------*/

#include <QtCore/QtGlobal>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        /**
         * Virtual file system file.
         */
        class VfsFile
        {
        public:
            /** Read operation result. */
            enum ReadResult
            {
                /** No more data to read currently available. */
                READ_WAIT = 0,
                /** Read error occurred. */
                READ_ERROR = -1,
                /** Read reached end-of-file. */
                READ_EOF = -2
            };

        public:
            /**
             * Constructs new virtual file.
             */
            VfsFile();

            /**
             * Destroys new virtual file.
             */
            virtual ~VfsFile();

            /**
             * Reads data from file.
             *
             * @param[in] buffer
             *      Buffer where data will be stored.
             * @param[in] size
             *      Size of the buffer.
             *
             * @return
             * Number of bytes read or ReadStatus.
             */
            virtual qint64 read(char* buffer, qint64 size) = 0;

            /**
             * Moves file pointer to given offset.
             *
             * @param[in] pos
             *      Requested seek position.
             *
             * @return
             * True on success, false on error.
             */
            virtual bool seek(qint64 pos) = 0;

            /**
             * Returns content type (mime type) of file.
             *
             * @return
             * Type of content inside the file.
             */
            virtual QString contentType() const = 0;

            /**
             * Returns length of the file in bytes.
             *
             * @return
             * Length in bytes or -1 if unknown.
             */
            virtual qint64 length() const = 0;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__VFS_FILE_H_ */

/**
 * @}
 * @}
 * @}
 */
