/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__VFS_REQUEST_HANDLER_PROVIDER_H_
#define _OCT_HTTP_SERVER__VFS_REQUEST_HANDLER_PROVIDER_H_

/*-----------------------------------------------------------------------------*/

#include "RequestHandlerProvider.h"
#include "Vfs.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class VfsRequestHandlerProvider : public RequestHandlerProvider
        {
        Q_OBJECT

        private:
            Vfs* m_vfs; // TODO: shared pointer?

        public:
            VfsRequestHandlerProvider(Vfs* vfs, QObject* parent = 0);

            virtual RequestHandler* createHandler(void);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__VFS_REQUEST_HANDLER_PROVIDER_H_ */

/**
 * @}
 * @}
 * @}
 */
