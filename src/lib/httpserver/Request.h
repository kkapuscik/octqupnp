/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__REQUEST_H_
#define _OCT_HTTP_SERVER__REQUEST_H_

/*-----------------------------------------------------------------------------*/

#include <QtCore/QObject>

#include <httpcore/RequestLineReader.h>
#include <httpcore/HeadersReader.h>
#include <httpcore/BodyReader.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class Request: public QObject
        {
        Q_OBJECT

        private:
            Oct::HttpCore::RequestLineReader m_requestLineReader;
            Oct::HttpCore::HeadersReader m_headersReader;
            Oct::HttpCore::BodyReader* m_bodyReader;
            QString m_pathRemainder;

        public:
            Request(QObject* parent);
            virtual ~Request();

            const Oct::HttpCore::RequestLine& requestLine() const
            {
                return m_requestLineReader.requestLine();
            }

            const Oct::HttpCore::HeaderSet& headers() const
            {
                return m_headersReader.headers();
            }

            const QString& pathRemainder() const
            {
                return m_pathRemainder;
            }

            void setPathRemainder(const QString& pathRemainder)
            {
                m_pathRemainder = pathRemainder;
            }

            Oct::HttpCore::IOStatus readRequestLine(Oct::HttpCore::Reader* reader)
            {
                return m_requestLineReader.readRequestLine(reader);
            }

            Oct::HttpCore::IOStatus readHeaders(Oct::HttpCore::Reader* reader)
            {
                return m_headersReader.readHeaders(reader);
            }

            bool createBodyReader(Oct::HttpCore::Reader* reader);

            Oct::HttpCore::BodyReader* bodyReader() const
            {
                return m_bodyReader;
            }
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__REQUEST_H_ */

/**
 * @}
 * @}
 * @}
 */
