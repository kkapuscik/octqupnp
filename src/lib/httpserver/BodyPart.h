/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__BODY_PART_H_
#define _OCT_HTTP_SERVER__BODY_PART_H_

/*-----------------------------------------------------------------------------*/

#include <httpcore/IOStatus.h>
#include <httpcore/Writer.h>

#include <QtCore/QObject>
#include <QtCore/QString>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class BodyPart : public QObject
        {
        Q_OBJECT

        private:
            QString m_contentType;

        public:
            BodyPart(const QString& contentType, QObject* parent = 0);

            virtual Oct::HttpCore::IOStatus write(Oct::HttpCore::Writer* writer) = 0;

            virtual qint64 length() const = 0;

            const QString& contentType() const
            {
                return m_contentType;
            }
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__BODY_PART_H_ */

/**
 * @}
 * @}
 * @}
 */
