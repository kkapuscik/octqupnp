/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ByteArrayVfsFile.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        ByteArrayVfsFile::ByteArrayVfsFile(const QString& contentType, const QByteArray& data) :
            VfsFile(), m_contentType(contentType), m_data(data), m_readPosition(0)
        {
            // nothing to do
        }

        ByteArrayVfsFile::~ByteArrayVfsFile()
        {
            // do nothing
        }

        bool ByteArrayVfsFile::seek(qint64 pos)
        {
            if (pos >= 0 && pos <= length()) {
                m_readPosition = pos;
                return true;
            }
            return false;
        }

        qint64 ByteArrayVfsFile::read(char* buffer, qint64 size)
        {
            int todo = length() - m_readPosition;

            if (todo > 0) {
                if (todo > size) {
                    todo = size;
                }

                const char* ptr = m_data.constData();

                memcpy(buffer, ptr + m_readPosition, todo);
                m_readPosition += todo;

                return todo;
            } else if (todo == 0) {
                return READ_EOF;
            } else {
                return READ_ERROR;
            }
        }

        QString ByteArrayVfsFile::contentType() const
        {
            return m_contentType;
        }

        qint64 ByteArrayVfsFile::length() const
        {
            return m_data.size();
        }
    }
}
