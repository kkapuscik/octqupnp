/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__REQUEST_HANDLER_H_
#define _OCT_HTTP_SERVER__REQUEST_HANDLER_H_

/*-----------------------------------------------------------------------------*/

#include "Request.h"
#include "Response.h"

#include <QtCore/QObject>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class RequestHandler : public QObject
        {
        Q_OBJECT

        public:
            enum ProcessingResult
            {
                PROCESSING_NOT_FINISHED,
                PROCESSING_FINISHED,
                PROCESSING_ERROR
            };

        public:
            RequestHandler(QObject* parent = 0);
            virtual ~RequestHandler();

            virtual bool analyzeRequestLine(const Oct::HttpCore::RequestLine& requestLine)
            {
                /* do nothing - report success - not needed in most cases */
                return true;
            }

            virtual bool analyzeHeaders(const Oct::HttpCore::HeaderSet& headers)
            {
                /* do nothing - report success - not needed in most cases */
                return true;
            }

            virtual bool checkExpectations(const Request& request, Response& response)
            {
                /* 100-continue is not needed in most cases */
                return false;
            }

            virtual ProcessingResult processRequest(const Request& request, Response& response) = 0;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__REQUEST_HANDLER_H_ */

/**
 * @}
 * @}
 * @}
 */
