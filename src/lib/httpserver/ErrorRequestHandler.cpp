/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ErrorRequestHandler.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        ErrorRequestHandler::ErrorRequestHandler(QObject* parent) :
            RequestHandler(parent)
        {
            // nothing to do
        }

        ErrorRequestHandler::~ErrorRequestHandler()
        {
            // nothing to do
        }

        RequestHandler::ProcessingResult ErrorRequestHandler::processRequest(const Request& request, Response& response)
        {
            response.setStandardError(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_403_FORBIDDEN);
            return PROCESSING_FINISHED;
        }
    }
}
