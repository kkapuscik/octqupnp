/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "VfsRequestHandlerProvider.h"

#include "VfsRequestHandler.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        VfsRequestHandlerProvider::VfsRequestHandlerProvider(Vfs* vfs, QObject* parent) :
            RequestHandlerProvider(parent), m_vfs(vfs)
        {
            Q_ASSERT(vfs != NULL);
        }

        RequestHandler* VfsRequestHandlerProvider::createHandler(void)
        {
            return new VfsRequestHandler(m_vfs, this);
        }
    }
}
