/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__VFS_H_
#define _OCT_HTTP_SERVER__VFS_H_

/*-----------------------------------------------------------------------------*/

#include "VfsFile.h"

#include <QtCore/QObject>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        /**
         * Virtual file system of HTTP server.
         */
        class Vfs : public QObject
        {
        Q_OBJECT

        public:
            /**
             * Constructs new HTTP server virtual file system.
             *
             * @param[in] parent
             *      File system parent object.
             */
            Vfs(QObject* parent = 0);

            virtual VfsFile* open(const QString& path) = 0;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__VFS_H_ */

/**
 * @}
 * @}
 * @}
 */
