/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__SIMPLE_REQUEST_HANDLER_H_
#define _OCT_HTTP_SERVER__SIMPLE_REQUEST_HANDLER_H_

/*-----------------------------------------------------------------------------*/

#include "RequestHandler.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class SimpleRequestHandler: public RequestHandler
        {
        Q_OBJECT

        private:
            QByteArray m_requestBody;

        public:
            SimpleRequestHandler(QObject* parent = 0);
            virtual ~SimpleRequestHandler();

            virtual ProcessingResult processRequest(const Request& request, Response& response);

        protected:
            virtual bool processRequest(const Request& request, Response& response, const QByteArray& requestBody) = 0;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__SIMPLE_REQUEST_HANDLER_H_ */

/**
 * @}
 * @}
 * @}
 */
