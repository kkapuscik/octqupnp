/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "BodyPart.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        BodyPart::BodyPart(const QString& contentType, QObject* parent) :
            QObject(parent), m_contentType(contentType.trimmed())
        {
            // nothing to do
            Q_ASSERT(m_contentType.length() > 0);
        }
    }
}
