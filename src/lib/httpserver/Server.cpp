/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Server.h"
#include "Connection.h"

#include <QtNetwork/QTcpSocket>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        Oct::Log::Logger<Server> Server::Logger("/oct/httpserver/Server");

        Server::Server(QObject* parent) :
            QObject(parent), m_tcpServer(NULL), m_port(0), m_portFallback(false)
        {
            m_tcpServer = new QTcpServer(this);

            connect(m_tcpServer, SIGNAL( newConnection() ), this, SLOT( acceptNewConnection() ));
        }

        Server::~Server()
        {
            // do nothing
        }

        bool Server::isStarted() const
        {
            return m_tcpServer->isListening();
        }

        bool Server::start()
        {
            Logger.trace(this) << "start()";

            if (m_tcpServer->isListening()) {
                Logger.trace(this) << "start() - failed, already listening";
                return false;
            }

            do {
                /* try to listen on given port or any port (if 0) */
                if (m_tcpServer->listen(QHostAddress::Any, m_port)) {
                    break;
                }

                /* if there is fallback and default port was used previously */
                if (m_portFallback && m_port != 0) {
                    /* try to bind now on any port */
                    if (m_tcpServer->listen(QHostAddress::Any, 0)) {
                        break;
                    }
                }

                // TODO: anything more?
            } while (0);

            bool rv = m_tcpServer->isListening();

            Logger.trace(this) << "start() => " << rv;
            Logger.trace(this) << "start() server is listening at: " << m_tcpServer->serverAddress().toString() << ":" << m_tcpServer->serverPort();

            return rv;
        }

        void Server::stop()
        {
            Logger.trace(this) << "stop()";

            m_tcpServer->close();
        }

        void Server::acceptNewConnection()
        {
            for (;;) {
                // TODO: limits

                QTcpSocket* clientSocket = m_tcpServer->nextPendingConnection();
                if (clientSocket == NULL) {
                    break;
                }

                Logger.trace(this) << "acceptNewConnection() connection from: " << clientSocket->peerAddress().toString() << ":"
                        << clientSocket->peerPort();

                // TODO: add to list?
                // TODO: do not use server as parent QObject? The design should
                //       be changed for something with multithread support
                //       the server is used inside connection to create handlers
                //       and this must be properly cleaned on destroy
                new Connection(this, clientSocket);
            }
        }
    }
}
