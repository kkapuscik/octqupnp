/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__VFS_REQUEST_HANDLER_H_
#define _OCT_HTTP_SERVER__VFS_REQUEST_HANDLER_H_

/*-----------------------------------------------------------------------------*/

#include "Vfs.h"
#include "RequestHandler.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class VfsRequestHandler : public RequestHandler
        {
        Q_OBJECT

        private:
            Vfs* m_vfs; // TODO: shared pointer?

        public:
            VfsRequestHandler(Vfs* vfs, QObject* parent = 0);
            virtual ~VfsRequestHandler();

            virtual ProcessingResult processRequest(const Request& request, Response& response);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__VFS_REQUEST_HANDLER_H_ */

/**
 * @}
 * @}
 * @}
 */
