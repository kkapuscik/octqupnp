/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__FILE_BODY_PART_H_
#define _OCT_HTTP_SERVER__FILE_BODY_PART_H_

/*-----------------------------------------------------------------------------*/

#include "BodyPart.h"

#include <httpcore/IOBuffer.h>

#include <QtCore/QFile>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class FileBodyPart : public BodyPart
        {
        Q_OBJECT

        private:
            QFile m_file;
            Oct::HttpCore::IOBuffer m_buffer;

        public:
            FileBodyPart(const QString& contentType, const QString& fileName, QObject* parent = 0);

            virtual Oct::HttpCore::IOStatus write(Oct::HttpCore::Writer* writer);

            virtual qint64 length() const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__FILE_BODY_PART_H_ */

/**
 * @}
 * @}
 * @}
 */
