/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__VFS_FILE_BODY_PART_H_
#define _OCT_HTTP_SERVER__VFS_FILE_BODY_PART_H_

/*-----------------------------------------------------------------------------*/

#include "BodyPart.h"
#include "VfsFile.h"

#include <httpcore/IOBuffer.h>
#include <log/Logger.h>

#include <QtCore/QSharedPointer>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class VfsFileBodyPart : public BodyPart
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<VfsFileBodyPart> Logger;

        public:
            typedef QSharedPointer<VfsFile> VirtualFilePointer;

        private:
            VirtualFilePointer m_file;
            Oct::HttpCore::IOBuffer m_buffer;
            bool m_offsetSet;
            qint64 m_lengthLeft;
            qint64 m_offset;
            qint64 m_length;

        public:
            VfsFileBodyPart(const VirtualFilePointer& filePointer, qint64 offset, qint64 length, QObject* parent = 0);

            virtual Oct::HttpCore::IOStatus write(Oct::HttpCore::Writer* writer);

            virtual qint64 length() const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__VFS_FILE_BODY_PART_H_ */

/**
 * @}
 * @}
 * @}
 */
