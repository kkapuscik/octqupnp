/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__SERVER_H_
#define _OCT_HTTP_SERVER__SERVER_H_

/*-----------------------------------------------------------------------------*/

#include "Vfs.h"
#include "RequestHandlerManager.h"

#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtNetwork/QTcpServer>

#include <QtNetwork/QNetworkInterface> // TODO: remove

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class Server : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<Server> Logger;

        private:
            QTcpServer* m_tcpServer;
            quint16 m_port;
            bool m_portFallback;
            RequestHandlerManager m_requestHandlerManager;

        public:
            Server(QObject* parent = 0);
            virtual ~Server();

            void setPort(quint16 port, bool fallback = false)
            {
                m_port = port;
                m_portFallback = fallback;
            }

            quint16 port() const
            {
                return m_tcpServer->serverPort();
            }

            QString urlBase(QString address = QString::null) const
            {
                if (address.isNull()) {
                    // TODO: remove, it should be client-address-dependent

                    QList<QHostAddress> allAddresses = QNetworkInterface::allAddresses();
                    for (int i = 0; i < allAddresses.size(); ++i) {
                        Logger.trace(this) << "Host address: " << allAddresses.at(i).toString();

                        if (allAddresses[i].protocol() == QAbstractSocket::IPv4Protocol) {
                            if (allAddresses[i] != QHostAddress(QHostAddress::LocalHost)) {
                                address = allAddresses[i].toString();
                                break;
                            }
                        }
                    }

                    if (address.isNull()) {
                        address = "127.0.0.1";
                    }
                }

                if (port() == 80) {
                    return QString("http://%1/").arg(address);
                } else {
                    return QString("http://%1:%2/").arg(address).arg(port());
                }
            }

            bool isStarted() const;
            bool start();
            void stop();

            RequestHandlerManager& requestHandlerManager()
            {
                return m_requestHandlerManager;
            }

            const RequestHandlerManager& requestHandlerManager() const
            {
                return m_requestHandlerManager;
            }

        private slots:
            void acceptNewConnection();

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__SERVER_H_ */

/**
 * @}
 * @}
 * @}
 */
