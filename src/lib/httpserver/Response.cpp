/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Response.h"

#include "ByteArrayBodyPart.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        Oct::Log::Logger<Response> Response::Logger("/oct/httpserver/Response");

        static const QString ERROR_PAGE_TEMPLATE(
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
                    "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n"
                    "<head>\n"
                    "<title>Error %1</title>\n"
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
                    "</head>\n"
                    "<body>\n"
                    "<center>\n"
                    "<h1>Error occurred</h1>\n"
                    "<h2>Error: %1 - %2</h2>\n"
                    "</center>\n"
                    "</body>\n"
                    "</head>\n");

        Response::Response(QObject* parent) :
            QObject(parent)
        {
            // nothing to do
        }

        Response::~Response()
        {
            // nothing to do
        }

        Oct::HttpCore::IOStatus Response::writeBody(Oct::HttpCore::Writer* writer)
        {
            // TODO: auto switching to multi-part body

            for (;;) {
                if (m_bodyParts.empty()) {
                    return Oct::HttpCore::IO_STATUS_READY;
                }

                BodyPart* part = m_bodyParts.first();

                Logger.trace(this) << "Writing body part: " << part << " length: " << part->length();

                Oct::HttpCore::IOStatus ioStatus = part->write(writer);

                Logger.trace(this) << "Writing body part: " << part << " ioStatus " << ioStatus;

                if (ioStatus == Oct::HttpCore::IO_STATUS_READY) {
                    delete m_bodyParts.takeFirst();
                } else {
                    return ioStatus;
                }
            }
        }

        void Response::setStandardError(Oct::HttpCore::RequestLine requestLine, int code)
        {
            setStatusLine(Oct::HttpCore::StatusLine(requestLine.version(), code));

            if (requestLine.method() == "GET" || requestLine.method() == "POST") {
                QString reasonPhrase = statusLine().reasonPhrase();

                appendBodyPart(new ByteArrayBodyPart("\"text/html; charset=utf-8\"",
                        QString(ERROR_PAGE_TEMPLATE).arg(code).arg(reasonPhrase).toLatin1()));
            }
        }

    }
}
