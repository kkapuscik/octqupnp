/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Connection.h"

#include "Server.h"
#include "ErrorRequestHandler.h"

/*-----------------------------------------------------------------------------*/

// TODO: error handling. On errors we MUST send error message or at least the code

namespace Oct
{
    namespace HttpServer
    {
        Oct::Log::Logger<Connection> Connection::Logger("/oct/httpserver/Connection");

        Connection::Connection(Server* server, QTcpSocket* clientSocket) :
            QObject(server), m_tcpSocket(clientSocket), m_request(NULL), m_response(NULL), m_handler(NULL)
        {
            m_tcpSocket->setParent(this);

            m_socketReader = new Oct::HttpCore::SocketReader(clientSocket, this);
            m_socketWriter = new Oct::HttpCore::SocketWriter(clientSocket, this);

            connect(m_socketReader, SIGNAL( readyRead() ), this, SLOT( process() ));
            connect(m_socketWriter, SIGNAL( readyWrite() ), this, SLOT( process() ));

            setState(CONNECTION_STATE_READING_REQUEST_LINE);
        }

        Connection::~Connection()
        {
            Logger.trace(this) << "Connection destroyed";
        }

        bool Connection::createHandler()
        {
            Logger.trace(this) << "HttpServerConnection.createHandler";

            const Oct::HttpCore::RequestLine& requestLine = m_request->requestLine();
            const RequestHandlerManager& manager = server()->requestHandlerManager();

            m_handler = manager.createHandler(*m_request);
            if (m_handler == NULL) {
                m_handler = new ErrorRequestHandler(this);
            }

            Q_ASSERT(m_handler != NULL);

            /* take ownership */
            m_handler->setParent(this);

            if (m_handler->analyzeRequestLine(requestLine)) {
                return true;
            }

            return false;
        }

        bool Connection::readRequestLine()
        {
            Logger.trace(this) << "HttpServerConnection.readRequestLine";

            // TODO: request line URI normalization

            switch (m_request->readRequestLine(m_socketReader)) {
                case Oct::HttpCore::IO_STATUS_READY:
                    Logger.trace(this) << "Request line ready:" << m_request->requestLine();

                    if (!createHandler()) {
                        setState(CONNECTION_STATE_ERROR);
                        return true;
                    }

                    setState(CONNECTION_STATE_READING_HEADERS);
                    return false;

                case Oct::HttpCore::IO_STATUS_END_OF_DATA:
                    setState(CONNECTION_STATE_FINISHED);

                    // TODO: ?
                    this->deleteLater();
                    return true;

                case Oct::HttpCore::IO_STATUS_WAIT:
                    // nothing to do
                    return true;

                default:
                    Q_ASSERT(0);
                    /* no break */
                case Oct::HttpCore::IO_STATUS_ERROR:
                    setState(CONNECTION_STATE_ERROR);
                    return true;
            }
        }

        bool Connection::readHeaders()
        {
            Logger.trace(this) << "HttpServerConnection.readHeaders";

            Oct::HttpCore::IOStatus status = m_request->readHeaders(m_socketReader);
            switch (status) {
                case Oct::HttpCore::IO_STATUS_READY:
                {
                    QList<Oct::HttpCore::Header> headers = m_request->headers().allHeaders();
                    for (int i = 0; i < headers.size(); ++i) {
                        Logger.trace(this) << "Header ready: " << headers[i];
                    }

                    // TODO: analyze connection headers

                    if (m_handler->analyzeHeaders(m_request->headers())) {
                        setState(CONNECTION_STATE_CHECKING_100_CONTINUE);
                        return false;
                    }

                    setState(CONNECTION_STATE_ERROR);
                    return true;
                }

                case Oct::HttpCore::IO_STATUS_WAIT:
                    // nothing to do
                    return true;

                case Oct::HttpCore::IO_STATUS_END_OF_DATA:
                case Oct::HttpCore::IO_STATUS_ERROR:
                    setState(CONNECTION_STATE_ERROR);
                    Logger.info(this) << "Header reading failed with status: " << status;
                    return true;

                default:
                    setState(CONNECTION_STATE_ERROR);
                    Q_ASSERT(0);
                    return true;
            }
        }

        bool Connection::check100Continue()
        {
            if (m_request->headers().contains("Expect")) {
                if (m_handler->checkExpectations(*m_request, *m_response)) {
                    m_response->set100ContinueVersion(m_request->requestLine().version());
                    setState(CONNECTION_STATE_WRITING_100_CONTINUE);
                    return false;
                } else {
                    m_response->setStandardError(m_request->requestLine(),
                            Oct::HttpCore::HTTP_STATUS_CODE_417_EXPECTATION_FAILED);

                    // TODO: this is ugly... to be polished
                    m_response->headers().set("Connection", "close");

                    setState(CONNECTION_STATE_WRITING_RESPONSE);
                    return false;
                }
            } else {
                setState(CONNECTION_STATE_READING_BODY);
                return false;
            }
        }

        bool Connection::write100Continue()
        {
            switch (m_response->write100Continue(m_socketWriter)) {
                case Oct::HttpCore::IO_STATUS_READY:
                    setState(CONNECTION_STATE_READING_BODY);
                    return false;

                case Oct::HttpCore::IO_STATUS_WAIT:
                    // nothing to do
                    return true;

                case Oct::HttpCore::IO_STATUS_ERROR:
                default:
                    setState(CONNECTION_STATE_ERROR);
                    return true;
            }
        }

        bool Connection::prepareBodyReader()
        {
            if (m_request->createBodyReader(m_socketReader)) {
                setState(CONNECTION_STATE_PROCESSING_REQUEST);
            } else {
                // tODO: is that correct?
                setState(CONNECTION_STATE_ERROR);
            }
            return false;
        }

        bool Connection::processRequest()
        {
            switch (m_handler->processRequest(*m_request, *m_response)) {
                case RequestHandler::PROCESSING_NOT_FINISHED:
                    return true;

                case RequestHandler::PROCESSING_FINISHED:
                    setState(CONNECTION_STATE_WRITING_RESPONSE);
                    return false;

                case RequestHandler::PROCESSING_ERROR:
                    setState(CONNECTION_STATE_ERROR);
                    return true;

                default:
                    Q_ASSERT(0);
                    setState(CONNECTION_STATE_ERROR);
                    return true;
            }
        }

        bool Connection::writeResponse()
        {
            // TODO: calculate body length, set connection close if not available

            // TODO: !!!!!!!!!!!!!!!!!!!

            m_response->setContentType();

            qint64 length = m_response->contentLength();
            if (length >= 0) {
                m_response->headers().set("Content-Length", QString("%1").arg(length));
            } else {
                m_response->headers().set("Connection", "close");
            }

            if (!m_response->statusLine().isValid()) {
                // TODO: ERROR ERROR ERROR - message, assert?

                m_response->setStandardError(m_request->requestLine(),
                        Oct::HttpCore::HTTP_STATUS_CODE_500_INTERNAL_SERVER_ERROR);
            }

            m_response->headers().set("Server", "OctHttpServer/0.1");

            // TODO: remove it when implementation is ready
            m_response->headers().set("Connection", "close");

            setState(CONNECTION_STATE_WRITING_STATUS_LINE);
            return false;
        }

        bool Connection::writeStatusLine()
        {
            switch (m_response->writeStatusLine(m_socketWriter)) {
                case Oct::HttpCore::IO_STATUS_READY:
                    setState(CONNECTION_STATE_WRITING_HEADERS);
                    return false;

                case Oct::HttpCore::IO_STATUS_WAIT:
                    // nothing to do
                    return true;

                case Oct::HttpCore::IO_STATUS_ERROR:
                default:
                    setState(CONNECTION_STATE_ERROR);
                    return true;
            }
        }

        bool Connection::writeHeaders()
        {
            switch (m_response->writeHeaders(m_socketWriter)) {
                case Oct::HttpCore::IO_STATUS_READY:
                    setState(CONNECTION_STATE_WRITING_BODY);
                    return false;

                case Oct::HttpCore::IO_STATUS_WAIT:
                    // nothing to do
                    return true;

                case Oct::HttpCore::IO_STATUS_ERROR:
                default:
                    setState(CONNECTION_STATE_ERROR);
                    return true;
            }
        }

        bool Connection::writeBody()
        {
            switch (m_response->writeBody(m_socketWriter)) {
                case Oct::HttpCore::IO_STATUS_READY:
                    // TODO: go to idle if connection is not closing
                    setState(CONNECTION_STATE_FLUSH);
                    return false;

                case Oct::HttpCore::IO_STATUS_WAIT:
                    // nothing to do
                    return true;

                case Oct::HttpCore::IO_STATUS_ERROR:
                default:
                    setState(CONNECTION_STATE_ERROR);
                    return true;
            }
        }

        bool Connection::flush()
        {
            // TODO

            Logger.trace(this) << "Bytes to write: " << m_tcpSocket->bytesToWrite();

            if (!m_tcpSocket->isValid()) {
                setState(CONNECTION_STATE_ERROR);
                return true;
            }

            if (m_tcpSocket->bytesToWrite() > 0) {
                return true;
            } else {
                setState(CONNECTION_STATE_FINISHED);
                return true;
            }
        }

        void Connection::process()
        {
            Logger.trace(this) << "HttpConnection.process()";

            for (bool isFinished = false; !isFinished;) {
                switch (m_state) {
                    case CONNECTION_STATE_READING_REQUEST_LINE:
                        isFinished = readRequestLine();
                        break;

                    case CONNECTION_STATE_READING_HEADERS:
                        isFinished = readHeaders();
                        break;

                    case CONNECTION_STATE_CHECKING_100_CONTINUE:
                        isFinished = check100Continue();
                        break;

                    case CONNECTION_STATE_WRITING_100_CONTINUE:
                        isFinished = write100Continue();
                        break;

                    case CONNECTION_STATE_READING_BODY:
                        isFinished = prepareBodyReader();
                        break;

                    case CONNECTION_STATE_PROCESSING_REQUEST:
                        isFinished = processRequest();
                        break;

                    case CONNECTION_STATE_WRITING_RESPONSE:
                        isFinished = writeResponse();
                        break;

                    case CONNECTION_STATE_WRITING_STATUS_LINE:
                        isFinished = writeStatusLine();
                        break;

                    case CONNECTION_STATE_WRITING_HEADERS:
                        isFinished = writeHeaders();
                        break;

                    case CONNECTION_STATE_WRITING_BODY:
                        isFinished = writeBody();
                        break;

                    case CONNECTION_STATE_FLUSH:
                        isFinished = flush();
                        break;

                    default:
                        setState(CONNECTION_STATE_ERROR);
                        return;
                }
            }
        }

        void Connection::setState(State newState)
        {
            Logger.trace(this) << "HttpServerConnection.setState() oldState:" << m_state << " newState:" << newState;

            m_state = newState;

            switch (m_state) {
                case CONNECTION_STATE_READING_REQUEST_LINE:
                    Q_ASSERT(m_request == NULL);
                    Q_ASSERT(m_response == NULL);
                    m_request = new Request(this);
                    m_response = new Response(this);
                    break;

                case CONNECTION_STATE_FINISHED:
                case CONNECTION_STATE_ERROR:
                    // self delete
                    deleteLater();
                    break;

                default:
                    // nothing to do
                    break;
            }
        }

        Server* Connection::server()
        {
            Server* server = qobject_cast<Server*> (parent());
            Q_ASSERT(server != NULL);
            return server;
        }

    }
}
