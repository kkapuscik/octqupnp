/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__BYTE_ARRAY_BODY_PART_H_
#define _OCT_HTTP_SERVER__BYTE_ARRAY_BODY_PART_H_

/*-----------------------------------------------------------------------------*/

#include "BodyPart.h"

#include <httpcore/IOBuffer.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class ByteArrayBodyPart : public BodyPart
        {
        Q_OBJECT

        private:
            Oct::HttpCore::IOBuffer m_buffer;

        public:
            ByteArrayBodyPart(const QString& contentType, const QByteArray& buffer, QObject* parent = 0);

            virtual Oct::HttpCore::IOStatus write(Oct::HttpCore::Writer* writer);

            virtual qint64 length() const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__BYTE_ARRAY_BODY_PART_H_ */

/**
 * @}
 * @}
 * @}
 */
