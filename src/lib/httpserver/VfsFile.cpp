/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "VfsFile.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        VfsFile::VfsFile()
        {
            // nothing to do
        }

        VfsFile::~VfsFile()
        {
            // nothing to do
        }
    }
}
