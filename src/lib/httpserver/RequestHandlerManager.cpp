/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "RequestHandlerManager.h"

#include "GetRequestHandler.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        Oct::Log::Logger<RequestHandlerManager> RequestHandlerManager::Logger("/oct/httpserver/RequestHandlerManager");

        RequestHandlerManager::RequestHandlerManager(QObject* parent) :
            QObject(parent)
        {
            // nothing to do
        }

        bool RequestHandlerManager::registerHandlerProvider(const QString& path, RequestHandlerProvider* provider)
        {
            Q_ASSERT(provider != NULL);

            /* quick check */
            if (!path.startsWith("/") || !path.endsWith("/")) {
                Logger.error(this) << "registerHandlerProvider() - invalid path (invalid start/end): " << path;
                return false;
            }

            /* check if path is valid */
            if (path != "/") {
                /* remove '/' from start & end */
                QString tmpPath = path.mid(1, path.length() - 2);

                /* split path to segments */
                QStringList pathSegments = tmpPath.split('/', QString::KeepEmptyParts);

                /* safety check */
                if (pathSegments.length() == 0) {
                    Logger.error(this) << "registerHandlerProvider() - invalid path (no parts): " << path;
                    return false;
                }

                /* checking parts */
                // TODO: make it better
                for (int i = 0; i < pathSegments.length(); ++i) {
                    /* there must be some characters in segment (protection from "//") */
                    if (pathSegments[i].length() == 0) {
                        Logger.error(this) << "registerHandlerProvider() - invalid path (empty part): " << path;
                        return false;
                    }

                    /* only alphanumeric characters and '_', '-' allowed */
                    for (int j = 0; j < pathSegments[i].length(); ++j) {
                        char c = pathSegments[i].at(j).toLatin1();
                        if (c >= '0' && c <= '9') {
                            continue;
                        }
                        if (c >= 'A' && c <= 'Z') {
                            continue;
                        }
                        if (c >= 'a' && c <= 'z') {
                            continue;
                        }
                        if (c == '_' || c == '-') {
                            continue;
                        }
                        Logger.error(this) << "registerHandlerProvider() - invalid path (bad characters): " << path;
                        return false;
                    }
                }
            }

            /* check if we already have one */
            if (m_providers.contains(path)) {
                Logger.error(this) << "registerHandlerProvider() - path already registered: " << path;
                return false;
            }

            /* put into collection */
            m_providers.insert(path, provider);
            provider->setParent(this);

            Logger.trace(this) << "registerHandlerProvider() - registered provider path: " << path << " provider: " << provider;

            return true;
        }

        RequestHandler* RequestHandlerManager::createHandler(Request& request) const
        {
            RequestHandlerProvider* provider = NULL;
            RequestHandler* handler = NULL;
            QString pathRemainder;

            QString uri = request.requestLine().uri();

            Logger.trace(this) << "createHandler() - uri: " << uri;

            if (uri.startsWith('/')) {
                QMapIterator<QString,RequestHandlerProvider*> iterator(m_providers);

                iterator.toBack();
                while (iterator.hasPrevious()) {
                    iterator.previous();

                    const QString& providerPath = iterator.key();

                    Logger.trace(this) << "createHandler() - checking provider: " << providerPath << " provider: " << iterator.value();

                    if (uri.startsWith(providerPath)) {
                        provider = iterator.value();
                        pathRemainder = uri.mid(providerPath.length());
                        break;
                    }
                }
            }

            if (provider != NULL) {
                Logger.trace(this) << "createHandler() - found provider: " << provider;
                Logger.trace(this) << "createHandler() - path remainder: " << pathRemainder;

                request.setPathRemainder(pathRemainder);
                handler = provider->createHandler();

                Logger.trace(this) << "createHandler() - created handler: " << handler;
            }

            return handler;
        }
    }
}
