/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ByteArrayBodyPart.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        ByteArrayBodyPart::ByteArrayBodyPart(const QString& contentType, const QByteArray& buffer, QObject* parent) :
            BodyPart(contentType, parent), m_buffer(buffer)
        {
            // nothing to do
        }

        Oct::HttpCore::IOStatus ByteArrayBodyPart::write(Oct::HttpCore::Writer* writer)
        {
            return m_buffer.write(writer);
        }

        qint64 ByteArrayBodyPart::length() const
        {
            return m_buffer.size();
        }
    }
}
