/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "VfsRequestHandler.h"
#include "VfsFileBodyPart.h"

#include <httpcore/ByteRangeSet.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        VfsRequestHandler::VfsRequestHandler(Vfs* vfs, QObject* parent) :
            RequestHandler(parent), m_vfs(vfs)
        {
            Q_ASSERT(vfs != NULL);
        }

        VfsRequestHandler::~VfsRequestHandler()
        {
            // nothing to do
        }

        RequestHandler::ProcessingResult VfsRequestHandler::processRequest(const Request& request, Response& response)
        {
            do {
                if (request.requestLine().method() != "GET" && request.requestLine().method() != "HEAD") {
                    response.setStandardError(request.requestLine(),
                            Oct::HttpCore::HTTP_STATUS_CODE_405_METHOD_NOT_ALLOWED);
                    break;
                }

                const QString& path = request.pathRemainder();

                VfsFile* file = m_vfs->open(path);
                if (file == NULL) {
                    response.setStandardError(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_404_NOT_FOUND);
                    break;
                }

                // 406 - server produced response that may not be accepted by client
                // 416 - server was not able to send valid response
                if (request.headers().contains("Range")) {
                    /* parse range header */
                    Oct::HttpCore::ByteRangeSet rangeSet = Oct::HttpCore::ByteRangeSet::parse(request.headers().value("Range"));

                    if (rangeSet.size() == 0) {
                        /* no valid ranges */

                        response.setStandardError(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_400_BAD_REQUEST);
                        break;

                    } else if (rangeSet.size() == 1) {
                        /* single range processing*/

                        Oct::HttpCore::ByteRange range = rangeSet.range(0);

                        qint64 fileLength = file->length();

                        /* unknown or zero length - range cannot be processed */
                        if (fileLength <= 0 || !range.isValid()) {
                            response.setStandardError(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_416_REQUESTED_RANGE_NOT_SATISFIABLE);
                            break;
                        }

                        qint64 firstByte = range.firstByte();
                        qint64 lastByte = range.lastByte();
                        if (firstByte >= 0 && lastByte >= 0) {
                            Q_ASSERT(firstByte <= lastByte);

                            if (lastByte >= fileLength) {
                                lastByte = fileLength - 1;
                            }

                            if (firstByte > lastByte) {
                                response.setStandardError(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_416_REQUESTED_RANGE_NOT_SATISFIABLE);
                                break;
                            }
                        } else if (firstByte >= 0) {
                            if (firstByte < fileLength) {
                                lastByte = fileLength - 1;
                            } else {
                                response.setStandardError(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_416_REQUESTED_RANGE_NOT_SATISFIABLE);
                                break;
                            }
                        } else if (lastByte >= 0) {
                            firstByte = fileLength - lastByte;
                            if (firstByte < 0) {
                                firstByte = 0;
                            }
                            lastByte = fileLength - 1;
                        } else {
                            response.setStandardError(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_416_REQUESTED_RANGE_NOT_SATISFIABLE);
                            break;
                        }

                        Oct::HttpCore::ByteRange responseRange(Oct::HttpCore::ByteRange::MODE_RESPONSE, firstByte, lastByte, fileLength);

                        response.setStatusLine(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_206_PARTIAL_CONTENT);
                        response.headers().set("Content-Range", "bytes " + responseRange.toString());
                        response.appendBodyPart(new VfsFileBodyPart(VfsFileBodyPart::VirtualFilePointer(file),
                                responseRange.firstByte(), responseRange.lastByte() - responseRange.firstByte() + 1, this));
                    } else {
                        Q_ASSERT(rangeSet.size() > 1);

                        // TODO: support multirange range

                        response.setStandardError(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_406_NOT_ACCEPTABLE);
                        break;
                    }
                } else {
                    response.setStatusLine(request.requestLine(), Oct::HttpCore::HTTP_STATUS_CODE_200_OK);
                    response.appendBodyPart(new VfsFileBodyPart(VfsFileBodyPart::VirtualFilePointer(file),
                            0, file->length(), this));
                }
            } while (0);

            return PROCESSING_FINISHED;
        }

    }
}
