/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__REQUEST_HANDLER_MANAGER_H_
#define _OCT_HTTP_SERVER__REQUEST_HANDLER_MANAGER_H_

/*-----------------------------------------------------------------------------*/

#include "RequestHandler.h"
#include "RequestHandlerProvider.h"
#include "Request.h"

#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtCore/QMap>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class RequestHandlerManager : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<RequestHandlerManager> Logger;

        private:
            QMap<QString,RequestHandlerProvider*> m_providers;

        public:
            RequestHandlerManager(QObject* parent = 0);

            bool registerHandlerProvider(const QString& path, RequestHandlerProvider* provider);

            RequestHandler* createHandler(Request& request) const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__REQUEST_HANDLER_MANAGER_H_ */

/**
 * @}
 * @}
 * @}
 */
