/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__CONNECTION_H_
#define _OCT_HTTP_SERVER__CONNECTION_H_

/*-----------------------------------------------------------------------------*/

#include "RequestHandler.h"
#include "Request.h"
#include "Response.h"

#include <httpcore/SocketReader.h>
#include <httpcore/SocketWriter.h>

#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtNetwork/QTcpSocket>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class Server;

        class Connection : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<Connection> Logger;

        private:
            enum State
            {
                CONNECTION_STATE_IDLE,
                CONNECTION_STATE_READING_REQUEST_LINE,
                CONNECTION_STATE_READING_HEADERS,
                CONNECTION_STATE_CHECKING_100_CONTINUE,
                CONNECTION_STATE_WRITING_100_CONTINUE,
                CONNECTION_STATE_READING_BODY,
                CONNECTION_STATE_PROCESSING_REQUEST,
                CONNECTION_STATE_WRITING_RESPONSE,
                CONNECTION_STATE_WRITING_STATUS_LINE,
                CONNECTION_STATE_WRITING_HEADERS,
                CONNECTION_STATE_WRITING_BODY,
                CONNECTION_STATE_FLUSH,
                CONNECTION_STATE_ERROR,
                CONNECTION_STATE_FINISHED
            };

        private:
            State m_state;
            QTcpSocket* m_tcpSocket;
            Oct::HttpCore::SocketReader* m_socketReader;
            Oct::HttpCore::SocketWriter* m_socketWriter;
            Request* m_request;
            Response* m_response;
            RequestHandler* m_handler;

        public:
            Connection(Server* server, QTcpSocket* clientSocket);
            virtual ~Connection();

        private:
            Server* server();
            void setState(State newState);
            bool readRequestLine();
            bool createHandler();
            bool readHeaders();
            bool check100Continue();
            bool write100Continue();
            bool prepareBodyReader();
            bool processRequest();
            bool writeResponse();
            bool writeStatusLine();
            bool writeHeaders();
            bool writeBody();
            bool flush();

        private slots:
            void process();

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__CONNECTION_H_ */

/**
 * @}
 * @}
 * @}
 */
