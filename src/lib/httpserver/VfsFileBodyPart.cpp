/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "VfsFileBodyPart.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        Oct::Log::Logger<VfsFileBodyPart> VfsFileBodyPart::Logger("/oct/httpserver/VfsFileBodyPart");

        VfsFileBodyPart::VfsFileBodyPart(const VirtualFilePointer& filePointer, qint64 offset, qint64 length,
                QObject* parent) :
            BodyPart(filePointer->contentType(), parent), m_file(filePointer), m_offsetSet(false),
                    m_lengthLeft(length), m_offset(offset), m_length(length)
        {
            // nothing to do
        }

        Oct::HttpCore::IOStatus VfsFileBodyPart::write(Oct::HttpCore::Writer* writer)
        {
            if (!m_offsetSet) {
                Logger.trace(this) << "Settings offset: offset=" << m_offset;

                if (!m_file->seek(m_offset)) {
                    Logger.info(this) << "Seek failed: offset=." << m_offset;
                    return Oct::HttpCore::IO_STATUS_ERROR;
                }

                m_offsetSet = true;
            }

            for (;;) {
                Oct::HttpCore::IOStatus status;

                Logger.trace(this) << "Writing buffer: size=" << m_buffer.size();

                status = m_buffer.write(writer);

                Logger.trace(this) << "Writing buffer: status=" << status;

                if (status != Oct::HttpCore::IO_STATUS_READY) {
                    return status;
                }

                QByteArray tmpBuffer(64 * 1024, Qt::Uninitialized);
                qint64 toRead = tmpBuffer.capacity();

                if (m_lengthLeft >= 0) {
                    if (toRead > m_lengthLeft) {
                        toRead = m_lengthLeft;
                    }
                }

                qint64 bytesRead;

                if (toRead > 0) {
                    bytesRead = m_file->read(tmpBuffer.data(), toRead);
                } else {
                    bytesRead = VfsFile::READ_EOF;
                }

                Logger.trace(this) << "Preparing buffer: bytesRead=" << bytesRead;

                if (bytesRead > 0) {
                    tmpBuffer.resize(bytesRead);

                    if (m_lengthLeft >= 0) {
                        m_lengthLeft -= bytesRead;
                    }

                    m_buffer = Oct::HttpCore::IOBuffer(tmpBuffer);

                    // we have new buffer - repeat loop
                } else if (bytesRead == VfsFile::READ_WAIT) {
                    Q_ASSERT(0); // TODO how to emit write-ready signal
                    return Oct::HttpCore::IO_STATUS_WAIT;
                } else if (bytesRead == VfsFile::READ_ERROR) {
                    return Oct::HttpCore::IO_STATUS_ERROR;
                } else if (bytesRead == VfsFile::READ_EOF) {
                    // TODO: EOF handling
                    return Oct::HttpCore::IO_STATUS_READY;
                } else {
                    Q_ASSERT(0);
                    return Oct::HttpCore::IO_STATUS_ERROR;
                }
            }
        }

        qint64 VfsFileBodyPart::length() const
        {
            if (m_length >= 0) {
                return m_length;
            } else {
                return m_file->length();
            }
        }
    }
}
