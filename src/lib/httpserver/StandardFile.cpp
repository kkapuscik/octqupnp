/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "StandardFile.h"

#include <QtCore/QFileInfo>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        StandardFile::StandardFile(const QString& fileName, const QString& mimeType) :
            m_file(fileName), m_mimeType(mimeType)
        {
            m_file.open(QFile::ReadOnly);

            if (m_mimeType.isEmpty()) {
                QFileInfo info(m_file);
                QString suffix = info.suffix().toLower();

                // TODO: clean up this mess

                if (suffix == "png") {
                    m_mimeType = "image/png";
                } else if (suffix == "jpg" || suffix == "jpeg") {
                    m_mimeType = "image/jpeg";
                } else if (suffix == "gif") {
                    m_mimeType = "audio/gif";
                } else if (suffix == "mp3") {
                    m_mimeType = "audio/mpeg";
                } else if (suffix == "mpg") {
                    m_mimeType = "video/mpeg";
                } else if (suffix == "htm" || suffix == "html") {
                    m_mimeType = "text/html";
                } else if (suffix == "txt") {
                    m_mimeType = "text/plain";
                } else if (suffix == "xml") {
                    m_mimeType = "text/xml";
                } else {
                    m_mimeType = "application/octet-stream";
                }
            }
        }

        StandardFile::~StandardFile()
        {
            m_file.close();
        }

        bool StandardFile::isValid() const
        {
            return m_file.isOpen() && m_file.isReadable();
        }

        bool StandardFile::seek(qint64 pos)
        {
            return m_file.seek(pos);
        }

        qint64 StandardFile::read(char* buffer, qint64 size)
        {
            qint64 rv = m_file.read(buffer, size);
            if (rv > 0) {
                return rv;
            } else if (rv == 0) {
                return READ_EOF;
            } else {
                return READ_ERROR;
            }
        }

        qint64 StandardFile::length() const
        {
            return m_file.size();
        }

        QString StandardFile::contentType() const
        {
            return m_mimeType;
        }
    }
}
