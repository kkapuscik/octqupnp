/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "RequestHandlerProvider.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        RequestHandlerProvider::RequestHandlerProvider(QObject* parent) :
            QObject(parent)
        {
            // nothing to do
        }
    }
}
