/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__STANDARD_FS_H_
#define _OCT_HTTP_SERVER__STANDARD_FS_H_

/*-----------------------------------------------------------------------------*/

#include "Vfs.h"

#include <log/Logger.h>

#include <QtCore/QDir>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        /**
         * VFS implementation on standard file system.
         */
        class StandardFS : public Vfs
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<StandardFS> Logger;

        private:
            /** Directory being root of vfs. */
            QDir m_baseDir;

        public:
            /**
             * Constructs new standard file system Vfs.
             *
             * @param[in] basePath
             *      Base path on the file system.
             * @param[in] parent
             *      File system parent object.
             */
            StandardFS(const QString& basePath, QObject* parent = 0);

            virtual VfsFile* open(const QString& path);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__STANDARD_FS_H_ */

/**
 * @}
 * @}
 * @}
 */
