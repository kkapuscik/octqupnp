/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "FileBodyPart.h"

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        FileBodyPart::FileBodyPart(const QString& contentType, const QString& fileName, QObject* parent) :
            BodyPart(contentType, parent), m_file(fileName)
        {
            m_file.open(QFile::ReadOnly);
        }

        qint64 FileBodyPart::length() const
        {
            return m_file.size();
        }

        Oct::HttpCore::IOStatus FileBodyPart::write(Oct::HttpCore::Writer* writer)
        {
            if (!m_file.isReadable()) {
                return Oct::HttpCore::IO_STATUS_ERROR;
            }

            for (;;) {
                Oct::HttpCore::IOStatus status = m_buffer.write(writer);
                if (status != Oct::HttpCore::IO_STATUS_READY) {
                    return status;
                }

                m_file.unsetError();
                QByteArray data = m_file.read(64 * 1024);
                if (m_file.error() != QFile::NoError) {
                    return Oct::HttpCore::IO_STATUS_ERROR;
                } else if (data.size() == 0) {
                    Q_ASSERT(m_file.atEnd());
                    return Oct::HttpCore::IO_STATUS_READY;
                } else {
                    m_buffer = Oct::HttpCore::IOBuffer(data);
                }
            }
        }
    }
}
