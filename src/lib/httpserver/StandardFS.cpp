/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "StandardFS.h"
#include "StandardFile.h"

#include <QtCore/QFileInfo>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        Oct::Log::Logger<StandardFS> StandardFS::Logger("/oct/httpserver/StandardFS");

        StandardFS::StandardFS(const QString& basePath, QObject* parent) :
            Vfs(parent), m_baseDir(basePath)
        {
            Logger.trace(this) << "StandardFS() basePath: " << basePath << " baseDir: " << m_baseDir.absolutePath();
        }

        VfsFile* StandardFS::open(const QString& path)
        {
            Logger.trace(this) << "open() path: " << path;

            if (path.length() == 0) {
                /* special case */
                return NULL;
            }

            QFileInfo fileInfo(m_baseDir, path);

            QString dirPath = m_baseDir.absolutePath();
            QString filePath = fileInfo.absoluteFilePath();

            Logger.trace(this) << "open() dir path:  " << dirPath;
            Logger.trace(this) << "open() file path: " << filePath;

            if (!filePath.startsWith(dirPath)) {
                return NULL;
            }

            if (fileInfo.exists()) {
                Logger.trace(this) << "open() file exists: " << filePath;

                if (fileInfo.isDir()) {
                    Logger.trace(this) << "open() path is a directory: " << filePath;

                    // TODO
                } else if (fileInfo.isFile()) {
                    Logger.trace(this) << "open() path is a file: " << filePath;

                    StandardFile* file = new StandardFile(fileInfo.absoluteFilePath());
                    if (file->isValid()) {
                        Logger.trace(this) << "open() file valid: " << filePath;
                        return file;
                    } else {
                        Logger.trace(this) << "open() file is not valid: " << filePath;
                        delete file;
                    }
                }
            }

            return NULL;
        }

    }
}
