/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__REQUEST_HANDLER_PROVIDER_H_
#define _OCT_HTTP_SERVER__REQUEST_HANDLER_PROVIDER_H_

/*-----------------------------------------------------------------------------*/

#include "RequestHandler.h"

#include <QtCore/QObject>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class RequestHandlerProvider : public QObject
        {
        Q_OBJECT

        public:
            RequestHandlerProvider(QObject* parent = 0);

            virtual RequestHandler* createHandler() = 0;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__REQUEST_HANDLER_PROVIDER_H_ */

/**
 * @}
 * @}
 * @}
 */
