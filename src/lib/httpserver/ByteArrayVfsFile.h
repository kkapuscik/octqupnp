/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__BYTE_ARRAY_VFS_FILE_H_
#define _OCT_HTTP_SERVER__BYTE_ARRAY_VFS_FILE_H_

/*-----------------------------------------------------------------------------*/

#include "VfsFile.h"

#include <QtCore/QString>
#include <QtCore/QByteArray>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        /**
         * Virtual file system file implementation over QByteArray.
         */
        class ByteArrayVfsFile : public VfsFile
        {
        private:
            QString m_contentType;
            QByteArray m_data;
            int m_readPosition;

        public:
            /**
             * Constructs new virtual file.
             */
            ByteArrayVfsFile(const QString& contentType, const QByteArray& data);

            /**
             * Destroys new virtual file.
             */
            virtual ~ByteArrayVfsFile();

            virtual qint64 read(char* buffer, qint64 size);
            virtual bool seek(qint64 pos);
            virtual QString contentType() const;
            virtual qint64 length() const;
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__BYTE_ARRAY_VFS_FILE_H_ */

/**
 * @}
 * @}
 * @}
 */
