/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__ERROR_REQUEST_HANDLER_H_
#define _OCT_HTTP_SERVER__ERROR_REQUEST_HANDLER_H_

/*-----------------------------------------------------------------------------*/

#include "RequestHandler.h"

#include <httpcore/StatusLine.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class ErrorRequestHandler : public RequestHandler
        {
        Q_OBJECT

        public:
            ErrorRequestHandler(QObject* parent = 0);
            virtual ~ErrorRequestHandler();
            virtual ProcessingResult processRequest(const Request& request, Response& response);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__ERROR_REQUEST_HANDLER_H_ */

/**
 * @}
 * @}
 * @}
 */
