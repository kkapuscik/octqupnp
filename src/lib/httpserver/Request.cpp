/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Request.h"

#include <httpcore/IdentityBodyReader.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        Request::Request(QObject* parent) :
            QObject(parent), m_bodyReader(NULL)
        {
            // nothing to do
        }

        Request::~Request()
        {
            // nothing to do
        }

        bool Request::createBodyReader(Oct::HttpCore::Reader* reader)
        {
            qint64 length = -1;

            // TODO: polish it, etc.

            if (headers().contains("Transfer-Encoding")) {
                // TODO: warning
                return false;
            }

            if (headers().contains("Content-Length")) {
                QString val = headers().value("Content-Length");
                bool ok;

                length = val.toLongLong(&ok, 10);
                if (!ok) {
                    // TODO: warning
                    return false;
                }
            } else {
                length = 0;
            }

            m_bodyReader = new Oct::HttpCore::IdentityBodyReader(reader, length, this);

            return true;
        }

    }
}
