/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup HttpServer
 * @{
 */

#ifndef _OCT_HTTP_SERVER__RESPONSE_H_
#define _OCT_HTTP_SERVER__RESPONSE_H_

/*-----------------------------------------------------------------------------*/

#include "BodyPart.h"
#include <httpcore/StatusLineWriter.h>
#include <httpcore/HeadersWriter.h>
#include <httpcore/RequestLine.h>
#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtCore/QList>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace HttpServer
    {
        class Response : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<Response> Logger;

        private:
            Oct::HttpCore::StatusLineWriter m_statusLineWriter;
            Oct::HttpCore::StatusLineWriter m_status100Writer;
            Oct::HttpCore::HeadersWriter m_headersWriter;
            QList<BodyPart*> m_bodyParts;

        public:
            Response(QObject* parent);
            virtual ~Response();

            const Oct::HttpCore::StatusLine& statusLine() const
            {
                return m_statusLineWriter.statusLine();
            }

            void setStatusLine(const Oct::HttpCore::StatusLine& statusLine)
            {
                m_statusLineWriter.setStatusLine(statusLine);
            }

            void setStatusLine(const Oct::HttpCore::RequestLine& requestLine, int code)
            {
                setStatusLine(Oct::HttpCore::StatusLine(requestLine.version(), code));
            }

            quint64 contentLength() const
            {
                if (m_bodyParts.length() == 0) {
                    return 0;
                } else if (m_bodyParts.length() == 1) {
                    return m_bodyParts.at(0)->length();
                } else {
                    // TODO: verify
                    /* unknown */
                    return -1;
                }
            }

            void setContentType()
            {
                if (m_bodyParts.length() == 1) {
                    headers().set("Content-Type", m_bodyParts.first()->contentType());
                } else if (m_bodyParts.length() > 1) {
                    // multipart/byteranges or other
                    Q_ASSERT(0);
                }
            }

            const Oct::HttpCore::HeaderSet& headers() const
            {
                return m_headersWriter.headers();
            }

            Oct::HttpCore::HeaderSet& headers()
            {
                return m_headersWriter.headers();
            }

            void appendBodyPart(BodyPart* part)
            {
                Q_ASSERT(part != NULL);

                part->setParent(this);

                m_bodyParts.append(part);
            }

            void set100ContinueVersion(Oct::HttpCore::HttpVersion version)
            {
                m_status100Writer.setStatusLine(Oct::HttpCore::StatusLine(version,
                        Oct::HttpCore::HTTP_STATUS_CODE_100_CONTINUE));
            }

            Oct::HttpCore::IOStatus writeStatusLine(Oct::HttpCore::Writer* writer)
            {
                return m_statusLineWriter.writeStatusLine(writer);
            }

            Oct::HttpCore::IOStatus write100Continue(Oct::HttpCore::Writer* writer)
            {
                Q_ASSERT(m_status100Writer.statusLine().isValid());

                return m_status100Writer.writeStatusLine(writer);
            }

            Oct::HttpCore::IOStatus writeHeaders(Oct::HttpCore::Writer* writer)
            {
                return m_headersWriter.writeHeaders(writer);
            }

            Oct::HttpCore::IOStatus writeBody(Oct::HttpCore::Writer* writer);

            void setStandardError(Oct::HttpCore::RequestLine requestLine, int code);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_HTTP_SERVER__RESPONSE_H_ */

/**
 * @}
 * @}
 * @}
 */
