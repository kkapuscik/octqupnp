/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp     UPnP protocol Control Point elements
 * @{
 */

#ifndef _OCT_UPNP_CP__PACKAGE_H_
#define _OCT_UPNP_CP__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * UPnP protocol Control Point elements.
     */
    namespace UpnpCp
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_UPNP_CP__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
