/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DescriptionDownloader.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Oct::Log::Logger<DescriptionDownloader> DescriptionDownloader::Logger("/oct/upnpcp/DescriptionDownloader");

        DescriptionDownloader::DescriptionDownloader(QObject* parent) :
            QObject(parent)
        {
            m_networkManager = new QNetworkAccessManager(this);

            // nothing to do
        }

        DescriptionDownloader::~DescriptionDownloader()
        {
            // nothing to do
        }

        bool DescriptionDownloader::isProcessing(const UUID& uuid) const
        {
            return m_taskMap.contains(uuid);
        }

        void DescriptionDownloader::process(const UUID& uuid, const QString& location, int expTimeout)
        {
            Q_ASSERT(! isProcessing(uuid));

            DescriptionDownloaderTask* newTask;

            newTask = new DescriptionDownloaderTask(this, m_networkManager, uuid, location, expTimeout);

            connect(newTask, SIGNAL( taskSucceeded(Oct::UpnpCp::DescriptionDownloaderTask*, Device) ), this,
                    SLOT( processTaskSucceeded(Oct::UpnpCp::DescriptionDownloaderTask*, Device) ));
            connect(newTask, SIGNAL( taskFailed(Oct::UpnpCp::DescriptionDownloaderTask*) ), this,
                    SLOT( processTaskFailed(Oct::UpnpCp::DescriptionDownloaderTask*) ));

            Logger.trace(this) << "Downloader - New task added: " << newTask->uuid().toString() << " " << newTask;

            m_taskMap.insert(newTask->uuid(), newTask);

            newTask->start();
        }

        void DescriptionDownloader::abort(const UUID& uuid)
        {
            TaskMap::Iterator iter = m_taskMap.find(uuid);
            if (iter != m_taskMap.end()) {
                DescriptionDownloaderTask* task = iter.value();

                Logger.trace(this) << "Downloader - Task aborted: " << task->uuid().toString() << " " << task;

                m_taskMap.erase(iter);
                delete task;
            }
        }

        void DescriptionDownloader::processTaskSucceeded(Oct::UpnpCp::DescriptionDownloaderTask* task, Device device)
        {
            TaskMap::iterator iter = m_taskMap.find(task->uuid());
            if (iter != m_taskMap.end()) {
                if (iter.value() == task) {
                    /* remove from map */
                    m_taskMap.erase(iter);

                    Logger.trace(this) << "Success, we have the device: " << device.udn().toString();

                    emit rootDeviceReady(device);
                } else {
                    Logger.trace(this) << "FATAL: Other task found in downloader map: " << task->uuid().toString();
                    Q_ASSERT(0);
                }
            } else {
                Logger.trace(this) << "Task not found in downloader map: " << task->uuid().toString();
                Q_ASSERT(0);
            }

            task->deleteLater();
        }

        void DescriptionDownloader::processTaskFailed(Oct::UpnpCp::DescriptionDownloaderTask* task)
        {
            TaskMap::iterator iter = m_taskMap.find(task->uuid());
            if (iter != m_taskMap.end()) {
                if (iter.value() == task) {
                    /* remove from map */
                    m_taskMap.erase(iter);

                    Logger.trace(this) << "Downloader - Failure, cannot load device: " << task->uuid().toString();

                    // TODO: some timeout so there will be no busy loop?
                } else {
                    Logger.trace(this) << "Other task found in downloader map: " << task->uuid().toString();
                    Q_ASSERT(0);
                }
            } else {
                Logger.trace(this) << "Task not found in downloader map: " << task->uuid().toString();
                Q_ASSERT(0);
            }

            task->deleteLater();
        }
    }
}
