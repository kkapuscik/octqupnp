/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ActionExecutor.h"

#include <upnp/XmlTools.h>
// TODO: cleanup
using Oct::Upnp::XmlTools;

#include <QtNetwork/QNetworkReply>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Oct::Log::Logger<ActionExecutor> ActionExecutor::Logger("/oct/upnpcp/ActionExecutor");

        /* internal consts - TODO: Cleanup? Move to XmlTools? */
        static const QString XML_SOAP_NAMESPACE("http://schemas.xmlsoap.org/soap/envelope/");
        static const QString XML_SOAP_ELEMENT_ENVELOPE("Envelope");
        static const QString XML_SOAP_ELEMENT_BODY("Body");
        static const QString XML_SOAP_ELEMENT_FAULT("Fault");
        //static const QString XML_SOAP_ELEMENT_FAULTCODE("faultcode");
        //static const QString XML_SOAP_ELEMENT_FAULTSTRING("faultstring");
        static const QString XML_SOAP_ELEMENT_DETAIL("detail");
        static const QString XML_SOAP_UPNP_NAMESPACE("urn:schemas-upnp-org:control-1-0");
        static const QString XML_SOAP_ELEMENT_UPNP_ERROR("UPnPError");
        static const QString XML_SOAP_ELEMENT_UPNP_ERROR_CODE("errorCode");
        static const QString XML_SOAP_ELEMENT_UPNP_ERROR_DESCRIPTION("errorDescription");

        ActionExecutor::ActionExecutor(QObject* owner, const ActionRequest& request) :
            QObject(owner), m_service(request.service()), m_action(request.action()), m_state(
                    EXECUTION_STATE_INITIALIZED), m_networkManager(NULL), m_networkReply(NULL), m_errorCode(0)
        {
            // TODO: refactor this
            QList<ActionArgument> args = m_action.arguments();
            for (int i = 0; i < args.length(); ++i) {
                setInputArgument(args[i].name(), request.argument(args[i].name()));
            }
        }

        ActionExecutor::~ActionExecutor()
        {
            cleanup();
        }

        bool ActionExecutor::execute()
        {
            Q_ASSERT(state() == EXECUTION_STATE_INITIALIZED);

            Logger.trace(this) << "Executing action: " << m_action.name();

            QNetworkRequest networkRequest(m_service.controlURL());

            networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, "text/xml; charset=\"utf-8\"");
            networkRequest.setRawHeader("SOAPACTION", QString("\"%1#%2\"").arg(m_service.serviceType()).arg(
                    m_action.name()).toUtf8());

            QString doc;

            /* document header */
            doc.append("<?xml version=\"1.0\"?>\n"
                "<s:Envelope\n"
                "    xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"\n"
                "    s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
                "  <s:Body>\n"
                "    <u:");
            doc.append(m_action.name());
            doc.append(" xmlns:u=\"");
            doc.append(m_service.serviceType());
            doc.append("\">\n");

            /* arguments */
            QList<ActionArgument> args = m_action.arguments();
            for (int i = 0; i < args.size(); ++i) {
                if (args[i].isInput()) {
                    ArgumentMap::ConstIterator iter = m_inputArguments.find(args[i].name());
                    if (iter != m_inputArguments.end()) {
                        doc.append('<');
                        doc.append(args[i].name());
                        doc.append('>');

                        doc.append(iter.value().toString());

                        doc.append("</");
                        doc.append(args[i].name());
                        doc.append('>');
                    } else {
                        return false;
                    }
                }
            }

            /* document footer */
            doc.append("    </u:");
            doc.append(m_action.name());
            doc.append(">\n"
                "  </s:Body>\n"
                "</s:Envelope>\n");

            Logger.trace(this) << "DATA: " << doc << doc.length();

#if 0
            POST path of control URL HTTP/1.1
            HOST: host of control URL:port of control URL
            CONTENT-LENGTH: bytes in body
            CONTENT-TYPE: text/xml; charset="utf-8"
            SOAPACTION: "urn:schemas-upnp-org:service:serviceType:v#actionName"
            <?xml version="1.0"?>
            <s:Envelope
            xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
            s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            <s:Body>
            <u:actionName xmlns:u="urn:schemas-upnp-org:service:serviceType:v">
            <argumentName>in arg value</argumentName>
            other in args and their values go here, if any
            </u:actionName>
            </s:Body>
            </s:Envelope>
#endif

            if (m_networkManager == NULL) {
                m_networkManager = new QNetworkAccessManager(this);
            }

            m_networkReply = m_networkManager->post(networkRequest, doc.toUtf8());

            if (m_networkReply != NULL) {
                Logger.trace(this) << "Executing action - reply: " << m_networkReply;

                connect(m_networkReply, SIGNAL( readyRead() ), this, SLOT( processDownloadRead() ));
                connect(m_networkReply, SIGNAL( finished() ), this, SLOT( processDownloadFinish() ));

                setState(EXECUTION_STATE_IN_PROGRESS);

                return true;
            } else {
                Logger.trace(this) << "Executing action - POST failed";

                return false;
            }
        }

        void ActionExecutor::setInputArgument(const QString& name, const QVariant& value)
        {
            Q_ASSERT(state() == EXECUTION_STATE_INITIALIZED);

            // TODO: checking name & value

            m_inputArguments.insert(name, value);
        }

        void ActionExecutor::setUserData(QVariant userData)
        {
            m_userData = userData;
        }

        QVariant ActionExecutor::inputArgument(const QString& name) const
        {
            ArgumentMap::ConstIterator iter = m_inputArguments.find(name);
            if (iter != m_inputArguments.end()) {
                return iter.value();
            } else {
                return QVariant();
            }
        }

        QVariant ActionExecutor::outputArgument(const QString& name) const
        {
            Q_ASSERT(state() == EXECUTION_STATE_SUCCEEDED);

            ArgumentMap::ConstIterator iter = m_outputArguments.find(name);
            if (iter != m_outputArguments.end()) {
                return iter.value();
            } else {
                return QVariant();
            }
        }

        void ActionExecutor::processDownloadFinish()
        {
            Logger.trace(this) << "Executing action - Download finished: " << m_networkReply->error();

            QVariant httpStatus = m_networkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
            bool ok;

            int status = httpStatus.toInt(&ok);
            if (ok) {
                Logger.trace(this) << "Executing action - Call finished with status code: " << status;
            } else {
                Logger.trace(this) << "Executing action - Cannot get response HTTP status code";
                status = -1;
            }

            if (status == 200) {
                processResponseDocument();
            } else if (status == 500) {
                processErrorDocument();
            } else {
                notifyError();
            }
        }

        void ActionExecutor::processDownloadRead()
        {
            Logger.trace(this) << "Executing action - Download ready";

            while (m_networkReply->bytesAvailable() > 0) {
                m_replyBuffer.append(m_networkReply->readAll());
            }
        }

#if 0
        HTTP/1.1 200 OK
        CONTENT-LENGTH: bytes in body
        CONTENT-TYPE: text/xml; charset="utf-8"
        DATE: when response was generated
        EXT:
        SERVER: OS/version UPnP/1.0 product/version
        <?xml version="1.0"?>
        <s:Envelope
        xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
        s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
        <s:Body>
        <u:actionNameResponse xmlns:u="urn:schemas-upnp-org:service:serviceType:v">
        <argumentName>out arg value</argumentName>
        other out args and their values go here, if any
        </u:actionNameResponse>
        </s:Body>
        </s:Envelope>
#endif
        void ActionExecutor::processResponseDocument()
        {
            do {
                /* the parsed XML */
                QDomDocument document;

                /* parse the XML */
                int errorLine, errorColumn;
                QString error;
                bool xmlParseOK = document.setContent(m_replyBuffer, true, &error, &errorLine, &errorColumn);

                /* check if parsed successfully */
                if (!xmlParseOK) {
                    Logger.trace(this) << "XML parsing error" << error << errorLine << errorColumn;
                    break;
                }

                QDomElement envelopeElement = document.documentElement();
                if (envelopeElement.namespaceURI() != XML_SOAP_NAMESPACE || envelopeElement.localName()
                        != XML_SOAP_ELEMENT_ENVELOPE) {
                    Logger.trace(this) << "Envelope element not found: " << envelopeElement.namespaceURI()
                            << envelopeElement.localName();
                    break;
                }

                QDomElement bodyElement = XmlTools::findChildElement(envelopeElement, XML_SOAP_NAMESPACE,
                        XML_SOAP_ELEMENT_BODY);
                if (bodyElement.isNull()) {
                    Logger.trace(this) << "Body element not found";
                    break;
                }

                QString actionElementName = m_action.name() + "Response";

                QDomElement actionElement = XmlTools::findChildElement(bodyElement, m_service.serviceType(),
                        actionElementName);
                if (actionElement.isNull()) {
                    Logger.trace(this) << "Action element not found";
                    break;
                }

                // TODO: what to do with missing arguments?

                for (QDomElement argumentElement = actionElement.firstChildElement(); !argumentElement.isNull(); argumentElement
                        = argumentElement.nextSiblingElement()) {

                    QString argName = argumentElement.localName();
                    QString argValue = XmlTools::extractValue(argumentElement);
                    if (argValue.isNull()) {
                        argValue = "";
                    }

                    m_outputArguments.insert(argName, argValue);
                }

                notifySuccess();

                return;
            } while (0);

            notifyError();
        }

        void ActionExecutor::processErrorDocument()
        {
#if 0
            <?xml version="1.0"?>
            <s:Envelope
            xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
            s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            <s:Body>
            <s:Fault>
            <faultcode>s:Client</faultcode>
            <faultstring>UPnPError</faultstring>
            <detail>
            <UPnPError xmlns="urn:schemas-upnp-org:control-1-0">
            <errorCode>error code</errorCode>
            <errorDescription>error string</errorDescription>
            </UPnPError>
            </detail>
            </s:Fault>
            </s:Body>
            </s:Envelope>
#endif

            do {
                Logger.trace(this) << "XML parsing started: " << m_replyBuffer.size();

                /* the parsed XML */
                QDomDocument document;

                /* parse the XML */
                int errorLine, errorColumn;
                QString error;
                bool xmlParseOK = document.setContent(m_replyBuffer, true, &error, &errorLine, &errorColumn);

                /* check if parsed successfully */
                if (!xmlParseOK) {
                    Logger.trace(this) << "XML parsing error: " << error << " " << errorLine << " " << errorColumn;
                    break;
                }

                QDomElement envelopeElement = document.documentElement();
                if (envelopeElement.namespaceURI() != XML_SOAP_NAMESPACE || envelopeElement.localName()
                        != XML_SOAP_ELEMENT_ENVELOPE) {
                    Logger.trace(this) << "Envelope element not found: " << envelopeElement.namespaceURI()
                            << envelopeElement.localName();
                    break;
                }

                QDomElement bodyElement = XmlTools::findChildElement(envelopeElement, XML_SOAP_NAMESPACE,
                        XML_SOAP_ELEMENT_BODY);
                if (bodyElement.isNull()) {
                    Logger.trace(this) << "Body element not found";
                    break;
                }

                QDomElement faultElement = XmlTools::findChildElement(bodyElement, XML_SOAP_NAMESPACE,
                        XML_SOAP_ELEMENT_FAULT);
                if (faultElement.isNull()) {
                    Logger.trace(this) << "Fault element not found";
                    break;
                }

                // do we need to check fault code & string?
                // skipping for now

                QDomElement detailElement = XmlTools::findChildElement(faultElement, "", XML_SOAP_ELEMENT_DETAIL);
                if (detailElement.isNull()) {
                    Logger.trace(this) << "Detail element not found";
                    break;
                }

                QDomElement upnpErrorElement = XmlTools::findChildElement(detailElement, XML_SOAP_UPNP_NAMESPACE,
                        XML_SOAP_ELEMENT_UPNP_ERROR);
                if (upnpErrorElement.isNull()) {
                    Logger.trace(this) << "UPnP error element not found";
                    break;
                }

                QDomElement upnpErrorCodeElement = XmlTools::findChildElement(upnpErrorElement,
                        XML_SOAP_UPNP_NAMESPACE, XML_SOAP_ELEMENT_UPNP_ERROR_CODE);
                if (upnpErrorCodeElement.isNull()) {
                    Logger.trace(this) << "UPnP error code element not found";
                    break;
                }
                QDomElement upnpErrorDescriptionElement = XmlTools::findChildElement(upnpErrorElement,
                        XML_SOAP_UPNP_NAMESPACE, XML_SOAP_ELEMENT_UPNP_ERROR_DESCRIPTION);
                if (upnpErrorDescriptionElement.isNull()) {
                    Logger.trace(this) << "UPnP error description element not found";
                    break;
                }

                QString errorCode = XmlTools::extractValue(upnpErrorCodeElement);
                QString errorDescription = XmlTools::extractValue(upnpErrorDescriptionElement);

                bool errorCodeOk;
                int errorCodeInt = errorCode.toInt(&errorCodeOk, 10);
                if (!errorCodeOk) {
                    Logger.trace(this) << "Invalid UPnP error code: " << errorCode;
                    break;
                }

                notifyFailure(errorCodeInt, errorDescription);

                return;
            } while (0);

            notifyError();
        }

        void ActionExecutor::cleanup()
        {
            if (m_networkManager != NULL) {
                m_networkManager->deleteLater();
                m_networkManager = NULL;
            }
            if (m_networkReply != NULL) {
                m_networkReply->deleteLater();
                m_networkReply = NULL;
            }
            m_replyBuffer.clear();
        }

        void ActionExecutor::notifySuccess()
        {
            Logger.trace(this) << "Action execution success";

            ArgumentMap::ConstIterator iter;

            for (iter = m_outputArguments.begin(); iter != m_outputArguments.end(); ++iter) {
                Logger.trace(this) << "Output argument: " << iter.key() << " = " << iter.value().toString();
            }

            cleanup();

            setState(EXECUTION_STATE_SUCCEEDED);

            emit executionFinished(this);
        }

        void ActionExecutor::notifyError()
        {
            Logger.trace(this) << "Action execution error";

            notifyFailure(-1, "Execution error");
        }

        void ActionExecutor::notifyFailure(int code, const QString& description)
        {
            Logger.trace(this) << "Action execution failure: " << code << " " << description;

            cleanup();

            m_errorCode = code;
            m_errorDescription = description;

            setState(EXECUTION_STATE_FAILED);

            emit executionFinished(this);
        }
    }
}
