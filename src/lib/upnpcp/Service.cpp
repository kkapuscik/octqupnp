/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Service.h"
#include "Device.h"

#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtCore/QList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Service::Service(QSharedPointer<DeviceDataStorage> dataStorage, ServicePrivate* servicePrivate) :
            Oct::Upnp::Service(), m_dataStorage(dataStorage), m_private(servicePrivate)
        {
            // do nothing
        }

        Device Service::device() const
        {
            DevicePrivate* pdevice = m_private->ownerDevice();
            return Device(m_dataStorage, pdevice);
        }

        const QString& Service::serviceType() const
        {
            return m_private->serviceType();
        }

        const QString& Service::serviceId() const
        {
            return m_private->serviceId();
        }

        const QUrl& Service::scpdURL() const
        {
            return m_private->scpdURL();
        }

        const QUrl& Service::controlURL() const
        {
            return m_private->controlURL();
        }

        const QUrl& Service::eventURL() const
        {
            return m_private->eventURL();
        }

        bool Service::hasStateVariable(const QString& name) const
        {
            return m_private->hasStateVariable(name);
        }

        StateVariable Service::stateVariable(const QString& name) const
        {
            return m_private->stateVariable(name);
        }

        QList<StateVariable> Service::stateVariables() const
        {
            return m_private->stateVariables();
        }

        bool Service::hasAction(const QString& name) const
        {
            return m_private->hasAction(name);
        }

        Action Service::action(const QString& name) const
        {
            return m_private->action(name);
        }

        QList<Action> Service::actions() const
        {
            return m_private->actions();
        }
    }
}
