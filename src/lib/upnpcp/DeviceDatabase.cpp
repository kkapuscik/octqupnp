/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DeviceDatabase.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Oct::Log::Logger<DeviceDatabase> DeviceDatabase::Logger("/oct/upnpcp/DeviceDatabase");

        DeviceDatabase::DeviceDatabase(QObject* parent) :
            QObject(parent)
        {
            // TODO Auto-generated constructor stub
        }

        DeviceDatabase::~DeviceDatabase()
        {
        }

        bool DeviceDatabase::contains(const UUID& udn) const
        {
            return m_rootDeviceMap.contains(udn);
        }

        void DeviceDatabase::addRootDevice(Device device)
        {
            if (contains(device.udn())) {
                Logger.warn(this) << "Skipping - device already added: " << device.udn().toString();
                return;
            }

            m_rootDeviceMap.insert(device.udn(), device);

            emitDeviceSignals(device, true, true);
        }

        void DeviceDatabase::update(const UUID& /*udn*/, int /*expTimeout*/)
        {
            // TODO
        }

        void DeviceDatabase::remove(const UUID& udn)
        {
            DeviceMap::iterator iter = m_rootDeviceMap.find(udn);
            if (iter != m_rootDeviceMap.end()) {
                Device device = iter.value();

                m_rootDeviceMap.erase(iter);

                emitDeviceSignals(device, true, false);
            }
        }

        void DeviceDatabase::emitDeviceSignals(const Device& device, bool isRoot, bool isAdded)
        {
            if (isRoot) {
                if (isAdded) {
                    emit rootDeviceAdded(device);
                } else {
                    emit rootDeviceRemoved(device);
                }
            }

            if (isAdded) {
                emit deviceAdded(device);
            } else {
                emit deviceRemoved(device);
            }

            QList<Device> subdevices = device.subdevices();
            for (int i = 0; i < subdevices.size(); ++i) {
                emitDeviceSignals(subdevices.at(i), false, isAdded);
            }
        }
    }
}
