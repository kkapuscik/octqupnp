/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__DEVICE_DATA_STORAGE_H_
#define _OCT_UPNP_CP__DEVICE_DATA_STORAGE_H_

/*---------------------------------------------------------------------------*/

#include "DevicePrivate.h"
#include "ServicePrivate.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        /**
         * Root device structure objects data.
         */
        class DeviceDataStorage
        {
        private:
            QList<DevicePrivate*> m_deviceList;
            QList<ServicePrivate*> m_serviceList;

        private:
            DeviceDataStorage();

        public:
            ~DeviceDataStorage();

            ServicePrivate* createService(DevicePrivate* ownerDevice, const QString& serviceType,
                    const QString& serviceId, const QUrl& scpdURL, const QUrl& controlURL, const QUrl& eventURL)
            {
                Q_ASSERT(hasDevice(ownerDevice));

                /* create service */
                ServicePrivate* newService = new ServicePrivate(ownerDevice, serviceType, serviceId, scpdURL,
                        controlURL, eventURL);

                /* link service to owner device */
                ownerDevice->addService(newService);

                /* store in internal database */
                m_serviceList.append(newService);

                return newService;
            }

            DevicePrivate* createDevice(DevicePrivate* parentDevice, const UUID& uuid, const QUrl& location)
            {
                if (parentDevice != NULL) {
                    Q_ASSERT(hasDevice(parentDevice));
                } else {
                    Q_ASSERT(m_deviceList.isEmpty());
                }

                /* create device */
                DevicePrivate* newDevice = new DevicePrivate(parentDevice, uuid, location);

                /* link device to parent device */
                if (parentDevice != NULL) {
                    parentDevice->addDevice(newDevice);
                }

                /* store in internal database */
                m_deviceList.append(newDevice);

                return newDevice;
            }

        private:
            bool hasDevice(DevicePrivate* device) const
            {
                return m_deviceList.contains(device);
            }

        private:
            friend class DescriptionParser;

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__DEVICE_DATA_STORAGE_H_ */

/**
 * @}
 * @}
 * @}
 */
