/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__DISCOVERY_H_
#define _OCT_UPNP_CP__DISCOVERY_H_

/*---------------------------------------------------------------------------*/

#include "Types.h"

#include <upnpssdp/DiscoverySocket.h>

#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class Discovery : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<Discovery> Logger;

        private:
            Oct::UpnpSsdp::DiscoverySocket* m_ssdpSocket;
            Oct::UpnpSsdp::DiscoverySocket* m_searchSocket;

        public:
            Discovery(QObject* parent = 0);
            virtual ~Discovery();

            void start();
            void stop();

            void sendSearch();

        signals:
            void deviceAlive(const Oct::UpnpCp::UUID& uuid, const QString& location, int expTimeout);
            void deviceByeBye(const Oct::UpnpCp::UUID& uuid);

        private slots:
            void processAliveMessage(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::AliveMessage& message);

            void processByeByeMessage(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::ByeByeMessage& message);

            void processSearchResponseMessage(const QHostAddress& sender, quint16 senderPort,
                    const Oct::UpnpSsdp::SearchResponseMessage& message);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__DISCOVERY_H_ */

/**
 * @}
 * @}
 * @}
 */
