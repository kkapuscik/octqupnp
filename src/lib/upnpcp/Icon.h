/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__ICON_H_
#define _OCT_UPNP_CP__ICON_H_

/*---------------------------------------------------------------------------*/

#include <upnp/Icon.h>
#include <QtCore/QUrl>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class Icon : public Oct::Upnp::Icon
        {
        private:
            QUrl m_url;

        public:
            Icon();
            Icon(const QString& mimeType, int width, int height, int depth, const QUrl& url);
            virtual ~Icon();

            const QUrl& url() const
            {
                return m_url;
            }
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__ICON_H_ */

/**
 * @}
 * @}
 * @}
 */
