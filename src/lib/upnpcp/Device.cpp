/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Device.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        /* OctUpnpCpDevice consts */
        const QString Device::PROPERTY_DEVICE_TYPE("deviceType");
        const QString Device::PROPERTY_FRIENDLY_NAME("friendlyName");

        Device::Device(QSharedPointer<DeviceDataStorage> dataStorage, DevicePrivate* devicePrivate) :
            Oct::Upnp::Device(), m_dataStorage(dataStorage), m_private(devicePrivate)
        {
            // do nothing
        }

        UUID Device::udn() const
        {
            return m_private != NULL ? m_private->udn() : UUID();
        }

        QUrl Device::location() const
        {
            return m_private != NULL ? m_private->location() : QUrl();
        }

        QString Device::property(const QString& name) const
        {
            return m_private != NULL ? m_private->property(name) : QString();
        }

        QList<QString> Device::propertyKeys() const
        {
            return m_private != NULL ? m_private->propertyKeys() : QList<QString> ();
        }

        QList<Device> Device::subdevices() const
        {
            QList<Device> subs;

            if (m_private != NULL) {
                QList<DevicePrivate*> psubs = m_private->subdevices();

                for (int i = 0; i < psubs.size(); ++i) {
                    subs.append(Device(m_dataStorage, psubs.at(i)));
                }
            }

            return subs;
        }

        QList<Service> Device::services() const
        {
            QList<Service> serv;

            if (m_private != NULL) {
                QList<ServicePrivate*> pserv = m_private->services();

                for (int i = 0; i < pserv.size(); ++i) {
                    serv.append(Service(m_dataStorage, pserv.at(i)));
                }
            }

            return serv;
        }
    }
}
