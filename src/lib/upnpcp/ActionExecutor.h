/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__ACTION_EXECUTOR_H_
#define _OCT_UPNP_CP__ACTION_EXECUTOR_H_

/*---------------------------------------------------------------------------*/

#include "Service.h"
#include "ActionRequest.h"

#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtNetwork/QNetworkAccessManager>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class ActionExecutor : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<ActionExecutor> Logger;

        private:
            typedef QMap<QString, QVariant> ArgumentMap;

        public:
            enum State
            {
                EXECUTION_STATE_INITIALIZED,
                EXECUTION_STATE_IN_PROGRESS,
                EXECUTION_STATE_CANCELED,
                EXECUTION_STATE_SUCCEEDED,
                EXECUTION_STATE_FAILED
            };

        private:
            Service m_service;
            Action m_action;
            QVariant m_userData;

            State m_state;

            ArgumentMap m_inputArguments;
            ArgumentMap m_outputArguments;

            QNetworkAccessManager* m_networkManager;
            QNetworkReply* m_networkReply;

            QByteArray m_replyBuffer;

            int m_errorCode;
            QString m_errorDescription;

        private:
            ActionExecutor(QObject* owner, const ActionRequest& request);

            void setInputArgument(const QString& name, const QVariant& value);

            bool execute();

        public:
            virtual ~ActionExecutor();

            void setUserData(QVariant userData);

            QVariant inputArgument(const QString& name) const;
            QVariant outputArgument(const QString& name) const;

            int errorCode() const
            {
                Q_ASSERT(state() == EXECUTION_STATE_FAILED);

                return m_errorCode;
            }

            const QString& errorDescription() const
            {
                Q_ASSERT(state() == EXECUTION_STATE_FAILED);

                return m_errorDescription;
            }

            State state() const
            {
                return m_state;
            }

        signals:
            void executionFinished(Oct::UpnpCp::ActionExecutor* executor);

        private slots:
            void processDownloadFinish();
            void processDownloadRead();

        private:
            void setState(State newState)
            {
                switch (m_state) {
                    case EXECUTION_STATE_INITIALIZED:
                        Q_ASSERT(newState == EXECUTION_STATE_IN_PROGRESS);
                        break;

                    case EXECUTION_STATE_IN_PROGRESS:
                        Q_ASSERT(newState != EXECUTION_STATE_INITIALIZED && newState != EXECUTION_STATE_IN_PROGRESS);
                        break;

                    default:
                        Q_ASSERT(0);
                        break;
                }

                m_state = newState;
            }

            void processResponseDocument();
            void processErrorDocument();

            void cleanup();

            void notifySuccess();
            void notifyError();
            void notifyFailure(int code, const QString& description);

            // to allow creating executors
            friend class ControlPoint;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__ACTION_EXECUTOR_H_ */

/**
 * @}
 * @}
 * @}
 */
