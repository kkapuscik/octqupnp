/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__DEVICE_PRIVATE_H_
#define _OCT_UPNP_CP__DEVICE_PRIVATE_H_

/*---------------------------------------------------------------------------*/

#include "ServicePrivate.h"
#include "Icon.h"
#include "Types.h"

#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtCore/QUrl>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class DevicePrivate
        {
        private:
            typedef QMap<QString, QString> PropertyMap;
            typedef QList<DevicePrivate*> DeviceList;
            typedef QList<ServicePrivate*> ServiceList;
            typedef QList<Icon> IconList;

        private:
            DevicePrivate* m_parentDevice;
            UUID m_uuid;
            QUrl m_location;
            PropertyMap m_properties;
            DeviceList m_deviceList;
            ServiceList m_serviceList;
            IconList m_iconList;

        private:
            Q_DISABLE_COPY(DevicePrivate)

            DevicePrivate(DevicePrivate* parentDevice, const UUID& uuid, const QUrl& location) :
                m_parentDevice(parentDevice), m_uuid(uuid), m_location(location)
            {
                // do nothing
            }

            void addDevice(DevicePrivate* childDevice)
            {
                m_deviceList.append(childDevice);
            }

            void addService(ServicePrivate* service)
            {
                m_serviceList.append(service);
            }

        public:
            UUID udn() const
            {
                return m_uuid;
            }

            const QUrl& location() const
            {
                return m_location;
            }

            QString property(const QString& name) const
            {
                if (m_properties.contains(name)) {
                    return m_properties.value(name);
                } else {
                    return QString::null;
                }
            }

            QList<QString> propertyKeys() const
            {
                return m_properties.keys();
            }

            void addIcon(const Icon& icon)
            {
                m_iconList.append(icon);
            }

            QList<ServicePrivate*> services() const
            {
                return ServiceList(m_serviceList);
            }

            QList<DevicePrivate*> subdevices() const
            {
                return DeviceList(m_deviceList);
            }

            void addProperty(const QString& name, const QString& value)
            {
                if (m_properties.contains(name)) {
                    m_properties.insert(name, m_properties.value(name) + value);
                } else {
                    m_properties.insert(name, value);
                }
            }

            friend class DeviceDataStorage;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__DEVICE_PRIVATE_H_ */

/**
 * @}
 * @}
 * @}
 */
