/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DescriptionParser.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Oct::Log::Logger<DescriptionParser> DescriptionParser::Logger("/oct/upnpcp/DescriptionParser");

        bool DescriptionParser::processDeviceListElement(QDomElement listElement, DeviceDataStorage* storage,
                DevicePrivate* currentDevice, const QUrl& urlBase)
        {
            Q_ASSERT(currentDevice != NULL);

            for (QDomElement childElement = listElement.firstChildElement(); !childElement.isNull(); childElement
                    = childElement.nextSiblingElement()) {

                if (childElement.namespaceURI() == DEVICE_NAMESPACE && childElement.localName()
                        == DEVICE_ELEMENT_DEVICE) {

                    DevicePrivate* newDevice = processDeviceElement(childElement, storage, currentDevice,
                            currentDevice->location(), urlBase);
                    if (newDevice != NULL) {
                        // success, continue
                    } else {
                        return false;
                    }
                }
            }

            return true;
        }

        bool DescriptionParser::processServiceListElement(QDomElement listElement, DeviceDataStorage* storage,
                DevicePrivate* currentDevice, const QUrl& urlBase)
        {
            for (QDomElement childElement = listElement.firstChildElement(); !childElement.isNull(); childElement
                    = childElement.nextSiblingElement()) {

                if (childElement.namespaceURI() == DEVICE_NAMESPACE && childElement.localName()
                        == DEVICE_ELEMENT_SERVICE) {

                    ServicePrivate* newService = parseServiceInfo(childElement, storage, currentDevice, urlBase);
                    if (newService != NULL) {
                        // success, continue
                    } else {
                        return false;
                    }
                }
            }

            return true;
        }

        bool DescriptionParser::processIconListElement(QDomElement listElement, DevicePrivate* currentDevice,
                const QUrl& urlBase)
        {
            for (QDomElement childElement = listElement.firstChildElement(); !childElement.isNull(); childElement
                    = childElement.nextSiblingElement()) {

                if (childElement.namespaceURI() == DEVICE_NAMESPACE && childElement.localName()
                        == DEVICE_ELEMENT_ICON) {

                    Icon icon = parseIconInfo(childElement, urlBase);
                    if (icon.isValid()) {
                        currentDevice->addIcon(icon);
                    } else {
                        Logger.strace() << "Icon parsing error";
                        // skipping the error
                    }
                }
            }

            return true;
        }

        DevicePrivate* DescriptionParser::processDeviceElement(QDomElement deviceElement, DeviceDataStorage* storage,
                DevicePrivate* parentDevice, const QUrl& location, const QUrl& urlBase)
        {
            do {
                QDomElement udnElement = findChildElement(deviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_UDN);
                if (udnElement.isNull()) {
                    Logger.strace() << "Device is missing UDN";
                    break;
                }
                QString udnString = extractValue(udnElement);
                if (udnString.isNull()) {
                    Logger.strace() << "Device UDN is empty";
                    break;
                }

                DevicePrivate* newDevice = storage->createDevice(parentDevice, UUID(udnString), location);

                bool error = false;
                for (QDomElement childElement = deviceElement.firstChildElement(); !childElement.isNull() && !error; childElement
                        = childElement.nextSiblingElement()) {

                    if (childElement.namespaceURI() == DEVICE_NAMESPACE) {
                        if (childElement.localName() == DEVICE_ELEMENT_UDN) {
                            // do nothing, already processed
                        } else if (childElement.localName() == DEVICE_ELEMENT_DEVICE_LIST) {
                            if (!processDeviceListElement(childElement, storage, newDevice, urlBase)) {
                                error = true;
                            }
                        } else if (childElement.localName() == DEVICE_ELEMENT_SERVICE_LIST) {
                            if (!processServiceListElement(childElement, storage, newDevice, urlBase)) {
                                error = true;
                            }
                        } else if (childElement.localName() == DEVICE_ELEMENT_ICON_LIST) {
                            if (!processIconListElement(childElement, newDevice, urlBase)) {
                                error = true;
                            }
                        } else {
                            // TODO: how to handle 'generic' properties
                            QString propertyName = childElement.localName();
                            QString propertyValue = extractValue(childElement);

                            if (!propertyName.isNull() && !propertyValue.isNull()) {
                                newDevice->addProperty(propertyName, propertyValue);
                            }
                        }
                    } else {
                        // TODO: how to handle 'custom' properties
                        QString propertyName = childElement.tagName();
                        QString propertyValue = extractValue(childElement);

                        if (!propertyName.isNull() && !propertyValue.isNull()) {
                            newDevice->addProperty(propertyName, propertyValue);
                        }
                    }
                }

                if (error) {
                    break;
                }

                /* verify device (required elements) */
                if (newDevice->property(Device::PROPERTY_DEVICE_TYPE).isNull()) {
                    Logger.strace() << "Device is missing" << Device::PROPERTY_DEVICE_TYPE << "property";
                    break;
                }
                if (newDevice->property(Device::PROPERTY_FRIENDLY_NAME).isNull()) {
                    Logger.strace() << "Device is missing" << Device::PROPERTY_FRIENDLY_NAME << "property";
                    break;
                }

                return newDevice;
            } while (0);

            return NULL;
        }

        Device DescriptionParser::parseDeviceXML(const QUrl& location, QDomElement docElement)
        {
            do {
                /* check root element */
                if (docElement.namespaceURI() != DEVICE_NAMESPACE || docElement.localName()
                        != DEVICE_ELEMENT_ROOT) {

                    Logger.strace() << "Invalid root element: " << docElement.namespaceURI() << docElement.localName();
                    break;
                }

                /* find and check spec version */
                QDomElement specVersionElem = findChildElement(docElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_SPEC_VERSION);
                if (specVersionElem.isNull()) {
                    break;
                }
                QDomElement versionMajorElem = findChildElement(specVersionElem, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_MAJOR);
                QDomElement versionMinorElem = findChildElement(specVersionElem, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_MINOR);
                if (versionMajorElem.isNull() || versionMinorElem.isNull()) {
                    break;
                }
                QString versionMajor = extractValue(versionMajorElem);
                QString versionMinor = extractValue(versionMajorElem);

                if (versionMajor != "1" /*|| versionMinor != "0"*/) {
                    Logger.strace() << "Invalid spec version: " << versionMajor << versionMinor;
                    break;
                }

                /* get and verify URL base address */
                QUrl baseURL = location;

                QDomElement urlBaseElement = findChildElement(docElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_URL_BASE);
                if (!urlBaseElement.isNull()) {
                    QString urlBase = extractValue(urlBaseElement);
                    if (!urlBase.isNull()) {
                        QUrl newURL = baseURL.resolved(urlBase);
                        if (newURL.isValid()) {
                            baseURL = newURL;
                        } else {
                            Logger.strace() << "Cannot resolve URLBase";
                        }
                    } else {
                        Logger.strace() << "Invalid value of URLBase element";
                    }
                }

                /* find and root device object */
                QDomElement rootDeviceElement = findChildElement(docElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_DEVICE);
                if (docElement.isNull()) {
                    break;
                }

                /* process device description */
                QSharedPointer<DeviceDataStorage> dataStorage(new DeviceDataStorage());

                DevicePrivate* devicePrivate = processDeviceElement(rootDeviceElement, dataStorage.data(), NULL,
                        location, baseURL);
                if (devicePrivate != NULL) {
                    return Device(dataStorage, devicePrivate);
                }
            } while (0);

            return Device();
        }

        ServicePrivate* DescriptionParser::parseServiceInfo(QDomElement serviceElement, DeviceDataStorage* storage,
                DevicePrivate* currentDevice, const QUrl& urlBase)
        {
            Q_ASSERT(storage != NULL);
            Q_ASSERT(currentDevice != NULL);

            do {
                QDomElement serviceTypeElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_SERVICE_TYPE);
                QDomElement serviceIdElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_SERVICE_ID);
                QDomElement scpdURLElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_SCPDURL);
                QDomElement controlURLElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_CONTROL_URL);
                QDomElement eventURLElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_EVENT_SUB_URL);

                if (serviceTypeElem.isNull() || serviceIdElem.isNull() || scpdURLElem.isNull()
                        || controlURLElem.isNull()) {
                    break;
                }

                QString serviceType = extractValue(serviceTypeElem);
                QString serviceId = extractValue(serviceIdElem);
                QString scpdStr = extractValue(scpdURLElem);
                QString controlStr = extractValue(controlURLElem);
                QString eventStr = extractValue(eventURLElem);

                if (serviceType.isNull() || serviceId.isNull() || scpdStr.isNull() || controlStr.isNull()) {
                    break;
                }

                QUrl scpdURL = urlBase.resolved(scpdStr);
                QUrl controlURL = urlBase.resolved(controlStr);
                QUrl eventURL = urlBase.resolved(eventStr);

                if (!scpdURL.isValid() || !controlURL.isValid()) {
                    break;
                }

                return storage->createService(currentDevice, serviceType, serviceId, scpdURL, controlURL, eventURL);
            } while (0);

            return NULL;
        }

        bool DescriptionParser::parseServiceXML(QDomElement documentElement, Service& service)
        {
            bool result = false;

            do {
                QList<StateVariable> stateVarList;
                QList<Action> actionList;

                /* parse the XML */
                if (!Oct::Upnp::DescriptionParser::parseServiceXML(documentElement, stateVarList, actionList)) {
                    break;
                }

                /* verify and add state variables */
                bool stateVarsCorrect = true;
                for (int i = 0; stateVarsCorrect && i < stateVarList.size(); ++i) {
                    if (!service.hasStateVariable(stateVarList[i].name())) {
                        service.m_private->addStateVariable(stateVarList[i]);
                    } else {
                        Logger.strace() << "Duplicated state variable: " << stateVarList[i].name();
                        stateVarsCorrect = false;
                    }
                }
                if (!stateVarsCorrect) {
                    Logger.swarn() << "Incorrect state variables.";
                    break;
                }

                /* verify and add actions */
                bool actionsCorrect = true;
                for (int i = 0; i < actionList.size(); ++i) {
                    if (!service.hasAction(actionList[i].name())) {
                        bool argsCorrect = true;

                        QList<ActionArgument> arguments = actionList[i].arguments();

                        for (int j = 0; j < arguments.size(); ++j) {
                            if (!service.hasStateVariable(arguments[j].relatedStateVariable())) {
                                Logger.strace() << "Missing related state variable: " << arguments[j].relatedStateVariable();
                                argsCorrect = false;
                            }
                        }

                        if (argsCorrect) {
                            service.m_private->addAction(actionList[i]);
                        } else {
                            Logger.strace() << "Invalid action arguments: " << actionList[i].name();
                            actionsCorrect = false;
                        }
                    } else {
                        Logger.strace() << "Duplicated action: " << actionList[i].name();
                        actionsCorrect = false;
                    }
                }
                if (!actionsCorrect) {
                    Logger.swarn() << "Incorrect actions.";
                    // break;
                }

                /* success */
                result = true;
            } while (0);

            return result;
        }

        Icon DescriptionParser::parseIconInfo(QDomElement serviceElement, const QUrl& urlBase)
        {
            do {
                QDomElement mimeTypeElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_MIME_TYPE);
                QDomElement widthElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_WIDTH);
                QDomElement heightElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_HEIGHT);
                QDomElement depthElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_DEPTH);
                QDomElement iconURLElem = findChildElement(serviceElement, DEVICE_NAMESPACE,
                        DEVICE_ELEMENT_URL);

                if (mimeTypeElem.isNull() || widthElem.isNull() || heightElem.isNull() || depthElem.isNull()
                        || iconURLElem.isNull()) {
                    break;
                }

                QString mimeType = extractValue(mimeTypeElem);
                QString widthStr = extractValue(widthElem);
                QString heightStr = extractValue(heightElem);
                QString depthStr = extractValue(depthElem);
                QString iconStr = extractValue(iconURLElem);

                if (mimeType.isNull() || widthStr.isNull() || heightStr.isNull() || depthStr.isNull()
                        || iconStr.isNull()) {
                    break;
                }

                bool widthOK, heightOK, depthOK;

                int width = widthStr.toInt(&widthOK, 10);
                int height = heightStr.toInt(&heightOK, 10);
                int depth = depthStr.toInt(&depthOK, 10);
                QUrl iconURL = urlBase.resolved(iconStr);

                if (mimeType.length() > 0 && widthOK && width > 0 && heightOK && height > 0 && depthOK && depth > 0
                        && iconURL.isValid()) {
                    return Icon(mimeType, width, height, depth, iconURL);
                }
            } while (0);

            return Icon();
        }
    }
}
