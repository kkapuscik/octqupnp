/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Icon.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Icon::Icon() :
            Oct::Upnp::Icon()
        {
        }

        Icon::Icon(const QString& mimeType, int width, int height, int depth, const QUrl& url) :
            Oct::Upnp::Icon(mimeType, width, height, depth), m_url(url)
        {
            // nothing to do
        }

        Icon::~Icon()
        {
            // nothing to do
        }
    }
}
