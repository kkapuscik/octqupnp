/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__DEVICE_DATABASE_H_
#define _OCT_UPNP_CP__DEVICE_DATABASE_H_

/*---------------------------------------------------------------------------*/

#include "Device.h"

#include <log/Logger.h>
#include <QtCore/QObject>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class DeviceDatabase : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<DeviceDatabase> Logger;

        private:
            typedef QMap<UUID, Device> DeviceMap;

        private:
            DeviceMap m_rootDeviceMap;

        public:
            DeviceDatabase(QObject* parent = 0);
            virtual ~DeviceDatabase();

            bool contains(const UUID& udn) const;

        public slots:
            void addRootDevice(Oct::UpnpCp::Device device);
            void update(const Oct::UpnpCp::UUID& udn, int expTimeout);
            void remove(const Oct::UpnpCp::UUID& udn);

        private:
            void emitDeviceSignals(const Device& device, bool isRoot, bool isAdded);

        signals:
            void rootDeviceAdded(Oct::UpnpCp::Device device);
            void rootDeviceRemoved(Oct::UpnpCp::Device device);
            void deviceAdded(Oct::UpnpCp::Device device);
            void deviceRemoved(Oct::UpnpCp::Device device);

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__DEVICE_DATABASE_H_ */

/**
 * @}
 * @}
 * @}
 */
