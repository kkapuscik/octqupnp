/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__DESCRIPTION_DOWNLOADER_H_
#define _OCT_UPNP_CP__DESCRIPTION_DOWNLOADER_H_

/*---------------------------------------------------------------------------*/

#include "DescriptionDownloaderTask.h"
#include "Types.h"

#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtNetwork/QNetworkAccessManager>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class DescriptionDownloader : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<DescriptionDownloader> Logger;

        private:
            typedef QMap<UUID, DescriptionDownloaderTask*> TaskMap;

        private:
            QNetworkAccessManager* m_networkManager;
            TaskMap m_taskMap;

        public:
            DescriptionDownloader(QObject* parent = 0);
            virtual ~DescriptionDownloader();

            bool isProcessing(const UUID& uuid) const;
            void process(const UUID& uuid, const QString& location, int expTimeout);
            void abort(const UUID& uuid);

        private slots:
            void processTaskSucceeded(Oct::UpnpCp::DescriptionDownloaderTask* task, Device device);
            void processTaskFailed(Oct::UpnpCp::DescriptionDownloaderTask* task);

        signals:
            void rootDeviceReady(Oct::UpnpCp::Device device);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__DESCRIPTION_DOWNLOADER_H_ */

/**
 * @}
 * @}
 * @}
 */
