/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__DESCRIPTION_DOWNLOADER_TASK_H_
#define _OCT_UPNP_CP__DESCRIPTION_DOWNLOADER_TASK_H_

/*---------------------------------------------------------------------------*/

#include "Device.h"
#include "Types.h"

#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtNetwork/QNetworkAccessManager>
#include <QtCore/QUrl>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class DescriptionDownloaderTask : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<DescriptionDownloaderTask> Logger;

        private:
            QNetworkAccessManager* m_networkManager;
            QByteArray m_dataBuffer;
            UUID m_uuid;
            QUrl m_location;
            int m_expTimeout;
            QNetworkReply* m_currentReply;
            Device m_device;
            QList<Service> m_serviceList;
            bool m_notified; // TODO: remove this

        public:
            DescriptionDownloaderTask(QObject* parent, QNetworkAccessManager* networkManager,
                    const UUID& uuid, const QString& location, int expTimeout);
            virtual ~DescriptionDownloaderTask();

            void start();

            const UUID& uuid() const
            {
                return m_uuid;
            }

        private slots:
            void processDownloadRead();
            void processDownloadFinish();

        private:
            void cleanup();
            void addServices(const Device& device);
            void setupNextDownload();
            void requestDocument(const QUrl& url);
            void processDownloadedDocument();
            void processDownloadFailure();

        signals:
            void taskSucceeded(Oct::UpnpCp::DescriptionDownloaderTask* task, Device device);
            void taskFailed(Oct::UpnpCp::DescriptionDownloaderTask* task);

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__DESCRIPTION_DOWNLOADER_TASK_H_ */

/**
 * @}
 * @}
 * @}
 */
