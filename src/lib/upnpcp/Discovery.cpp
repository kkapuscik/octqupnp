/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "Discovery.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Oct::Log::Logger<Discovery> Discovery::Logger("/oct/upnpcp/Discovery");

        static const int MIN_EXP_TIMEOUT = 120; /* s */

        Discovery::Discovery(QObject* parent) :
            QObject(parent), m_ssdpSocket(NULL), m_searchSocket(NULL)
        {
            // nothing to do
        }

        Discovery::~Discovery()
        {
        }

        void Discovery::start()
        {
            if (m_ssdpSocket == NULL) {
                /* create SSDP socket */
                m_ssdpSocket = new Oct::UpnpSsdp::DiscoverySocket(this);
                /* create search socket */
                m_searchSocket = new Oct::UpnpSsdp::DiscoverySocket(this);

                if (m_ssdpSocket->joinSsdpGroup() && m_searchSocket->bind(0)) {
                    /* connect socket events */
                    connect(
                            m_ssdpSocket,
                            SIGNAL( aliveMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::AliveMessage&) ),
                            this,
                            SLOT ( processAliveMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::AliveMessage&) ));

                    connect(
                            m_ssdpSocket,
                            SIGNAL( byeByeMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::ByeByeMessage&) ),
                            this,
                            SLOT ( processByeByeMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::ByeByeMessage&) ));

                    connect(
                            m_searchSocket,
                            SIGNAL( searchResponseReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchResponseMessage&) ),
                            this,
                            SLOT ( processSearchResponseMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchResponseMessage&) ));
                } else {
                    stop();
                }
            }
        }

        void Discovery::stop()
        {
            if (m_ssdpSocket != NULL) {
                delete m_ssdpSocket;
                m_ssdpSocket = NULL;
            }
            if (m_searchSocket != NULL) {
                delete m_searchSocket;
                m_searchSocket = NULL;
            }
        }

        void Discovery::sendSearch()
        {
            if (m_searchSocket != NULL) {
                m_searchSocket->sendSearch(Oct::UpnpSsdp::SearchMessage("upnp:rootdevice"));
            }
        }

        void Discovery::processAliveMessage(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::AliveMessage& message)
        {
            USN usn = message.usn();

            Logger.trace(this) << "CpDiscovery - Alive message - " << sender.toString() << ':' << senderPort << ' ' << usn.type()
                    << usn.deviceUUID().toString() << usn.elementInfo();

            if (usn.type() == USN::USN_TYPE_ROOT_DEVICE) {
                int expTimeout = message.cacheControl();
                if (expTimeout < MIN_EXP_TIMEOUT) {
                    expTimeout = MIN_EXP_TIMEOUT;
                }

                Logger.trace(this) << "CpDiscovery - Emiting Alive";

                emit deviceAlive(usn.deviceUUID(), message.location(), expTimeout);
            }
        }

        void Discovery::processByeByeMessage(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::ByeByeMessage& message)
        {
            Logger.trace(this) << "CpDiscovery - ByeBye message - " << sender.toString() << ':' << senderPort;

            Logger.trace(this) << "CpDiscovery - Emiting ByeBye";

            emit deviceByeBye(message.usn().deviceUUID());
        }

        void Discovery::processSearchResponseMessage(const QHostAddress& sender, quint16 senderPort,
                const Oct::UpnpSsdp::SearchResponseMessage& message)
        {
            USN usn = message.usn();

            Logger.trace(this) << "CpDiscovery - Search response - " << sender.toString() << ':' << senderPort << ' '
                    << usn.type() << usn.deviceUUID().toString() << usn.elementInfo();

            if (usn.type() == USN::USN_TYPE_ROOT_DEVICE) {
                int expTimeout = message.cacheControl();
                if (expTimeout < MIN_EXP_TIMEOUT) {
                    expTimeout = MIN_EXP_TIMEOUT;
                }

                Logger.trace(this) << "CpDiscovery - Emiting Alive";

                emit deviceAlive(usn.deviceUUID(), message.location(), expTimeout);
            }
        }
    }
}
