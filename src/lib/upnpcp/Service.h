/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__SERVICE_H_
#define _OCT_UPNP_CP__SERVICE_H_

/*---------------------------------------------------------------------------*/

#include "DeviceDataStorage.h"
#include "Types.h"

#include <upnp/Service.h>

#include <QtCore/QObject>
#include <QtXml/QDomElement>
#include <QtCore/QUrl>
#include <QtCore/QMap>
#include <QtCore/QSharedPointer>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class Device;

        class Service : public Oct::Upnp::Service
        {
        private:
            QSharedPointer<DeviceDataStorage> m_dataStorage;
            ServicePrivate* m_private;

        private:
            /**
             * Constructs service.
             *
             * @param dataStorage
             *      Data storage with full device tree.
             * @param servicePrivate
             *      This specific private service information.
             *      The object is contained in \p dataStorage.
             */
            Service(QSharedPointer<DeviceDataStorage> dataStorage, ServicePrivate* servicePrivate);

        public:
            virtual const QString& serviceType() const;
            virtual const QString& serviceId() const;

            virtual bool hasStateVariable(const QString& name) const;
            virtual QList<StateVariable> stateVariables() const;
            virtual StateVariable stateVariable(const QString& name) const;

            virtual bool hasAction(const QString& name) const;
            virtual QList<Action> actions() const;
            virtual Action action(const QString& name) const;

            Device device() const;

            const QUrl& scpdURL() const;
            const QUrl& controlURL() const;
            const QUrl& eventURL() const;

        private:
            friend class Device;
            friend class DescriptionParser;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__SERVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
