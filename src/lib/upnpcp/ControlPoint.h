/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__CONTROL_POINT_H_
#define _OCT_UPNP_CP__CONTROL_POINT_H_

/*---------------------------------------------------------------------------*/

#include "ActionExecutor.h"
#include "Discovery.h"
#include "DeviceDatabase.h"
#include "DescriptionDownloader.h"

#include <log/Logger.h>

#include <QtCore/QObject>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class ControlPoint : public QObject
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<ControlPoint> Logger;

        private:
            Discovery* m_discovery;
            DeviceDatabase* m_deviceDatabase;
            DescriptionDownloader* m_descriptionDownloader;

        public:
            ControlPoint(QObject* parent = 0);
            virtual ~ControlPoint();

            void start();
            void stop();

            void sendSearch();

            ActionExecutor* execute(const ActionRequest& request);

        signals:
            void rootDeviceAdded(Oct::UpnpCp::Device device);
            void rootDeviceRemoved(Oct::UpnpCp::Device device);
            void deviceAdded(Oct::UpnpCp::Device device);
            void deviceRemoved(Oct::UpnpCp::Device device);

        private slots:
            void deviceAlive(const Oct::UpnpCp::UUID& uuid, const QString& location, int expTimeout);
            void deviceByeBye(const Oct::UpnpCp::UUID& uuid);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__CONTROL_POINT_H_ */

/**
 * @}
 * @}
 * @}
 */
