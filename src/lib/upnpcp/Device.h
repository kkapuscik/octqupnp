/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__DEVICE_H_
#define _OCT_UPNP_CP__DEVICE_H_

/*---------------------------------------------------------------------------*/

#include "DeviceDataStorage.h"
#include "Service.h"
#include "Icon.h"
#include "Types.h"

#include <upnp/Device.h>

#include <QtCore/QMap>
#include <QtXml/QDomElement>
#include <QtCore/QUrl>
#include <QtCore/QSharedPointer>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class Device : public Oct::Upnp::Device
        {
        public:
            static const QString PROPERTY_DEVICE_TYPE;
            static const QString PROPERTY_FRIENDLY_NAME;

        private:
            QSharedPointer<DeviceDataStorage> m_dataStorage;
            DevicePrivate* m_private;

        private:
            Device(QSharedPointer<DeviceDataStorage> dataStorage, DevicePrivate* devicePrivate);

        public:
            Device() :
                Oct::Upnp::Device(), m_dataStorage(), m_private(NULL)
            {
                // do nothing
            }

            bool isValid() const
            {
                return m_private != NULL;
            }

            virtual UUID udn() const;
            virtual QString property(const QString& name) const;
            virtual QList<QString> propertyKeys() const;

            QUrl location() const;

            QList<Service> services() const;
            QList<Device> subdevices() const;

            bool operator ==(const Device& other) const
            {
                return m_private == other.m_private;
            }

            bool operator !=(const Device& other) const
            {
                return m_private != other.m_private;
            }

        private:
            friend class DescriptionParser;
            friend class Service;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__DEVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
