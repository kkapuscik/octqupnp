/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__ACTION_REQUEST_H_
#define _OCT_UPNP_CP__ACTION_REQUEST_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QMap>
#include <QtCore/QVariant>

#include "Service.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class ActionRequest
        {
        private:
            typedef QMap<QString, QVariant> ArgumentMap;

        private:
            Service m_service;
            Action m_action;
            ArgumentMap m_inputArguments;

        public:
            ActionRequest(Service service, Action action);

            Service service() const
            {
                return m_service;
            }

            Action action() const
            {
                return m_action;
            }

            QVariant argument(const QString& name) const;

            void setArgument(const QString& name, const QVariant& value);

        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__ACTION_REQUEST_H_ */

/**
 * @}
 * @}
 * @}
 */
