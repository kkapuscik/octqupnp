/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__DESCRIPTION_PARSER_H_
#define _OCT_UPNP_CP__DESCRIPTION_PARSER_H_

/*---------------------------------------------------------------------------*/

#include "Device.h"
#include "Service.h"

#include <upnp/DescriptionParser.h>

#include <log/Logger.h>

#include <QtCore/QUrl>
#include <QtXml/QDomElement>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class DescriptionParser : protected Oct::Upnp::DescriptionParser
        {
        private:
            static Oct::Log::Logger<DescriptionParser> Logger;

        public:
            static Device parseDeviceXML(const QUrl& location, QDomElement docElement);
            static bool parseServiceXML(QDomElement documentElement, Service& service);

            // TODO: REMOVE THE METHODS

        private:
            static DevicePrivate* processDeviceElement(QDomElement deviceElement, DeviceDataStorage* storage,
                    DevicePrivate* parentDevice, const QUrl& location, const QUrl& urlBase);

            static bool processDeviceListElement(QDomElement listElement, DeviceDataStorage* storage,
                    DevicePrivate* currentDevice, const QUrl& urlBase);

            static bool processServiceListElement(QDomElement listElement, DeviceDataStorage* storage,
                    DevicePrivate* currentDevice, const QUrl& urlBase);

            static bool processIconListElement(QDomElement listElement, DevicePrivate* currentDevice,
                    const QUrl& urlBase);

            static ServicePrivate* parseServiceInfo(QDomElement serviceElement, DeviceDataStorage* storage,
                    DevicePrivate* currentDevice, const QUrl& urlBase);

            static Icon parseIconInfo(QDomElement serviceElement, const QUrl& urlBase);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__DESCRIPTION_PARSER_H_ */

/**
 * @}
 * @}
 * @}
 */
