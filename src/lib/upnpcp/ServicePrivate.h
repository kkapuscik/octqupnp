/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup UpnpCp
 * @{
 */

#ifndef _OCT_UPNP_CP__SERVICE_PRIVATE_H_
#define _OCT_UPNP_CP__SERVICE_PRIVATE_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QUrl>

#include "Types.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        class DevicePrivate;

        class ServicePrivate
        {
        private:
            typedef QMap<QString, StateVariable> StateVarMap;
            typedef QMap<QString, Action> ActionMap;

        private:
            DevicePrivate* m_ownerDevice;
            QString m_serviceType;
            QString m_serviceId;
            QUrl m_scpdURL;
            QUrl m_controlURL;
            QUrl m_eventURL;
            StateVarMap m_stateVariables;
            ActionMap m_actions;

        private:
            Q_DISABLE_COPY(ServicePrivate)

            ServicePrivate(DevicePrivate* ownerDevice, const QString& serviceType, const QString& serviceId,
                    const QUrl& scpdURL, const QUrl& controlURL, const QUrl& eventURL) :
                m_ownerDevice(ownerDevice), m_serviceType(serviceType), m_serviceId(serviceId), m_scpdURL(scpdURL),
                        m_controlURL(controlURL), m_eventURL(eventURL)
            {
            }

        public:
            DevicePrivate* ownerDevice()
            {
                return m_ownerDevice;
            }

            const QString& serviceType() const
            {
                return m_serviceType;
            }

            const QString& serviceId() const
            {
                return m_serviceId;
            }

            const QUrl& scpdURL() const
            {
                return m_scpdURL;
            }

            const QUrl& controlURL() const
            {
                return m_controlURL;
            }

            const QUrl& eventURL() const
            {
                return m_eventURL;
            }

            bool hasStateVariable(const QString& name) const
            {
                return m_stateVariables.contains(name);
            }

            StateVariable stateVariable(const QString& name) const
            {
                StateVarMap::ConstIterator iter = m_stateVariables.find(name);

                Q_ASSERT(iter != m_stateVariables.end());

                return iter.value();
            }

            QList<StateVariable> stateVariables() const
            {
                return m_stateVariables.values();
            }

            bool hasAction(const QString& name) const
            {
                return m_actions.contains(name);
            }

            Action action(const QString& name) const
            {
                ActionMap::ConstIterator iter = m_actions.find(name);

                Q_ASSERT(iter != m_actions.end());

                return iter.value();
            }

            QList<Action> actions() const
            {
                return m_actions.values();
            }

            void addStateVariable(const StateVariable& stateVar)
            {
                m_stateVariables.insert(stateVar.name(), stateVar);
            }

            void addAction(const Action& action)
            {
                m_actions.insert(action.name(), action);
            }

        private:
            friend class DeviceDataStorage;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_UPNP_CP__SERVICE_PRIVATE_H_ */

/**
 * @}
 * @}
 * @}
 */
