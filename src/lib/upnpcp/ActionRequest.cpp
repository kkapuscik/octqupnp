/*
 * ActionRequest.cpp
 *
 *  Created on: 27-04-2011
 *      Author: saveman
 */

#include "ActionRequest.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        ActionRequest::ActionRequest(Service service, Action action) :
                m_service(service), m_action(action)
        {
            // nothing to do
        }

        QVariant ActionRequest::argument(const QString& name) const
        {
            ArgumentMap::ConstIterator iter = m_inputArguments.find(name);
            if (iter != m_inputArguments.end()) {
                return iter.value();
            } else {
                return QVariant();
            }
        }

        void ActionRequest::setArgument(const QString& name, const QVariant& value)
        {
            // TODO: checking name & value

            m_inputArguments.insert(name, value);
        }
    }
}
