/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "ControlPoint.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Oct::Log::Logger<ControlPoint> ControlPoint::Logger("/oct/upnpcp/ControlPoint");

        ControlPoint::ControlPoint(QObject* parent) :
            QObject(parent)
        {
            m_discovery = new Discovery(this);
            m_deviceDatabase = new DeviceDatabase(this);
            m_descriptionDownloader = new DescriptionDownloader(this);

            connect(m_discovery, SIGNAL( deviceAlive(const Oct::UpnpCp::UUID&, const QString&, int) ), this,
                    SLOT( deviceAlive(const Oct::UpnpCp::UUID&, const QString&, int) ));
            connect(m_discovery, SIGNAL( deviceByeBye(const Oct::UpnpCp::UUID&) ), this,
                    SLOT( deviceByeBye(const Oct::UpnpCp::UUID&) ));

            connect(m_descriptionDownloader, SIGNAL( rootDeviceReady(Oct::UpnpCp::Device) ), m_deviceDatabase,
                    SLOT( addRootDevice(Oct::UpnpCp::Device) ));

            connect(m_deviceDatabase, SIGNAL( rootDeviceAdded(Oct::UpnpCp::Device) ), this,
                    SIGNAL( rootDeviceAdded(Oct::UpnpCp::Device) ));
            connect(m_deviceDatabase, SIGNAL( rootDeviceRemoved(Oct::UpnpCp::Device) ), this,
                    SIGNAL( rootDeviceRemoved(Oct::UpnpCp::Device) ));
            connect(m_deviceDatabase, SIGNAL( deviceAdded(Oct::UpnpCp::Device) ), this,
                    SIGNAL( deviceAdded(Oct::UpnpCp::Device) ));
            connect(m_deviceDatabase, SIGNAL( deviceRemoved(Oct::UpnpCp::Device) ), this,
                    SIGNAL( deviceRemoved(Oct::UpnpCp::Device) ));
        }

        ControlPoint::~ControlPoint()
        {
        }

        void ControlPoint::sendSearch()
        {
            m_discovery->sendSearch();
        }

        void ControlPoint::start()
        {
            m_discovery->start();
            m_discovery->sendSearch();
        }

        void ControlPoint::stop()
        {
            m_discovery->stop();
        }

        void ControlPoint::deviceAlive(const Oct::UpnpCp::UUID& uuid, const QString& location, int expTimeout)
        {
            Logger.trace(this) << "ControlPoint - device alive: " << uuid.toString() << location;

            if (m_deviceDatabase->contains(uuid)) {
                // device already there, just update status
                m_deviceDatabase->update(uuid, expTimeout);
            } else if (!m_descriptionDownloader->isProcessing(uuid)) {
                m_descriptionDownloader->process(uuid, location, expTimeout);
            }
        }

        void ControlPoint::deviceByeBye(const Oct::UpnpCp::UUID& uuid)
        {
            Logger.trace(this) << "ControlPoint - device byebye: " << uuid.toString();

            m_descriptionDownloader->abort(uuid);
            m_deviceDatabase->remove(uuid);
        }

        ActionExecutor* ControlPoint::execute(const ActionRequest& request)
        {
            ActionExecutor* executor = NULL;

            executor = new ActionExecutor(this, request);
            executor->execute();

            return executor;
        }
    }
}
