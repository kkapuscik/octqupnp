/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DescriptionDownloaderTask.h"
#include "DescriptionParser.h"

#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtXml/QDomDocument>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        Oct::Log::Logger<DescriptionDownloaderTask> DescriptionDownloaderTask::Logger(
                "/oct/upnpcp/DescriptionDownloaderTask");

        DescriptionDownloaderTask::DescriptionDownloaderTask(QObject* parent, QNetworkAccessManager* networkManager,
                const UUID& uuid, const QString& location, int expTimeout) :
            QObject(parent), m_networkManager(networkManager), m_uuid(uuid), m_location(location), m_expTimeout(
                    expTimeout), m_currentReply(NULL), m_notified(false)
        {
        }

        void DescriptionDownloaderTask::start()
        {
            setupNextDownload();
        }

        DescriptionDownloaderTask::~DescriptionDownloaderTask()
        {
            cleanup();
        }

        void DescriptionDownloaderTask::cleanup()
        {
            if (m_currentReply != NULL) {
                m_currentReply->deleteLater();
                m_currentReply = NULL;
            }

            m_dataBuffer.resize(0);
        }

        void DescriptionDownloaderTask::addServices(const Device& device)
        {
            m_serviceList.append(device.services());

            QList<Device> subdevices = device.subdevices();
            for (int i = 0; i < subdevices.size(); ++i) {
                addServices(subdevices[i]);
            }
        }

        void DescriptionDownloaderTask::requestDocument(const QUrl& url)
        {
            Logger.trace(this) << "DescriptionDownloadTask - request document: " << url.toString();

            QNetworkRequest request(url);

            QNetworkReply* reply = m_networkManager->get(request);
            if (reply != NULL) {
                m_currentReply = reply;
                m_currentReply->setParent(this);

                connect(m_currentReply, SIGNAL( readyRead() ), this, SLOT( processDownloadRead() ));
                connect(m_currentReply, SIGNAL( finished() ), this, SLOT( processDownloadFinish() ));

            } else {
                Logger.trace(this) << "Failed GET on " << m_location.toString();

                processDownloadFailure();
            }
        }

        void DescriptionDownloaderTask::setupNextDownload()
        {
            if (!m_device.isValid()) {
                requestDocument(m_location);
            } else {
                if (m_serviceList.isEmpty()) {
                    Logger.trace(this) << "Notifying download success: " << m_location.toString();

                    if (!m_notified) {
                        m_notified = true;
                        emit taskSucceeded(this, m_device);
                    }
                } else {
                    requestDocument(m_serviceList.front().scpdURL());
                }
            }
        }

        void DescriptionDownloaderTask::processDownloadFinish()
        {
            if (m_currentReply != NULL) {
                if (m_currentReply->error() == QNetworkReply::NoError) {
                    processDownloadedDocument();
                    return;
                }
            }

            processDownloadFailure();
        }

        void DescriptionDownloaderTask::processDownloadRead()
        {
            while (m_currentReply->bytesAvailable() > 0) {
                m_dataBuffer.append(m_currentReply->readAll());
            }
        }

        void DescriptionDownloaderTask::processDownloadedDocument()
        {
            Logger.trace(this) << "XML parsing started: " << m_dataBuffer.size();

            /* the parsed XML */
            QDomDocument document;

            /* parse the XML */
            int errorLine, errorColumn;
            QString error;
            bool xmlParseOK = document.setContent(m_dataBuffer, true, &error, &errorLine, &errorColumn);

            /* clear the content buffer */
            cleanup();

            /* check if parsed successfully */
            if (!xmlParseOK) {
                Logger.trace(this) << "XML parsing error: " << error << " at " << errorLine << ":" << errorColumn;
                processDownloadFailure();
            }

            Logger.trace(this) << "XML parsing success";

            /* check for which element this document was downloaded */
            if (!m_device.isValid()) {
                /* process the device document */
                Device device = DescriptionParser::parseDeviceXML(m_location, document.documentElement());
                if (device.isValid()) {
                    if (device.udn() == m_uuid) {
                        /* we have the device now */
                        m_device = device;

                        /* add all services to be processed */
                        addServices(m_device);

                        /* move to next document */
                        setupNextDownload();
                    } else {
                        Logger.trace(this) << "Device UUID from XML is invalid: " << device.udn().toString()
                                << m_uuid.toString();

                        /* processing failed - error */
                        processDownloadFailure();
                    }
                } else {
                    Logger.trace(this) << "Device XML processing failed: " << m_location.toString();

                    /* processing failed - error */
                    processDownloadFailure();
                }
            } else {
                /* get the first service */
                Service service = m_serviceList.front();
                m_serviceList.pop_front();

                /* process the service document */
                if (DescriptionParser::parseServiceXML(document.documentElement(), service)) {
                    /* success - move to next element */
                    setupNextDownload();
                } else {
                    /* failed */
                    Logger.trace(this) << "Service XML processing failed: " << service.scpdURL().toString();

                    /* processing failed - error */
                    processDownloadFailure();
                }
            }
        }

        void DescriptionDownloaderTask::processDownloadFailure()
        {
            Logger.trace(this) << "Notifying download failure: " << m_location.toString();

            cleanup();

            if (!m_notified) {
                m_notified = true;
                emit taskFailed(this);
            }
        }
    }
}
