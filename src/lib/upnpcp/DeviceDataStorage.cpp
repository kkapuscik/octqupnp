/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "DeviceDataStorage.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace UpnpCp
    {
        DeviceDataStorage::DeviceDataStorage()
        {
        }

        DeviceDataStorage::~DeviceDataStorage()
        {
            while (!m_deviceList.isEmpty()) {
                delete m_deviceList.takeFirst();
            }

            while (!m_serviceList.isEmpty()) {
                delete m_serviceList.takeFirst();
            }
        }
    }
}
