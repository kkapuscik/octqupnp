/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup SoapClient   SOAP protocol client
 * @{
 */

#ifndef _OCT_SOAP_CLIENT__PACKAGE_H_
#define _OCT_SOAP_CLIENT__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * SOAP protocol client.
     */
    namespace SoapClient
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_SOAP_CLIENT__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
