/*
 * $Id: LogLinesModel.h 126 2011-03-28 21:28:59Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup App
 * @{
 */

#ifndef _OCT_APP__LOG_LINES_MODEL_H_
#define _OCT_APP__LOG_LINES_MODEL_H_

/*-----------------------------------------------------------------------------*/

#include <QtGui/QStandardItemModel>

/*-----------------------------------------------------------------------------*/

namespace Oct {
    namespace App {
        class LogLinesModel : public QStandardItemModel {
        Q_OBJECT

        private:
            qint64 m_line;

        public:
            explicit LogLinesModel(QObject *parent = 0);

            void addLine(const QString& path, const QString& prefix,
                    const QString& level, const QString& instance,
                    const QString& message);

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_APP__LOG_LINES_MODEL_H_ */

/**
 * @}
 * @}
 * @}
 */
