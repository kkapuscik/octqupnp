cmake_minimum_required(VERSION 2.8.11)

project(liboctapp)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

include_directories(${Qt5Core_INCLUDE_DIRS})
include_directories(${Qt5Widgets_INCLUDE_DIRS})

file(GLOB liboctapp_SOURCES *.cpp)
file(GLOB liboctapp_HEADERS *.h)

QT5_WRAP_CPP(liboctapp_MOCSOURCES ${liboctapp_HEADERS})
#macro QT5_WRAP_UI(outfiles inputfile ... OPTIONS ...)
#macro QT5_ADD_RESOURCES(outfiles inputfile ... OPTIONS ...)

add_library(octapp STATIC ${liboctapp_SOURCES} ${liboctapp_HEADERS} ${liboctapp_MOCSOURCES})
target_link_libraries(octapp octlog Qt5::Core Qt5::Widgets)
