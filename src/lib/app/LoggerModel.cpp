/*
 * $Id: LoggerModel.cpp 109 2011-03-15 22:21:40Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "LoggerModel.h"

#include <log/Manager.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace App
    {
        LoggerModel::LoggerModel(QObject* parent) :
            QStandardItemModel(parent)
        {
            int i = 0;
            setHorizontalHeaderItem(i++, new QStandardItem("Logger"));
        }

        void LoggerModel::processPath(const Oct::Log::PathEntry& entry)
        {
            QStringList segments = entry.segments();
            QStandardItem* item = invisibleRootItem();

            for (int i = 1; i < segments.size(); ++i) {
                QString name = segments[i];
                QStandardItem* segmentItem = NULL;

                for (int j = 0; j < item->rowCount(); ++j) {
                    QStandardItem* child = item->child(j, 0);
                    if (child->text() == name) {
                        segmentItem = child;
                        break;
                    }
                }

                if (segmentItem == NULL) {
                    segmentItem = new QStandardItem(name);
                    segmentItem->setCheckState(Qt::Checked);

                    segmentItem->setCheckable(false);
                    segmentItem->setEditable(false);
                    segmentItem->setDragEnabled(false);
                    segmentItem->setDropEnabled(false);

                    item->appendRow(segmentItem);
                }

                item = segmentItem;
            }

            if (item != invisibleRootItem()) {
                item->setCheckState(entry.isEnabled() ? Qt::Checked : Qt::Unchecked);
            }
        }

        void LoggerModel::setLoggerEnabled(QModelIndex index, bool enabled, bool recursive)
        {
            QStandardItem* item = itemFromIndex(index);
            if (item != NULL) {
                setLoggerEnabled(item, enabled, recursive);
            }
        }

        void LoggerModel::setLoggerEnabled(QStandardItem* item, bool enabled, bool recursive)
        {
            QString path;
            for (QStandardItem* iter = item; iter != NULL; iter = iter->parent()) {
                path = "/" + iter->text() + path;
            }

            Oct::Log::Manager::instance()->setEnabled(path, enabled);

            if (recursive) {
                for (int j = 0; j < item->rowCount(); ++j) {
                    QStandardItem* child = item->child(j, 0);

                    setLoggerEnabled(child, enabled, recursive);
                }
            }
        }

    }
}
