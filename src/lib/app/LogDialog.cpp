/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "LogDialog.h"

#include <QtWidgets/QTabWidget>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QMenuBar>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace App
    {
        LogDialog::LogDialog(QWidget* parent, Qt::WindowFlags flags) :
            QDialog(parent, flags)
        {
            /* create document */
            m_logDocument = new LogDocument(this);

            /* initial setup */
            setModal(false);
            setWindowTitle(tr("Log"));

            /* create main widget */
            QTabWidget* mainTabWidget = new QTabWidget(this);

            QFont monospaceFont("Monospace");
            monospaceFont.setStyleHint(QFont::TypeWriter);

            /* create tab widgets - loggers tree */
            m_loggerTreeWidget = new QTreeView(mainTabWidget);
            m_loggerTreeWidget->setContextMenuPolicy(Qt::CustomContextMenu);

            /* create tab widgets - log table */
            m_logTableView = new QTableView(mainTabWidget);

            /* create tab widgets - log text */
            m_logTextEdit = new QTextEdit(mainTabWidget);
            m_logTextEdit->setReadOnly(true);
            m_logTextEdit->setCurrentFont(monospaceFont);
            m_logTextEdit->setWordWrapMode(QTextOption::NoWrap);
            m_logTextEdit->setAcceptRichText(false);

            /* add tabs */
            mainTabWidget->addTab(m_loggerTreeWidget, tr("Loggers"));
            mainTabWidget->addTab(m_logTableView, tr("Table"));
            mainTabWidget->addTab(m_logTextEdit, tr("Log"));

            /* setup menu */
            QMenuBar* menuBar = new QMenuBar(this);
            QMenu* fileMenu = menuBar->addMenu(tr("&File"));
            fileMenu->addAction(tr("C&lear"), m_logDocument, SLOT( clear() ), QKeySequence("Ctrl+N"));
            fileMenu->addSeparator();
            fileMenu->addAction(tr("&Close"), this, SLOT( closeDialog() ), QKeySequence("Alt+F4"));

            /* setup main layout */
            QBoxLayout* mainLayout = new QVBoxLayout(this);
            mainLayout->addWidget(menuBar, 0);
            mainLayout->addWidget(mainTabWidget, 1);

            /* set models */
            m_loggerTreeWidget->setModel(m_logDocument->loggerModel());
            m_logTableView->setModel(m_logDocument->linesModel());

            /* connect signals */
            connect(m_logDocument, SIGNAL( logLineReady(QString) ),
                    this, SLOT( appendLogLine(QString) ));
            connect(m_logDocument, SIGNAL( cleared() ),
                    this, SLOT( clearLogLines() ));

            connect(m_loggerTreeWidget, SIGNAL( customContextMenuRequested(const QPoint&) ),
                    this, SLOT( loggerTreeContextMenuRequested(const QPoint&)));
        }

        LogDialog::~LogDialog()
        {
            disconnect(m_logDocument, SIGNAL( logLineReady(QString) ),
                    this, SLOT( appendLogLine(QString) ));
            disconnect(m_logDocument, SIGNAL( cleared() ),
                    this, SLOT( clearLogLines() ));
        }

        void LogDialog::loggerTreeContextMenuRequested(const QPoint& point)
        {
            QModelIndex index = m_loggerTreeWidget->indexAt(point);
            if (index.isValid()) {
                QMenu contextMenu;

                QAction* actionEnable = contextMenu.addAction(tr("Enable"));
                QAction* actionDisable = contextMenu.addAction(tr("Disable"));
                contextMenu.addSeparator();
                QAction* actionEnableRecursive = contextMenu.addAction(tr("Enable Recursive"));
                QAction* actionDisableRecursive = contextMenu.addAction(tr("Disable Recursive"));

                QPoint contextPos = m_loggerTreeWidget->mapToGlobal(point);

                QAction* action = contextMenu.exec(contextPos);
                if (action == actionEnable) {
                    m_logDocument->setLoggerEnabled(index, true, false);
                } else if (action == actionDisable) {
                    m_logDocument->setLoggerEnabled(index, false, false);
                } else if (action == actionEnableRecursive) {
                    m_logDocument->setLoggerEnabled(index, true, true);
                } else if (action == actionDisableRecursive) {
                    m_logDocument->setLoggerEnabled(index, false, true);
                }
            }
        }

        void LogDialog::closeDialog()
        {
            close();
        }

        void LogDialog::clearLogLines()
        {
            m_logTextEdit->clear();
        }

        void LogDialog::appendLogLine(QString line)
        {
            m_logTextEdit->insertPlainText(line);
        }

    }
}
