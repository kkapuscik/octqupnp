/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup App
 * @{
 */

#ifndef _OCT_APP__GUI_APPLICATION_H_
#define _OCT_APP__GUI_APPLICATION_H_

/*-----------------------------------------------------------------------------*/

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace App
    {
        // forward
        class LogDialog;

        /**
         * Base class for OCTaedr GUI applications.
         */
        class GuiApplication : public QApplication
        {
        Q_OBJECT

        private:
            /** Debug menu enabled flag. */
            bool m_debugMenuEnabled;
            /** Log dialog. */
            LogDialog* m_logDialog;

        protected:
            /**
             * Constructs application.
             *
             * @param argc
             *      Arguments count (from main function).
             * @param argv
             *      Arguments array (from main function).
             */
            GuiApplication(int& argc, char** argv);

        public slots:
            /**
             * Terminates application.
             *
             * Does quit() internally but allows cleanup
             * code to be called before.
             */
            virtual void terminate();

        private:
            /**
             * Executes the application.
             *
             * @return
             * Application return code.
             */
            int execute();

            /**
             * Performs console log setup.
             */
            void setupConsoleLog();

            /**
             * Performs file log setup.
             *
             * @param fileName
             *      Name of the log file.
             */
            void setupFileLog(QString fileName);

            /**
             * Creates and appends debug menu to main window.
             */
            void createDebugMenu();

        protected:
            /**
             * Prints application options to console.
             */
            virtual void printUsage();

            /**
             * Processes command line arguments.
             *
             * @return
             * Zero on success, error code on error.
             */
            virtual int processArguments();

            /**
             * Returns application main window.
             *
             * @return
             * Main window of the application or NULL if not available.
             */
            virtual QMainWindow* mainWindow();

            /**
             * Initializes application resources.
             *
             * @return
             * Zero on success, error code on error.
             */
            virtual int initResources();

            /**
             * Initializes & shows application windows.
             *
             * @return
             * Zero on success, error code on error.
             */
            virtual int initWindows();

            /**
             * Releases windows before application terminate.
             */
            virtual void releaseWindows();

            /**
             * Releases resources before application terminate.
             */
            virtual void releaseResources();

        public:
            /**
             * Shows application about dialog.
             *
             * @param parent
             *      Parent window.
             *
             * TODO: change to non-static.
             */
            static void showAboutDialog(QWidget* parent);

            /**
             * Shows Qt about dialog.
             *
             * @param parent
             *      Parent window.
             *
             * TODO: change to non-static.
             */
            static void showAboutQtDialog(QWidget* parent);

            /**
             * Starts application.
             *
             * @param appName
             *      Application name.
             * @param appVersion
             *      Application version.
             * @param argc
             *      Arguments count (from main function).
             * @param argv
             *      Arguments array (from main function).
             *
             * @return
             * Application result code. Zero on success, error code on error.
             */
            template<class T>
            static int startApp(const QString& appName, const QString& appVersion, int& argc, char** argv)
            {
                /* init global variables */
                QCoreApplication::setOrganizationName("OCTaedr Software");
                QCoreApplication::setOrganizationDomain("octaedr.info");
                QCoreApplication::setApplicationName(appName);
                QCoreApplication::setApplicationVersion(appVersion);

                GuiApplication* app = new T(argc, argv);

                int rv = app->execute();

                delete app;

                return rv;
            }
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_APP__GUI_APPLICATION_H_ */

/**
 * @}
 * @}
 * @}
 */
