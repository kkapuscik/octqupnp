/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "GuiApplication.h"
#include "LogDialog.h"

#include <log/ConsoleHandler.h>
#include <log/FileHandler.h>
#include <log/Manager.h>

#include <QtCore/QDebug>
#include <QtCore/QStringList>

#include <QtWidgets/QMessageBox>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace App
    {
        GuiApplication::GuiApplication(int& argc, char** argv) :
            QApplication(argc, argv), m_debugMenuEnabled(false), m_logDialog(NULL)
        {
            // nothing to do
        }

        void GuiApplication::printUsage()
        {
            qDebug() << "Options:";
            qDebug() << "--help            Print this info";
            qDebug() << "--logtty          Enables console debug mode";
            qDebug() << "--loggui          Enables GUI debug mode";
            qDebug() << "--logfile <path>  Enables file debug mode (printing message to <file>";
        }

        void GuiApplication::setupConsoleLog()
        {
            Oct::Log::ConsoleHandler* consoleLogHandler;

            consoleLogHandler = new Oct::Log::ConsoleHandler(this);

            consoleLogHandler->connectHandler();
        }

        void GuiApplication::setupFileLog(QString fileName)
        {
            Oct::Log::FileHandler* fileLogHandler;

            fileLogHandler = new Oct::Log::FileHandler(fileName, this);

            fileLogHandler->connectHandler();
        }

        int GuiApplication::processArguments()
        {
            int rv = 0;

            QStringList args = arguments();
            for (int i = 0; rv == 0 && i < args.size(); ++i) {
                if (args[i] == "--help") {
                    // force 'error' to print message
                    rv = -1;
                } else if (args[i] == "--logtty") {
                    setupConsoleLog();
                } else if (args[i] == "--loggui") {
                    m_debugMenuEnabled = true;
                } else if (args[i] == "--logfile") {
                    if (i + 1 < args.size()) {
                        ++i;
                        QString logFile = args[i];

                        setupFileLog(logFile);
                    } else {
                        rv = -1;
                    }
                }
            }

            if (rv != 0) {
                printUsage();
            }

            return rv;
        }

        QMainWindow* GuiApplication::mainWindow()
        {
            return NULL;
        }

        int GuiApplication::initResources()
        {
            return 0;
        }

        int GuiApplication::initWindows()
        {
            return 0;
        }

        void GuiApplication::createDebugMenu()
        {
            QMainWindow* mainWnd = mainWindow();

            QMenuBar* mainMenuBar = mainWnd->menuBar();
            if (mainMenuBar != NULL) {
                Q_ASSERT(m_logDialog == NULL);

                m_logDialog = new LogDialog();

                QMenu* debugMenu = mainMenuBar->addMenu(tr("Debug"));
                debugMenu->addAction(tr("Log"), m_logDialog, SLOT( show() ));
            }
        }

        void GuiApplication::releaseWindows()
        {
            // do nothing
        }

        void GuiApplication::releaseResources()
        {
            // do nothing
        }

        void GuiApplication::terminate()
        {
            /* call quit */
            QApplication::quit();
        }

        int GuiApplication::execute()
        {
            int rv = 1;

            Oct::Log::Manager::instance()->loadSettings();

            do {
                rv = processArguments();
                if (rv != 0) {
                    break;
                }

                rv = initResources();
                if (rv != 0) {
                    break;
                }

                rv = initWindows();
                if (rv != 0) {
                    break;
                }

                if (m_debugMenuEnabled) {
                    createDebugMenu();
                }

                rv = exec();
                if (rv != 0) {
                    break;
                }
            } while (0);

            releaseWindows();
            releaseResources();

            Oct::Log::Manager::instance()->saveSettings();

            return rv;
        }

        void GuiApplication::showAboutDialog(QWidget* parent)
        {
            QMessageBox::about(parent, tr("&About"),
                    QString("%1 %2\n\n%3 (%4)\n\nCopyright (C) 2010-2012 Krzysztof Kapuscik").
                    arg(QCoreApplication::applicationName()).
                    arg(QCoreApplication::applicationVersion()).
                    arg(QCoreApplication::organizationName()).
                    arg(QCoreApplication::organizationDomain()));
        }

        void GuiApplication::showAboutQtDialog(QWidget* parent)
        {
            QMessageBox::aboutQt(parent, tr("About Qt"));
        }

    }
}

/*-----------------------------------------------------------------------------*/
