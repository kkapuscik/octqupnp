/*
 * $Id: LoggerModel.h 126 2011-03-28 21:28:59Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup App
 * @{
 */

#ifndef _OCT_APP__LOGGER_MODEL_H_
#define _OCT_APP__LOGGER_MODEL_H_

/*-----------------------------------------------------------------------------*/

#include <log/PathEntry.h>

#include <QtGui/QStandardItemModel>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace App
    {
        /**
         * Logger management model.
         */
        class LoggerModel : public QStandardItemModel
        {
        Q_OBJECT

        public:
            /**
             * Constructs new model.
             *
             * @param parent
             *      Object owning this model.
             */
            explicit LoggerModel(QObject *parent = 0);

            /**
             * Sets logger(s) status.
             *
             * @param index
             *      Index of logger in logger model.
             * @param enabled
             *      Target state.
             * @param recursive
             *      Recursive operation flag.
             */
            void setLoggerEnabled(QModelIndex index, bool enabled, bool recursive);

            /**
             * Processes path.
             *
             * Adds path if it was unknown.
             * Updates path information if it was known.
             *
             * @param entry
             *      Path entry to process.
             */
            void processPath(const Oct::Log::PathEntry& entry);

        private:
            /**
             * Sets logger(s) status.
             *
             * @param item
             *      Logger tree item.
             * @param enabled
             *      Target state.
             * @param recursive
             *      Recursive operation flag.
             */
            void setLoggerEnabled(QStandardItem* item, bool enabled, bool recursive);

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_APP__LOGGER_MODEL_H_ */

/**
 * @}
 * @}
 * @}
 */
