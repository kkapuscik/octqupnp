/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "LogDocument.h"
#include "LogLinesModel.h"
#include "LoggerModel.h"

#include <log/Manager.h>
#include <log/Handler.h>

#include <QtGui/QStandardItemModel>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace App
    {
        LogDocument::LogDocument(QObject* parent) :
            QObject(parent)
        {
            m_logLinesModel = new LogLinesModel(this);
            m_loggerModel = new LoggerModel(this);

            Oct::Log::Manager* manager = Oct::Log::Manager::instance();

            connect(manager, SIGNAL( pathChanged(Oct::Log::PathEntry) ),
                    this, SLOT( onPathChange(Oct::Log::PathEntry) ));
            connect(manager, SIGNAL( messageReady(const QString&,const QString&,Oct::Log::Level,const QString&,const QString&) ),
                    this, SLOT( onMessageReady(const QString&,const QString&,Oct::Log::Level,const QString&,const QString&) ));

            QList<Oct::Log::PathEntry> entries = manager->pathEntries();
            for (int i = 0; i < entries.length(); ++i) {
                m_loggerModel->processPath(entries[i]);
            }
        }

        QAbstractItemModel* LogDocument::loggerModel() const
        {
            return m_loggerModel;
        }

        QAbstractItemModel* LogDocument::linesModel() const
        {
            return m_logLinesModel;
        }

        void LogDocument::clear()
        {
            m_logLinesModel->clear();
            emit cleared();
        }

        void LogDocument::onPathChange(Oct::Log::PathEntry entry)
        {
            m_loggerModel->processPath(entry);
        }

        void LogDocument::onMessageReady(const QString& path, const QString& prefix, Oct::Log::Level level,
                const QString& instance, const QString& message)
        {
            QString levelStr = Oct::Log::Handler::levelString(level);

            m_logLinesModel->addLine(path, prefix, levelStr, instance, message);

            QString prefixNormalized = Oct::Log::Handler::normalizePrefix(prefix);
            QString logLine = QString("%1 [%2] [%3] %4\n").arg(levelStr).arg(prefixNormalized).arg(instance).arg(message);

            emit logLineReady(logLine);
        }

        void LogDocument::setLoggerEnabled(QModelIndex index, bool enabled, bool recursive)
        {
            m_loggerModel->setLoggerEnabled(index, enabled, recursive);
        }

    }
}
