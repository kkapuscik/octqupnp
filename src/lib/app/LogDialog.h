/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup App
 * @{
 */

#ifndef _OCT_APP__LOG_DIALOG_H_
#define _OCT_APP__LOG_DIALOG_H_

/*-----------------------------------------------------------------------------*/

#include "LogDocument.h"

#include <QtWidgets/QDialog>

/*-----------------------------------------------------------------------------*/

class QTextEdit;
class QTreeView;
class QTableView;

namespace Oct
{
    namespace App
    {
        class LogDialog : public QDialog
        {
        Q_OBJECT

        private:
            LogDocument* m_logDocument;
            QTextEdit* m_logTextEdit;
            QTableView* m_logTableView;
            QTreeView* m_loggerTreeWidget;

        public:
            explicit LogDialog(QWidget *parent = 0, Qt::WindowFlags flags = 0);
            virtual ~LogDialog();

        private slots:
            void closeDialog();

            void loggerTreeContextMenuRequested(const QPoint& point);

            void clearLogLines();
            void appendLogLine(QString line);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_APP__LOG_DIALOG_H_ */

/**
 * @}
 * @}
 * @}
 */
