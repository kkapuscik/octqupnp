/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup App
 * @{
 */

#ifndef _OCT_APP__LOG_DOCUMENT_H_
#define _OCT_APP__LOG_DOCUMENT_H_

/*-----------------------------------------------------------------------------*/

#include <log/Level.h>
#include <log/PathEntry.h>

#include <QtCore/QObject>
#include <QtCore/QAbstractItemModel>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace App
    {
        class LogLinesModel;
        class LoggerModel;

        /**
         * Log document.
         */
        class LogDocument : public QObject
        {
        Q_OBJECT

        private:
            /** Log lines model. */
            LogLinesModel* m_logLinesModel;
            /** Logger management model. */
            LoggerModel* m_loggerModel;

        public:
            /**
             * Constructs log document.
             *
             * @param parent
             *      Owner of the document.
             */
            explicit LogDocument(QObject* parent = 0);

            /**
             * Returns logger management model.
             *
             * @return
             * Logger management model.
             */
            QAbstractItemModel* loggerModel() const;

            /**
             * Returns log lines model.
             *
             * @return
             * Log lines model.
             */
            QAbstractItemModel* linesModel() const;

        public slots:
            /**
             * Clears log contents.
             */
            void clear();

            /**
             * Sets logger(s) status.
             *
             * @param index
             *      Index of logger in logger model.
             * @param enabled
             *      Target state.
             * @param recursive
             *      Recursive operation flag.
             */
            void setLoggerEnabled(QModelIndex index, bool enabled, bool recursive);

        signals:
            /**
             * Notifies that new log line is ready.
             *
             * @param line
             *      New log line.
             */
            void logLineReady(QString line);

            /**
             * Notifies that log contents has been cleared.
             */
            void cleared();

        private slots:
            /**
             * Slot listening for logging manager path change signal.
             *
             * @param entry
             *      Entry that changed.
             */
            virtual void onPathChange(Oct::Log::PathEntry entry);

            /**
             * Slot listening for logging manager messages.
             *
             * @param path
             *      Logger path.
             * @param prefix
             *      Message prefix.
             * @param level
             *      Message level.
             * @param instance
             *      Instance pointer as string.
             * @param message
             *      The message.
             */
            void onMessageReady(const QString& path, const QString& prefix, Oct::Log::Level level,
                    const QString& instance, const QString& message);
        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_APP__LOG_DOCUMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
