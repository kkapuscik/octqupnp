/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup App      Application base elements
 * @{
 */

#ifndef _OCT_APP__PACKAGE_H_
#define _OCT_APP__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * Application base elements.
     */
    namespace App
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_APP__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
