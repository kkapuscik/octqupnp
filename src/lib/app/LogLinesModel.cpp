/*
 * $Id: LogLinesModel.cpp 108 2011-03-15 15:09:12Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "LogLinesModel.h"

/*-----------------------------------------------------------------------------*/

namespace Oct {
    namespace App {
        LogLinesModel::LogLinesModel(QObject* parent) :
            QStandardItemModel(parent), m_line(0)
        {
            int i = 0;
            setHorizontalHeaderItem(i++, new QStandardItem("Line"));
            setHorizontalHeaderItem(i++, new QStandardItem("Path"));
            setHorizontalHeaderItem(i++, new QStandardItem("Prefix"));
            setHorizontalHeaderItem(i++, new QStandardItem("Level"));
            setHorizontalHeaderItem(i++, new QStandardItem("Instance"));
            setHorizontalHeaderItem(i++, new QStandardItem("Message"));
        }

        void LogLinesModel::addLine(const QString& path, const QString& prefix,
                const QString& level, const QString& instance,
                const QString& message)
        {
            QList<QStandardItem*> newRow;

            ++m_line;

            newRow.append(new QStandardItem(QString("%1").arg(m_line)));
            newRow.append(new QStandardItem(path));
            newRow.append(new QStandardItem(prefix));
            newRow.append(new QStandardItem(level));
            newRow.append(new QStandardItem(instance));
            newRow.append(new QStandardItem(message));

            appendRow(newRow);
        }

    }
}
