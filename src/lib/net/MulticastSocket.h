/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Net
 * @{
 */

#ifndef _OCT_NET__MULTICAST_SOCKET_H_
#define _OCT_NET__MULTICAST_SOCKET_H_

/*-----------------------------------------------------------------------------*/

#include <log/Logger.h>
#include <QtNetwork/QUdpSocket>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Net
    {
        class MulticastSocket : public QUdpSocket
        {
        Q_OBJECT

        private:
            static Oct::Log::Logger<MulticastSocket> Logger;

        public:
            MulticastSocket(QObject* parent = 0);
            virtual ~MulticastSocket();

            bool joinGroup(const QHostAddress &address, const QHostAddress &iface = QHostAddress(QHostAddress::Any));
            bool setTTL(int ttl);
            bool setMulticastTTL(int ttl);
            bool setMulticastLoop(bool enable);

        };
    }
}

/*-----------------------------------------------------------------------------*/

#endif /* _OCT_NET__MULTICAST_SOCKET_H_ */

/**
 * @}
 * @}
 * @}
 */
