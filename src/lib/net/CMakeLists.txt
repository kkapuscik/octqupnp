cmake_minimum_required(VERSION 2.8.11)

project(liboctnet)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Network REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

include_directories(${Qt5Core_INCLUDE_DIRS})
include_directories(${Qt5Network_INCLUDE_DIRS})

file(GLOB liboctnet_SOURCES *.cpp)
file(GLOB liboctnet_HEADERS *.h)

QT5_WRAP_CPP(liboctnet_MOCSOURCES ${liboctnet_HEADERS})
#macro QT5_WRAP_UI(outfiles inputfile ... OPTIONS ...)
#macro QT5_ADD_RESOURCES(outfiles inputfile ... OPTIONS ...)

add_library(octnet STATIC ${liboctnet_SOURCES} ${liboctnet_HEADERS} ${liboctnet_MOCSOURCES})
target_link_libraries(octnet octlog Qt5::Core Qt5::Network)
