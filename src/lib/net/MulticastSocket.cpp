/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "MulticastSocket.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <errno.h>
#include <string.h>

/*-----------------------------------------------------------------------------*/

namespace Oct
{
    namespace Net
    {
        Oct::Log::Logger<MulticastSocket> MulticastSocket::Logger("/oct/net/MulticastSocket");

        MulticastSocket::MulticastSocket(QObject* parent) :
            QUdpSocket(parent)
        {
        }

        MulticastSocket::~MulticastSocket()
        {
        }

        bool MulticastSocket::joinGroup(const QHostAddress& address, const QHostAddress& iface)
        {
            bool rv = false;

            int sockFD = socketDescriptor();
            if (sockFD >= 0) {
                struct ip_mreqn joinData;

                memset(&joinData, 0, sizeof(joinData));

                if (address.protocol() == IPv4Protocol && iface.protocol() == IPv4Protocol) {
                    joinData.imr_multiaddr.s_addr = htonl(address.toIPv4Address());
                    joinData.imr_address.s_addr = htonl(iface.toIPv4Address());
                    joinData.imr_ifindex = 0;

                    if (::setsockopt(sockFD, IPPROTO_IP, IP_ADD_MEMBERSHIP, &joinData, sizeof(joinData)) == 0) {
                        rv = true;
                    } else {
                        Logger.trace(this) << "Setsockopt failed for sockfd=" << sockFD << " with error " << errno
                                << " " << strerror(errno);
                    }
                }
            }

            return rv;
        }

        bool MulticastSocket::setTTL(int ttl)
        {
            bool rv = false;

            int sockFD = socketDescriptor();
            if (sockFD >= 0) {
                unsigned char option = (unsigned char)ttl;
                if (::setsockopt(sockFD, IPPROTO_IP, IP_TTL, &option, sizeof(option)) == 0) {
                    rv = true;
                } else {
                    Logger.trace(this) << "Setsockopt failed for sockfd=" << sockFD << " with error " << errno << " "
                            << strerror(errno);
                }
            }

            return rv;
        }

        bool MulticastSocket::setMulticastTTL(int ttl)
        {
            bool rv = false;

            int sockFD = socketDescriptor();
            if (sockFD >= 0) {
                if (::setsockopt(sockFD, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl)) == 0) {
                    rv = true;
                } else {
                    Logger.trace(this) << "Setsockopt failed for sockfd=" << sockFD << " with error " << errno << " "
                            << strerror(errno);
                }
            }

            return rv;
        }

        bool MulticastSocket::setMulticastLoop(bool enable)
        {
            bool rv = false;

            int sockFD = socketDescriptor();
            if (sockFD >= 0) {
                int value = enable ? 1 : 0;

                if (::setsockopt(sockFD, IPPROTO_IP, IP_MULTICAST_LOOP, &value, sizeof(value)) == 0) {
                    rv = true;
                } else {
                    Logger.trace(this) << "Setsockopt failed for sockfd=" << sockFD << " with error " << errno << " "
                            << strerror(errno);
                }
            }

            return rv;
        }
    }
}
