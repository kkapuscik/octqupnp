/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Net    Networking elements
 * @{
 */

#ifndef _OCT_NET__PACKAGE_H_
#define _OCT_NET__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * Networking elements.
     */
    namespace Net
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_NET__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
