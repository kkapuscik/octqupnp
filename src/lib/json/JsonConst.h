/*
 * JsonArray.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_JSON__JSON_CONST_H_
#define _OCT_JSON__JSON_CONST_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        /**
         * JSON value constants.
         */
        enum JsonConst
        {
            /** Value 'true' constant. */
            JsonTrue,
            /** Value 'false' constant. */
            JsonFalse,
            /** Value 'null' constant. */
            JsonNull
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_JSON__JSON_CONST_H_ */
