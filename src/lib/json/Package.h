/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Libs
 * @{
 * \addtogroup Json      JSON related elements
 * @{
 */

#ifndef _OCT_JSON__PACKAGE_H_
#define _OCT_JSON__PACKAGE_H_

/*---------------------------------------------------------------------------*/

namespace Oct
{
    /**
     * JSON related elements
     */
    namespace Json
    {
        // other documentation goes here
    }
}

/*-----------------------------------------------------------------------------*/

#endif /*_OCT_JSON__PACKAGE_H_*/

/**
 * @}
 * @}
 * @}
 */
