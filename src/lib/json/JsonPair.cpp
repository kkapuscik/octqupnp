/*
 * JsonPair.cpp
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "JsonPair.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        const JsonPair::Null JsonPair::null = JsonPair::Null();

        JsonPair::JsonPair() :
            m_key(QString::null), m_value(JsonValue::null)
        {
            // nothing to do
        }

        JsonPair::JsonPair(const Null&) :
            m_key(QString::null), m_value(JsonValue::null)
        {
            // nothing to do
        }

        JsonPair::JsonPair(const QString& key, const JsonValue& value) :
            m_key(key), m_value(value)
        {
            Q_ASSERT(!m_key.isNull());
            Q_ASSERT(!m_value.isNull());
        }

        bool JsonPair::isNull() const
        {
            return m_key.isNull();
        }
    }
}
