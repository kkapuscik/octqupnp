/*
 * JsonValue.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_JSON__JSON_VALUE_H_
#define _OCT_JSON__JSON_VALUE_H_

/*---------------------------------------------------------------------------*/

#include "JsonConst.h"

#include <util/SharedDataPointer.h>

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        // forward
        class JsonObject;
        class JsonArray;
        class JsonNumber;

        class JsonValuePrivateData;

        /**
         * JSON value.
         */
        class JsonValue
        {
        public:
            /**
             * \internal
             *
             * Null structure - helper for creating null objects.
             */
            struct Null
            {
                // intentionally empty
            };

            /**
             * Constant for creating null values.
             */
            static const Null null;

        private:
            /**
             * Value data.
             */
            Oct::Util::SharedDataPointer<JsonValuePrivateData> m_data;

        public:
            /**
             * Constructs null JSON value.
             */
            JsonValue();

            /**
             * Constructs null JSON value.
             */
            JsonValue(const Null&);

            /**
             * Constructs JSON string value.
             *
             * @param value
             *      The value to set.
             */
            JsonValue(QString value);

            /**
             * Constructs JSON number value.
             *
             * @param value
             *      The value to set.
             */
            JsonValue(JsonNumber value);

            /**
             * Constructs JSON object value.
             *
             * @param value
             *      The value to set.
             */
            JsonValue(JsonObject value);

            /**
             * Constructs JSON array value.
             *
             * @param value
             *      The value to set.
             */
            JsonValue(JsonArray value);

            /**
             * Constructs JSON const value.
             *
             * @param value
             *      The value to set.
             */
            JsonValue(JsonConst value);

            /**
             * Checks if it is null value.
             *
             * @return
             * True if object is null (invalid).
             */
            bool isNull() const;

            /**
             * Checks if the value is a string.
             *
             * @return
             * True if value is a string, false otherwise.
             */
            bool isString() const;

            /**
             * Checks if the value is a number.
             *
             * @return
             * True if value is a number, false otherwise.
             */
            bool isNumber() const;

            /**
             * Checks if the value is an object.
             *
             * @return
             * True if value is an object, false otherwise.
             */
            bool isObject() const;

            /**
             * Checks if the value is an array.
             *
             * @return
             * True if value is an array, false otherwise.
             */
            bool isArray() const;

            /**
             * Checks if the value is a 'null' constant.
             *
             * @return
             * True if value is a 'null' constant, false otherwise.
             */
            bool isNullConst() const;

            /**
             * Checks if the value is a 'true' constant.
             *
             * @return
             * True if value is a 'true' constant, false otherwise.
             */
            bool isTrueConst() const;

            /**
             * Checks if the value is a 'false' constant.
             *
             * @return
             * True if value is a 'false' constant, false otherwise.
             */
            bool isFalseConst() const;

            /**
             * Checks if value is a constant.
             *
             * Checks if value is one of predefined constants: 'null', 'false' or 'true'.
             *
             * @return
             * True if value is a constant, false otherwise.
             */
            bool isConst() const;

            /**
             * Returns value as a string.
             *
             * @return
             * Value on success or null string on error (the value is not a string).
             */
            QString toString() const;

            /**
             * Returns value as a number.
             *
             * @return
             * Value on success or null number on error (the value is not a number).
             */
            JsonNumber toNumber() const;

            /**
             * Returns value as an object.
             *
             * @return
             * Value on success or null object on error (the value is not an object).
             */
            JsonObject toObject() const;

            /**
             * Returns value as an array.
             *
             * @return
             * Value on success or null array on error (the value is not an array).
             */
            JsonArray toArray() const;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_JSON__JSON_VALUE_H_ */
