/*
 * JsonObject.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_JSON__JSON_OBJECT_H_
#define _OCT_JSON__JSON_OBJECT_H_

/*---------------------------------------------------------------------------*/

#include "JsonPair.h"

#include <util/SharedDataPointer.h>

#include <QtCore/QList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        class JsonObjectPrivateData;

        /**
         * JSON object.
         */
        class JsonObject
        {
        public:
            /**
             * \internal
             *
             * Null structure - helper for creating null objects.
             */
            struct Null
            {
                // intentionally empty
            };

            /**
             * Constant for creating null objects.
             */
            static const Null null;

        private:
            /**
             * Object data.
             */
            Oct::Util::SharedDataPointer<JsonObjectPrivateData> m_data;

        public:
            /**
             * Constructs empty JSON object.
             */
            JsonObject();

            /**
             * Constructs null (invalid) JSON object.
             */
            JsonObject(const Null&);

            /**
             * Constructs object with given members.
             *
             * @param members
             *      Members of the created object.
             */
            JsonObject(const QList<JsonPair>& members);

            /**
             * Checks if it is null object.
             *
             * @return
             * True if object is null (invalid).
             */
            bool isNull() const;

            /**
             * Appends member to object.
             *
             * @param member
             *      Member to add.
             */
            void appendMember(const JsonPair& member);

            /**
             * Add member to object.
             *
             * @param index
             *      Index at which member shall be added.
             * @param member
             *      Member to add.
             */
            void addMember(int index, const JsonPair& member);

            /**
             * Removes member.
             *
             * @param index
             *      Index of the member to remove.
             */
            void removeMember(int index);

            /**
             * Returns number of members.
             *
             * @return
             * Number of members.
             */
            int memberCount() const;

            /**
             * Returns member at given position.
             *
             * @param index
             *      Position of member.
             *
             * @return
             * Member at given position.
             */
            JsonPair member(int index) const;

            /**
             * Returns all members.
             *
             * @return
             * Collection of all members.
             */
            QList<JsonPair> members() const;

            /**
             * Returns member for given key.
             *
             * @param key
             *      Key of the member to get.
             *
             * @return
             * Member object if member was found or null pair otherwise.
             */
            JsonPair member(QString key) const;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_JSON__JSON_OBJECT_H_ */
