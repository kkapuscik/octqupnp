cmake_minimum_required(VERSION 2.8.11)

project(liboctjson)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

include_directories(${Qt5Core_INCLUDE_DIRS})

file(GLOB liboctjson_SOURCES *.cpp)
file(GLOB liboctjson_HEADERS *.h)

QT5_WRAP_CPP(liboctjson_MOCSOURCES ${liboctjson_HEADERS})
#macro QT5_WRAP_UI(outfiles inputfile ... OPTIONS ...)
#macro QT5_ADD_RESOURCES(outfiles inputfile ... OPTIONS ...)

add_library(octjson STATIC ${liboctjson_SOURCES} ${liboctjson_MOCSOURCES})
target_link_libraries(octjson octlog Qt5::Core)
