/*
 * JsonObject.cpp
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "JsonObject.h"

#include <QtCore/QList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        const JsonObject::Null JsonObject::null = JsonObject::Null();

        /**
         * Shared object data.
         */
        class JsonObjectPrivateData : public Oct::Util::SharedData<JsonObjectPrivateData>
        {
        public:
            /**
             * \internal
             *
             * Null object data.
             */
            static Oct::Util::SharedDataPointer<JsonObjectPrivateData> NullData;

            /**
             * \internal
             *
             * Empty object data.
             */
            static Oct::Util::SharedDataPointer<JsonObjectPrivateData> EmptyData;

        public:
            /**
             * Null data flag.
             */
            bool m_isNull;

            /**
             * Members collection.
             */
            QList<JsonPair> m_members;

        public:
            /**
             * Constructs empty data object.
             *
             * @param isNull
             *      Null data flag.
             */
            JsonObjectPrivateData(bool isNull = false) :
                m_isNull(isNull)
            {
                // nothing to do
            }

        protected:
            JsonObjectPrivateData* clone() const
            {
                return new JsonObjectPrivateData(*this);
            }
        };

        Oct::Util::SharedDataPointer<JsonObjectPrivateData> JsonObjectPrivateData::NullData(new JsonObjectPrivateData(
                true));
        Oct::Util::SharedDataPointer<JsonObjectPrivateData> JsonObjectPrivateData::EmptyData(new JsonObjectPrivateData(
                false));

        JsonObject::JsonObject() :
            m_data(JsonObjectPrivateData::EmptyData)
        {
            // nothing to do
        }

        JsonObject::JsonObject(const Null&) :
            m_data(JsonObjectPrivateData::NullData)
        {
            // nothing to do
        }

        JsonObject::JsonObject(const QList<JsonPair>& members) :
            m_data(JsonObjectPrivateData::EmptyData)
        {
            if (members.size() > 0) {
                m_data.data()->m_members.append(members);

                for (int i = 0; i < members.size(); ++i) {
                    Q_ASSERT(!members[i].isNull());
                }
            }
        }

        bool JsonObject::isNull() const
        {
            return m_data.constData()->m_isNull;
        }

        void JsonObject::appendMember(const JsonPair& member)
        {
            Q_ASSERT(!member.isNull());

            JsonObjectPrivateData* data = m_data.data();

            data->m_isNull = false;
            data->m_members.append(member);
        }

        void JsonObject::addMember(int index, const JsonPair& member)
        {
            Q_ASSERT(!member.isNull());

            JsonObjectPrivateData* data = m_data.data();

            data->m_isNull = false;
            data->m_members.insert(index, member);
        }

        void JsonObject::removeMember(int index)
        {
            JsonObjectPrivateData* data = m_data.data();

            data->m_members.removeAt(index);
        }

        int JsonObject::memberCount() const
        {
            const JsonObjectPrivateData* data = m_data.constData();

            return data->m_members.size();
        }

        JsonPair JsonObject::member(int index) const
        {
            const JsonObjectPrivateData* data = m_data.constData();

            return data->m_members.at(index);
        }

        QList<JsonPair> JsonObject::members() const
        {
            const JsonObjectPrivateData* data = m_data.constData();

            return data->m_members;
        }

        /**
         * Returns member for given key.
         *
         * @param key
         *      Key of the member to get.
         *
         * @return
         * Member object if member was found or null pair otherwise.
         */
        JsonPair JsonObject::member(QString key) const
        {
            for (int i = 0; i < m_data.constData()->m_members.size(); ++i) {
                if (m_data.constData()->m_members.at(i).key() == key) {
                    return m_data.constData()->m_members.at(i);
                }
            }
            return JsonPair::null;
        }

    }
}
