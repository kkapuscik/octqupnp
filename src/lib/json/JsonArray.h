/*
 * JsonArray.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_JSON__JSON_ARRAY_H_
#define _OCT_JSON__JSON_ARRAY_H_

/*---------------------------------------------------------------------------*/

#include "JsonValue.h"

#include <util/SharedDataPointer.h>

#include <QtCore/QList>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        class JsonArrayPrivateData;

        /**
         * JSON array.
         */
        class JsonArray
        {
        public:
            /**
             * \internal
             *
             * Null structure - helper for creating null objects.
             */
            struct Null
            {
                // intentionally empty
            };

            /**
             * Constant for creating null arrays.
             */
            static const Null null;

        private:
            /**
             * Array data.
             */
            Oct::Util::SharedDataPointer<JsonArrayPrivateData> m_data;

        public:
            /**
             * Constructs empty JSON array.
             */
            JsonArray();

            /**
             * Constructs null (invalid) JSON array.
             */
            JsonArray(const Null&);

            /**
             * Constructs array with given values.
             *
             * @param values
             *      Values of the created array.
             */
            JsonArray(const QList<JsonValue>& values);

            /**
             * Checks if it is null array.
             *
             * @return
             * True if array is null (invalid).
             */
            bool isNull() const;

            /**
             * Appends value to array.
             *
             * @param value
             *      Value to add.
             */
            void appendValue(const JsonValue& value);

            /**
             * Add value to array.
             *
             * @param index
             *      Index at which value shall be added.
             * @param value
             *      Value to add.
             */
            void addValue(int index, const JsonValue& value);

            /**
             * Removes value.
             *
             * @param index
             *      Index of the value to remove.
             */
            void removeValue(int index);

            /**
             * Returns number of values.
             *
             * @return
             * Number of values in array.
             */
            int valueCount() const;

            /**
             * Returns value at given position.
             *
             * @param index
             *      Position of value.
             *
             * @return
             * Value at given position.
             */
            JsonValue value(int index) const;

            /**
             * Returns all values.
             *
             * @return
             * Collection of all values.
             */
            QList<JsonValue> values() const;
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_JSON__JSON_ARRAY_H_ */
