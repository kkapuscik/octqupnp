/*
 * JsonArray.cpp
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "JsonArray.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        const JsonArray::Null JsonArray::null = JsonArray::Null();

        /**
         * Shared object data.
         */
        class JsonArrayPrivateData : public Oct::Util::SharedData<JsonArrayPrivateData>
        {
        public:
            /**
             * \internal
             *
             * Null object data.
             */
            static Oct::Util::SharedDataPointer<JsonArrayPrivateData> NullData;

            /**
             * \internal
             *
             * Empty object data.
             */
            static Oct::Util::SharedDataPointer<JsonArrayPrivateData> EmptyData;

        public:
            /**
             * Null data flag.
             */
            bool m_isNull;

            /**
             * Values collection.
             */
            QList<JsonValue> m_values;

        public:
            /**
             * Constructs empty data object.
             *
             * @param isNull
             *      Null data flag.
             */
            JsonArrayPrivateData(bool isNull = false) :
                m_isNull(isNull)
            {
                // nothing to do
            }

        protected:
            JsonArrayPrivateData* clone() const
            {
                return new JsonArrayPrivateData(*this);
            }
        };

        Oct::Util::SharedDataPointer<JsonArrayPrivateData> JsonArrayPrivateData::NullData(new JsonArrayPrivateData(true));
        Oct::Util::SharedDataPointer<JsonArrayPrivateData> JsonArrayPrivateData::EmptyData(new JsonArrayPrivateData(false));

        JsonArray::JsonArray() :
            m_data(JsonArrayPrivateData::EmptyData)
        {
            // nothing to do
        }

        JsonArray::JsonArray(const Null&) :
            m_data(JsonArrayPrivateData::NullData)
        {
            // nothing to do
        }

        JsonArray::JsonArray(const QList<JsonValue>& values) :
            m_data(JsonArrayPrivateData::EmptyData)
        {
            if (values.size() > 0) {
                m_data.data()->m_values.append(values);

                for (int i = 0; i < values.size(); ++i) {
                    Q_ASSERT(!values[i].isNull());
                }
            }
        }

        bool JsonArray::isNull() const
        {
            return m_data.constData()->m_isNull;
        }

        void JsonArray::appendValue(const JsonValue& value)
        {
            Q_ASSERT(!value.isNull());

            JsonArrayPrivateData* data = m_data.data();

            data->m_isNull = false;
            data->m_values.append(value);
        }

        void JsonArray::addValue(int index, const JsonValue& value)
        {
            Q_ASSERT(!value.isNull());

            JsonArrayPrivateData* data = m_data.data();

            data->m_isNull = false;
            data->m_values.insert(index, value);
        }

        void JsonArray::removeValue(int index)
        {
            JsonArrayPrivateData* data = m_data.data();

            data->m_values.removeAt(index);
        }

        int JsonArray::valueCount() const
        {
            const JsonArrayPrivateData* data = m_data.constData();

            return data->m_values.size();
        }

        JsonValue JsonArray::value(int index) const
        {
            const JsonArrayPrivateData* data = m_data.constData();

            return data->m_values.at(index);
        }

        QList<JsonValue> JsonArray::values() const
        {
            const JsonArrayPrivateData* data = m_data.constData();

            return data->m_values;
        }
    }
}
