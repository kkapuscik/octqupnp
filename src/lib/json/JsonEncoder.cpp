/*
 * JsonEncoder.cpp
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "JsonEncoder.h"

#include "JsonArray.h"
#include "JsonNumber.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        Oct::Log::Logger<JsonEncoder> JsonEncoder::Logger("/oct/json/JsonEncoder");

        QByteArray JsonEncoder::encode(JsonObject object, bool breakLines)
        {
            QBuffer buffer;

            if (buffer.open(QBuffer::WriteOnly)) {
                JsonEncoder encoder(&buffer, breakLines);

                if (encoder.encodeObject(object)) {
                    buffer.close();

                    return buffer.data();
                } else {
                    Logger.strace() << "Cannot encode object";
                }
            } else {
                Logger.strace() << "Cannot open output buffer";
            }

            return QByteArray();
        }

        JsonEncoder::JsonEncoder(QIODevice* ioDevice, bool breakLines) :
            m_ioDevice(ioDevice), m_breakLines(breakLines)
        {
            Q_ASSERT(ioDevice->isOpen() && ioDevice->isWritable());
        }

        bool JsonEncoder::write(const char* chars, int length)
        {
            qint64 rv = m_ioDevice->write(chars, length);
            return (rv == length);
        }

        bool JsonEncoder::write(const QByteArray& array)
        {
            qint64 rv = m_ioDevice->write(array);
            return rv == array.length();
        }

        bool JsonEncoder::putChar(char c)
        {
            return m_ioDevice->putChar(c);
        }

        bool JsonEncoder::putBreak()
        {
            if (m_breakLines) {
                return putChar('\n');
            } else {
                return true;
            }
        }

        bool JsonEncoder::putSpace()
        {
            if (m_breakLines) {
                return putChar(' ');
            } else {
                return true;
            }
        }

        bool JsonEncoder::encodeObject(const JsonObject& object)
        {
            if (object.isNull()) {
                return false;
            }

            if (!putChar('{')) {
                return false;
            }
            if (!putBreak()) {
                return false;
            }

            if (!encodeMembers(object)) {
                return false;
            }

            if (!putChar('}')) {
                return false;
            }
            if (!putBreak()) {
                return false;
            }

            return true;
        }

        bool JsonEncoder::encodeMembers(const JsonObject& object)
        {
            int count = object.memberCount();

            for (int i = 0; i < count; ++i) {
                JsonPair pair = object.member(i);

                if (i > 0) {
                    if (!putChar(',')) {
                        return false;
                    }
                    if (!putBreak()) {
                        return false;
                    }
                }

                if (!encodePair(pair)) {
                    return false;
                }
            }

            if (count > 0) {
                if (!putBreak()) {
                    return false;
                }
            }

            return true;
        }

        bool JsonEncoder::encodePair(const JsonPair& pair)
        {
            if (pair.isNull()) {
                return false;
            }

            if (!encodeString(pair.key())) {
                return false;
            }

            if (!putSpace()) {
                return false;
            }
            if (!putChar(':')) {
                return false;
            }
            if (!putSpace()) {
                return false;
            }

            if (!encodeValue(pair.value())) {
                return false;
            }

            return true;
        }

        bool JsonEncoder::encodeValue(const JsonValue& value)
        {
            if (value.isNull()) {
                return false;
            }

            if (value.isArray()) {
                return encodeArray(value.toArray());
            } else if (value.isNumber()) {
                return encodeNumber(value.toNumber());
            } else if (value.isObject()) {
                return encodeObject(value.toObject());
            } else if (value.isString()) {
                return encodeString(value.toString());
            } else if (value.isNullConst()) {
                return encodeConst("null", 4);
            } else if (value.isFalseConst()) {
                return encodeConst("false", 5);
            } else if (value.isTrueConst()) {
                return encodeConst("true", 4);
            } else {
                return false;
            }

            return true;
        }

        bool JsonEncoder::encodeArray(const JsonArray& array)
        {
            if (array.isNull()) {
                return false;
            }

            if (!putChar('[')) {
                return false;
            }
            if (!putBreak()) {
                return false;
            }

            if (!encodeElements(array)) {
                return false;
            }

            if (!putChar(']')) {
                return false;
            }
            if (!putBreak()) {
                return false;
            }

            return true;
        }

        bool JsonEncoder::encodeElements(const JsonArray& array)
        {
            int count = array.valueCount();

            for (int i = 0; i < count; ++i) {
                JsonValue value = array.value(i);

                if (i > 0) {
                    if (!putChar(',')) {
                        return false;
                    }
                    if (!putBreak()) {
                        return false;
                    }
                }

                if (!encodeValue(value)) {
                    return false;
                }
            }

            if (count > 0) {
                if (!putBreak()) {
                    return false;
                }
            }

            return true;
        }

        bool JsonEncoder::encodeNumber(const JsonNumber& number)
        {
            if (number.isNull()) {
                return false;
            }

            return write(number.toString().toUtf8());
        }

        bool JsonEncoder::encodeConst(const char* cstr, int clen)
        {
            return write(cstr, clen);
        }

        bool JsonEncoder::encodeString(const QString& str)
        {
            if (str.isNull()) {
                return false;
            }

            if (!putChar('"')) {
                return false;
            }

            int len = str.length();
            for (int i = 0; i < len; ++i) {
                QChar qc = str.at(i);
                ushort val = qc.unicode();

                if (val == '"') {
                    if (!write("\\\"", 2)) {
                        return false;
                    }
                } else if (val == '\\') {
                    if (!write("\\\\", 2)) {
                        return false;
                    }
                } else if (val == '/') {
                    if (!write("\\/", 2)) {
                        return false;
                    }
                } else if (val == '\b') {
                    if (!write("\\b", 2)) {
                        return false;
                    }
                } else if (val == '\f') {
                    if (!write("\\f", 2)) {
                        return false;
                    }
                } else if (val == '\n') {
                    if (!write("\\n", 2)) {
                        return false;
                    }
                } else if (val == '\r') {
                    if (!write("\\r", 2)) {
                        return false;
                    }
                } else if (val == '\t') {
                    if (!write("\\t", 2)) {
                        return false;
                    }
                } else if (val >= 0x20 && val <= 0xFF) {
                    if (!putChar((char)val)) {
                        return false;
                    }
                } else {
                    QByteArray data = QString("\\u%1").arg(val, 4, 16, QChar('0')).toUtf8();

                    if (!write(data)) {
                        return false;
                    }
                }
            }

            if (!putChar('"')) {
                return false;
            }

            return true;
        }

    }
}
