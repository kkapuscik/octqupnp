/*
 * JsonNumber.cpp
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "JsonNumber.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        const JsonNumber::Null JsonNumber::null = JsonNumber::Null();

        JsonNumber::JsonNumber() :
            m_value(QString::null)
        {
            // nothing to do
        }

        JsonNumber::JsonNumber(const Null&) :
            m_value(QString::null)
        {
            // nothing to do
        }

        JsonNumber::JsonNumber(QString value) :
            m_value(value)
        {
            // nothing to do
        }

        JsonNumber::JsonNumber(int value)
        {
            m_value = QString("%1").arg(value, 0, 10);
        }

        JsonNumber::JsonNumber(uint value)
        {
            m_value = QString("%1").arg(value, 0, 10);
        }

        JsonNumber::JsonNumber(qlonglong value)
        {
            m_value = QString("%1").arg(value, 0, 10);
        }

        JsonNumber::JsonNumber(qulonglong value)
        {
            m_value = QString("%1").arg(value, 0, 10);
        }

        JsonNumber::JsonNumber(float value)
        {
            m_value = QString("%1").arg(value, 0, 'e');
        }

        JsonNumber::JsonNumber(double value)
        {
            m_value = QString("%1").arg(value, 0, 'e');
        }

    }
}
