/*
 * JsonValue.cpp
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "JsonValue.h"

#include "JsonNumber.h"
#include "JsonObject.h"
#include "JsonArray.h"

#include <QtCore/QSharedData>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        const JsonValue::Null JsonValue::null = JsonValue::Null();

        class JsonValuePrivateData : public Oct::Util::SharedData<JsonValuePrivateData>
        {
        public:
            /**
             * \internal
             *
             * Null object data.
             */
            static Oct::Util::SharedDataPointer<JsonValuePrivateData> NullData;

        public:
            enum Type
            {
                TYPE_NULL,
                TYPE_STRING,
                TYPE_NUMBER,
                TYPE_OBJECT,
                TYPE_ARRAY,
                TYPE_CONST
            };

        public:
            Type m_type;
            QString m_string;
            JsonNumber m_number;
            JsonObject m_object;
            JsonArray m_array;
            JsonConst m_const;

        public:
            JsonValuePrivateData() :
                m_type(TYPE_NULL)
            {
                // nothing to do
            }

            JsonValuePrivateData(QString value) :
                m_type(TYPE_STRING), m_string(value)
            {
                Q_ASSERT(!value.isNull());
            }

            JsonValuePrivateData(JsonNumber value) :
                m_type(TYPE_NUMBER), m_number(value)
            {
                Q_ASSERT(!value.isNull());
            }

            JsonValuePrivateData(JsonObject value) :
                m_type(TYPE_OBJECT), m_object(value)
            {
                Q_ASSERT(!value.isNull());
            }

            JsonValuePrivateData(JsonArray value) :
                m_type(TYPE_ARRAY), m_array(value)
            {
                Q_ASSERT(!value.isNull());
            }

            JsonValuePrivateData(JsonConst value) :
                m_type(TYPE_CONST), m_const(value)
            {
                // nothing to do
            }

        protected:
            JsonValuePrivateData* clone() const
            {
                return new JsonValuePrivateData(*this);
            }
        };

        Oct::Util::SharedDataPointer<JsonValuePrivateData> JsonValuePrivateData::NullData(new JsonValuePrivateData());

        JsonValue::JsonValue() :
            m_data(JsonValuePrivateData::NullData)
        {
            // nothing to do
        }

        JsonValue::JsonValue(const Null&) :
            m_data(JsonValuePrivateData::NullData)
        {
            // nothing to do
        }

        JsonValue::JsonValue(QString value) :
            m_data(new JsonValuePrivateData(value))
        {
            // nothing to do
        }

        JsonValue::JsonValue(JsonNumber value) :
            m_data(new JsonValuePrivateData(value))
        {
            // nothing to do
        }

        JsonValue::JsonValue(JsonObject value) :
            m_data(new JsonValuePrivateData(value))
        {
            // nothing to do
        }

        JsonValue::JsonValue(JsonArray value) :
            m_data(new JsonValuePrivateData(value))
        {
            // nothing to do
        }

        JsonValue::JsonValue(JsonConst value) :
            m_data(new JsonValuePrivateData(value))
        {
            // nothing to do
        }

        bool JsonValue::isNull() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_NULL;
        }

        bool JsonValue::isString() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_STRING;
        }

        bool JsonValue::isNumber() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_NUMBER;
        }

        bool JsonValue::isObject() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_OBJECT;
        }

        bool JsonValue::isArray() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_ARRAY;
        }

        bool JsonValue::isNullConst() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_CONST && m_data.constData()->m_const
                    == JsonNull;
        }

        bool JsonValue::isTrueConst() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_CONST && m_data.constData()->m_const
                    == JsonTrue;
        }

        bool JsonValue::isFalseConst() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_CONST && m_data.constData()->m_const
                    == JsonFalse;
        }

        bool JsonValue::isConst() const
        {
            return m_data.constData()->m_type == JsonValuePrivateData::TYPE_CONST;
        }

        QString JsonValue::toString() const
        {
            if (isString()) {
                return m_data.constData()->m_string;
            } else {
                return QString::null;
            }
        }

        JsonNumber JsonValue::toNumber() const
        {
            if (isNumber()) {
                return m_data.constData()->m_number;
            } else {
                return JsonNumber::null;
            }
        }

        JsonObject JsonValue::toObject() const
        {
            if (isObject()) {
                return m_data.constData()->m_object;
            } else {
                return JsonObject::null;
            }
        }

        JsonArray JsonValue::toArray() const
        {
            if (isArray()) {
                return m_data.constData()->m_array;
            } else {
                return JsonArray::null;
            }
        }
    }
}
