/*
 * JsonPair.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_JSON__JSON_PAIR_H_
#define _OCT_JSON__JSON_PAIR_H_

/*---------------------------------------------------------------------------*/

#include "JsonValue.h"

#include <util/SharedDataPointer.h>

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        /**
         * JSON object member.
         *
         * Object member is a pair of key and value.
         */
        class JsonPair
        {
        public:
            /**
             * \internal
             *
             * Null structure - helper for creating null objects.
             */
            struct Null
            {
                // intentionally empty
            };

            /**
             * Constant for creating null objects.
             */
            static const Null null;

        private:
            /**
             * Pair key.
             *
             * Set to QString::null for null pairs.
             */
            QString m_key;

            /**
             * Pair value.
             *
             * Set to JsonValue::null for null pairs.
             */
            JsonValue m_value;

        public:
            /**
             * Constructs null pair.
             */
            JsonPair();

            /**
             * Constructs null pair.
             */
            JsonPair(const Null&);

            /**
             * Constructs valid member.
             *
             * @param key
             *      Key of the member.
             * @param value
             *      Value of the member.
             */
            JsonPair(const QString& key, const JsonValue& value);

            /**
             * Checks if pair is null (uninitialized).
             *
             * @return
             * True if pair is null, false otherwise.
             */
            bool isNull() const;

            /**
             * Returns member key.
             *
             * @return
             * Key of the pair. QString::null for null pairs.
             */
            QString key() const
            {
                return m_key;
            }

            /**
             * Returns member value.
             *
             * @return
             * Value of the pair. JsonValue::null for null pairs.
             */
            JsonValue value() const
            {
                return m_value;
            }
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_JSON__JSON_PAIR_H_ */
