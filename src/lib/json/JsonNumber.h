/*
 * JsonNumber.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_JSON__JSON_NUMBER_H_
#define _OCT_JSON__JSON_NUMBER_H_

/*---------------------------------------------------------------------------*/

#include <QtCore/QString>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        /**
         * JSON number value.
         */
        class JsonNumber
        {
        public:
            /**
             * \internal
             *
             * Null structure - helper for creating null objects.
             */
            struct Null
            {
                // intentionally empty
            };

            /**
             * Constant for creating null objects.
             */
            static const Null null;

        private:
            QString m_value;

        public:
            /**
             * Constructs null JSON number.
             */
            JsonNumber();

            /**
             * Constructs null JSON number.
             */
            JsonNumber(const Null&);

            /**
             * Constructs new JSON number from string.
             *
             * \note The value is NOT validated.
             *
             * @param value
             *      Value to assign.
             */
            JsonNumber(QString value);

            /**
             * Constructs new int number.
             *
             * @param value
             *      Value to set.
             */
            JsonNumber(int value);

            /**
             * Constructs new unsigned int number.
             *
             * @param value
             *      Value to set.
             */
            JsonNumber(uint value);

            /**
             * Constructs new long long number.
             *
             * @param value
             *      Value to set.
             */
            JsonNumber(qlonglong value);

            /**
             * Constructs new unsigned long long number.
             *
             * @param value
             *      Value to set.
             */
            JsonNumber(qulonglong value);

            /**
             * Constructs new float number.
             *
             * @param value
             *      Value to set.
             */
            JsonNumber(float value);

            /**
             * Constructs new double number.
             *
             * @param value
             *      Value to set.
             */
            JsonNumber(double value);

            /**
             * Checks if number is null (invalid).
             *
             * @return
             * True if number is null, false otherwise.
             */
            bool isNull() const
            {
                return m_value.isNull();
            }

            /**
             * Returns string representation of the number.
             *
             * @return
             * String representation.
             */
            QString toString() const
            {
                return m_value;
            }

            /**
             * Converts number value to integer.
             *
             * @param ok
             *      Pointer to store operation result.
             *      Will be set to true on success and false on failure.
             *      If NULL the result will not be stored.
             *
             * @return
             * Converted value.
             */
            int toInt(bool* ok) const
            {
                return m_value.toInt(ok, 10);
            }

            /**
             * Converts number value to unsigned integer.
             *
             * @param ok
             *      Pointer to store operation result.
             *      Will be set to true on success and false on failure.
             *      If NULL the result will not be stored.
             *
             * @return
             * Converted value.
             */
            uint toUInt(bool* ok) const
            {
                return m_value.toUInt(ok);
            }

            /**
             * Converts number value to long long integer.
             *
             * @param ok
             *      Pointer to store operation result.
             *      Will be set to true on success and false on failure.
             *      If NULL the result will not be stored.
             *
             * @return
             * Converted value.
             */
            qlonglong toLongLong(bool* ok)
            {
                return m_value.toLongLong(ok, 10);
            }

            /**
             * Converts number value to unsigned long long integer.
             *
             * @param ok
             *      Pointer to store operation result.
             *      Will be set to true on success and false on failure.
             *      If NULL the result will not be stored.
             *
             * @return
             * Converted value.
             */
            qulonglong toULongLong(bool* ok)
            {
                return m_value.toULongLong(ok, 10);
            }

            /**
             * Converts number value to float.
             *
             * @param ok
             *      Pointer to store operation result.
             *      Will be set to true on success and false on failure.
             *      If NULL the result will not be stored.
             *
             * @return
             * Converted value.
             */
            int toFloat(bool* ok) const
            {
                return m_value.toFloat(ok);
            }

            /**
             * Converts number value to double.
             *
             * @param ok
             *      Pointer to store operation result.
             *      Will be set to true on success and false on failure.
             *      If NULL the result will not be stored.
             *
             * @return
             * Converted value.
             */
            int toDouble(bool* ok) const
            {
                return m_value.toDouble(ok);
            }
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_JSON__JSON_NUMBER_H_ */
