/*
 * JsonDecoder.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_JSON__JSON_DECODER_H_
#define _OCT_JSON__JSON_DECODER_H_

/*---------------------------------------------------------------------------*/

#include "JsonObject.h"
#include "JsonNumber.h"
#include "JsonArray.h"

#include <log/Logger.h>

#include <QtCore/QByteArray>
#include <QtCore/QBuffer>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        /**
         * JSON string representation decoder.
         */
        class JsonDecoder
        {
        private:
            /** Logger object. */
            static Oct::Log::Logger<JsonDecoder> Logger;

        private:
            /**
             * Input IO device.
             */
            QIODevice* m_ioDevice;

        private:
            /**
             * Constructs decoder.
             *
             * @param ioDevice
             *      Input IO device to use.
             */
            JsonDecoder(QIODevice* ioDevice);

            /**
             * Checks if more characters are available.
             *
             * @return
             * True if there are more characters, false otherwise.
             */
            bool hasMoreChars();

            /**
             * Skips whitespace characters.
             *
             * @return
             * True on success, false otherwise.
             */
            bool skipWhitespaces();

            /**
             * Skip one character.
             *
             * @return
             * True on success, false otherwise.
             */
            bool skipChar();

            /**
             * Reads one character.
             *
             * @param c
             *      Place to store the character.
             *
             * @return
             * True on success, false otherwise.
             */
            bool getChar(char& c);

            /**
             * Peeks one character (read without moving read position).
             *
             * @param c
             *      Place to store the character.
             *
             * @return
             * True on success, false otherwise.
             */
            bool peekChar(char& c);

            /**
             * Converts digit character to its value
             *
             * @param c
             *      Character to convert.
             * @param base
             *      Number base in range 2..16.
             *
             * @return
             * True on success, false otherwise.
             */
            int digitValue(char c, int base);

            /**
             * Decodes JSON object.
             *
             * @return
             * Valid object on success. null on error.
             */
            JsonObject decodeObject();

            /**
             * Decodes JSON object members.
             *
             * @return
             * Valid object on success. null on error.
             */
            JsonObject decodeMembers();

            /**
             * Decodes JSON member pair.
             *
             * @return
             * Valid pair on success. null on error.
             */
            JsonPair decodePair();

            /**
             * Decodes JSON array value.
             *
             * @return
             * Valid array on success. null on error.
             */
            JsonArray decodeArray();

            /**
             * Decodes JSON array value elements.
             *
             * @return
             * Valid array on success. null on error.
             */
            JsonArray decodeElements();

            /**
             * Decodes JSON value.
             *
             * @return
             * Valid value on success. null on error.
             */
            JsonValue decodeValue();

            /**
             * Decodes JSON number.
             *
             * @return
             * Valid number on success. null on error.
             */
            JsonNumber decodeNumber();

            /**
             * Decodes string.
             *
             * @return
             * Valid string on success. null on error.
             */
            QString decodeString();

            /**
             * Decodes JSON constant.
             *
             * @param pattern
             *      Expected constant pattern.
             * @param retValue
             *      Expected return value.
             *
             * @return
             * Valid value on success. null on error.
             */
            JsonValue decodeConst(const char* pattern, JsonConst retValue);

        public:
            /**
             * Decodes JSON object from given byte array.
             *
             * @param value
             *      Byte array with JSON object string representation.
             *
             * @return
             * Decoded objects on success or null object on error.
             */
            static JsonObject decode(QByteArray value);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_JSON__JSON_DECODER_H_ */
