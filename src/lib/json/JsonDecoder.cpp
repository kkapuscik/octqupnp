/*
 * JsonDecoder.cpp
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#include "JsonDecoder.h"

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        Oct::Log::Logger<JsonDecoder> JsonDecoder::Logger("/oct/json/JsonDecoder");

        JsonObject JsonDecoder::decode(QByteArray value)
        {
            QBuffer buffer(&value);

            if (buffer.open(QBuffer::ReadOnly)) {
                JsonDecoder decoder(&buffer);

                return decoder.decodeObject();
            } else {
                Logger.strace() << "Cannot open input buffer";

                return JsonObject::null;
            }
        }

        JsonDecoder::JsonDecoder(QIODevice* ioDevice) :
                m_ioDevice(ioDevice)
        {
            Q_ASSERT(ioDevice->isOpen() && ioDevice->isReadable());
        }

        bool JsonDecoder::hasMoreChars()
        {
            return m_ioDevice->bytesAvailable() > 0;
        }

        bool JsonDecoder::skipWhitespaces()
        {
            while (hasMoreChars()) {
                char c;

                if (! peekChar(c)) {
                    return false;
                }

                if (c < 0 || c > 0x20) {
                    break;
                }

                if (! skipChar()) {
                    return false;
                }
            }

            return true;
        }

        bool JsonDecoder::skipChar()
        {
            if (hasMoreChars()) {
                return m_ioDevice->getChar(NULL);
            }
            return false;
        }

        bool JsonDecoder::getChar(char& c)
        {
            if (hasMoreChars()) {
                return m_ioDevice->getChar(&c);
            }
            return false;
        }

        bool JsonDecoder::peekChar(char& c)
        {
            if (hasMoreChars()) {
                return m_ioDevice->peek(&c, 1) == 1;
            }
            return false;
        }

        /*
         * object
         *      {}
         *      { members }
         */
        JsonObject JsonDecoder::decodeObject()
        {
            do {
                /* check object start */
                char startChar;
                if (!getChar(startChar)) {
                    Logger.trace(this) << "decodeObject() - failed: cannot get start char.";
                    break;
                }
                if (startChar != '{') {
                    Logger.trace(this) << "decodeObject() - failed: invalid start char: '" << startChar << "'.";
                    break;
                }

                /* parse members */
                JsonObject rvObject = decodeMembers();
                if (rvObject.isNull()) {
                    Logger.trace(this) << "decodeObject() - failed: cannot decode members.";
                    break;
                }

                /* check end */
                char endChar;
                if (!getChar(endChar)) {
                    Logger.trace(this) << "decodeObject() - failed: cannot get end char.";
                    break;
                }
                if (endChar != '}') {
                    Logger.trace(this) << "decodeObject() - failed: invalid end char: '" << endChar << "'.";
                    break;
                }

                /* skip WS after object */
                if (!skipWhitespaces()) {
                    Logger.trace(this) << "decodeObject() - failed: cannot skip whitespaces at end.";
                    break;
                }

                /* check if we are at end */
                if (hasMoreChars()) {
                    Logger.trace(this) << "decodeObject() - failed: chars after object end.";
                    break;
                }

                /* success */
                return rvObject;
            } while (0);

            /* failure */
            return JsonObject::null;
        }

        /*
         * members
         *      pair
         *      pair , members
         */
        JsonObject JsonDecoder::decodeMembers()
        {
            QList<JsonPair> memberList;
            bool success = false;

            for (bool first = true; ; first = false) {
                /* remove any whitespaces */
                if (!skipWhitespaces()) {
                    break;
                }

                /* check separator/end char */
                char separatorChar;
                if (!peekChar(separatorChar)) {
                    break;
                }

                /* is it end */
                if (separatorChar == '}') {
                    success = true;
                    break;
                } else if (!first) {
                    if (separatorChar == ',') {
                        /* ok, just skip the pair separator */
                        if (!skipChar()) {
                            break;
                        }
                    } else {
                        /* error */
                        break;
                    }
                }

                /* decode single member */
                JsonPair member = decodePair();
                if (member.isNull()) {
                    break;
                }

                /* store member */
                memberList.append(member);
            }

            if (success) {
                return JsonObject(memberList);
            } else {
                return JsonObject::null;
            }
        }

        /*
         * pair
         *      string : value
         */
        JsonPair JsonDecoder::decodePair()
        {
            do {
                /* remove any whitespaces */
                if (!skipWhitespaces()) {
                    break;
                }

                /* decode string (key) */
                QString key = decodeString();
                if (key.isNull()) {
                    break;
                }

                /* remove any whitespaces */
                if (!skipWhitespaces()) {
                    break;
                }

                /* check separator */
                char separatorChar;
                if (!getChar(separatorChar)) {
                    break;
                }
                if (separatorChar != ':') {
                    break;
                }

                /* decode value */
                JsonValue value = decodeValue();
                if (value.isNull()) {
                    break;
                }

                /* return decoded pair */
                return JsonPair(key, value);
            } while (0);

            return JsonPair::null;
        }

        /*
         * array
         *      [ ]
         *      [ elements ]
         */
        JsonArray JsonDecoder::decodeArray()
        {
            do {
                /* check object start */
                char startChar;
                if (!getChar(startChar)) {
                    break;
                }
                if (startChar != '[') {
                    break;
                }

                /* parse members */
                JsonArray rvArray = decodeElements();
                if (rvArray.isNull()) {
                    break;
                }

                /* check end */
                char endChar;
                if (!getChar(endChar)) {
                    break;
                }
                if (endChar != ']') {
                    break;
                }

                /* success */
                return rvArray;
            } while (0);

            /* failure */
            return JsonArray::null;
        }

        /*
         * elements
         *      value
         *      value , elements
         */
        JsonArray JsonDecoder::decodeElements()
        {
            QList<JsonValue> elementList;
            bool success = false;

            for (bool first = true; ; first = false) {
                /* remove any whitespaces */
                if (!skipWhitespaces()) {
                    break;
                }

                /* check separator/end char */
                char separatorChar;
                if (!peekChar(separatorChar)) {
                    break;
                }

                /* is it end */
                if (separatorChar == ']') {
                    success = true;
                    break;
                } else if (!first && separatorChar == ',') {
                    /* ok, just skip the separator */
                    if (!skipChar()) {
                        break;
                    }
                } else {
                    /* error */
                    break;
                }

                /* decode single member */
                JsonValue element = decodeValue();
                if (element.isNull()) {
                    break;
                }

                /* store member */
                elementList.append(element);
            }

            if (success) {
                return JsonArray(elementList);
            } else {
                return JsonArray::null;
            }
        }

        /*
         * value
         *      string      -> "
         *      number      -> - 0-9
         *      object      -> {
         *      array       -> [
         *      true        -> t
         *      false       -> f
         *      null        -> n
         */
        JsonValue JsonDecoder::decodeValue()
        {
            do {
                /* remove any whitespaces */
                if (!skipWhitespaces()) {
                    break;
                }

                /* get the type char */
                char typeChar;
                if (!peekChar(typeChar)) {
                    break;
                }

                bool success = true;
                switch (typeChar) {
                    case '"':
                    {
                        QString value = decodeString();
                        if (!value.isNull()) {
                            return JsonValue(value);
                        }
                        break;
                    }

                    case '-':
                    case '0': case '1': case '2': case '3': case '4':
                    case '5': case '6': case '7': case '8': case '9':
                    {
                        JsonNumber value = decodeNumber();
                        if (!value.isNull()) {
                            return JsonValue(value);
                        }
                        break;
                    }

                    case '{':
                    {
                        JsonObject value = decodeObject();
                        if (!value.isNull()) {
                            return JsonValue(value);
                        }
                        break;
                    }

                    case '[':
                    {
                        JsonArray value = decodeArray();
                        if (!value.isNull()) {
                            return JsonValue(value);
                        }
                        break;
                    }

                    case 't':
                        return decodeConst("true", JsonTrue);
                    case 'f':
                        return decodeConst("false", JsonFalse);
                    case 'n':
                        return decodeConst("null", JsonNull);

                    default:
                        break;
                }
            } while (0);

            return JsonValue::null;
        }

        /*
         * value
         *      true
         *      false
         *      null
         */
        JsonValue JsonDecoder::decodeConst(const char* pattern, JsonConst retValue)
        {
            for (; *pattern; ++pattern) {
                char c;

                if (!getChar(c)) {
                    return JsonValue::null;
                }
                if (c != *pattern) {
                    return JsonValue::null;
                }
            }

            return retValue;
        }

        /*
         * number
         *      int
         *      int frac
         *      int exp
         *      int frac exp
         * int
         *      digit
         *      digit1-9 digits
         *      - digit
         *      - digit1-9 digits
         * frac
         *      . digits
         * exp
         *      e digits
         * digits
         *      digit
         *      digit digits
         * e
         *      e
         *      e+
         *      e-
         *      E
         *      E+
         *      E-
         */
        JsonNumber JsonDecoder::decodeNumber()
        {
            QString str;

            do {
                /* == parse 'int' part */

                /* parse sign */
                char signChar;
                if (!peekChar(signChar)) {
                    break;
                }

                if (signChar == '-') {
                    str.append(signChar);

                    if (!skipChar()) {
                        break;
                    }
                }

                /* parse diits */
                bool intValid = true;
                char firstIntDigit = 0;
                while (intValid) {
                    char digit;

                    if (!peekChar(digit)) {
                        intValid = false;
                        break;
                    }

                    if (digitValue(digit, 10) < 0) {
                        if (firstIntDigit == 0) {
                            intValid = false;
                        }
                        break;
                    }

                    if (!skipChar()) {
                        intValid = false;
                        break;
                    }

                    if (firstIntDigit == 0) {
                        firstIntDigit = digit;
                    } else {
                        if (firstIntDigit == '0') {
                            intValid = false;
                            break;
                        }
                    }

                    str.append(digit);
                }
                if (!intValid) {
                    break;
                }

                /* == parse 'frac' part */
                char fracSep;
                if (!peekChar(fracSep)) {
                    break;
                }
                if (fracSep == '.') {
                    str.append(fracSep);

                    if (!skipChar()) {
                        break;
                    }

                    bool fracValid = true;
                    char firstFracDigit = 0;
                    while (fracValid) {
                        char digit;

                        if (!peekChar(digit)) {
                            fracValid = false;
                            break;
                        }

                        if (digitValue(digit, 10) < 0) {
                            if (firstFracDigit == 0) {
                                fracValid = false;
                            }
                            break;
                        }

                        if (!skipChar()) {
                            fracValid = false;
                            break;
                        }

                        if (firstFracDigit == 0) {
                            firstFracDigit = digit;
                        }

                        str.append(digit);
                    }
                    if (!fracValid) {
                        break;
                    }
                }

                /* == parse 'exp' part */
                char expSep;
                if (!peekChar(expSep)) {
                    break;
                }
                if (expSep == 'e' || expSep == 'E') {
                    str.append(expSep);

                    if (!skipChar()) {
                        break;
                    }

                    char expSign;
                    if (!peekChar(expSign)) {
                        break;
                    }

                    if (expSign == '-' || expSign == '+') {
                        str.append(expSign);

                        if (!skipChar()) {
                            break;
                        }
                    }

                    bool expValid = true;
                    char firstExpDigit = 0;
                    while (expValid) {
                        char digit;

                        if (!peekChar(digit)) {
                            expValid = false;
                            break;
                        }

                        if (digitValue(digit, 10) < 0) {
                            if (firstExpDigit == 0) {
                                expValid = false;
                            }
                            break;
                        }

                        if (!skipChar()) {
                            expValid = false;
                            break;
                        }

                        if (firstExpDigit == 0) {
                            firstExpDigit = digit;
                        }

                        str.append(digit);
                    }
                    if (!expValid) {
                        break;
                    }
                }

                return JsonNumber(str);
            } while (0);

            return JsonNumber::null;
        }

        int JsonDecoder::digitValue(char c, int base)
        {
            int val;

            Q_ASSERT(base >= 2 && base <= 16);

            switch (c) {
                case '0':
                    val = 0x0;
                    break;
                case '1':
                    val = 0x1;
                    break;
                case '2':
                    val = 0x2;
                    break;
                case '3':
                    val = 0x3;
                    break;
                case '4':
                    val = 0x4;
                    break;
                case '5':
                    val = 0x5;
                    break;
                case '6':
                    val = 0x6;
                    break;
                case '7':
                    val = 0x7;
                    break;
                case '8':
                    val = 0x8;
                    break;
                case '9':
                    val = 0x9;
                    break;
                case 'a': case 'A':
                    val = 0xA;
                    break;
                case 'b': case 'B':
                    val = 0xB;
                    break;
                case 'c': case 'C':
                    val = 0xC;
                    break;
                case 'd': case 'D':
                    val = 0xD;
                    break;
                case 'e': case 'E':
                    val = 0xE;
                    break;
                case 'f': case 'F':
                    val = 0xF;
                    break;
                default:
                    val = -1;
                    break;
            }

            if (val < 0 || val >= base) {
                return -1;
            }

            return val;
        }

        /*
         * string
         *      ""
         *      " chars "
         * chars
         *      char
         *      char chars
         * char
         *      any-Unicode-character-except-"-or-\-or-control-character
         *      \"
         *      \\
         *      \/
         *      \b
         *      \f
         *      \n
         *      \r
         *      \t
         *      \u four-hex-digits
         */
        QString JsonDecoder::decodeString()
        {
            QString str("");
            bool success = false;

            do {

                /* remove any whitespaces */
                if (!skipWhitespaces()) {
                    break;
                }

                /* check object start */
                char startChar;
                if (!getChar(startChar)) {
                    break;
                }
                if (startChar != '"') {
                    break;
                }

                for (;;) {
                    char c;

                    if (!getChar(c)) {
                        break;
                    } else if (c == '"') {
                        success = true;
                        break;
                    } else if (c >= 0x00 && c <= 0x1f) {
                        break;
                    } else if (c == '\\') {
                        char esc;

                        if (!getChar(esc)) {
                            break;
                        }

                        if (c == '"') {
                            str.append('"');
                        } else if (c == '\\') {
                            str.append('\\');
                        } else if (c == '/') {
                            str.append('/');
                        } else if (c == 'b') {
                            str.append('\b');
                        } else if (c == 'f') {
                            str.append('\f');
                        } else if (c == 'n') {
                            str.append('\n');
                        } else if (c == 'r') {
                            str.append('\r');
                        } else if (c == 't') {
                            str.append('\t');
                        } else if (c == 'u') {
                            bool error = false;
                            char digitChars[4];
                            int digitValues[4];
                            int utfValue;

                            for (int i = 0; !error && i < 4; ++i) {
                                if (!getChar(digitChars[i])) {
                                    error = true;
                                    break;
                                }
                            }
                            if (error) {
                                break;
                            }

                            for (int i = 0; !error && i < 4; ++i) {
                                digitValues[i] = digitValue(digitChars[i], 16);
                                if (digitValues[i] < 0) {
                                    error = true;
                                    break;
                                }
                            }
                            if (error) {
                                break;
                            }

                            utfValue = 0;
                            for (int i = 0; !error && i < 4; ++i) {
                                utfValue <<= 8;
                                utfValue |= digitValues[i];
                            }

                            str.append(QChar(utfValue));
                        } else {
                            break;
                        }
                    } else {
                        str.append(c);
                    }
                }
            } while (0);

            if (success) {
                return str;
            } else {
                return QString::null;
            }
        }

    }
}
