/*
 * JsonEncoder.h
 *
 *  Created on: 13-04-2011
 *      Author: saveman
 */

#ifndef _OCT_JSON__JSON_ENCODER_H_
#define _OCT_JSON__JSON_ENCODER_H_

/*---------------------------------------------------------------------------*/

#include "JsonObject.h"

#include <log/Logger.h>

#include <QtCore/QByteArray>
#include <QtCore/QBuffer>

/*---------------------------------------------------------------------------*/

namespace Oct
{
    namespace Json
    {
        /**
         * JSON string representation encoder.
         */
        class JsonEncoder
        {
        private:
            /** Logger object. */
            static Oct::Log::Logger<JsonEncoder> Logger;

        private:
            /**
             * Output IO device.
             */
            QIODevice* m_ioDevice;

            /**
             * Break output using newlines.
             */
            bool m_breakLines;

        private:
            /**
             * Constructs encoder.
             *
             * @param ioDevice
             *      Output IO device to use.
             * @param breakLines
             *      If true the lines are broken using newline character
             *      to produce more human-friendly output.
             */
            JsonEncoder(QIODevice* ioDevice, bool breakLines);

            /**
             * Writes given characters.
             *
             * @param chars
             *      Characters to write.
             * @param length
             *      Length of characters array.
             *
             * @return
             * True on success, false otherwise.
             */
            bool write(const char* chars, int length);

            /**
             * Writes given characters.
             *
             * @param array
             *      Characters array.
             *
             * @return
             * True on success, false otherwise.
             */
            bool write(const QByteArray& array);

            /**
             * Writes single character.
             *
             * @param c
             *      Character to write
             *
             * @return
             * True on success, false otherwise.
             */
            bool putChar(char c);

            /**
             * Writes break character (newline) if breaks are enabled.
             *
             * @return
             * True on success, false otherwise.
             */
            bool putBreak();

            /**
             * Writes break character (space) if breaks are enabled.
             *
             * @return
             * True on success, false otherwise.
             */
            bool putSpace();

            /**
             * Encodes object to string representation.
             *
             * @param object
             *      Object to encode.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodeObject(const JsonObject& object);

            /**
             * Encodes object members to string representation.
             *
             * @param object
             *      Object to encode.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodeMembers(const JsonObject& object);

            /**
             * Encodes pair to string representation.
             *
             * @param pair
             *      Pair to encode.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodePair(const JsonPair& pair);

            /**
             * Encodes string to JSON string representation.
             *
             * @param str
             *      String to encode.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodeString(const QString& str);

            /**
             * Encodes string to string representation.
             *
             * @param value
             *      Value to encode.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodeValue(const JsonValue& value);

            /**
             * Encodes JSON constant to string representation.
             *
             * @param cstr
             *      Constant characters (string representation).
             * @param clen
             *      Length of characters array.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodeConst(const char* cstr, int clen);

            /**
             * Encodes array to string representation.
             *
             * @param array
             *      Array to encode.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodeArray(const JsonArray& array);

            /**
             * Encodes array elements to string representation.
             *
             * @param array
             *      Array to encode.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodeElements(const JsonArray& array);

            /**
             * Encodes number to string representation.
             *
             * @param number
             *      Number to encode.
             *
             * @return
             * True on success, false otherwise.
             */
            bool encodeNumber(const JsonNumber& number);

        public:
            /**
             * Encodes object to string representation.
             *
             * @param object
             *      Object to encode.
             * @param breakLines
             *      If true the lines are broken using newline character
             *      to produce more human-friendly output.
             *
             * @return
             * String representation on success, null string on error.
             */
            static QByteArray encode(JsonObject object, bool breakLines);
        };
    }
}

/*---------------------------------------------------------------------------*/

#endif /* _OCT_JSON__JSON_ENCODER_H_ */
