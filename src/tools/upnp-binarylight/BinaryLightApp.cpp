/*
 * AVPlayerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "BinaryLightApp.h"

#include <QtCore/QResource>

BinaryLightApp::BinaryLightApp(int& argc, char** argv) :
    Oct::App::GuiApplication(argc, argv)
{
    // nothing to do
}

int BinaryLightApp::initWindows()
{
    m_mainWindow = new BinaryLightMainWindow();
    m_mainWindow->show();

    return 0;
}

QMainWindow* BinaryLightApp::mainWindow()
{
    return m_mainWindow;
}
