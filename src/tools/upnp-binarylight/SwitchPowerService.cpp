/*
 * SwitchPowerService.cpp
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#include "SwitchPowerService.h"

#include <upnpdev/Types.h>

class ActionConstructor
{
private:
    QString m_name;
    QList<Oct::UpnpDev::ActionArgument> m_args;

public:
    ActionConstructor(const QString& name = QString::null) :
        m_name(name)
    {
        // do nothing
    }

    void addArgument(const QString& name, bool inputArg, const QString& relStateVar)
    {
        m_args .append(Oct::UpnpDev::ActionArgument(name,
                inputArg ? Oct::UpnpDev::ActionArgument::ACTION_ARGUMENT_TYPE_INPUT
                        : Oct::UpnpDev::ActionArgument::ACTION_ARGUMENT_TYPE_OUTPUT, relStateVar));
    }

    operator Oct::UpnpDev::Action() const
    {
        return Oct::UpnpDev::Action(m_name, m_args);
    }
};

SwitchPowerService::SwitchPowerService(QObject* parent) :
    Oct::UpnpDev::Service(parent), m_status(false), m_target(false)
{
    setServiceType("urn:schemas-upnp-org:service:SwitchPower:1");
    setServiceId("urn:upnp-org:serviceId:SwitchPower:1");

    ActionConstructor ac;

    ac = ActionConstructor("SetTarget");
    ac.addArgument("newTargetValue", true, "Target");
    addAction(ac);

    ac = ActionConstructor("GetTarget");
    ac.addArgument("RetTargetValue", false, "Target");
    addAction(ac);

    ac = ActionConstructor("GetStatus");
    ac.addArgument("ResultStatus", false, "Status");
    addAction(ac);

    // const QString& name, bool sendEvents, const QString& dataType
    addStateVariable(Oct::UpnpDev::StateVariable("Target", false, "boolean", "0"));
    addStateVariable(Oct::UpnpDev::StateVariable("Status", true, "boolean", "0"));
}

SwitchPowerService::~SwitchPowerService()
{
    // nothing to do
}

bool SwitchPowerService::target()
{
    return m_target;
}

bool SwitchPowerService::status()
{
    return m_status;
}

void SwitchPowerService::setTarget(bool powerOn)
{
    m_target = powerOn;
    emit targetChanged(m_target);

    m_status = powerOn;
    emit statusChanged(m_status);
}

void SwitchPowerService::togglePower()
{
    setTarget(! target());
}

bool SwitchPowerService::processActionRequest(Oct::UpnpDev::ActionRequest& actionRequest)
{
    if (actionRequest.actionName() == "SetTarget") {
        QVariant value = actionRequest.argumentValueBoolean("newTargetValue");
        if (!value.isNull()) {
            setTarget(value.toBool());
        } else {
            actionRequest.setStandardError(Oct::UpnpDev::ActionRequest::ERROR_CODE__INVALID_ARGS);
        }
    } else if (actionRequest.actionName() == "GetTarget") {
        actionRequest.setOutputArgumentValue("RetTargetValue", m_target ? "1" : "0");
    } else if (actionRequest.actionName() == "GetStatus") {
        actionRequest.setOutputArgumentValue("ResultStatus", m_status ? "1" : "0");
    } else {
        actionRequest.setStandardError(Oct::UpnpDev::ActionRequest::ERROR_CODE__INVALID_ACTION);
    }

    return true;
}
