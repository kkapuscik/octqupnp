/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "BinaryLightMainWindow.h"

#include <app/GuiApplication.h>
#include <QtCore/QSettings>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QDesktopWidget>
#include <QtGui/QCloseEvent>

/*---------------------------------------------------------------------------*/

BinaryLightMainWindow::BinaryLightMainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    m_ui.setupUi(this);

    m_devManager = new Oct::UpnpDev::Manager(NULL, this);

    m_binaryLightDevice = new BinaryLightDevice(this);
    m_switchPowerService = new SwitchPowerService(this);

    m_binaryLightDevice->addService(m_switchPowerService);

    readSettings();

    connect(m_switchPowerService, SIGNAL( statusChanged(bool) ), this, SLOT( updateStatus(bool) ));
    connect(m_switchPowerService, SIGNAL( targetChanged(bool) ), this, SLOT( updateTarget(bool) ));

    updateStatus(m_switchPowerService->status());
    updateTarget(m_switchPowerService->target());

    updateUIData();
    updateUIActions();

    startDevice();
}

void BinaryLightMainWindow::readSettings()
{
    QSettings settings;

    settings.beginGroup("MainWindow");
    if (settings.contains("size") && settings.contains("pos")) {
        resize(settings.value("size", size()).toSize());
        move(settings.value("pos", pos()).toPoint());
    } else {
        setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(),
                QApplication::desktop()->availableGeometry()));
    }
    settings.endGroup();

    settings.beginGroup("Device");
    if (settings.contains("udn")) {
        QString udnStr = settings.value("udn", QString()).toString();
        if (! m_binaryLightDevice->setUDN(udnStr)) {
            m_binaryLightDevice->generateUDN();
        }
    }
    if (settings.contains("fname")) {
        QString fnameStr = settings.value("fname", QString()).toString();
        if (! m_binaryLightDevice->setFriendlyName(fnameStr)) {
            m_binaryLightDevice->resetFriendlyName();
        }
    }
    settings.endGroup();
}

void BinaryLightMainWindow::writeSettings()
{
    QSettings settings;

    settings.beginGroup("MainWindow");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();

    settings.beginGroup("Device");
    settings.setValue("udn", m_binaryLightDevice->udn().toString());
    settings.setValue("fname", m_binaryLightDevice->friendlyName());
    settings.endGroup();
}

void BinaryLightMainWindow::closeEvent(QCloseEvent* event)
{
    if (m_devManager->isRegistered(m_binaryLightDevice)) {
        stopDevice();
    }

    writeSettings();

    event->accept();
}

void BinaryLightMainWindow::appAbout()
{
    Oct::App::GuiApplication::showAboutDialog(this);
}

void BinaryLightMainWindow::updateUIData()
{
    bool deviceRegistered = m_devManager->isRegistered(m_binaryLightDevice);

    m_ui.m_deviceStatusLineEdit->setText(deviceRegistered ? tr("On") : tr("Off"));

    m_ui.m_friendlyNameLineEdit->setText(m_binaryLightDevice->friendlyName());
    m_ui.m_udnLineEdit->setText(m_binaryLightDevice->udn().toString());
}

void BinaryLightMainWindow::updateUIActions()
{
    bool deviceRegistered = m_devManager->isRegistered(m_binaryLightDevice);

    m_ui.m_actionStartDevice->setEnabled(!deviceRegistered);
    m_ui.m_actionStopDevice->setEnabled(deviceRegistered);

    m_ui.m_startDeviceButton->setEnabled(!deviceRegistered);
    m_ui.m_stopDeviceButton->setEnabled(deviceRegistered);

    m_ui.m_udnLineEdit->setEnabled(!deviceRegistered);
    m_ui.m_generateUdnButton->setEnabled(!deviceRegistered);
    m_ui.m_friendlyNameLineEdit->setEnabled(!deviceRegistered);
    m_ui.m_resetFriendlyNameButton->setEnabled(!deviceRegistered);
}

void BinaryLightMainWindow::startDevice()
{
    QString udn = m_ui.m_udnLineEdit->text();
    QString friendlyName = m_ui.m_friendlyNameLineEdit->text();

    if (!m_binaryLightDevice->setUDN(udn)) {
        QMessageBox::warning(this, tr("Start Device"), tr("Cannot start device.\n\nUDN is invalid."));
        return;
    }

    if (!m_binaryLightDevice->setFriendlyName(friendlyName)) {
        QMessageBox::warning(this, tr("Start Device"), tr("Cannot start device.\n\nFriendly name is invalid."));
        return;
    }

    bool success = m_devManager->registerDevice(m_binaryLightDevice);

    if (!success) {
        QMessageBox::warning(this, tr("Start Device"), tr("Cannot start device!"));
    }

    updateUIData();
    updateUIActions();
}

void BinaryLightMainWindow::stopDevice()
{
    bool success = m_devManager->unregisterDevice(m_binaryLightDevice);

    if (!success) {
        QMessageBox::warning(this, tr("Stop Device"), tr("Cannot stop device!"));
    }

    updateUIData();
    updateUIActions();
}

void BinaryLightMainWindow::togglePower()
{
    m_switchPowerService->togglePower();
}

void BinaryLightMainWindow::resetFriendlyName()
{
    m_binaryLightDevice->resetFriendlyName();

    updateUIData();
}

void BinaryLightMainWindow::generateUDN()
{
    m_binaryLightDevice->generateUDN();

    updateUIData();
}

void BinaryLightMainWindow::updateStatus(bool on)
{
    m_ui.m_statusLineEdit->setText(on ? "1" : "0");
}

void BinaryLightMainWindow::updateTarget(bool on)
{
    m_ui.m_targetLineEdit->setText(on ? "1" : "0");
}
