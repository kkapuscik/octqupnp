/*
 * SwitchPowerService.h
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#ifndef SWITCHPOWERSERVICE_H_
#define SWITCHPOWERSERVICE_H_

#include <upnpdev/Service.h>

class SwitchPowerService : public Oct::UpnpDev::Service
{
    Q_OBJECT

private:
    bool m_status;
    bool m_target;

public:
    SwitchPowerService(QObject* parent = 0);
    virtual ~SwitchPowerService();

    bool processActionRequest(Oct::UpnpDev::ActionRequest& actionRequest);

    bool target();
    bool status();

public slots:
    void setTarget(bool powerOn);
    void togglePower();

signals:
    void statusChanged(bool on);
    void targetChanged(bool on);
};

#endif /* SWITCHPOWERSERVICE_H_ */
