/*
 * BinaryLightDevice.h
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#ifndef BINARYLIGHTDEVICE_H_
#define BINARYLIGHTDEVICE_H_

#include <upnpdev/Device.h>

class BinaryLightDevice : public Oct::UpnpDev::Device
{
    Q_OBJECT

public:
    BinaryLightDevice(QObject* parent = 0);
    virtual ~BinaryLightDevice();

    void resetFriendlyName();
    void generateUDN();

    bool setUDN(const QString& udnStr);
    bool setFriendlyName(const QString& friendlyName);

};

#endif /* BINARYLIGHTDEVICE_H_ */
