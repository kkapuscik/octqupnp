/*
 * AVPlayerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef _BINARY_LIGHT__BINARY_LIGHT_APP_H_
#define _BINARY_LIGHT__BINARY_LIGHT_APP_H_

#include "BinaryLightMainWindow.h"

#include <app/GuiApplication.h>

class BinaryLightApp : public Oct::App::GuiApplication
{
    Q_OBJECT

private:
    BinaryLightMainWindow* m_mainWindow;

public:
    /**
     * Constructs application.
     *
     * @param argc
     *      Arguments count (from main function).
     * @param argv
     *      Arguments array (from main function).
     */
    BinaryLightApp(int& argc, char** argv);

protected:
    virtual int initWindows();

    virtual QMainWindow* mainWindow();

};

#endif /* _BINARY_LIGHT__BINARY_LIGHT_APP_H_ */
