/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP-BinaryLight
 * @{
 */

#ifndef _BINARY_LIGHT__BINARY_LIGHT_MAIN_WINDOW_H_
#define _BINARY_LIGHT__BINARY_LIGHT_MAIN_WINDOW_H_

/*---------------------------------------------------------------------------*/

#include "BinaryLightDevice.h"
#include "SwitchPowerService.h"

#include <ui_BinaryLightMainWindow.h>
#include <upnpdev/Manager.h>
#include <QtWidgets/QMainWindow>

/*---------------------------------------------------------------------------*/

class BinaryLightMainWindow : public QMainWindow
{
Q_OBJECT

private:
    Ui::BinaryLightMainWindowClass m_ui;
    Oct::UpnpDev::Manager* m_devManager;
    BinaryLightDevice* m_binaryLightDevice;
    SwitchPowerService* m_switchPowerService;

public:
    BinaryLightMainWindow(QWidget *parent = 0);

protected:
    void closeEvent(QCloseEvent* event);

private:
    void readSettings();
    void writeSettings();
    void updateUIData();
    void updateUIActions();

protected slots:
    void appAbout();
    void startDevice();
    void stopDevice();
    void togglePower();
    void resetFriendlyName();
    void generateUDN();

private slots:
    void updateStatus(bool on);
    void updateTarget(bool on);

};

/*---------------------------------------------------------------------------*/

#endif // _BINARY_LIGHT__BINARY_LIGHT_MAIN_WINDOW_H_
/**
 * @}
 * @}
 * @}
 */
