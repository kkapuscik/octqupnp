/*
 * BinaryLightDevice.cpp
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#include "BinaryLightDevice.h"

BinaryLightDevice::BinaryLightDevice(QObject* parent) :
    Oct::UpnpDev::Device(parent)
{
    setProperty(PROPERTY_DEVICE_TYPE, "urn:schemas-upnp-org:device:BinaryLight:1");
    setProperty(PROPERTY_MANUFACTURER, "OCTaedr Software");
    setProperty(PROPERTY_MANUFACTURER_URL, "http://www.octaedr.info/");
    setProperty(PROPERTY_MODEL_NAME, "Binary Light");
    setProperty(PROPERTY_MODEL_NUMBER, "1.0");
    setProperty(PROPERTY_MODEL_DESCRIPTION, "Binary Light Test Device");
    resetFriendlyName();
}

BinaryLightDevice::~BinaryLightDevice()
{
    // do nothing
}

void BinaryLightDevice::resetFriendlyName()
{
    setProperty(PROPERTY_FRIENDLY_NAME, "OCTaedr Binary Light");
}

void BinaryLightDevice::generateUDN()
{
    Device::setUDN(Oct::UpnpDev::UUID::generate());
}

bool BinaryLightDevice::setUDN(const QString& udnStr)
{
    Oct::UpnpDev::UUID uuid(udnStr);

    if (uuid.isValid()) {
        Device::setUDN(uuid);
        return true;
    } else {
        return false;
    }
}

bool BinaryLightDevice::setFriendlyName(const QString& friendlyName)
{
    QString fn = friendlyName.trimmed();
    if (!fn.isEmpty()) {
        setProperty(PROPERTY_FRIENDLY_NAME, fn);
        return true;
    } else {
        return false;
    }
}
