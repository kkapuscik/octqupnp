#include "MediaServerMainWindow.h"
#include "MediaServerApp.h"

#include <plugin/ContentPluginFactoryManager.h>
#include <ContentPluginConfigDialog.h>

#include <QtCore/QDateTime>
#include <QtWidgets/QMessageBox>
#include <QtGui/QCloseEvent>

MediaServerMainWindow::MediaServerMainWindow(MediaServerApp* app, QWidget *parent) :
    QMainWindow(parent), m_msApp(app), m_addSharesContextMenu(NULL)
{
    m_ui.setupUi(this);

    ContentDatabase* contentDatabase = m_msApp->contentDatabase();

    /* init gui state */
    updateServerState(m_msApp->isDeviceStarted());
    resetServerSettings();
    resetCompatibilitySettings();
    updatePluginList();

    /* connect signals */
    connect(m_msApp, SIGNAL( deviceStateChanged(bool) ), this, SLOT( updateServerState(bool) ));
    connect(contentDatabase, SIGNAL( pluginsChanged() ), this, SLOT( updatePluginList() ));

    buildAddSharesMenu();
}

void MediaServerMainWindow::buildAddSharesMenu()
{
    QList<const ContentPluginFactory*> factories;

    factories = ContentPluginFactoryManager::instance()->factories();
    if (factories.size() == 0) {
        m_ui.m_sharesButtonAdd->setEnabled(false);
        return;
    }

    m_addSharesContextMenu = new QMenu(this);

    for (int i = 0; i < factories.length(); ++i) {
        const ContentPluginFactory* factory = factories[i];

        QAction* action =
                m_addSharesContextMenu->addAction(tr("Add %1").arg(factory->name()), this, SLOT( addPlugin() ));

        m_addSharesMapping.insert(action, factory);
    }
}

void MediaServerMainWindow::addPlugin()
{
    /* take the signal sender (add action) */
    QObject* signalSender = this->sender();

    /* get the factory from mapping */
    Q_ASSERT(m_addSharesMapping.contains(signalSender));
    const ContentPluginFactory* factory = m_addSharesMapping.find(signalSender).value();

    /* create add dialog */
    ContentPluginConfigDialog configDialog(factory);
    if (configDialog.exec() != QDialog::Accepted) {
        return;
    }

    /* get the settings */
    ContentPluginSettings* settings = configDialog.pluginSettings();
    Q_ASSERT(settings != NULL);

    /* get the database */
    ContentDatabase* contentDatabase = m_msApp->contentDatabase();

    /* create & register plugin */
    ContentPlugin* plugin = factory->createPlugin(settings, contentDatabase);
    contentDatabase->registerPlugin(plugin);

    /* free the settings */
    delete settings;
}

void MediaServerMainWindow::appendLogLine(const QString& line)
{
    QDateTime time = QDateTime::currentDateTime();

    m_ui.m_logTextEdit->insertPlainText(time.toString(Qt::ISODate) + " " + line + "\n");
}

void MediaServerMainWindow::updatePluginList()
{
    ContentDatabase* contentDatabase = m_msApp->contentDatabase();

    m_ui.m_sharesTableWidget->clearContents();
    m_ui.m_sharesTableWidget->setRowCount(0);
    m_ui.m_sharesTableWidget->setSortingEnabled(false);

    QList<QString> plugins = contentDatabase->registeredPlugins();
    for (int i = 0; i < plugins.size(); ++i) {
        /* get plugin data */
        ContentPluginSettingsPointer settings = ContentPluginSettingsPointer(
                contentDatabase->pluginSettings(plugins[i]));

        /* add new row */
        int row = m_ui.m_sharesTableWidget->rowCount();
        m_ui.m_sharesTableWidget->insertRow(row);

        /* fill the row contents */
        m_ui.m_sharesTableWidget->setItem(row, 0, new QTableWidgetItem(
                settings->data(ContentPluginSettings::ROLE_NAME).toString()));
        m_ui.m_sharesTableWidget->setItem(row, 1, new QTableWidgetItem(
                settings->data(ContentPluginSettings::ROLE_PATH).toString()));
        m_ui.m_sharesTableWidget->setItem(row, 2, new QTableWidgetItem(settings->data(
                ContentPluginSettings::ROLE_REFRESH_RATE).toString()));

        QTableWidgetItem* item = m_ui.m_sharesTableWidget->item(row, 0);
        item->setData(Qt::UserRole, plugins[i]);
    }

    m_ui.m_sharesTableWidget->setSortingEnabled(true);
}

void MediaServerMainWindow::updateServerState(bool started)
{
    if (started) {
        appendLogLine("Server started.");
    } else {
        appendLogLine("Server stopped.");
    }

    const MediaServerDevice* msDevice = m_msApp->device();

    m_ui.m_serverStatusValue->setText(started ? tr("Started") : tr("Stopped"));
    m_ui.m_serverUdnValue->setText(msDevice->udn().toString(true));
    m_ui.m_serverFriendlyNameValue->setText(msDevice->friendlyName());
}

void MediaServerMainWindow::showAboutDialog()
{
    MediaServerApp::showAboutDialog(this);
}

void MediaServerMainWindow::showAboutQtDialog()
{
    MediaServerApp::showAboutQtDialog(this);
}

void MediaServerMainWindow::startServer()
{
    m_msApp->startDevice();
}

void MediaServerMainWindow::stopServer()
{
    m_msApp->stopDevice();
}

void MediaServerMainWindow::applyCompatibilitySettings()
{
    // TODO
}

void MediaServerMainWindow::resetCompatibilitySettings()
{
    // TODO
}

void MediaServerMainWindow::applyServerSettings()
{
    MediaServerDevice* msDevice = m_msApp->device();

    QString udn = m_ui.m_settingsServerUdnEdit->text();
    QString friendlyName = m_ui.m_settingsServerNameEdit->text();

    if (!msDevice->setUDN(udn)) {
        QMessageBox::warning(this, tr("Server settings"), tr("Cannot apply settings.\n\nUDN is invalid."));
        return;
    }

    if (!msDevice->setFriendlyName(friendlyName)) {
        QMessageBox::warning(this, tr("Server settings"), tr("Cannot apply settings.\n\nFriendly name is invalid."));
        return;
    }

    if (m_msApp->isDeviceStarted()) {
        if (QMessageBox::question(this, tr("Server settings"), tr(
                "To use the new settings you must restart the server.\n\nWould you like to do it now?"),
                QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {

            m_msApp->stopDevice();
            m_msApp->startDevice();
        }
    }
}

void MediaServerMainWindow::resetServerSettings()
{
    const MediaServerDevice* msDevice = m_msApp->device();

    m_ui.m_settingsServerUdnEdit->setText(msDevice->udn().toString(true));
    m_ui.m_settingsServerNameEdit->setText(msDevice->friendlyName());
}

void MediaServerMainWindow::setServerDefaultName()
{
    const MediaServerDevice* msDevice = m_msApp->device();
    QString friendlyName = msDevice->defaultFriendlyName();

    m_ui.m_settingsServerNameEdit->setText(friendlyName);
}

void MediaServerMainWindow::generateServerUdn()
{
    Oct::UpnpDev::UUID udn = Oct::UpnpDev::UUID::generate();

    m_ui.m_settingsServerUdnEdit->setText(udn.toString(true));
}

QTableWidgetItem* MediaServerMainWindow::getSelectedSharedFolderItem()
{
    QList<QTableWidgetItem*> items = m_ui.m_sharesTableWidget->selectedItems();
    if (items.size() > 0) {
        int row = items[0]->row();
        QTableWidgetItem* selItem = m_ui.m_sharesTableWidget->item(row, 0);

        Q_ASSERT(selItem != NULL);

        return selItem;
    }

    return NULL;
}

void MediaServerMainWindow::addSharedFolder()
{
    QPoint point = m_ui.m_sharesButtonAdd->mapToGlobal(QPoint(m_ui.m_sharesButtonAdd->width() / 2,
            m_ui.m_sharesButtonAdd->height() / 2));

    m_addSharesContextMenu->popup(point);
}

void MediaServerMainWindow::removeSelectedSharedFolders()
{
    QTableWidgetItem* item = getSelectedSharedFolderItem();
    if (item == NULL) {
        return;
    }    struct SharedFolder
    {
        typedef QList<ContentObject*> SharedObjectsList;

        QString m_name;
        QString m_path;
        int m_refreshRate;

        SharedObjectsList m_objects;
    };


    QString pluginID = item->data(Qt::UserRole).toString();

    ContentDatabase* contentDatabase = m_msApp->contentDatabase();

    contentDatabase->unregisterPlugin(pluginID);

    // view should update automatically
}

void MediaServerMainWindow::editSelectedSharedFolders()
{
    QTableWidgetItem* item = getSelectedSharedFolderItem();
    if (item == NULL) {
        return;
    }

    QString id = item->data(Qt::UserRole).toString();

    /* get database */
    ContentDatabase* contentDatabase = m_msApp->contentDatabase();

    // TODO: remove
    //    ContentPluginConfigDialog dialog;
    //    dialog.exec();

#if 0
    /* get folder data */
    QString name;
    QString path;
    int refreshRate;

    if (!contentDatabase->sharedFolderData(id, &name, &path, &refreshRate)) {
        Q_ASSERT(0);
    }

    /* show edit dialog */
    SharedFolderDialog folderDialog;

    folderDialog.setData(name, path, refreshRate);

    if (folderDialog.exec() != SharedFolderDialog::Accepted) {
        return;
    }

    folderDialog.data(&name, &path, &refreshRate);

    /* update folder */
    if (!contentDatabase->modifySharedFolder(id, name, path, refreshRate)) {
        QMessageBox::warning(this, tr("Edit failed"), tr("Could not update folder"));
        return;
    }

    // view should update automatically
#endif
}

void MediaServerMainWindow::refreshSelectedSharedFolders()
{
    QTableWidgetItem* item = getSelectedSharedFolderItem();
    if (item == NULL) {
        return;
    }

    QString pluginID = item->data(Qt::UserRole).toString();

    /* update folder contents */
    ContentDatabase* contentDatabase = m_msApp->contentDatabase();

    contentDatabase->updatePluginContents(pluginID);
}

void MediaServerMainWindow::refreshAllSharedFolders()
{
    /* update folder contents */
    ContentDatabase* contentDatabase = m_msApp->contentDatabase();

    contentDatabase->updatePluginContents();
}
