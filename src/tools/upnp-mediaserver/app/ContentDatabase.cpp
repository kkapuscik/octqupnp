/*
 * ContentDatabase.cpp
 *
 *  Created on: 21-03-2011
 *      Author: saveman
 */

#include "ContentDatabase.h"

#include "RootContentContainer.h"

#include <upnpdev/Types.h>

#include <QtCore/QDir>
#include <QtWidgets/QMessageBox>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

Oct::Log::Logger<ContentDatabase> ContentDatabase::Logger("/oct/tools/mediaserver/ContentDatabase");

ContentDatabase::ContentDatabase(QObject* parent) :
    QObject(parent), m_accessLock(QReadWriteLock::NonRecursive)
{
    m_rootContentContainer = new RootContentContainer("Root");
    m_sharedObjects.insert(m_rootContentContainer->objectID(), m_rootContentContainer);
}

ContentDatabase::~ContentDatabase()
{
    // nothing to do
}

bool ContentDatabase::init()
{
#if 0
    // TODO: connection name?

    /* create connection */
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");

    /* setup connection */
    db.setDatabaseName(":memory:");
    if (!db.open()) {
        QMessageBox::critical(0, tr("Cannot open database"), tr("Unable to establish a database connection.\n"
            "SQLite support is required by this application."
            "Click Cancel to exit."), QMessageBox::Cancel);
        return false;
    }

    /* build database structure */
    QSqlQuery query;
#endif

#if 0
    query.exec("create table person (id int primary key, "
            "firstname varchar(20), lastname varchar(20))");

    /* - directories */
    query.exec("CREATE TABLE files("
            "id INT PRIMARY KEY,"
            "imagefile int,"
            "location varchar(20),"
            "country varchar(20),"
            "description varchar(100))");
#endif

    return true;
}

ContentPlugin* ContentDatabase::unsafeFindPlugin(const QString& id) const
{
    for (int i = 0; i < m_registeredPlugins.size(); ++i) {
        if (m_registeredPlugins[i]->uniqueID() == id) {
            return m_registeredPlugins[i];
        }
    }

    return NULL;
}

QList<QString> ContentDatabase::registeredPlugins() const
{
    QList<QString> list;

    {
        QReadLocker locker(&m_accessLock);

        for (int i = 0; i < m_registeredPlugins.size(); ++i) {
            list.append(m_registeredPlugins[i]->uniqueID());
        }
    }

    Logger.trace(this) << "registeredPlugins() => count=" << list.size();

    return list;
}

void ContentDatabase::unsafeRegisterPlugin(ContentPlugin* plugin)
{
    Q_ASSERT(plugin != NULL);
    Q_ASSERT(!m_registeredPlugins.contains(plugin));

    /* store the plugin */
    m_registeredPlugins.append(plugin);

    /* add the contents */
    ContentContainer* pluginContainer = plugin->rootContainer();
    if (pluginContainer != NULL) {
        /* add to content directory tree */
        pluginContainer->setParent(m_rootContentContainer);

        /* rebuild id->object table */
        unsafeRecursiveAddItems(pluginContainer);

        // TODO: events, etc.
    } else {
        // TODO: message box?
    }
}

void ContentDatabase::unsafeUnregisterPlugin(ContentPlugin* plugin)
{
    Q_ASSERT(plugin != NULL);
    Q_ASSERT(m_registeredPlugins.contains(plugin));

    /* stop storing the plugin */
    m_registeredPlugins.removeAll(plugin);

    /* remnove contents */
    ContentContainer* pluginContainer = plugin->rootContainer();
    if (pluginContainer != NULL) {
        /* remove from content directory tree */
        pluginContainer->setParent(NULL);

        /* rebuild id->object table */
        unsafeRecursiveRemoveItems(pluginContainer);

        // TODO: events, etc.
    }
}

void ContentDatabase::registerPlugin(ContentPlugin* plugin, bool initContents)
{
    Q_ASSERT(plugin);

    if (initContents) {
        if (!plugin->updateContents()) {
            // TODO: message box
        }
    }

    {
        QWriteLocker locker(&m_accessLock);
        unsafeRegisterPlugin(plugin);
    }

    emit pluginsChanged();
}

void ContentDatabase::registerPlugin(ContentPlugin* plugin)
{
    registerPlugin(plugin, true);
}

ContentPlugin* ContentDatabase::unregisterPlugin(const QString& pluginID)
{
    ContentPlugin* plugin = NULL;

    {
        QWriteLocker locker(&m_accessLock);

        ContentPlugin* plugin = unsafeFindPlugin(pluginID);

        Q_ASSERT(plugin != NULL);

        unsafeUnregisterPlugin(plugin);
    }

    emit pluginsChanged();

    return plugin;
}

void ContentDatabase::unsafeRecursiveAddItems(ContentContainer* container)
{
    /* add to shares */
    m_sharedObjects.insert(container->objectID(), container);

    /* process children */
    for (uint i = 0; i < container->childCount(); ++i) {
        ContentObject* childObject = container->child(i);
        if (childObject->isContainer()) {
            unsafeRecursiveAddItems((ContentContainer*)childObject);
        } else if (childObject->isItem()) {
            /* add to shares */
            m_sharedObjects.insert(childObject->objectID(), childObject);
        } else {
            Q_ASSERT(0);
        }
    }
}

void ContentDatabase::unsafeRecursiveRemoveItems(ContentContainer* container)
{
    /* process children */
    for (uint i = 0; i < container->childCount(); ++i) {
        ContentObject* childObject = container->child(i);
        if (childObject->isContainer()) {
            unsafeRecursiveRemoveItems((ContentContainer*)childObject);
        } else if (childObject->isItem()) {
            /* add to shares */
            m_sharedObjects.remove(childObject->objectID());
        } else {
            Q_ASSERT(0);
        }
    }

    /* remove from shares */
    m_sharedObjects.remove(container->objectID());
}

void ContentDatabase::setPluginSettings(const QString& pluginID, const ContentPluginSettings* settings)
{
    // TODO: change design

    ContentPlugin* plugin = unregisterPlugin(pluginID);

    Q_ASSERT(plugin != NULL);

    if (!plugin->setSettings(settings)) {
        // TODO: message box
    }

    registerPlugin(plugin, true);

    emit pluginsChanged();
}

ContentPluginSettings* ContentDatabase::pluginSettings(const QString& pluginID) const
{
    ContentPluginSettings* settings = NULL;

    {
        QReadLocker locker(&m_accessLock);

        ContentPlugin* contentPlugin = unsafeFindPlugin(pluginID);

        Q_ASSERT(contentPlugin != NULL);

        settings = contentPlugin->settings();
    }

    return settings;
}

void ContentDatabase::updatePluginContents(const QString& pluginID)
{
    // TODO: change design

    ContentPlugin* plugin = unregisterPlugin(pluginID);

    Q_ASSERT(plugin != NULL);

    registerPlugin(plugin, true);
}

void ContentDatabase::updatePluginContents()
{
    QList<QString> pluginIDs = registeredPlugins();

    for (int i = 0; i < pluginIDs.size(); ++i) {
        updatePluginContents(pluginIDs[i]);
    }
}

bool ContentDatabase::readLock()
{
    m_accessLock.lockForRead();
    return true;
}

void ContentDatabase::readUnlock()
{
    m_accessLock.unlock();
}

ContentObject* ContentDatabase::unsafeFindObject(const QString& objectID)
{
    SharedObjectsMap::iterator iter = m_sharedObjects.find(objectID);
    if (iter != m_sharedObjects.end()) {
        return iter.value();
    } else {
        return NULL;
    }
}
