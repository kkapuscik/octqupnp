/*
 * ContentVfs.h
 *
 *  Created on: 28-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONTENT_VFS_H_
#define _MEDIA_SERVER__CONTENT_VFS_H_

#include <httpserver/Vfs.h>

class ContentDatabase;

/**
 * Content virtual file system of HTTP server.
 */
class ContentVfs : public Oct::HttpServer::Vfs
{
Q_OBJECT

private:
    ContentDatabase* m_database;

public:
    /**
     * Constructs new HTTP server virtual file system.
     *
     * @param[in] database
     *      Database providing content and owner of the vfs.
     */
    ContentVfs(ContentDatabase* database);

    virtual Oct::HttpServer::VfsFile* open(const QString& path);
};

#endif /* _MEDIA_SERVER__CONTENT_VFS_H_ */
