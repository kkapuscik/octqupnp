#ifndef _MEDIA_SERVER__MEDIA_SERVER_MAIN_WINDOW_H_
#define _MEDIA_SERVER__MEDIA_SERVER_MAIN_WINDOW_H_

#include "MediaServerDevice.h"
#include "ui_MediaServerMainWindow.h"

#include <QtCore/QMap>
#include <QtCore/QSignalMapper>
#include <QtWidgets/QMainWindow>

// forward
class MediaServerApp;
class ContentPluginFactory;

class MediaServerMainWindow : public QMainWindow
{
Q_OBJECT

private:
    Ui::MediaServerMainWindowClass m_ui;
    MediaServerApp* m_msApp;

    QMenu* m_addSharesContextMenu;
    QMap<QObject*,const ContentPluginFactory*> m_addSharesMapping;

public:
    MediaServerMainWindow(MediaServerApp* app, QWidget* parent = 0);

private:
    QTableWidgetItem* getSelectedSharedFolderItem();

private:
    void buildAddSharesMenu();

private slots:
    void addPlugin();

    void appendLogLine(const QString& line);

    void updatePluginList();
    void updateServerState(bool started);

    void showAboutDialog();
    void showAboutQtDialog();
    void startServer();
    void stopServer();
    void applyCompatibilitySettings();
    void resetCompatibilitySettings();
    void applyServerSettings();
    void resetServerSettings();
    void setServerDefaultName();
    void generateServerUdn();
    void addSharedFolder();
    void removeSelectedSharedFolders();
    void editSelectedSharedFolders();
    void refreshSelectedSharedFolders();
    void refreshAllSharedFolders();
};

#endif // _MEDIA_SERVER__MEDIA_SERVER_MAIN_WINDOW_H_
