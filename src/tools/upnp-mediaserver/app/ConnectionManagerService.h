/*
 * SwitchPowerService.h
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONNECTION_MANAGER_SERVICE_H_
#define _MEDIA_SERVER__CONNECTION_MANAGER_SERVICE_H_

/*---------------------------------------------------------------------------*/

#include <upnpdev/Service.h>

#include <log/Logger.h>

/*---------------------------------------------------------------------------*/

class ConnectionManagerService : public Oct::UpnpDev::Service
{
    Q_OBJECT

private:
    static Oct::Log::Logger<ConnectionManagerService> Logger;

public:
    ConnectionManagerService(QObject* parent = 0);
    virtual ~ConnectionManagerService();

    bool processActionRequest(Oct::UpnpDev::ActionRequest& actionRequest);
};

/*---------------------------------------------------------------------------*/

#endif /* _MEDIA_SERVER__CONNECTION_MANAGER_SERVICE_H_ */
