/*
 * ContentVfs.cpp
 *
 *  Created on: 28-03-2011
 *      Author: saveman
 */

#include "ContentVfs.h"
#include "ContentDatabase.h"

#include <httpserver/StandardFile.h>

ContentVfs::ContentVfs(ContentDatabase* database) :
    Oct::HttpServer::Vfs(database)
{
    // nothing to do
}

Oct::HttpServer::VfsFile* ContentVfs::open(const QString& path)
{
    ContentDatabase* database = (ContentDatabase*)parent();
    Oct::HttpServer::VfsFile* vfsFile = NULL;

    QString objectID;
    int resourceIndex = -1;

    /* separate resource number from path, etc. */
    int sepIndex = path.indexOf('/');
    if (sepIndex < 0) {
        return NULL;
    }
    objectID = path.mid(0, sepIndex);
    bool resourceOK;
    resourceIndex = path.mid(sepIndex + 1).toInt(&resourceOK, 10);
    if (!resourceOK || resourceIndex < 0) {
        return NULL;
    }

    database->readLock();

    /* find the object */
    ContentObject* object = database->unsafeFindObject(objectID);

    if (object != NULL) {
        QList<const ContentResource*> resources = object->resources();

        if (resourceIndex >= 0 && resourceIndex < resources.size()) {
            vfsFile = resources[resourceIndex]->openFile();
        }
    }

    database->readUnlock();

    return vfsFile;
}
