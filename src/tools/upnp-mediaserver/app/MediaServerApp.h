#ifndef _MEDIA_SERVER__MEDIA_SERVER_APP_H_
#define _MEDIA_SERVER__MEDIA_SERVER_APP_H_

#include "MediaServerMainWindow.h"
#include "ContentDatabase.h"

#include <app/GuiApplication.h>
#include <httpserver/Server.h>
#include <upnpdev/Manager.h>

#include <QtWidgets/QSystemTrayIcon>

class MediaServerApp : public Oct::App::GuiApplication
{
Q_OBJECT

private:
    MediaServerMainWindow* m_mainWindow;
    QSystemTrayIcon* m_trayIcon;

    Oct::HttpServer::Server* m_httpServer;
    Oct::UpnpDev::Manager* m_deviceManager;
    MediaServerDevice* m_msDevice;
    ContentDatabase* m_contentDatabase;

public:
    /**
     * Constructs application.
     *
     * @param argc
     *      Arguments count (from main function).
     * @param argv
     *      Arguments array (from main function).
     */
    MediaServerApp(int& argc, char** argv);

    ContentDatabase* contentDatabase();
    const ContentDatabase* contentDatabase() const;

    MediaServerDevice* device();
    const MediaServerDevice* device() const;
    bool isDeviceStarted() const;

protected:
    virtual int initResources();
    virtual int initWindows();
    virtual QMainWindow* mainWindow();

public slots:
    virtual void terminate();

private slots:
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason reason);

public slots:
    void startDevice();
    void stopDevice();

signals:
    void deviceStateChanged(bool started);
};

#endif /* _MEDIA_SERVER__MEDIA_SERVER_APP_H_ */
