/*
 * RootContentContainer.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#ifndef ROOTCONTENTCONTAINER_H_
#define ROOTCONTENTCONTAINER_H_

#include <plugin/ContentContainer.h>

class RootContentContainer : public ContentContainer
{
public:
    RootContentContainer(const QString& title);
    virtual ~RootContentContainer();

    virtual void setParent(ContentContainer* newParent);
};

#endif /* ROOTCONTENTCONTAINER_H_ */
