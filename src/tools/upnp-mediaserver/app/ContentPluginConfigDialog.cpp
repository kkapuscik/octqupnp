#include "ContentPluginConfigDialog.h"

#include <QtWidgets/QBoxLayout>

ContentPluginConfigDialog::ContentPluginConfigDialog(const ContentPluginFactory* factory, QWidget *parent) :
    QDialog(parent)
{
    m_ui.setupUi(this);

    QVBoxLayout* boxLayout = new QVBoxLayout(m_ui.m_configFrame);

    m_configWidget = factory->createConfigWidget(this);

    boxLayout->addWidget(m_configWidget, 0);
    boxLayout->addStretch(1);
}

ContentPluginConfigDialog::~ContentPluginConfigDialog()
{
    // nothing to do
}

void ContentPluginConfigDialog::accept()
{
    if (!m_configWidget->validateSettings()) {
        return;
    }

    QDialog::accept();
}

void ContentPluginConfigDialog::reject()
{
    QDialog::reject();
}

ContentPluginSettings* ContentPluginConfigDialog::pluginSettings()
{
    return m_configWidget->settings();
}
