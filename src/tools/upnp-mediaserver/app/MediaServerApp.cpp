#include "MediaServerApp.h"

#include <QtCore/QResource>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMessageBox>

MediaServerApp::MediaServerApp(int& argc, char** argv) :
    Oct::App::GuiApplication(argc, argv),
    m_deviceManager(NULL), m_msDevice(NULL)
{
    m_contentDatabase = new ContentDatabase();

    /* we do not want to close app automatically */
    setQuitOnLastWindowClosed(false);
}

ContentDatabase* MediaServerApp::contentDatabase()
{
    return m_contentDatabase;
}

const ContentDatabase* MediaServerApp::contentDatabase() const
{
    return m_contentDatabase;
}

MediaServerDevice* MediaServerApp::device()
{
    return m_msDevice;
}

const MediaServerDevice* MediaServerApp::device() const
{
    return m_msDevice;
}

bool MediaServerApp::isDeviceStarted() const
{
    return m_deviceManager->isRegistered(m_msDevice);
}

#include "../plugin/ContentPluginFactoryManager.h"
#include "../plugin-fs/FileSystemContentPluginFactory.h"
#include "../plugin-xkcd/XkcdContentPluginFactory.h"

int MediaServerApp::initResources()
{
    Q_INIT_RESOURCE(MediaServer);

    if (!m_contentDatabase->init()) {
        return 1;
    }

    m_httpServer = new Oct::HttpServer::Server(this);
    m_deviceManager = new Oct::UpnpDev::Manager(m_httpServer, this);
    m_msDevice = new MediaServerDevice(m_httpServer, m_contentDatabase, this);

    startDevice();

    // TODO: refactoring needed
    ContentPluginFactoryManager::instance()->registerFactory(new FileSystemContentPluginFactory());
    ContentPluginFactoryManager::instance()->registerFactory(new XkcdContentPluginFactory());

    return 0;
}

void MediaServerApp::terminate()
{
    if (m_deviceManager->isRegistered(m_msDevice)) {
        stopDevice();
    }

    GuiApplication::terminate();
}

int MediaServerApp::initWindows()
{
    /* create main window */
    m_mainWindow = new MediaServerMainWindow(this);
    // m_mainWindow->show();

    /* create tray icon */
    m_trayIcon = new QSystemTrayIcon(m_mainWindow);
    m_trayIcon->setIcon(QIcon(":/oct/upnp-mediaserver/resources/icons/octaedr.svg"));
    m_trayIcon->setVisible(true);

    /* create tray icon menu (parent given to make sure it will be deleted) */
    QMenu* trayIconMenu = new QMenu(m_mainWindow);
    trayIconMenu->addAction(tr("Exit"), this, SLOT( terminate() ));
    m_trayIcon->setContextMenu(trayIconMenu);

    /* connect signals */
    connect(m_trayIcon, SIGNAL( activated(QSystemTrayIcon::ActivationReason) ),
            this, SLOT( onTrayIconActivated(QSystemTrayIcon::ActivationReason) ));

    return 0;
}

QMainWindow* MediaServerApp::mainWindow()
{
    return m_mainWindow;
}

void MediaServerApp::onTrayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::DoubleClick) {
        m_mainWindow->setVisible(!m_mainWindow->isVisible());
    }
}

void MediaServerApp::startDevice()
{
#if 0
    QString udn = m_ui.m_udnLineEdit->text();
    QString friendlyName = m_ui.m_friendlyNameLineEdit->text();

    if (!m_binaryLightDevice->setUDN(udn)) {
        QMessageBox::warning(this, tr("Start Device"), tr("Cannot start device.\n\nUDN is invalid."));
        return;
    }

    if (!m_binaryLightDevice->setFriendlyName(friendlyName)) {
        QMessageBox::warning(this, tr("Start Device"), tr("Cannot start device.\n\nFriendly name is invalid."));
        return;
    }
#endif

    bool success = m_deviceManager->registerDevice(m_msDevice);

    if (success) {
        emit deviceStateChanged(true);
    } else {
        QMessageBox::warning(NULL, tr("Start Device"), tr("Cannot start device!"));
    }

#if 0
    updateUIData();
    updateUIActions();
#endif
}

void MediaServerApp::stopDevice()
{
    bool success = m_deviceManager->unregisterDevice(m_msDevice);

    if (success) {
        emit deviceStateChanged(false);
    } else {
        QMessageBox::warning(NULL, tr("Stop Device"), tr("Cannot stop device!"));
    }
#if 0
    updateUIData();
    updateUIActions();
#endif
}
