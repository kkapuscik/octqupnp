/*
 * BinaryLightDevice.h
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__MEDIA_SERVER_DEVICE_H_
#define _MEDIA_SERVER__MEDIA_SERVER_DEVICE_H_

#include "ConnectionManagerService.h"
#include "ContentDirectoryService.h"
#include "ContentDatabase.h"

#include <upnpdev/Device.h>
#include <httpserver/Server.h>

class MediaServerDevice : public Oct::UpnpDev::Device
{
    Q_OBJECT

private:
    ConnectionManagerService* m_cms;
    ContentDirectoryService* m_cds;

public:
    MediaServerDevice(Oct::HttpServer::Server* httpServer, ContentDatabase* database, QObject* parent = 0);
    virtual ~MediaServerDevice();

    QString defaultFriendlyName() const;

    void resetFriendlyName();
    void generateUDN();

    bool setUDN(const QString& udnStr);
    bool setFriendlyName(const QString& friendlyName);
};

#endif /* _MEDIA_SERVER__MEDIA_SERVER_DEVICE_H_ */
