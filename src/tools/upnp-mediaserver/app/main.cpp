/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "MediaServerApp.h"

/*---------------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    return Oct::App::GuiApplication::startApp<MediaServerApp>(
            "UPnP-MediaServer", "0.1", argc, argv);
}

/*---------------------------------------------------------------------------*/
