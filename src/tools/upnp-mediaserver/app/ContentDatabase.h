/*
 * ContentDatabase.h
 *
 *  Created on: 21-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONTENT_DATABASE_H_
#define _MEDIA_SERVER__CONTENT_DATABASE_H_

#include <plugin/ContentObject.h>
#include "RootContentContainer.h"
#include <plugin/ContentItem.h>
#include <plugin/ContentPlugin.h>

#include <log/Logger.h>

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QReadWriteLock>

class ContentDatabase : public QObject
{
Q_OBJECT

private:
    static Oct::Log::Logger<ContentDatabase> Logger;

private:
    typedef QList<ContentPlugin*> PluginsCollection;
    typedef QMap<QString, ContentObject*> SharedObjectsMap;

private:
    mutable QReadWriteLock m_accessLock;
    PluginsCollection m_registeredPlugins;
    SharedObjectsMap m_sharedObjects;
    RootContentContainer* m_rootContentContainer;

public:
    ContentDatabase(QObject* parent = 0);
    virtual ~ContentDatabase();

    bool init();

    QList<QString> registeredPlugins() const;

    void registerPlugin(ContentPlugin* plugin);
    ContentPlugin* unregisterPlugin(const QString& pluginID);

    void setPluginSettings(const QString& plugin, const ContentPluginSettings* settings);
    ContentPluginSettings* pluginSettings(const QString& plugin) const;
    void updatePluginContents(const QString& plugin);
    void updatePluginContents();

    bool readLock();
    void readUnlock();
    ContentObject* unsafeFindObject(const QString& objectID);

private:
    ContentPlugin* unsafeFindPlugin(const QString& id) const;

    void registerPlugin(ContentPlugin* plugin, bool initContents);

    void unsafeRecursiveAddItems(ContentContainer* container);
    void unsafeRecursiveRemoveItems(ContentContainer* container);

    void unsafeRegisterPlugin(ContentPlugin* plugin);
    void unsafeUnregisterPlugin(ContentPlugin* plugin);


signals:
    void pluginsChanged();

};

#endif /* _MEDIA_SERVER__CONTENT_DATABASE_H_ */
