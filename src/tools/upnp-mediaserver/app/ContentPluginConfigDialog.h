#ifndef CONTENTPLUGINCONFIGDIALOG_H
#define CONTENTPLUGINCONFIGDIALOG_H

#include <plugin/ContentPluginFactory.h>

#include "ui_ContentPluginConfigDialog.h"

#include <QtWidgets/QDialog>

class ContentPluginConfigDialog : public QDialog
{
Q_OBJECT

private:
    Ui::ContentPluginConfigDialogClass m_ui;
    ContentPluginConfigWidget* m_configWidget;

public:
    ContentPluginConfigDialog(const ContentPluginFactory* factory, QWidget *parent = 0);
    virtual ~ContentPluginConfigDialog();

    ContentPluginSettings* pluginSettings();

public slots:
    virtual void accept();
    virtual void reject();
};

#endif // CONTENTPLUGINCONFIGDIALOG_H
