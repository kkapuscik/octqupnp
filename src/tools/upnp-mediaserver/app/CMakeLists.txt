cmake_minimum_required(VERSION 2.8.11)

project(oct-upnp-mediaserver)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

include_directories(${Qt5Core_INCLUDE_DIRS})
include_directories(${Qt5Widgets_INCLUDE_DIRS})

file(GLOB oct-upnp-mediaserver_SOURCES *.cpp)
file(GLOB oct-upnp-mediaserver_HEADERS *.h)
file(GLOB oct-upnp-mediaserver_UIS *.ui)
file(GLOB oct-upnp-mediaserver_RESOURCES *.qrc)

QT5_WRAP_CPP(oct-upnp-mediaserver_MOCSOURCES ${oct-upnp-mediaserver_HEADERS})
QT5_WRAP_UI(oct-upnp-mediaserver_UIHEADERS ${oct-upnp-mediaserver_UIS})
QT5_ADD_RESOURCES(oct-upnp-mediaserver_RESSRC ${oct-upnp-mediaserver_RESOURCES})

include_directories(${CMAKE_CURRENT_BINARY_DIR}) # for UIS
include_directories(${CMAKE_CURRENT_SOURCE_DIR}) # for UIS

add_executable(oct-upnp-mediaserver
    ${oct-upnp-mediaserver_SOURCES}
    ${oct-upnp-mediaserver_MOCSOURCES}
    ${oct-upnp-mediaserver_UIHEADERS}
    ${oct-upnp-mediaserver_RESSRC})
target_link_libraries(oct-upnp-mediaserver
    oct-upnp-mediaserver-plugin
    oct-upnp-mediaserver-plugin-fs
    oct-upnp-mediaserver-plugin-xkcd
    octfilemagic
    octupnpdev
    octjson
    octapp
    octlog
    octutil
    Qt5::Core Qt5::Widgets
    )
