/*
 * SwitchPowerService.cpp
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#include "ConnectionManagerService.h"

#include <upnpdev/Types.h>

Oct::Log::Logger<ConnectionManagerService> ConnectionManagerService::Logger(
        "/oct/tools/upnp-ms/ConnectionManagerService");

ConnectionManagerService::ConnectionManagerService(QObject* parent) :
    Oct::UpnpDev::Service(parent)
{
    setServiceType("urn:schemas-upnp-org:service:ConnectionManager:2");
    setServiceId("urn:upnp-org:serviceId:ConnectionManager");

    if (!addFromSCPD(QString(":/oct/upnp-mediaserver/resources/descriptions/ConnectionManager2.xml"))) {
        Logger.serror() << "Service initialization failed.";
        Q_ASSERT(0);
    }
}

ConnectionManagerService::~ConnectionManagerService()
{
    // nothing to do
}

bool ConnectionManagerService::processActionRequest(Oct::UpnpDev::ActionRequest& actionRequest)
{
    if (actionRequest.actionName() == "GetProtocolInfo") {

        // TODO
        actionRequest.setOutputArgumentValue("Source", "");
        actionRequest.setOutputArgumentValue("Sink", "");

    } else if (actionRequest.actionName() == "GetCurrentConnectionIDs") {

        // TODO
        actionRequest.setOutputArgumentValue("ConnectionIDs", "0");

    } else if (actionRequest.actionName() == "GetCurrentConnectionInfo") {

        QVariant value = actionRequest.argumentValue("ConnectionID");
        bool ok;
        int connectionID = value.toInt(&ok);
        if (ok) {
            /*
             * If OPTIONAL action PrepareForConnection() is not implemented then (limited) connection information
             * can be retrieved for ConnectionID 0.
             */
            if (connectionID == 0) {
                /*
                 * RcsID MUST be 0 (a single instance of the RenderingControl service is implemented)
                 * or -1 (RenderingControl Service is not implemented)
                 */
                actionRequest.setOutputArgumentValue("RcsID", -1);

                /*
                 * AVTransportID MUST be 0 (a single instance of the AVTransport service is implemented)
                 * or -1 (AVTransport service is not implemented)
                 */
                actionRequest.setOutputArgumentValue("AVTransportID", -1);

                /*
                 * ProtocolInfo MUST contain accurate information if it is known,
                 * otherwise it MUST be the empty string.
                 */
                actionRequest.setOutputArgumentValue("ProtocolInfo", "");

                /*
                 * PeerConnectionManager MUST be the empty string.
                 */
                actionRequest.setOutputArgumentValue("PeerConnectionManager", "");

                /*
                 * PeerConnectionID MUST be -1.
                 */
                actionRequest.setOutputArgumentValue("PeerConnectionID", "");

                /*
                 * Direction MUST be “Input” or “Output”.
                 */
                actionRequest.setOutputArgumentValue("Direction", "Output");

                /*
                 * Status MUST be “OK” or “Unknown”
                 */
                actionRequest.setOutputArgumentValue("Status", "Unknown");

            } else {
                actionRequest.setError(706, "Invalid connection reference");
            }
        } else {
            actionRequest.setStandardError(Oct::UpnpDev::ActionRequest::ERROR_CODE__INVALID_ARGS);
        }
    } else {
        actionRequest.setStandardError(Oct::UpnpDev::ActionRequest::ERROR_CODE__INVALID_ACTION);
    }

    return true;
}
