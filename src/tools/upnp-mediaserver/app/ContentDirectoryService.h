/*
 * SwitchPowerService.h
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONTENT_DIRECTORY_SERVICE_H_
#define _MEDIA_SERVER__CONTENT_DIRECTORY_SERVICE_H_

#include "ContentDatabase.h"

#include <upnpdev/Service.h>
#include <log/Logger.h>
#include <httpserver/Server.h>

class ContentDirectoryService : public Oct::UpnpDev::Service
{
    Q_OBJECT

private:
    static Oct::Log::Logger<ContentDirectoryService> Logger;

private:
    Oct::HttpServer::Server* m_httpServer;
    ContentDatabase* m_database;

public:
    ContentDirectoryService(Oct::HttpServer::Server* httpServer, ContentDatabase* database, QObject* parent = 0);
    virtual ~ContentDirectoryService();

    bool processActionRequest(Oct::UpnpDev::ActionRequest& actionRequest);

private:
    void processGetSearchCapabilities(Oct::UpnpDev::ActionRequest& actionRequest);
    void processGetSortCapabilities(Oct::UpnpDev::ActionRequest& actionRequest);
    void processGetFeatureList(Oct::UpnpDev::ActionRequest& actionRequest);
    void processGetSystemUpdateID(Oct::UpnpDev::ActionRequest& actionRequest);
    void processGetServiceResetToken(Oct::UpnpDev::ActionRequest& actionRequest);
    void processBrowse(Oct::UpnpDev::ActionRequest& actionRequest);
    void processSearch(Oct::UpnpDev::ActionRequest& actionRequest);

    QString createDidlFragment(ContentObject* object, QString filer);

};

#endif /* _MEDIA_SERVER__CONTENT_DIRECTORY_SERVICE_H_ */
