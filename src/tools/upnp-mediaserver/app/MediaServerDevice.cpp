/*
 * BinaryLightDevice.cpp
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#include "MediaServerDevice.h"

MediaServerDevice::MediaServerDevice(Oct::HttpServer::Server* httpServer, ContentDatabase* database, QObject* parent) :
    Oct::UpnpDev::Device(parent)
{
    /* setup device */
    setProperty(PROPERTY_DEVICE_TYPE, "urn:schemas-upnp-org:device:MediaServer:1");
    setProperty(PROPERTY_MANUFACTURER, "OCTaedr Software");
    setProperty(PROPERTY_MANUFACTURER_URL, "http://www.octaedr.info/");
    setProperty(PROPERTY_MODEL_NAME, "Media Server");
    setProperty(PROPERTY_MODEL_NUMBER, "1.0");
    setProperty(PROPERTY_MODEL_DESCRIPTION, "OCTaedr Media Server Device");
    resetFriendlyName();

    /* create services */
    m_cms = new ConnectionManagerService(this);
    m_cds = new ContentDirectoryService(httpServer, database, this);

    /* register services */
    addService(m_cms);
    addService(m_cds);
}

MediaServerDevice::~MediaServerDevice()
{
    // do nothing
}

QString MediaServerDevice::defaultFriendlyName() const
{
    return "OCTaedr Media Server";
}

void MediaServerDevice::resetFriendlyName()
{
    setProperty(PROPERTY_FRIENDLY_NAME, defaultFriendlyName());
}

void MediaServerDevice::generateUDN()
{
    Device::setUDN(Oct::UpnpDev::UUID::generate());
}

bool MediaServerDevice::setUDN(const QString& udnStr)
{
    Oct::UpnpDev::UUID uuid(udnStr);

    if (uuid.isValid()) {
        Device::setUDN(uuid);
        return true;
    } else {
        return false;
    }
}

bool MediaServerDevice::setFriendlyName(const QString& friendlyName)
{
    QString fn = friendlyName.trimmed();
    if (!fn.isEmpty()) {
        setProperty(PROPERTY_FRIENDLY_NAME, fn);
        return true;
    } else {
        return false;
    }
}
