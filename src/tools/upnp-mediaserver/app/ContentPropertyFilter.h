/*
 * ContentPropertyFilter.h
 *
 *  Created on: 31-03-2011
 *      Author: saveman
 */

#ifndef CONTENTPROPERTYFILTER_H_
#define CONTENTPROPERTYFILTER_H_

#include <QtCore/QString>

class ContentPropertyFilter
{
private:
    enum Type {
        TYPE_INVALID,
        TYPE_ASTERISK,
        TYPE_NORMAL
    };

public:
    Type m_type;

private:
    ContentPropertyFilter(Type type);

public:
    bool isValid() const;

    bool objectAttributeAllowed(const QString& attribute) const;
    bool propertyAllowed(const QString& property) const;
    bool propertyAttributeAllowed(const QString& property, const QString& attribute) const;


public:
    static ContentPropertyFilter parseFilter(const QString& filter);

};

#endif /* CONTENTPROPERTYFILTER_H_ */
