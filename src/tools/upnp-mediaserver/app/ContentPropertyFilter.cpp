/*
 * ContentPropertyFilter.cpp
 *
 *  Created on: 31-03-2011
 *      Author: saveman
 */

#include "ContentPropertyFilter.h"

#include <QtCore/QStringList>

/*
 2.3.15
 A_ARG_TYPE_Filter
 This state variable is introduced to provide type information for the Filter argument in the Browse() and
 Search() actions. The comma-separated list of property specifiers indicates which metadata properties are
 to be returned in the Result of the Browse() or Search() actions. Each property name MUST include the
 standard namespace prefix for that property, except for the DIDL-Lite namespace. Properties in the DIDL-
 Lite namespace MUST always be returned without the prefix. All property names MUST be fully qualified
 using the double colon (“::”) syntax as defined in Section 2.2.20, “property”. For example,
 “upnp:foreignMetadata::fmBody::fmURI”.
 The Filter argument allows control points to control the complexity of the object metadata properties that
 are returned within the DIDL-Lite Result argument of the Browse() and Search() actions. Properties
 REQUIRED by the DIDL-Lite schema are always returned in the Result output argument. The Filter
 argument allows a control point to specify additional properties, not REQUIRED by the DIDL-Lite schema
 to be returned in Result. Compliant ContentDirectory service implementations do not return optional
 properties unless they are explicitly requested in the Filter input argument.
 */

/*
 * [R]
 * @id
 * @parentID
 * @restricted
 * dc:title
 * upnp:class
 * res@protocolInfo
 * upnp:programID@type
 * upnp:seriesID@type
 * upnp:channelID@type
 * upnp:programCode@type
 * upnp:channelGroupName@id
 * upnp:price@currency
 * upnp:deviceUDN@serviceType
 * upnp:deviceUDN@serviceID
 * upnp:stateVariableCollection@serviceName
 *
 * upnp:foreignMetadata@type
 * upnp:foreignMetadata::fmId
 * upnp:foreignMetadata::fmClass
 * upnp:foreignMetadata::fmProvider
 * upnp:foreignMetadata::fmBody
 * upnp:foreignMetadata::fmBody@xmlFlag
 *
 * [R]-container
 * upnp:searchClass@includeDerived
 * upnp:createClass@includeDerived
 *
 * ------
 * Object.item:
 * upnp:objectUpdateID      Required if the Track Changes Option (TCO) is supported. Otherwise, prohibited.
 * res@updateCount          Required if the Track Changes Option (TCO) is supported. Otherwise, prohibited.
 * @refID                   REQUIRED for reference items, otherwise prohibited.
 *
 * Table C-16: bookmarkItem:item Properties
 * upnp:bookmarkedObjectID
 * upnp:deviceUDN
 * upnp:serviceType
 * upnp:serviceId
 * upnp:stateVariableCollection
 *
 * object.container
 * upnp:objectUpdateID          Required if the Track Changes Option (TCO) is supported. Otherwise, prohibited.
 * upnp:containerUpdateID       Required if the Track Changes Option (TCO) is supported. Otherwise, prohibited.
 * upnp:totalDeletedChildCount  Required if the Track Changes Option (TCO) is supported. Otherwise, prohibited.
 * @childCount                  Required if the Track Changes Option (TCO) is supported. Otherwise, optional.
 *
 * Table C-30: storageSystem:container Properties
 * upnp:storageTotal
 * upnp:storageUsed
 * upnp:storageFree
 * upnp:storageMaxPartition
 * upnp:storageMedium
 *
 * Table C-31: storageVolume:container Properties
 * upnp:storageTotal
 * upnp:storageUsed
 * upnp:storageFree
 * upnp:storageMedium
 *
 * Table C-32: storageFolder:container Properties
 * upnp:storageUsed
 *
 */
ContentPropertyFilter::ContentPropertyFilter(Type type) :
    m_type(type)
{
    // nothing to do
}

bool ContentPropertyFilter::isValid() const
{
    return m_type != TYPE_INVALID;
}

ContentPropertyFilter ContentPropertyFilter::parseFilter(const QString& filter)
{
    QStringList items = filter.split(',', QString::KeepEmptyParts);

    if (items.size() == 0 && items[0] == "*") {
        return ContentPropertyFilter(TYPE_ASTERISK);
    } else {
        // TODO

        return ContentPropertyFilter(TYPE_INVALID);
    }
}
