/*
 * SwitchPowerService.cpp
 *
 *  Created on: 2011-02-06
 *      Author: saveman
 */

#include "ContentDirectoryService.h"
#include "ContentVfs.h"

#include <upnpdev/Types.h>
#include <httpserver/VfsRequestHandlerProvider.h>

Oct::Log::Logger<ContentDirectoryService> ContentDirectoryService::Logger("/oct/tools/upnp-ms/ContentDirectoryService");

ContentDirectoryService::ContentDirectoryService(Oct::HttpServer::Server* httpServer, ContentDatabase* database, QObject* parent) :
    Oct::UpnpDev::Service(parent), m_httpServer(httpServer), m_database(database)
{
    setServiceType("urn:schemas-upnp-org:service:ContentDirectory:3");
    setServiceId("urn:upnp-org:serviceId:ContentDirectory");

    if (!addFromSCPD(QString(":/oct/upnp-mediaserver/resources/descriptions/ContentDirectory3.xml"))) {
        Logger.error(this) << "Service initialization failed.";
        Q_ASSERT(0);
    }

    // TODO: cleanup
    //QString baseURL = QString("/%1/").arg(Oct::UpnpDev::UUID::generate().toString(false));
    QString baseURL = QString("/content/");

    m_httpServer->requestHandlerManager().registerHandlerProvider(
            baseURL, new Oct::HttpServer::VfsRequestHandlerProvider(new ContentVfs(m_database)));
}

ContentDirectoryService::~ContentDirectoryService()
{
    // nothing to do
}

void ContentDirectoryService::processGetSearchCapabilities(Oct::UpnpDev::ActionRequest& actionRequest)
{
    // TODO
    actionRequest.setOutputArgumentValue(
            "SearchCaps",
            "*"
    );

#if 0
    <action>
        <name>GetSearchCapabilities</name>
        <argumentList>
            <argument>
                <name>SearchCaps</name>
                <direction>out</direction>
                <relatedStateVariable>SearchCapabilities</relatedStateVariable>
            </argument>
        </argumentList>
    </action>
#endif
}

void ContentDirectoryService::processGetSortCapabilities(Oct::UpnpDev::ActionRequest& actionRequest)
{
    // TODO
    actionRequest.setOutputArgumentValue(
            "SearchCaps",
            "*"
    );
#if 0
    <action>
        <name>GetSortCapabilities</name>
        <argumentList>
            <argument>
                <name>SortCaps</name>
                <direction>out</direction>
                <relatedStateVariable>SortCapabilities</relatedStateVariable>
            </argument>
        </argumentList>
    </action>
#endif
}

void ContentDirectoryService::processGetFeatureList(Oct::UpnpDev::ActionRequest& actionRequest)
{
    // TODO
    actionRequest.setOutputArgumentValue(
            "FeatureList",
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            "<Features\n"
            "    xmlns=\"urn:schemas-upnp-org:av:avs\"\n"
            "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
            "    xsi:schemaLocation=\"urn:schemas-upnp-org:av:avs http://www.upnp.org/schemas/av/avs.xsd\">\n"
            "</Features>\n"
    );

/*
    Response:
    GetFeatureList("
    <?xml version="1.0" encoding="UTF-8"?>
    <Features
    xmlns="urn:schemas-upnp-org:av:avs"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
    urn:schemas-upnp-org:av:avs
    http://www.upnp.org/schemas/av/avs.xsd">
    <Feature name="FOREIGN_METADATA" version="1">
    <type id="acme.org_MD1" provider="acme_metadata.org"></type>
    <type id="acme.org_MD2" provider="acme_metadata.org"></type>
    </Feature>
    </Features>")
*/

/*
    <action>
        <name>GetFeatureList</name>
        <argumentList>
            <argument>
                <name>FeatureList</name>
                <direction>out</direction>
                <relatedStateVariable>FeatureList</relatedStateVariable>
            </argument>
        </argumentList>
    </action>
*/
}

void ContentDirectoryService::processGetSystemUpdateID(Oct::UpnpDev::ActionRequest& actionRequest)
{
    // FIXME
    actionRequest.setOutputArgumentValue("Id", 0);

#if 0
    <action>
        <name>GetSystemUpdateID</name>
        <argumentList>
            <argument>
                <name>Id</name>
                <direction>out</direction>
                <relatedStateVariable>SystemUpdateID</relatedStateVariable>
            </argument>
        </argumentList>
    </action>
#endif
}

void ContentDirectoryService::processGetServiceResetToken(Oct::UpnpDev::ActionRequest& actionRequest)
{
    // FIXME
    actionRequest.setOutputArgumentValue("ResetToken", 0);
/*
    <action>
        <name>GetServiceResetToken</name>
        <argumentList>
            <argument>
                <name>ResetToken</name>
                <direction>out</direction>
                <relatedStateVariable>ServiceResetToken</relatedStateVariable>
            </argument>
        </argumentList>
    </action>
*/
}

QString ContentDirectoryService::createDidlFragment(ContentObject* object, QString filer)
{
    bool isItem = object->isItem();
    QString result;
    QString objectType = isItem ? "item" : "container";

    result += QString("<%1 id=\"%2\" parentID=\"%3\">\n").
            arg(objectType).
            arg(object->objectID()).
            arg(object->parentID());

    result += QString("<dc:title>%1</dc:title>\n").arg(object->title());
    result += QString("<upnp:class>%1</upnp:class>\n").arg(object->upnpClass());

    QList<const ContentResource*> resources = object->resources();
    for (int i = 0; i < resources.size(); ++i) {
        // TODO
        QString protocolInfo = QString("http-get:*:%1:*").arg(resources[i]->mimeType());

        QString resURI = resources[i]->externalURI();
        if (resURI.isNull()) {
            resURI = m_httpServer->urlBase() + QString("content/%1/%2").arg(object->objectID()).arg(i);
        }

        result += QString("<res protocolInfo=\"%1\">%2</res>\n").arg(protocolInfo).arg(resURI);
    }

    result += QString("</%1>\n").arg(objectType);

    return result;
}

void ContentDirectoryService::processBrowse(Oct::UpnpDev::ActionRequest& actionRequest)
{
    int standardErrorCode = Oct::UpnpDev::ActionRequest::ERROR_CODE__INVALID_ARGS;
    int cdsErrorCode = 720;
    QString cdsErrorReason = "Cannot process the request";

    do {
        QVariant argObjectID       = actionRequest.argumentValue("ObjectID");
        QVariant argBrowseFlag     = actionRequest.argumentValue("BrowseFlag");
        QVariant argFilter         = actionRequest.argumentValue("Filter");
        QVariant argStartingIndex  = actionRequest.argumentValue("StartingIndex");
        QVariant argRequestedCount = actionRequest.argumentValue("RequestedCount");
        QVariant argSortCriteria   = actionRequest.argumentValue("SortCriteria");

        bool okStartingIndex;
        bool okRequestedCount;

        QString objectID     = argObjectID.toString();
        QString browseFlag   = argBrowseFlag.toString();
        QString filter       = argFilter.toString();
        uint startingIndex   = argStartingIndex.toUInt(&okStartingIndex);
        uint requestedCount  = argRequestedCount.toUInt(&okRequestedCount);
        QString sortCriteria = argSortCriteria.toString();

        enum {
            BROWSE_FLAG_METATADA,
            BROWSE_FLAG_DIRECT_CHILDREN
        } browseFlagValue;

        if (objectID.length() == 0 ) {
            Logger.info(this) << "Browse: failed - invalid object ID: " << objectID;
            break;
        }
        if (browseFlag == "BrowseMetadata") {
            browseFlagValue = BROWSE_FLAG_METATADA;
        } else if (browseFlag == "BrowseDirectChildren") {
            browseFlagValue = BROWSE_FLAG_DIRECT_CHILDREN;
        } else {
            Logger.info(this) << "Browse: failed - invalid browse flag: " << browseFlag;
            break;
        }
        if (filter.isNull()) {
            Logger.info(this) << "Browse: failed - invalid filter: " << filter;
            break;
        }
        if (!okStartingIndex) {
            Logger.info(this) << "Browse: failed - invalid starting index: " << argStartingIndex.toString();
            break;
        }
        if (!okRequestedCount) {
            Logger.info(this) << "Browse: failed - invalid requested count: " << argRequestedCount.toString();
            break;
        }
        if (sortCriteria.isNull()) {
            Logger.info(this) << "Browse: failed - invalid sort criteria: " << sortCriteria;
            break;
        }

        standardErrorCode = 0;

        QString result = "";
        uint numberReturned = 0;
        uint totalMatches = 0;
        uint updateID = 0;

        if (m_database->readLock()) {
            if (browseFlagValue == BROWSE_FLAG_METATADA) {
                ContentObject* object = m_database->unsafeFindObject(objectID);
                if (object != NULL) {
                    numberReturned = 1;
                    totalMatches = 1;
                    updateID = 0; /* TODO */

                    result +=
                            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                            "<DIDL-Lite"
                            " xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\""
                            " xmlns:dc=\"http://purl.org/dc/elements/1.1/\""
                            " xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\""
                            " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                            " xsi:schemaLocation=\""
                            "  urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/ http://www.upnp.org/schemas/av/didl-lite.xsd"
                            "  urn:schemas-upnp-org:metadata-1-0/upnp/ http://www.upnp.org/schemas/av/upnp.xsd\""
                            ">\n";

                    result += createDidlFragment(object, filter);

                    result +=
                            "</DIDL-Lite>\n";

                    cdsErrorCode = 0;
                } else {
                    cdsErrorCode = 701;
                    cdsErrorReason = "No such object";
                }
            } else if (browseFlagValue == BROWSE_FLAG_DIRECT_CHILDREN) {
                ContentObject* object = m_database->unsafeFindObject(objectID);
                if (object != NULL) {
                    if (object->isContainer()) {
                        ContentContainer* container = (ContentContainer*)object;

                        numberReturned = 0;
                        totalMatches = container->childCount();
                        updateID = 0; /* TODO */

                        result +=
                                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                "<DIDL-Lite"
                                " xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\""
                                " xmlns:dc=\"http://purl.org/dc/elements/1.1/\""
                                " xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\""
                                " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                                " xsi:schemaLocation=\""
                                "  urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/ http://www.upnp.org/schemas/av/didl-lite.xsd"
                                "  urn:schemas-upnp-org:metadata-1-0/upnp/ http://www.upnp.org/schemas/av/upnp.xsd\""
                                ">\n";

                        // TODO: limit
                        for (uint i = startingIndex; i < totalMatches && numberReturned < 30; ++i) {
                            result += createDidlFragment(container->child(i), filter);
                            ++numberReturned;
                        }

                        result +=
                                "</DIDL-Lite>\n";
                    } else {
                        numberReturned = 0;
                        totalMatches = 0;
                        updateID = 0; /* TODO */
                    }

                    cdsErrorCode = 0;
                } else {
                    cdsErrorCode = 701;
                    cdsErrorReason = "No such object";
                }
            } else {
                Q_ASSERT(0);
            }

            m_database->readUnlock();
        }

        // TODO

        if (cdsErrorCode == 0) {
            actionRequest.setOutputArgumentValue("Result", result);
            actionRequest.setOutputArgumentValue("NumberReturned", numberReturned);
            actionRequest.setOutputArgumentValue("TotalMatches", totalMatches);
            actionRequest.setOutputArgumentValue("UpdateID", updateID);
        }
    } while (0);

    if (standardErrorCode != 0) {
        actionRequest.setStandardError(standardErrorCode);
    } else if (cdsErrorCode != 0) {
        actionRequest.setError(cdsErrorCode, cdsErrorReason);
    }

#if 0
    701
    No such object
    The Browse() request failed because the specified ObjectID
    argument is invalid.
    709
    Unsupported or
    invalid sort criteria
    The Browse() request failed because the specified SortCriteria is
    not supported or is invalid.
    720
    Cannot process the request
    The Browse() request failed because the ContentDirectory service
    is unable to compute, in the time allotted, the total number of
    objects that are a match for the browse criteria and is additionally
    unable to return, in the time allotted, any objects that match the
    browse criteria.
#endif

#if 0
    <action>
        <name>Browse</name>
        <argumentList>
            <argument>
                <name>ObjectID</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_ObjectID</relatedStateVariable>
            </argument>
            <argument>
                <name>BrowseFlag</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_BrowseFlag</relatedStateVariable>
            </argument>
            <argument>
                <name>Filter</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_Filter</relatedStateVariable>
            </argument>
            <argument>
                <name>StartingIndex</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_Index</relatedStateVariable>
            </argument>
            <argument>
                <name>RequestedCount</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable>
            </argument>
            <argument>
                <name>SortCriteria</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_SortCriteria</relatedStateVariable>
            </argument>

            <argument>
                <name>Result</name>
                <direction>out</direction>
                <relatedStateVariable>A_ARG_TYPE_Result</relatedStateVariable>
            </argument>
            <argument>
                <name>NumberReturned</name>
                <direction>out</direction>
                <relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable>
            </argument>
            <argument>
                <name>TotalMatches</name>
                <direction>out</direction>
                <relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable>
            </argument>
            <argument>
                <name>UpdateID</name>
                <direction>out</direction>
                <relatedStateVariable>A_ARG_TYPE_UpdateID</relatedStateVariable>
            </argument>
        </argumentList>
    </action>
#endif
}

void ContentDirectoryService::processSearch(Oct::UpnpDev::ActionRequest& actionRequest)
{
    int standardErrorCode = Oct::UpnpDev::ActionRequest::ERROR_CODE__INVALID_ARGS;

    do {
        QVariant argContainerID    = actionRequest.argumentValue("ContainerID");
        QVariant argSearchCriteria = actionRequest.argumentValue("SearchCriteria");
        QVariant argFilter         = actionRequest.argumentValue("Filter");
        QVariant argStartingIndex  = actionRequest.argumentValue("StartingIndex");
        QVariant argRequestedCount = actionRequest.argumentValue("RequestedCount");
        QVariant argSortCriteria   = actionRequest.argumentValue("SortCriteria");

        bool okStartingIndex;
        bool okRequestedCount;

        QString containerID    = argContainerID.toString();
        QString searchCriteria = argSearchCriteria.toString();
        QString filter         = argFilter.toString();
        uint startingIndex     = argStartingIndex.toUInt(&okStartingIndex);
        uint requestedCount    = argRequestedCount.toUInt(&okRequestedCount);
        QString sortCriteria   = argSortCriteria.toString();

        if (containerID.length() == 0 ) {
            Logger.info(this) << "Browse: failed - invalid container ID: " << containerID;
            break;
        }
        if (searchCriteria.isNull()) {
            Logger.info(this) << "Browse: failed - invalid search criteria: " << searchCriteria;
            break;
        }
        if (filter.isNull()) {
            Logger.info(this) << "Browse: failed - invalid filter: " << filter;
            break;
        }
        if (!okStartingIndex) {
            Logger.info(this) << "Browse: failed - invalid starting index: " << argStartingIndex.toString();
            break;
        }
        if (!okRequestedCount) {
            Logger.info(this) << "Browse: failed - invalid requested count: " << argRequestedCount.toString();
            break;
        }
        if (sortCriteria.isNull()) {
            Logger.info(this) << "Browse: failed - invalid sort criteria: " << sortCriteria;
            break;
        }

        standardErrorCode = 0;

        // TODO

        actionRequest.setOutputArgumentValue("Result", "");
        actionRequest.setOutputArgumentValue("NumberReturned", 0);
        actionRequest.setOutputArgumentValue("TotalMatches", 0);
        actionRequest.setOutputArgumentValue("UpdateID", 0);
    } while (0);

    if (standardErrorCode != 0) {
        actionRequest.setStandardError(standardErrorCode);
    }

#if 0
    <action>
        <Optional/>
        <name>Search</name>
        <argumentList>
            <argument>
                <name>ContainerID</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_ObjectID</relatedStateVariable>
            </argument>
            <argument>
                <name>SearchCriteria</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_SearchCriteria</relatedStateVariable>
            </argument>
            <argument>
                <name>Filter</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_Filter</relatedStateVariable>
            </argument>
            <argument>
                <name>StartingIndex</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_Index</relatedStateVariable>
            </argument>
            <argument>
                <name>RequestedCount</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable>
            </argument>
            <argument>
                <name>SortCriteria</name>
                <direction>in</direction>
                <relatedStateVariable>A_ARG_TYPE_SortCriteria</relatedStateVariable>
            </argument>

            <argument>
                <name>Result</name>
                <direction>out</direction>
                <relatedStateVariable>A_ARG_TYPE_Result</relatedStateVariable>
            </argument>
            <argument>
                <name>NumberReturned</name>
                <direction>out</direction>
                <relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable>
            </argument>
            <argument>
                <name>TotalMatches</name>
                <direction>out</direction>
                <relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable>
            </argument>
            <argument>
                <name>UpdateID</name>
                <direction>out</direction>
                <relatedStateVariable>A_ARG_TYPE_UpdateID</relatedStateVariable>
            </argument>
        </argumentList>
    </action>
#endif
}

bool ContentDirectoryService::processActionRequest(Oct::UpnpDev::ActionRequest& actionRequest)
{
    QString actionName = actionRequest.actionName();

    if (actionName == "GetSearchCapabilities") {
        processGetSearchCapabilities(actionRequest);
    } else if (actionName == "GetSortCapabilities") {
        processGetSortCapabilities(actionRequest);
    } else if (actionName == "GetFeatureList") {
        processGetFeatureList(actionRequest);
    } else if (actionName == "GetSystemUpdateID") {
        processGetSystemUpdateID(actionRequest);
    } else if (actionName == "GetServiceResetToken") {
        processGetServiceResetToken(actionRequest);
    } else if (actionName == "Browse") {
        processBrowse(actionRequest);
    } else if (actionName == "Search") {
        processSearch(actionRequest);
    }

    return true;
}
