/*
 * ContentItem.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONTENT_ITEM_H_
#define _MEDIA_SERVER__CONTENT_ITEM_H_

/*-----------------------------------------------------------------------------*/

#include "ContentObject.h"

/*-----------------------------------------------------------------------------*/

class ContentItem : public ContentObject
{
public:
    static const QString UPNP_CLASS_ITEM;
    static const QString UPNP_CLASS_ITEM_AUDIO;
    static const QString UPNP_CLASS_ITEM_VIDEO;
    static const QString UPNP_CLASS_ITEM_IMAGE;

public:
    ContentItem(const QString& objectID, const QString& upnpClass, const QString& title);
    virtual ~ContentItem();
};

/*-----------------------------------------------------------------------------*/

#endif /* _MEDIA_SERVER__CONTENT_ITEM_H_ */
