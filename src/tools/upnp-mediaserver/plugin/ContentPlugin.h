/*
 * ContentPlugin.h
 *
 *  Created on: 31-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONTENT_PLUGIN_H_
#define _MEDIA_SERVER__CONTENT_PLUGIN_H_

/*---------------------------------------------------------------------------*/

#include "ContentPluginType.h"
#include "ContentPluginSettings.h"
#include "ContentContainer.h"

#include <QtCore/QObject>

/*---------------------------------------------------------------------------*/

/**
 * Content plugin base class.
 *
 * Content plugins are logical elements providing ContentObjects for ContentDatabase.
 */
class ContentPlugin : public QObject
{
    Q_OBJECT

private:
    /**
     * Plugin type.
     */
    ContentPluginType m_type;

    /**
     * Unique plugin identifier.
     */
    QString m_uniqueID;

    /**
     * Plugin settings.
     */
    ContentPluginSettingsPointer m_settings;

protected:
    /**
     * Constructs new plugin.
     *
     * @param[in] type
     *      Plugin type.
     * @param[in] settings
     *      Initial plugin settings.
     *      The object is cloned and the original IS NOT deleted.
     * @param[in] parent
     *      Parent of the object.
     */
    ContentPlugin(ContentPluginType type, const ContentPluginSettings* settings, QObject* parent = 0);

public:
    /**
     * Destroys plugin.
     */
    virtual ~ContentPlugin();

    /**
     * Returns unique plugin ID.
     *
     * @return
     * Plugin ID.
     */
    QString uniqueID() const;

    /**
     * Returns plugin type.
     *
     * @return
     * Type of the plugin.
     */
    ContentPluginType type() const
    {
        return m_type;
    }

protected:
    /**
     * Returns plugin root container.
     *
     * @return
     * Root container of plugin.
     */
    virtual ContentContainer* rootContainer() = 0;

    /**
     * Updates plugin contents.
     *
     * Scans for content and (re)builds plugin objects hierarchy.
     *
     * \note In current design this operation is 'blocking' and may take a long time.
     *       In that case plugin should show progress dialog that will allow user
     *       to cancel the operation.
     *
     * @return
     * True on success, false otherwise.
     */
    virtual bool updateContents() = 0;

    /**
     * Sets plugin settings.
     *
     * \note This method does not change plugin contents.
     *       The updateContents() method shall be called separately.
     *
     * @param newSettings
     *      New plugin settings.
     *      The object is cloned and the original IS NOT deleted.
     *
     * @return
     * True on success, false on error.
     */
    virtual bool setSettings(const ContentPluginSettings* newSettings);

    /**
     * Returns current settings.
     *
     * @return
     * Copy of current settings object. Must be released by the caller.
     */
    virtual ContentPluginSettings* settings() const;

    /**
     * Returns current settings.
     *
     * @return
     * Current settings object.
     */
    const ContentPluginSettings* internalSettings() const;


    // to allow access to protected elements
    friend class ContentDatabase;
};

/*---------------------------------------------------------------------------*/

#endif /* _MEDIA_SERVER__CONTENT_PLUGIN_H_ */
