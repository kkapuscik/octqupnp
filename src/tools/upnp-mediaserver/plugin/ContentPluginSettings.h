/*
 * ContentPluginSettings.h
 *
 *  Created on: 06-04-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONTENT_PLUGIN_SETTINGS_H_
#define _MEDIA_SERVER__CONTENT_PLUGIN_SETTINGS_H_

/*---------------------------------------------------------------------------*/

#include "ContentPluginType.h"

#include <QtCore/QVariant>
#include <QtCore/QSharedPointer>

/*---------------------------------------------------------------------------*/

/**
 * Base class for content plugin settings.
 *
 * \todo Convert to some implicitly shared class or something similar.
 */
class ContentPluginSettings
{
public:
    enum Role {
        ROLE_NAME = Qt::UserRole,
        ROLE_PATH,
        ROLE_REFRESH_RATE,
    };

private:
    /**
     * Plugin type.
     */
    ContentPluginType m_type;

protected:
    ContentPluginSettings(ContentPluginType type);

public:
    virtual ~ContentPluginSettings();

    ContentPluginType type() const
    {
        return m_type;
    }

    virtual QVariant data(Role role) const = 0;

    virtual ContentPluginSettings* clone() const = 0;
};

/*---------------------------------------------------------------------------*/

/**
 * Pointer to content plugin settings.
 */
typedef QSharedPointer<ContentPluginSettings> ContentPluginSettingsPointer;

/*---------------------------------------------------------------------------*/

#endif /*_MEDIA_SERVER__CONTENT_PLUGIN_SETTINGS_H_*/
