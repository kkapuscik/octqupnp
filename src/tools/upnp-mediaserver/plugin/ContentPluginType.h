/*
 * ContentPluginSettings.h
 *
 *  Created on: 06-04-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONTENT_PLUGIN_TYPE_H_
#define _MEDIA_SERVER__CONTENT_PLUGIN_TYPE_H_

/*---------------------------------------------------------------------------*/

enum ContentPluginType {
    PLUGIN_TYPE_FILE_SYSTEM,
    PLUGIN_TYPE_XKCD
};

/*---------------------------------------------------------------------------*/

#endif /*_MEDIA_SERVER__CONTENT_PLUGIN_TYPE_H_*/
