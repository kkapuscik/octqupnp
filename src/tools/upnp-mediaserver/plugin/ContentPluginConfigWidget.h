/*
 * ContentPluginConfigWidget.h
 *
 *  Created on: 08-04-2011
 *      Author: saveman
 */

#ifndef CONTENTPLUGINCONFIGWIDGET_H_
#define CONTENTPLUGINCONFIGWIDGET_H_

/*-----------------------------------------------------------------------------*/

#include <QtWidgets/QWidget>

#include "ContentPluginSettings.h"

/*-----------------------------------------------------------------------------*/

class ContentPluginConfigWidget : public QWidget
{
Q_OBJECT

public:
    ContentPluginConfigWidget(QWidget* parent = 0);

    virtual bool validateSettings() = 0;
    virtual void setSettings(const ContentPluginSettings* settings) = 0;
    virtual ContentPluginSettings* settings() = 0;
};

/*-----------------------------------------------------------------------------*/

#endif /* CONTENTPLUGINCONFIGWIDGET_H_ */
