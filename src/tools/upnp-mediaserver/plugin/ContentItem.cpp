/*
 * ContentItem.cpp
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#include "ContentItem.h"

const QString ContentItem::UPNP_CLASS_ITEM("object.item");
const QString ContentItem::UPNP_CLASS_ITEM_AUDIO("object.item.audioItem");
const QString ContentItem::UPNP_CLASS_ITEM_VIDEO("object.item.videoItem");
const QString ContentItem::UPNP_CLASS_ITEM_IMAGE("object.item.imageItem");

ContentItem::ContentItem(const QString& objectID, const QString& upnpClass, const QString& title) :
    ContentObject(objectID, upnpClass, title)
{
    // TODO Auto-generated constructor stub
}

ContentItem::~ContentItem()
{
    // TODO Auto-generated destructor stub
}
