/*
 * ContentContainer.cpp
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#include "ContentContainer.h"

const QString ContentContainer::UPNP_CLASS_CONTAINER("object.container");

ContentContainer::ContentContainer(const QString& objectID, const QString& upnpClass, const QString& title) :
    ContentObject(objectID, upnpClass, title)
{
    // TODO Auto-generated constructor stub

}

ContentContainer::~ContentContainer()
{
    /* delete children */
    while (!m_children.isEmpty()) {
        ContentObject* object = m_children.first();

        /* remove object from parent */
        /* this is dangerous part - the setParent modifies m_children collection */
        object->setParent(NULL);

        /* delete child object */
        delete object;
    }
}

void ContentContainer::addChild(ContentObject* child)
{
    Q_ASSERT(child != NULL);
    Q_ASSERT(child != this);
    Q_ASSERT(child->parent() == this);

    Q_ASSERT(! m_children.contains(child));

    m_children.append(child);

    // TODO: events
}

void ContentContainer::removeChild(ContentObject* child)
{
    Q_ASSERT(child != NULL);
    Q_ASSERT(child != this);
    Q_ASSERT(child->parent() == this);

    Q_ASSERT(m_children.contains(child));

    m_children.removeAll(child);

    // TODO: events
}
