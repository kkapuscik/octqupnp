/*
 * ContentPluginFactoryManager.cpp
 *
 *  Created on: 25-04-2011
 *      Author: saveman
 */

#include "ContentPluginFactoryManager.h"

ContentPluginFactoryManager* ContentPluginFactoryManager::sm_theInstance = NULL;

ContentPluginFactoryManager* ContentPluginFactoryManager::instance()
{
    if (sm_theInstance == NULL) {
        sm_theInstance = new ContentPluginFactoryManager();
    }

    return sm_theInstance;
}

ContentPluginFactoryManager::ContentPluginFactoryManager()
{
    // nothing to do
}

ContentPluginFactoryManager::~ContentPluginFactoryManager()
{
    // nothing to do
}

void ContentPluginFactoryManager::registerFactory(const ContentPluginFactory* factory)
{
    Q_ASSERT(factory != NULL);
    Q_ASSERT(! m_factories.contains(factory));

    for (int i = 0; i < m_factories.size(); ++i) {
        Q_ASSERT(m_factories[i]->type() != factory->type());
    }

    // TODO: check for duplicated IDs

    m_factories.append(factory);
}

QList<const ContentPluginFactory*> ContentPluginFactoryManager::factories() const
{
    return m_factories;
}
