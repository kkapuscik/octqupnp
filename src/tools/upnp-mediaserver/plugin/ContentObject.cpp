/*
 * ContentObject.cpp
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#include "ContentObject.h"

#include "ContentContainer.h"
#include "ContentItem.h"

#include <upnpdev/Types.h>

/*---------------------------------------------------------------------------*/

const QString ContentObject::ROOT_PARENT_ID("-1");
const QList<const ContentResource*> ContentObject::EMPTY_RESOURCES;

/*---------------------------------------------------------------------------*/

ContentObject::ContentObject(const QString& objectID, const QString& upnpClass, const QString& title) :
    m_objectID(objectID), m_upnpClass(upnpClass), m_title(title.trimmed()), m_parent(NULL), m_resources(EMPTY_RESOURCES)
{
    Q_ASSERT(!objectID.isEmpty());
    Q_ASSERT(upnpClass.startsWith("object."));
    Q_ASSERT(!title.isEmpty());
}

ContentObject::~ContentObject()
{
    Q_ASSERT(m_parent == NULL);

    for (int i = 0; i < m_resources.size(); ++i) {
        delete m_resources[i];
    }
}

bool ContentObject::isItem() const
{
    return isDerivedFrom(ContentItem::UPNP_CLASS_ITEM);
}

bool ContentObject::isContainer() const
{
    return isDerivedFrom(ContentContainer::UPNP_CLASS_CONTAINER);
}

bool ContentObject::isDerivedFrom(const QString& upnpClass) const
{
    if (m_upnpClass == upnpClass) {
        return true;
    } else if (m_upnpClass.startsWith(upnpClass + '.')) {
        return true;
    }
    return false;
}

void ContentObject::setParent(ContentContainer* newParent)
{
    if (newParent == m_parent) {
        return;
    }

    if (m_parent != NULL) {
        m_parent->removeChild(this);
    }

    m_parent = newParent;

    if (m_parent != NULL) {
        m_parent->addChild(this);
    }
}

QString ContentObject::parentID() const
{
    if (m_parent != NULL) {
        return m_parent->objectID();
    } else {
        return ROOT_PARENT_ID;
    }
}

/*---------------------------------------------------------------------------*/

QString ContentObject::generateObjectID()
{
    return Oct::UpnpDev::UUID::generate().toString(false);
}
