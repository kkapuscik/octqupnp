/*
 * ContentPluginFactory.cpp
 *
 *  Created on: 25-04-2011
 *      Author: saveman
 */

#include "ContentPluginFactory.h"

#include "ContentPluginFactoryManager.h"

/*-----------------------------------------------------------------------------*/

ContentPluginFactory::ContentPluginFactory(ContentPluginType type) :
    m_type(type)
{
    // nothing to do
}

ContentPluginFactory::~ContentPluginFactory()
{
    Q_ASSERT(0);
}
