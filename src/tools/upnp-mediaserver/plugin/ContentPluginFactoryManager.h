/*
 * ContentPluginFactoryManager.h
 *
 *  Created on: 25-04-2011
 *      Author: saveman
 */

#ifndef CONTENTPLUGINFACTORYMANAGER_H_
#define CONTENTPLUGINFACTORYMANAGER_H_

/*-----------------------------------------------------------------------------*/

#include "ContentPluginFactory.h"

#include <QtCore/QList>

/*-----------------------------------------------------------------------------*/

class ContentPluginFactoryManager
{
private:
    static ContentPluginFactoryManager* sm_theInstance;

public:
    static ContentPluginFactoryManager* instance();

private:
    QList<const ContentPluginFactory*> m_factories;

private:
    ContentPluginFactoryManager();
    ~ContentPluginFactoryManager();

public:
    void registerFactory(const ContentPluginFactory* factory);

public:
    QList<const ContentPluginFactory*> factories() const;

};

/*-----------------------------------------------------------------------------*/

#endif /* CONTENTPLUGINFACTORYMANAGER_H_ */
