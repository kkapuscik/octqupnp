/*
 * ContentPlugin.cpp
 *
 *  Created on: 31-03-2011
 *      Author: saveman
 */

#include "ContentPlugin.h"

#include <upnpdev/Types.h>

/*---------------------------------------------------------------------------*/

ContentPlugin::ContentPlugin(ContentPluginType type, const ContentPluginSettings* settings, QObject* parent) :
    QObject(parent), m_type(type)
{
    Q_ASSERT(settings != NULL);
    Q_ASSERT(settings->type() == type);

    /* prepare unique ID */
    m_uniqueID = Oct::UpnpDev::UUID::generate().toString(false);

    /* store settings */
    m_settings = ContentPluginSettingsPointer(settings->clone());
}

ContentPlugin::~ContentPlugin()
{
    // nothing to do
}

QString ContentPlugin::uniqueID() const
{
    return m_uniqueID;
}

bool ContentPlugin::setSettings(const ContentPluginSettings* newSettings)
{
    bool rv = false;

    if (newSettings != NULL) {
        if (newSettings->type() == type()) {
            m_settings = ContentPluginSettingsPointer(newSettings->clone());
            rv = true;
        }
    } else {
        Q_ASSERT(0);
    }

    return rv;
}

ContentPluginSettings* ContentPlugin::settings() const
{
    return m_settings->clone();
}

const ContentPluginSettings* ContentPlugin::internalSettings() const
{
    return m_settings.data();
}
