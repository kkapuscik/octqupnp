/*
 * ContentContainer.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#ifndef CONTENTCONTAINER_H_
#define CONTENTCONTAINER_H_

/*-----------------------------------------------------------------------------*/

#include "ContentObject.h"

#include <QtCore/QSet>

/*-----------------------------------------------------------------------------*/

/**
 * Base class for all content container objects.
 */
class ContentContainer : public ContentObject
{
public:
    /**
     * Base container UPnP class.
     */
    static const QString UPNP_CLASS_CONTAINER;

private:
    /** Collection of child objects. */
    typedef QList<ContentObject*> ChildCollection;

private:
    /** Children of this container. */
    ChildCollection m_children;

public:
    /**
     * Constructs new container.
     *
     * @param objectID
     *      Unique object identifier.
     * @param upnpClass
     *      UPnP class of container.
     * @param title
     *      Container title.
     */
    ContentContainer(const QString& objectID, const QString& upnpClass, const QString& title);

    /**
     * Destroys the container.
     */
    virtual ~ContentContainer();

    /**
     * Returns number of child elements in container.
     *
     * @return
     * Number of child elements.
     */
    uint childCount() const
    {
        return m_children.size();
    }

    /**
     * Returns child element of container.
     *
     * @param i
     *      Index of child.
     *      Shall be in range 0..childCount()-1.
     *
     * @return
     * Child object.
     */
    ContentObject* child(uint i)
    {
        Q_ASSERT(i >= 0 && i < m_children.size());

        return m_children.at(i);
    }

private:
    /**
     * Adds child to container.
     *
     * @param child
     *      Child to be added.
     */
    void addChild(ContentObject* child);

    /**
     * Removes child from container.
     *
     * @param child
     *      Child to be removed.
     */
    void removeChild(ContentObject* child);

    // to allow access to addChild/removeChild
    friend class ContentObject;
};

/*-----------------------------------------------------------------------------*/

#endif /* CONTENTCONTAINER_H_ */
