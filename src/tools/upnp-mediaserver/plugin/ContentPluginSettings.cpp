/*
 * ContentPluginSettings.cpp
 *
 *  Created on: 06-04-2011
 *      Author: saveman
 */

#include "ContentPluginSettings.h"

ContentPluginSettings::ContentPluginSettings(ContentPluginType type) :
    m_type(type)
{
    // nothing to do
}

ContentPluginSettings::~ContentPluginSettings()
{
    // nothing to do
}
