/*
 * ContentPluginFactory.h
 *
 *  Created on: 25-04-2011
 *      Author: saveman
 */

#ifndef CONTENTPLUGINFACTORY_H_
#define CONTENTPLUGINFACTORY_H_

/*-----------------------------------------------------------------------------*/

#include "ContentPlugin.h"
#include "ContentPluginSettings.h"
#include "ContentPluginConfigWidget.h"

/*-----------------------------------------------------------------------------*/

/**
 * Base class of ContentPlugin factories.
 *
 * Each factory can produce ContentPlugins of specific type.
 */
class ContentPluginFactory
{
private:
    /**
     * Type of plugin this factory can produce.
     */
    ContentPluginType m_type;

protected:
    /**
     * Constructs factory.
     *
     * @param type
     *      Type of plugin this factory can produce.
     */
    ContentPluginFactory(ContentPluginType type);

    /**
     * Destroys the factory.
     *
     * Factory shall never be destroyed.
     */
    virtual ~ContentPluginFactory();

public:
    /**
     * Returns type of plugin this factory can produce.
     *
     * @return
     * Plugin type identifier.
     */
    ContentPluginType type() const
    {
        return m_type;
    }

    /**
     * Returns plugin type name.
     *
     * @return
     * Plugin type name.
     */
    virtual QString name() const = 0;

    /**
     * Constructs plugin.
     *
     * @param settings
     *      Plugin settings.
     * @param owner
     *
     *
     * @return
     */
    virtual ContentPlugin* createPlugin(const ContentPluginSettings* settings, QObject* owner) const = 0;

    virtual ContentPluginConfigWidget* createConfigWidget(QWidget* parent = 0) const = 0;
};

/*-----------------------------------------------------------------------------*/

#endif /* CONTENTPLUGINFACTORY_H_ */
