/*
 * ContentResource.h
 *
 *  Created on: 21-04-2011
 *      Author: saveman
 */

#ifndef CONTENTRESOURCE_H_
#define CONTENTRESOURCE_H_

/*-----------------------------------------------------------------------------*/

#include <httpserver/VfsFile.h>

#include <QtCore/QString>

/*-----------------------------------------------------------------------------*/

class ContentResource
{
protected:
    ContentResource();

public:
    virtual ~ContentResource();

    virtual QString mimeType() const = 0;

    /**
     * Returns external URI to resource content.
     *
     * @return
     * URI to 'external' resource content or
     * null string for 'internal' content.
     */
    virtual QString externalURI() const
    {
        return QString::null;
    }

    /**
     * Opens VFS file representing 'internal' resource content.
     *
     * @return
     * VFS to 'internal' resource content
     * or NULL for 'external' resources.
     */
    virtual Oct::HttpServer::VfsFile* openFile() const = 0;
};

/*-----------------------------------------------------------------------------*/

#endif /* CONTENTRESOURCE_H_ */
