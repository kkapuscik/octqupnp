/*
 * ContentObject.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__CONTENT_OBJECT_H_
#define _MEDIA_SERVER__CONTENT_OBJECT_H_

/*---------------------------------------------------------------------------*/

#include "ContentResource.h"

#include <QtCore/QString>
#include <QtCore/QList>

/*---------------------------------------------------------------------------*/

// forward
class ContentContainer;

/**
 * Base class of all content objects.
 *
 * Represents object.* elements of ContentDirectory shares structure.
 */
class ContentObject
{
private:
    static const QString ROOT_PARENT_ID;
    static const QList<const ContentResource*> EMPTY_RESOURCES;

private:
    QString m_objectID;
    QString m_upnpClass;
    QString m_title;
    ContentContainer* m_parent;
    QList<const ContentResource*> m_resources;

private:
    Q_DISABLE_COPY(ContentObject)

public:
    ContentObject(const QString& objectID, const QString& upnpClass, const QString& title);
    virtual ~ContentObject();

    bool isItem() const;
    bool isContainer() const;
    bool isDerivedFrom(const QString& upnpClass) const;

    const QString& objectID() const
    {
        return m_objectID;
    }

    const QString& upnpClass() const
    {
        return m_upnpClass;
    }

    const QString& title() const
    {
        return m_title;
    }

    virtual void setParent(ContentContainer* newParent);

    ContentContainer* parent() const
    {
        return m_parent;
    }

    QString parentID() const;

    virtual QList<const ContentResource*> resources() const
    {
        return m_resources;
    }

    void addResource(ContentResource* resource)
    {
        Q_ASSERT(resource != NULL);

        m_resources.append(resource);
    }

public:
    static QString generateObjectID();

};

/*---------------------------------------------------------------------------*/

#endif /* _MEDIA_SERVER__CONTENT_OBJECT_H_ */
