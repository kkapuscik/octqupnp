/*
 * FileSystemContentPlugin.h
 *
 *  Created on: 31-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__FILE_SYSTEM_CONTENT_PLUGIN_H_
#define _MEDIA_SERVER__FILE_SYSTEM_CONTENT_PLUGIN_H_Work

/*---------------------------------------------------------------------------*/

#include "FileSystemContentPluginSettings.h"
#include "FileSystemContentFeeder.h"

#include <plugin/ContentPlugin.h>
#include <plugin/ContentItem.h>

#include <log/Logger.h>
#include <util/EventThread.h>

/*---------------------------------------------------------------------------*/

class FileSystemContentPlugin : public ContentPlugin
{
    Q_OBJECT

private:
    static Oct::Log::Logger<FileSystemContentPlugin> Logger;

    /**
     * Plugin root container.
     *
     * NULL if plugin was not initialized.
     */
    ContentContainer* m_pluginContainer;
    FileSystemContentFeeder* m_feeder;
    Oct::Util::EventThread* m_feederThread;

public:
    FileSystemContentPlugin(const FileSystemContentPluginSettings* settings, QObject* parent = 0);
    virtual ~FileSystemContentPlugin();

    // ContentPlugin#rootContainer()
    virtual ContentContainer* rootContainer();

protected:
    // ContentPlugin#updateContents()
    virtual bool updateContents();

private slots:
    void contentUpdateFinished();

};

/*---------------------------------------------------------------------------*/

#endif /* _MEDIA_SERVER__FILE_SYSTEM_CONTENT_PLUGIN_H_ */
