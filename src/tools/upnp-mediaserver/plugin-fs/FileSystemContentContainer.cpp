/*
 * FileSystemContentContainer.cpp
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#include "FileSystemContentContainer.h"

FileSystemContentContainer::FileSystemContentContainer(const QString& path, const QString& objectID,
        const QString& upnpClass, const QString& title) :
    ContentContainer(objectID, upnpClass, title), m_path(path)
{
    // TODO ?
}

FileSystemContentContainer::~FileSystemContentContainer()
{
    // TODO Auto-generated destructor stub
}
