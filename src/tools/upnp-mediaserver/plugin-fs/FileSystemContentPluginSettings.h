/*
 * FileSystemContentPluginSettings.h
 *
 *  Created on: 07-04-2011
 *      Author: saveman
 */

#ifndef FILESYSTEMCONTENTPLUGINSETTINGS_H_
#define FILESYSTEMCONTENTPLUGINSETTINGS_H_

#include <plugin/ContentPluginSettings.h>

class FileSystemContentPluginSettings : public ContentPluginSettings
{
private:
    QString m_name;
    QString m_path;
    int m_refreshRate;

public:
    FileSystemContentPluginSettings(const QString& name, const QString& path, int refreshRate);
    virtual ~FileSystemContentPluginSettings();

    QString name() const
    {
        return m_name;
    }

    QString path() const
    {
        return m_path;
    }

    int refreshRate() const
    {
        return m_refreshRate;
    }

    virtual QVariant data(Role role) const;

    virtual ContentPluginSettings* clone() const;
};

#endif /* FILESYSTEMCONTENTPLUGINSETTINGS_H_ */
