#include "FileSystemContentPluginConfigWidget.h"

#include "FileSystemContentPluginSettings.h"

#include <QtCore/QDir>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>

FileSystemContentPluginConfigWidget::FileSystemContentPluginConfigWidget(QWidget *parent) :
    ContentPluginConfigWidget(parent)
{
    m_ui.setupUi(this);
}

bool FileSystemContentPluginConfigWidget::validateSettings()
{
    QString name = m_ui.m_nameEdit->text();
    if (name.length() == 0) {
        QMessageBox::warning(this, tr("Validate settings"), tr("Share name must be specified."));
        return false;
    }

    QString path = m_ui.m_pathEdit->text();
    if (path.length() == 0) {
        QMessageBox::warning(this, tr("Validate settings"), tr("Path must be specified."));
        return false;
    }

    QDir pathDir(path);
    if (!pathDir.exists()) {
        QMessageBox::information(this, tr("Validate settings"), tr("Path does not exists. No content will be shared."));
    }

    int refreshRate = m_ui.m_refreshSpinBox->value();
    if (refreshRate < 0) {
        QMessageBox::warning(this, tr("Validate settings"), tr("Refresh rate cannot be negative."));
        return false;
    }

    return true;
}

void FileSystemContentPluginConfigWidget::setSettings(const ContentPluginSettings* settings)
{
    Q_ASSERT(settings->type() == PLUGIN_TYPE_FILE_SYSTEM);

    const FileSystemContentPluginSettings* fsSettings = dynamic_cast<const FileSystemContentPluginSettings*>(settings);

    m_ui.m_nameEdit->setText(fsSettings->name());
    m_ui.m_pathEdit->setText(fsSettings->path());
    m_ui.m_refreshSpinBox->setValue(fsSettings->refreshRate());
}

ContentPluginSettings* FileSystemContentPluginConfigWidget::settings()
{
    if (validateSettings()) {
        return new FileSystemContentPluginSettings(
                m_ui.m_nameEdit->text().trimmed(),
                m_ui.m_pathEdit->text(),
                m_ui.m_refreshSpinBox->value());
    } else {
        return NULL;
    }
}

void FileSystemContentPluginConfigWidget::browseForPath()
{
    /* ask for new path */
    QString path = QFileDialog::getExistingDirectory(this, tr("Select directory"));
    if (!path.isNull()) {
        /* store path */
        m_ui.m_pathEdit->setText(path);

        /* set name if needed */
        QString name(m_ui.m_nameEdit->text().trimmed());
        if (name.isEmpty()) {
            QDir pathDir(path);

            m_ui.m_nameEdit->setText(pathDir.dirName());
        }
    }
}
