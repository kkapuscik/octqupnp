/*
 * FileSystemContentPlugin.cpp
 *
 *  Created on: 31-03-2011
 *      Author: saveman
 */

#include "FileSystemContentPlugin.h"
#include "FileSystemContentContainer.h"
#include "FileSystemContentItem.h"

#include <QtCore/QDir>

/*---------------------------------------------------------------------------*/

Oct::Log::Logger<FileSystemContentPlugin> FileSystemContentPlugin::Logger("/oct/tools/mediaserver/FileSystemContentPlugin");

/*---------------------------------------------------------------------------*/

FileSystemContentPlugin::FileSystemContentPlugin(const FileSystemContentPluginSettings* settings, QObject* parent) :
    ContentPlugin(PLUGIN_TYPE_FILE_SYSTEM, settings, parent), m_pluginContainer(NULL)
{
    // nothing to do
}

FileSystemContentPlugin::~FileSystemContentPlugin()
{
    if (m_pluginContainer != NULL) {
        delete m_pluginContainer;
        m_pluginContainer = NULL;
    }
}

bool FileSystemContentPlugin::updateContents()
{
    bool rv = false;

    Logger.trace(this) << "updateContents()";

    /* delete old contents */
    if (m_pluginContainer != NULL) {
        delete m_pluginContainer;
        m_pluginContainer = NULL;
    }

    /* remove old feeder */
    // TODO

    /* prepare new contents */
    {
        FileSystemContentPluginSettings* settings = (FileSystemContentPluginSettings*)internalSettings();

        m_feeder = new FileSystemContentFeeder();
        m_feederThread = new Oct::Util::EventThread(m_feeder, this);

        connect(m_feeder, SIGNAL( finished() ), this, SLOT( contentUpdateFinished() ));

#if 0
        m_pluginContainer = processFolder(settings->path(), settings->name());
        if (m_pluginContainer != NULL) {
            rv = true;
        }
#endif
    }

    Logger.trace(this) << "updateContents() => " << rv;

    return rv;
}

ContentContainer* FileSystemContentPlugin::rootContainer()
{
    return m_pluginContainer;
}

void FileSystemContentPlugin::contentUpdateFinished()
{
    Logger.trace(this) << "contentUpdateFinished()";

    // TODO
}
