/*
 * FileSystemContentItem
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#include "FileSystemContentItem.h"

FileSystemContentItem::FileSystemContentItem(const QString& path, const QString& mimeType, const QString& objectID,
        const QString& upnpClass, const QString& title) :
    ContentItem(objectID, upnpClass, title)
{
    addResource(new FileSystemContentResource(path, mimeType));
}
