/*
 * FileSystemContentContainer.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#include "FileSystemContentFeeder.h"

#include "FileSystemContentContainer.h"
#include "FileSystemContentItem.h"

#include <QtCore/QDir>

/*---------------------------------------------------------------------------*/

Oct::Log::Logger<FileSystemContentFeeder> FileSystemContentFeeder::Logger("/oct/tools/mediaserver/FileSystemContentFeeder");

/*---------------------------------------------------------------------------*/

FileSystemContentFeeder::FileSystemContentFeeder(QObject* parent) :
    Oct::Util::EventThreadExecutor(parent)
{
    m_fileMagic = new Oct::FileMagic::FileMagic(this);
    Q_ASSERT(m_fileMagic->isValid());
}

bool FileSystemContentFeeder::startExecution()
{
    // TODO
    return true;
}

void FileSystemContentFeeder::finishExecution()
{
    // TODO
}

void FileSystemContentFeeder::continueExecution()
{
    // TODO
}

ContentContainer* FileSystemContentFeeder::processFolder(const QString& dirPath, const QString& title)
{
    QDir dir(dirPath);
    if (!dir.exists()) {
        return NULL;
    }

    QString dirName;

    if (title.isNull()) {
        dirName = dir.dirName().trimmed();
        if (dirName.isEmpty()) {
            dirName = tr("Unknown Folder");
        }
    } else {
        dirName = title;
    }

    FileSystemContentContainer* newContainer = new FileSystemContentContainer(dirPath,
            ContentObject::generateObjectID(), ContentContainer::UPNP_CLASS_CONTAINER, dirName);

    Logger.trace(this) << "processFolder(path=" << dirPath << " title=" << title << ") added container="
            << newContainer;

    QFileInfoList entries = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);

    for (int i = 0; i < entries.size(); ++i) {
        Logger.trace(this) << "processFolder(path=" << dirPath << " title=" << title << ") processing path="
                << entries[i].absolutePath();

        if (entries[i].isDir()) {
            ContentContainer* childContainer = processFolder(entries[i].filePath());
            if (childContainer != NULL) {
                childContainer->setParent(newContainer);
            }
        } else if (entries[i].isFile()) {
            ContentItem* childItem = processFile(entries[i].filePath());
            if (childItem != NULL) {
                childItem->setParent(newContainer);
            }
        }
    }

    return newContainer;
}

ContentItem* FileSystemContentFeeder::processFile(const QString& filePath)
{
    QFileInfo info(filePath);

    if (!info.exists()) {
        return NULL;
    }

    // TODO
    QString mimeType = m_fileMagic->processFile(info.absoluteFilePath(), NULL, NULL);
    if (mimeType.isEmpty()) {
        mimeType = "application/octet-stream";
    }

    QString upnpClass;
    if (mimeType.startsWith("image/", Qt::CaseInsensitive)) {
        upnpClass = ContentItem::UPNP_CLASS_ITEM_IMAGE;
    } else if (mimeType.startsWith("audio/", Qt::CaseInsensitive)) {
        upnpClass = ContentItem::UPNP_CLASS_ITEM_AUDIO;
    } else if (mimeType.startsWith("video/", Qt::CaseInsensitive)) {
        upnpClass = ContentItem::UPNP_CLASS_ITEM_VIDEO;
    } else {
        upnpClass = ContentItem::UPNP_CLASS_ITEM;
    }

    FileSystemContentItem* newItem = new FileSystemContentItem(filePath, mimeType, ContentObject::generateObjectID(),
            upnpClass, info.fileName());

    Logger.trace(this) << "processFile(path=" << filePath << ") - added item=" << newItem << " class=" << upnpClass
            << " mime=" << mimeType;

    return newItem;
}

/*---------------------------------------------------------------------------*/
