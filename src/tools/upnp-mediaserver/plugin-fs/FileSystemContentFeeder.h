/*
 * FileSystemContentContainer.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__FILE_SYSTEM_CONTENT_FEEDER_H_
#define _MEDIA_SERVER__FILE_SYSTEM_CONTENT_FEEDER_H_

/*---------------------------------------------------------------------------*/

#include <util/EventThreadExecutor.h>
#include <log/Logger.h>
#include <filemagic/FileMagic.h>

#include <plugin/ContentContainer.h>
#include <plugin/ContentItem.h>

/*---------------------------------------------------------------------------*/

class FileSystemContentFeeder : public Oct::Util::EventThreadExecutor
{
    Q_OBJECT

private:
    static Oct::Log::Logger<FileSystemContentFeeder> Logger;

private:
    // TODO: make it static / shared
    Oct::FileMagic::FileMagic* m_fileMagic;

public:
    FileSystemContentFeeder(QObject* parent = 0);

protected:
    virtual bool startExecution();
    virtual void finishExecution();

private slots:
    void continueExecution();

private:
    ContentContainer* processFolder(const QString& dirPath, const QString& title = QString());
    ContentItem* processFile(const QString& dirPath);

};

/*---------------------------------------------------------------------------*/

#endif /* _MEDIA_SERVER__FILE_SYSTEM_CONTENT_FEEDER_H_ */
