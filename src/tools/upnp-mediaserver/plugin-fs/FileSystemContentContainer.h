/*
 * FileSystemContentContainer.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__FILE_SYSTEM_CONTENT_CONTAINER_H_
#define _MEDIA_SERVER__FILE_SYSTEM_CONTENT_CONTAINER_H_

#include <plugin/ContentContainer.h>

class FileSystemContentContainer : public ContentContainer
{
private:
    QString m_path;

public:
    FileSystemContentContainer(const QString& path, const QString& objectID, const QString& upnpClass,
            const QString& title);
    virtual ~FileSystemContentContainer();

};

#endif /* _MEDIA_SERVER__FILE_SYSTEM_CONTENT_CONTAINER_H_ */
