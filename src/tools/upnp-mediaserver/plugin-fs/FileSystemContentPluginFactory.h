/*
 * FileSystemContentPluginFactory.h
 *
 *  Created on: 26-04-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER_FS__FILE_SYSTEM_CONTENT_PLUGIN_FACTORY_H_
#define _MEDIA_SERVER_FS__FILE_SYSTEM_CONTENT_PLUGIN_FACTORY_H_

#include <plugin/ContentPluginFactory.h>

class FileSystemContentPluginFactory : public ContentPluginFactory
{
public:
    /**
     * Constructs the factory.
     */
    FileSystemContentPluginFactory();

    /**
     * Returns plugin type name.
     *
     * @return
     * Plugin type name.
     */
    virtual QString name() const
    {
        return "File System";
    }

    virtual ContentPlugin* createPlugin(const ContentPluginSettings* settings, QObject* owner) const;

    virtual ContentPluginConfigWidget* createConfigWidget(QWidget* parent = 0) const;

};

#endif /* _MEDIA_SERVER_FS__FILE_SYSTEM_CONTENT_PLUGIN_FACTORY_H_ */
