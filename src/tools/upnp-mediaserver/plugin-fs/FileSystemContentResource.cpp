/*
 * FileSystemContentResource.cpp
 *
 *  Created on: 21-04-2011
 *      Author: saveman
 */

#include "FileSystemContentResource.h"

FileSystemContentResource::FileSystemContentResource(const QString& path, const QString& mimeType) :
    m_path(path), m_mimeType(mimeType)
{
    // nothing to do
}

FileSystemContentResource::~FileSystemContentResource()
{
    // nothing to do
}
