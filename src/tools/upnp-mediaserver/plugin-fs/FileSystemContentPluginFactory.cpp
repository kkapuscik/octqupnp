/*
 * FileSystemContentPluginFactory.cpp
 *
 *  Created on: 26-04-2011
 *      Author: saveman
 */

#include "FileSystemContentPluginFactory.h"
#include "FileSystemContentPlugin.h"
#include "FileSystemContentPluginConfigWidget.h"

FileSystemContentPluginFactory::FileSystemContentPluginFactory() :
    ContentPluginFactory(PLUGIN_TYPE_FILE_SYSTEM)
{
    // nothing to do
}

ContentPlugin* FileSystemContentPluginFactory::createPlugin(const ContentPluginSettings* settings, QObject* owner) const
{
    const FileSystemContentPluginSettings* fsSettings = dynamic_cast<const FileSystemContentPluginSettings*> (settings);
    return new FileSystemContentPlugin(fsSettings, owner);
}

ContentPluginConfigWidget* FileSystemContentPluginFactory::createConfigWidget(QWidget* parent) const
{
    return new FileSystemContentPluginConfigWidget(parent);
}
