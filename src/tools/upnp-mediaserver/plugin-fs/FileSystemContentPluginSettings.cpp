/*
 * FileSystemContentPluginSettings.cpp
 *
 *  Created on: 07-04-2011
 *      Author: saveman
 */

#include "FileSystemContentPluginSettings.h"

FileSystemContentPluginSettings::FileSystemContentPluginSettings(const QString& name, const QString& path,
        int refreshRate) :
    ContentPluginSettings(PLUGIN_TYPE_FILE_SYSTEM), m_name(name.trimmed()), m_path(path), m_refreshRate(refreshRate)
{
    Q_ASSERT(m_name.length() > 0);
    Q_ASSERT(m_path.length() > 0);
    Q_ASSERT(m_refreshRate >= 0);
}

FileSystemContentPluginSettings::~FileSystemContentPluginSettings()
{
    // nothing to do
}

QVariant FileSystemContentPluginSettings::data(Role role) const
{
    switch (role) {
        case ROLE_NAME:
            return m_name;
        case ROLE_PATH:
            return m_path;
        case ROLE_REFRESH_RATE:
            return m_refreshRate;
        default:
            return QVariant();
    }
}

ContentPluginSettings* FileSystemContentPluginSettings::clone() const
{
    return new FileSystemContentPluginSettings(*this);
}
