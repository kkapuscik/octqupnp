/*
 * FileSystemContentResource.h
 *
 *  Created on: 21-04-2011
 *      Author: saveman
 */

#ifndef FILESYSTEMCONTENTRESOURCE_H_
#define FILESYSTEMCONTENTRESOURCE_H_

#include <plugin/ContentResource.h>

#include <httpserver/StandardFile.h>

class FileSystemContentResource : public ContentResource
{
private:
    QString m_path;
    QString m_mimeType;

public:
    FileSystemContentResource(const QString& path, const QString& mimeType);
    virtual ~FileSystemContentResource();

    const QString& path() const
    {
        return m_path;
    }

    virtual QString mimeType() const
    {
        return m_mimeType;
    }

    virtual Oct::HttpServer::VfsFile* openFile() const
    {
        return new Oct::HttpServer::StandardFile(m_path, m_mimeType);
    }

};

#endif /* FILESYSTEMCONTENTRESOURCE_H_ */
