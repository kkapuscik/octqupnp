/*
 * FileSystemContentContainer.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__FILE_SYSTEM_CONTENT_ITEM_H_
#define _MEDIA_SERVER__FILE_SYSTEM_CONTENT_ITEM_H_

#include "FileSystemContentResource.h"

#include <plugin/ContentItem.h>

class FileSystemContentItem : public ContentItem
{
public:
    FileSystemContentItem(const QString& path, const QString& mimeType, const QString& objectID,
            const QString& upnpClass, const QString& title);
};

#endif /* _MEDIA_SERVER__FILE_SYSTEM_CONTENT_ITEM_H_ */
