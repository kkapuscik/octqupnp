#ifndef FILESYSTEMCONTENTPLUGINCONFIGWIDGET_H
#define FILESYSTEMCONTENTPLUGINCONFIGWIDGET_H

#include <plugin/ContentPluginConfigWidget.h>

#include <ui_FileSystemContentPluginConfigWidget.h>

class FileSystemContentPluginConfigWidget : public ContentPluginConfigWidget
{
Q_OBJECT

private:
    Ui::FileSystemContentPluginConfigWidgetClass m_ui;

public:
    FileSystemContentPluginConfigWidget(QWidget *parent = 0);

    virtual bool validateSettings();
    virtual void setSettings(const ContentPluginSettings* settings);
    virtual ContentPluginSettings* settings();

private slots:
    void browseForPath();

};

#endif // FILESYSTEMCONTENTPLUGINCONFIGWIDGET_H
