#include "XkcdContentPluginConfigWidget.h"

#include "XkcdContentPluginSettings.h"

#include <QtWidgets/QMessageBox>

XkcdContentPluginConfigWidget::XkcdContentPluginConfigWidget(QWidget *parent) :
    ContentPluginConfigWidget(parent)
{
    m_ui.setupUi(this);
}

bool XkcdContentPluginConfigWidget::validateSettings()
{
    QString name = m_ui.m_nameEdit->text();
    if (name.length() == 0) {
        QMessageBox::warning(this, tr("Validate settings"), tr("Share name must be specified."));
        return false;
    }

    int itemsCount = m_ui.m_itemsCountSpinBox->value();
    if (itemsCount < 1) {
        QMessageBox::warning(this, tr("Validate settings"), tr("Items count must be positive."));
        return false;
    }

    return true;
}

void XkcdContentPluginConfigWidget::setSettings(const ContentPluginSettings* settings)
{
    const XkcdContentPluginSettings* xkcdSettings = dynamic_cast<const XkcdContentPluginSettings*>(settings);

    m_ui.m_nameEdit->setText(xkcdSettings->name());
    m_ui.m_itemsCountSpinBox->setValue(xkcdSettings->itemsPerFolder());
}

ContentPluginSettings* XkcdContentPluginConfigWidget::settings()
{
    if (validateSettings()) {
        return new XkcdContentPluginSettings(
                m_ui.m_nameEdit->text().trimmed(),
                m_ui.m_itemsCountSpinBox->value());
    } else {
        return NULL;
    }
}
