/*
 * XkcdContentPluginFeederThread.cpp
 *
 *  Created on: 12-04-2011
 *      Author: saveman
 */

#include "XkcdContentPluginFeederThread.h"

#include "XkcdContentItem.h"

#include <json/JsonDecoder.h>

// TODO: remove
#include <QtCore/QDebug>

/*---------------------------------------------------------------------------*/

Oct::Log::Logger<XkcdContentPluginFeederThread> XkcdContentPluginFeederThread::Logger(
        "/oct/tools/mediaserver/XkcdContentPluginFeederThread");

/*---------------------------------------------------------------------------*/

/*
 * TODO: reimplement to use:
 *
 *  Is there an interface for automated systems to access comics and metadata?
 *
 *  Yes. You can get comics through the JSON interface, at URLs like
 *  http://xkcd.com/info.0.json (current comic) and http://xkcd.com/614/info.0.json (comic #614).
 *
 * TODO: the 404 is missing
 */
XkcdContentPluginFeederThread::XkcdContentPluginFeederThread(QObject* parent) :
    Oct::Util::EventThreadExecutor(parent), m_netAccessManager(NULL), m_rootContainer(NULL),
    m_pluginSettings(NULL), m_currentReply(NULL), m_totalCount(0), m_resultRootContainer(NULL)
{
    // nothing to do
}

XkcdContentPluginFeederThread::~XkcdContentPluginFeederThread()
{
    if (m_rootContainer != NULL) {
        delete m_rootContainer;
        m_rootContainer = NULL;
    }

    if (m_resultRootContainer != NULL) {
        delete m_resultRootContainer;
        m_resultRootContainer = NULL;
    }

    if (m_pluginSettings != NULL) {
        delete m_pluginSettings;
        m_pluginSettings = NULL;
    }
}

ContentContainer* XkcdContentPluginFeederThread::takeRootContainer()
{
    ContentContainer* rv = NULL;

    if (m_resultRootContainer != NULL) {
        rv = m_resultRootContainer;
        m_resultRootContainer = NULL;
    }

    return rv;
}

bool XkcdContentPluginFeederThread::scheduleCountGet()
{
    Logger.trace(this) << "scheduleCountGet()";

    /* close old reply (if any) */
    if (m_currentReply != NULL) {
        m_currentReply->deleteLater();
        m_currentReply = NULL;
    }

    Logger.trace(this) << "scheduleCountGet()";

    /* clear the data buffer */
    m_currentReplyData.clear();

    /* try to create new request */
    m_currentReply = m_netAccessManager->get(QNetworkRequest(QString("http://xkcd.com/info.0.json")));
    if (m_currentReply == NULL) {
        Logger.trace(this) << "scheduleCountGet() cannot prepare GET request";
        return false;
    }

    /* connect signals to new reply */
    connect(m_currentReply, SIGNAL( finished() ), this, SLOT( onCountFinished() ));
    connect(m_currentReply, SIGNAL( error(QNetworkReply::NetworkError) ), this,
            SLOT( onReplyError(QNetworkReply::NetworkError) ));
    connect(m_currentReply, SIGNAL( readyRead() ), this, SLOT( onReplyReadyRead() ));

    return true;
}

bool XkcdContentPluginFeederThread::scheduleNextGet()
{
    Logger.trace(this) << "scheduleNextGet()";

    /* close old reply (if any) */
    if (m_currentReply != NULL) {
        m_currentReply->deleteLater();
        m_currentReply = NULL;
    }

    /* move to next element */
    ++m_currentIndex;
    if (m_currentIndex == 404) {
        ++m_currentIndex;
    }

    Logger.trace(this) << "scheduleNextGet() next id = " << m_currentIndex;

    if (m_currentIndex % 50 == 1) {
        m_currentContainer = NULL;
    }

    if (m_currentIndex > m_totalCount) {
        return false;
    }

    if (m_currentIndex >= 100) {
        return false;
    }

    /* clear the data buffer */
    m_currentReplyData.clear();

    /* try to create new request */
    m_currentReply = m_netAccessManager->get(QNetworkRequest(QString("http://xkcd.com/%1/").arg(m_currentIndex)));
    if (m_currentReply == NULL) {
        Logger.trace(this) << "scheduleNextGet() cannot prepare GET request";
        return false;
    }

    qDebug() << "Scheduled get = " << m_currentIndex;

    /* connect signals to new reply */
    connect(m_currentReply, SIGNAL( finished() ), this, SLOT( onReplyFinished() ));
    connect(m_currentReply, SIGNAL( error(QNetworkReply::NetworkError) ), this,
            SLOT( onReplyError(QNetworkReply::NetworkError) ));
    connect(m_currentReply, SIGNAL( readyRead() ), this, SLOT( onReplyReadyRead() ));

    return true;
}

void XkcdContentPluginFeederThread::onReplyFinished()
{
    Logger.trace(this) << "onReplyFinished() " << m_currentReply->url().toString();

    // TODO

    int startIdx = m_currentReplyData.indexOf("<h3>Image URL (for hotlinking/embedding): ");
    if (startIdx >= 0) {
        startIdx += strlen("<h3>Image URL (for hotlinking/embedding): ");

        int endIdx = m_currentReplyData.indexOf("</h3>", startIdx);
        if (endIdx >= 0) {
            QString imgURL = QString::fromUtf8(m_currentReplyData.mid(startIdx, endIdx - startIdx));

            Logger.trace(this) << "Found image " << m_currentIndex << " with URL: " << imgURL;

            if (m_currentContainer == NULL) {
                m_currentContainer = new ContentContainer(ContentObject::generateObjectID(),
                        ContentContainer::UPNP_CLASS_CONTAINER, QString("%1").arg(m_currentIndex, 3, 10, QChar('0')));
                m_currentContainer->setParent(m_rootContainer);
            }

            XkcdContentItem* item = new XkcdContentItem(imgURL, ContentObject::generateObjectID(),
                    ContentItem::UPNP_CLASS_ITEM_IMAGE, QString("%1").arg(m_currentIndex, 3, 10, QChar('0')));
            item->setParent(m_currentContainer);

            if (!scheduleNextGet()) {
                m_finished = true;
                quit();
            }
        } else {
            // TODO: finish
            m_finished = true;
            quit();
        }
    } else {
        // TODO: finish
        m_finished = true;
        quit();
    }
}

void XkcdContentPluginFeederThread::onCountFinished()
{
    Logger.trace(this) << "onCountFinished() " << m_currentReply->url().toString();

    bool success = false;
    do {
        Oct::Json::JsonObject jsonObject = Oct::Json::JsonDecoder::decode(m_currentReplyData);

        if (jsonObject.isNull()) {
            break;
        }

        Oct::Json::JsonValue numValue = jsonObject.member("num").value();
        if (!numValue.isNumber()) {
            break;
        }

        bool numOK;
        int num = numValue.toNumber().toInt(&numOK);
        if (!numOK) {
            break;
        }
        if (num <= 0) {
            break;
        }

        m_totalCount = num;

        qDebug() << "Set total count = " << m_totalCount;

        if (!scheduleNextGet()) {
            break;
        }

        success = true;
    } while (0);

    if (!success) {
        // TODO: finish
        m_finished = true;
        quit();
    }
}

void XkcdContentPluginFeederThread::onReplyError(QNetworkReply::NetworkError errorCode)
{
    Logger.trace(this) << "onReplyError() " << m_currentReply->url().toString() << " " << errorCode;

    // TODO

    /* finish thread execution */
    // TODO
    m_finished = true;
    quit();
}

void XkcdContentPluginFeederThread::onReplyReadyRead()
{
    Logger.trace(this) << "onReplyReadyRead() " << m_currentReply->url().toString();

    Q_ASSERT(m_currentReply != NULL);

    /* read all data available */
    while (m_currentReply->bytesAvailable() > 0) {
        QByteArray data = m_currentReply->readAll();
        m_currentReplyData.append(data);
    }
}

void XkcdContentPluginFeederThread::setSettings(const XkcdContentPluginSettings* settings)
{
    Q_ASSERT(settings != NULL);
    Q_ASSERT(m_pluginSettings == NULL);

    m_pluginSettings = (XkcdContentPluginSettings*) settings->clone();
}

bool XkcdContentPluginFeederThread::startExecution()
{
    m_netAccessManager = new QNetworkAccessManager(this);

    m_rootContainer = new ContentContainer(ContentObject::generateObjectID(), ContentContainer::UPNP_CLASS_CONTAINER,
            m_pluginSettings->name());

    m_currentContainer = NULL;

    m_currentIndex = 0;

    if (!scheduleCountGet()) {
        return false;
    }

    return true;
}

void XkcdContentPluginFeederThread::finishExecution()
{
    /* set the 'result' container */
    m_resultRootContainer = m_rootContainer;
    m_rootContainer = NULL;
}
