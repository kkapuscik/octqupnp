/*
 * FileSystemContentContainer.h
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__XKCD_CONTENT_ITEM_H_
#define _MEDIA_SERVER__XKCD_CONTENT_ITEM_H_

#include <plugin/ContentItem.h>

class XkcdContentItem : public ContentItem
{
public:
    XkcdContentItem(const QString& uri, const QString& objectID,
            const QString& upnpClass, const QString& title);
};

#endif /* _MEDIA_SERVER__XKCD_CONTENT_ITEM_H_ */
