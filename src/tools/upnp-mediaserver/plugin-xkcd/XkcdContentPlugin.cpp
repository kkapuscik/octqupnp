/*
 * XkcdContentPlugin.cpp
 *
 *  Created on: 08-04-2011
 *      Author: saveman
 */

#include "XkcdContentPlugin.h"
#include "XkcdContentPluginFeederThread.h"

#include <util/EventThread.h>

/*---------------------------------------------------------------------------*/

Oct::Log::Logger<XkcdContentPlugin> XkcdContentPlugin::Logger("/oct/tools/mediaserver/XkcdContentPlugin");

/*---------------------------------------------------------------------------*/

XkcdContentPlugin::XkcdContentPlugin(const XkcdContentPluginSettings* settings, QObject* parent) :
    ContentPlugin(PLUGIN_TYPE_XKCD, settings, parent), m_pluginContainer(NULL)
{
    // nothing to do
}

XkcdContentPlugin::~XkcdContentPlugin()
{
    // nothing to do
}

ContentContainer* XkcdContentPlugin::rootContainer()
{
    return m_pluginContainer;
}

bool XkcdContentPlugin::updateContents()
{
    bool rv = false;

    Logger.trace(this) << "updateContents()";

    /* delete old contents */
    if (m_pluginContainer != NULL) {
        delete m_pluginContainer;
        m_pluginContainer = NULL;
    }

    /* prepare new contents */
    {
        XkcdContentPluginSettings* settings = (XkcdContentPluginSettings*)internalSettings();

        XkcdContentPluginFeederThread* feeder = new XkcdContentPluginFeederThread();
        feeder->setSettings(settings);

        Oct::Util::EventThread* thread = new Oct::Util::EventThread(feeder, this);
        thread->start();
        thread->wait();

        m_pluginContainer = feeder->takeRootContainer();

        delete thread;

        rv = true;
    }

    Logger.trace(this) << "updateContents() => " << rv;

    return rv;
}
