/*
 * FileSystemContentPluginFactory.cpp
 *
 *  Created on: 26-04-2011
 *      Author: saveman
 */

#include "XkcdContentPluginFactory.h"
#include "XkcdContentPlugin.h"
#include "XkcdContentPluginConfigWidget.h"

XkcdContentPluginFactory::XkcdContentPluginFactory() :
    ContentPluginFactory(PLUGIN_TYPE_XKCD)
{
    // nothing to do
}

ContentPlugin* XkcdContentPluginFactory::createPlugin(const ContentPluginSettings* settings, QObject* owner) const
{
    const XkcdContentPluginSettings* xkcdSettings = dynamic_cast<const XkcdContentPluginSettings*> (settings);
    return new XkcdContentPlugin(xkcdSettings, owner);
}

ContentPluginConfigWidget* XkcdContentPluginFactory::createConfigWidget(QWidget* parent) const
{
    return new XkcdContentPluginConfigWidget(parent);
}
