/*
 * FileSystemContentPluginSettings.h
 *
 *  Created on: 07-04-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_SETTINGS_H_
#define _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_SETTINGS_H_

#include <plugin/ContentPluginSettings.h>

class XkcdContentPluginSettings : public ContentPluginSettings
{
private:
    QString m_name;
    int m_itemsPerFolder;

public:
    XkcdContentPluginSettings(const QString& name, int itemsPerFolder);
    virtual ~XkcdContentPluginSettings();

    QString name() const
    {
        return m_name;
    }

    int itemsPerFolder() const
    {
        return m_itemsPerFolder;
    }

    virtual QVariant data(Role role) const;

    virtual ContentPluginSettings* clone() const;
};

#endif /* _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_SETTINGS_H_ */
