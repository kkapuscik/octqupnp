/*
 * XkcdContentItem
 *
 *  Created on: 24-03-2011
 *      Author: saveman
 */

#include "XkcdContentItem.h"

#include "XkcdContentResource.h"

XkcdContentItem::XkcdContentItem(const QString& uri, const QString& objectID, const QString& upnpClass,
        const QString& title) :
    ContentItem(objectID, upnpClass, title)
{
    addResource(new XkcdContentResource(uri));
}
