/*
 * XkcdContentResource.h
 *
 *  Created on: 23-04-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER_XKCD_CONTENT_RESOURCE_H_
#define _MEDIA_SERVER_XKCD_CONTENT_RESOURCE_H_

#include <plugin/ContentResource.h>

class XkcdContentResource : public ContentResource
{
private:
    QString m_uri;

public:
    XkcdContentResource(QString uri);

    virtual QString mimeType() const
    {
        if (m_uri.endsWith(".png", Qt::CaseInsensitive)) {
            return "image/png";
        } else if (m_uri.endsWith(".jpg", Qt::CaseInsensitive)) {
            return "image/jpeg";
        } else {
            return "application/octet-stream";
        }
    }

    virtual QString externalURI() const
    {
        return m_uri;
    }

    virtual Oct::HttpServer::VfsFile* openFile() const
    {
        return NULL;
    }

};

#endif /* _MEDIA_SERVER_XKCD_CONTENT_RESOURCE_H_ */
