/*
 * XkcdContentPlugin.h
 *
 *  Created on: 08-04-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_H_
#define _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_H_

#include "XkcdContentPluginSettings.h"

#include <plugin/ContentPlugin.h>

#include <log/Logger.h>

class XkcdContentPlugin : public ContentPlugin
{
    Q_OBJECT

private:
    static Oct::Log::Logger<XkcdContentPlugin> Logger;

private:

    /**
     * Plugin root container.
     *
     * NULL if plugin was not initialized.
     */
    ContentContainer* m_pluginContainer;

public:
    XkcdContentPlugin(const XkcdContentPluginSettings* settings, QObject* parent = 0);
    virtual ~XkcdContentPlugin();

    // ContentPlugin#rootContainer()
    virtual ContentContainer* rootContainer();

protected:
    // ContentPlugin#updateContents()
    virtual bool updateContents();

};

#endif /* _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_H_ */
