/*
 * XkcdContentPluginFeederThread.h
 *
 *  Created on: 12-04-2011
 *      Author: saveman
 */

#ifndef _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_FEEDER_THREAD_H_
#define _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_FEEDER_THREAD_H_

#include "XkcdContentPluginSettings.h"

#include <plugin/ContentContainer.h>

#include <log/Logger.h>
#include <util/EventThreadExecutor.h>

#include <QtCore/QThread>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

class XkcdContentPluginFeederThread : public Oct::Util::EventThreadExecutor
{
Q_OBJECT

private:
    static Oct::Log::Logger<XkcdContentPluginFeederThread> Logger;

private:
    QNetworkAccessManager* m_netAccessManager;
    ContentContainer* m_rootContainer;
    XkcdContentPluginSettings* m_pluginSettings;
    bool m_started;
    bool m_aborted;
    bool m_finished;
    int m_currentIndex;
    QNetworkReply* m_currentReply;
    QByteArray m_currentReplyData;
    ContentContainer* m_currentContainer;
    int m_totalCount;

    ContentContainer* m_resultRootContainer;

public:
    XkcdContentPluginFeederThread(QObject* parent = 0);
    virtual ~XkcdContentPluginFeederThread();

    ContentContainer* takeRootContainer();

    void setSettings(const XkcdContentPluginSettings* settings);

protected:
    virtual bool startExecution();
    virtual void finishExecution();

private:
    bool scheduleCountGet();
    bool scheduleNextGet();

private slots:
    void onCountFinished();
    void onReplyFinished();
    void onReplyError(QNetworkReply::NetworkError);
    void onReplyReadyRead();

};

#endif /* _MEDIA_SERVER__XKCD_CONTENT_PLUGIN_FEEDER_THREAD_H_ */
