/*
 * FileSystemContentPluginSettings.cpp
 *
 *  Created on: 07-04-2011
 *      Author: saveman
 */

#include "XkcdContentPluginSettings.h"

XkcdContentPluginSettings::XkcdContentPluginSettings(const QString& name, int itemsPerFolder) :
    ContentPluginSettings(PLUGIN_TYPE_XKCD), m_name(name.trimmed()), m_itemsPerFolder(itemsPerFolder)
{
    Q_ASSERT(m_name.length() > 0);
    Q_ASSERT(itemsPerFolder > 0);
}

XkcdContentPluginSettings::~XkcdContentPluginSettings()
{
    // nothing to do
}

QVariant XkcdContentPluginSettings::data(Role role) const
{
    switch (role) {
        case ROLE_NAME:
            return m_name;
        case ROLE_PATH:
            return QString("xkcd.com");
        case ROLE_REFRESH_RATE:
            return QString("-not-supported-");
        default:
            return QVariant();
    }
}

ContentPluginSettings* XkcdContentPluginSettings::clone() const
{
    return new XkcdContentPluginSettings(*this);
}
