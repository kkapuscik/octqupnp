#ifndef _MEDIA_SERVER_XKCD__XKCD_CONTENT_PLUGIN_CONFIG_WIDGET_H_
#define _MEDIA_SERVER_XKCD__XKCD_CONTENT_PLUGIN_CONFIG_WIDGET_H_

#include <plugin/ContentPluginConfigWidget.h>

#include <ui_XkcdContentPluginConfigWidget.h>

class XkcdContentPluginConfigWidget : public ContentPluginConfigWidget
{
Q_OBJECT

private:
    Ui::XkcdContentPluginConfigWidgetClass m_ui;

public:
    XkcdContentPluginConfigWidget(QWidget *parent = 0);

    virtual bool validateSettings();
    virtual void setSettings(const ContentPluginSettings* settings);
    virtual ContentPluginSettings* settings();
};

#endif // _MEDIA_SERVER_XKCD__XKCD_CONTENT_PLUGIN_CONFIG_WIDGET_H_
