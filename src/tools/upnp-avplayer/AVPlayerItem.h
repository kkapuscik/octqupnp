/*
 * AVPlayerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef AVPLAYERITEM_H_
#define AVPLAYERITEM_H_

#include <QtGui/QStandardItem>

class AVPlayerItem : public QStandardItem
{
private:
    typedef QMap<int,QIcon> IconsMap;

public:
    enum MajorType
    {
        MAJOR_TYPE_UNKNOWN,
        MAJOR_TYPE_RETURN_ITEM,
        MAJOR_TYPE_CONTAINER,
        MAJOR_TYPE_MEDIA
    };

    enum Role
    {
        ROLE_TYPE = Qt::UserRole + 1,
        ROLE_MAJOR_TYPE,
        ROLE_MEDIA_URI
    };

    enum Type
    {
        TYPE_MEDIA_SERVER = UserType + 1,
        TYPE_RETURN_ITEM,
        TYPE_CONTENT_CONTAINER,
        TYPE_CONTENT_ITEM
    };

private:
    static IconsMap sm_iconsMap;

public:
    AVPlayerItem(int type, int majorType);

    virtual int type() const
    {
        bool ok;
        int val = data(ROLE_TYPE).toInt(&ok);
        if (ok) {
            return val;
        } else {
            return UserType;
        }
    }

protected:
    QIcon getIcon(int type);

};

#endif /* AVPLAYERITEM_H_ */
