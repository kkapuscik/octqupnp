/*
 * MediaControlWidget.h
 *
 *  Created on: 01-03-2011
 *      Author: saveman
 */

#ifndef MEDIACONTROLWIDGET_H_
#define MEDIACONTROLWIDGET_H_

#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <phonon/MediaObject>
#include <phonon/SeekSlider>
#include <phonon/VolumeSlider>

class MediaControlWidget : public QWidget
{
Q_OBJECT

private:
    QPushButton* m_playButton;
    QPushButton* m_pauseButton;
    QPushButton* m_stopButton;
    Phonon::SeekSlider* m_seekSlider;
    Phonon::VolumeSlider* m_volumeSlider;

    Phonon::MediaObject* m_mediaObject;
    Phonon::AudioOutput* m_audioOutput;

public:
    MediaControlWidget(QWidget* parent = 0);

public slots:
    void setMediaObject(Phonon::MediaObject* mediaObject);
    void setAudioOutput(Phonon::AudioOutput* audioOutput);

private slots:
//    void mediaAboutToFinish();
//    void mediaBufferStatus(int percentFilled);
//    void mediaCurrentSourceChanged(const Phonon::MediaSource & newSource);
//    void mediaFinished();
//    void mediaHasVideoChanged(bool hasVideo);
//    void mediaMetaDataChanged();
//    void mediaPrefinishMarkReached(qint32 msecToEnd);
//    void mediaSeekableChanged(bool isSeekable);
    void mediaStateChanged(Phonon::State newstate, Phonon::State oldstate);
//    void mediaTick(qint64 time);
//    void mediaTotalTimeChanged(qint64 newTotalTime);

};

#endif /* MEDIACONTROLWIDGET_H_ */
