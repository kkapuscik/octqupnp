/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "AVPlayerApp.h"

/*---------------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    return Oct::App::GuiApplication::startApp<AVPlayerApp>(
            "UPnP-AVPlayer", "0.2", argc, argv);
}

/*---------------------------------------------------------------------------*/
