/*
 * MediaVideoPlayerWidget.h
 *
 *  Created on: 05-03-2011
 *      Author: saveman
 */

#ifndef MEDIAVIDEOPLAYERWIDGET_H_
#define MEDIAVIDEOPLAYERWIDGET_H_

#include <log/Logger.h>

#include <phonon/VideoPlayer>
#include <phonon/VideoWidget>
#include <QtCore/QEvent>
#include <QtWidgets/QMenu>

class MediaVideoPlayerWidget : public Phonon::VideoPlayer
{
Q_OBJECT

private:
    static Oct::Log::Logger<MediaVideoPlayerWidget> Logger;

private:
    QAction* m_toggleFullScreenAction;
    QActionGroup* m_aspectRatioGroup;
    QActionGroup* m_scaleModeGroup;
    QMenu* m_videoWidgetContextMenu;

public:
    /**
     * Constructs a new video widget with a \p parent
     * using Phonon::VideoCategory as its category.
     *
     * \param parent
     *      The QObject parent.
     */
    MediaVideoPlayerWidget(QWidget *parent = 0);

private:
    void addAspectRatioAction(const QString& text, Phonon::VideoWidget::AspectRatio ratio);
    void addScaleModeAction(const QString& text, Phonon::VideoWidget::ScaleMode mode);


private slots:
    void setFullScreen(bool enable);
    void showVideoPlayerContextMenu(const QPoint& pos);
    void setAspectRatio(QAction* action);
    void setScaleMode(QAction* action);

};

#endif /* MEDIAVIDEOPLAYERWIDGET_H_ */
