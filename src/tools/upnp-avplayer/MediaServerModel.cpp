/*
 * UpnpDeviceModel.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "MediaServerModel.h"

Oct::Log::Logger<MediaServerModel> MediaServerModel::Logger("/oct/tools/avplayer/MediaServerModel");

MediaServerModel::MediaServerModel(Oct::UpnpCp::ControlPoint* controlPoint, MediaServerItem* item, QObject* parent) :
    ContentContainerModel(controlPoint, item->contentDirectoryService(), "0", parent)
{
    // TODO
}
