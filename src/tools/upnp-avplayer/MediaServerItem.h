/*
 * MediaServerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef MEDIASERVERITEM_H_
#define MEDIASERVERITEM_H_

#include "AVPlayerItem.h"

#include <upnpcp/Device.h>

class MediaServerItem : public AVPlayerItem
{
private:
    Oct::UpnpCp::Device m_dmsDevice;
    Oct::UpnpCp::Service m_cdService;

public:
    MediaServerItem(Oct::UpnpCp::Device dmsDevice, Oct::UpnpCp::Service cdService);

    Oct::UpnpCp::Service contentDirectoryService() const
    {
        return m_cdService;
    }

    Oct::UpnpCp::Device device() const
    {
        return m_dmsDevice;
    }
};

#endif /* MEDIASERVERITEM_H_ */
