/*
 * AVPlayerModel.h
 *
 *  Created on: 28-02-2011
 *      Author: saveman
 */

#ifndef AVPLAYERMODEL_H_
#define AVPLAYERMODEL_H_

#include <QtGui/QStandardItemModel>

class AVPlayerModel : public QStandardItemModel
{
    Q_OBJECT

public:
    AVPlayerModel(QObject* parent = 0);

    virtual AVPlayerModel* createChildView(QModelIndex index) = 0;
};

#endif /* AVPLAYERMODEL_H_ */
