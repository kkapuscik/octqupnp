/*
 * MediaControlWidget.cpp
 *
 *  Created on: 01-03-2011
 *      Author: saveman
 */

#include "MediaControlWidget.h"

#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QStyle>

MediaControlWidget::MediaControlWidget(QWidget* parent) :
    QWidget(parent), m_mediaObject(NULL), m_audioOutput(NULL)
{
    QHBoxLayout* mainLayout = new QHBoxLayout(this);

    m_playButton = new QPushButton(this);
    m_pauseButton = new QPushButton(this);
    m_stopButton = new QPushButton(this);
    m_seekSlider = new Phonon::SeekSlider(this);
    m_volumeSlider = new Phonon::VolumeSlider(this);

    m_playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    m_pauseButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    m_stopButton->setIcon(style()->standardIcon(QStyle::SP_MediaStop));

    mainLayout->addWidget(m_playButton, 0);
    mainLayout->addWidget(m_pauseButton, 0);
    mainLayout->addWidget(m_stopButton, 0);
    mainLayout->addSpacing(5);
    mainLayout->addWidget(m_seekSlider, 2);
    mainLayout->addSpacing(5);
    mainLayout->addWidget(m_volumeSlider, 1);

    mediaStateChanged(Phonon::ErrorState, Phonon::ErrorState);
//    mainLayout->
    // TODO Auto-generated constructor stub

}

void MediaControlWidget::setAudioOutput(Phonon::AudioOutput* audioOutput)
{
    /* disconnect old */
    /* - nothing to do - */

    // TODO: register to object delete

    /* store object */
    m_audioOutput = audioOutput;

    /* setup elements */
    m_volumeSlider->setAudioOutput(m_audioOutput);

    /* connect new */
    /* - nothing to do - */
}

void MediaControlWidget::setMediaObject(Phonon::MediaObject* mediaObject)
{
    /* disconnect old */
    if (m_mediaObject != NULL) {
        disconnect(m_mediaObject, SIGNAL( stateChanged(Phonon::State,Phonon::State) ),
                this, SLOT( mediaStateChanged(Phonon::State,Phonon::State) ));

        disconnect(m_playButton, SIGNAL( clicked() ), m_mediaObject, SLOT( play() ));
        disconnect(m_pauseButton, SIGNAL( clicked() ), m_mediaObject, SLOT( pause() ));
        disconnect(m_stopButton, SIGNAL( clicked() ), m_mediaObject, SLOT( stop() ));
    }

    // TODO: register to object delete

    /* store object */
    m_mediaObject = mediaObject;

    /* setup elements */
    m_seekSlider->setMediaObject(m_mediaObject);

    /* connect new */
    if (m_mediaObject != NULL) {
        connect(m_mediaObject, SIGNAL( stateChanged(Phonon::State,Phonon::State) ),
                this, SLOT( mediaStateChanged(Phonon::State,Phonon::State) ));

        connect(m_playButton, SIGNAL( clicked() ), m_mediaObject, SLOT( play() ));
        connect(m_pauseButton, SIGNAL( clicked() ), m_mediaObject, SLOT( pause() ));
        connect(m_stopButton, SIGNAL( clicked() ), m_mediaObject, SLOT( stop() ));
    }
}

void MediaControlWidget::mediaStateChanged(Phonon::State newState, Phonon::State oldState)
{
    bool canPlay = false;
    bool canPause = false;
    bool canStop = false;

    if (m_mediaObject != NULL && m_mediaObject->isValid()) {
        canPlay  = newState != Phonon::PlayingState;
        canPause = newState != Phonon::PausedState;
        canStop  = newState != Phonon::StoppedState;
    }

    m_playButton->setEnabled(canPlay);
    m_pauseButton->setEnabled(canPause);
    m_stopButton->setEnabled(canStop);
}
