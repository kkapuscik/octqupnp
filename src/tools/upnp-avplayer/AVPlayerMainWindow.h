#ifndef AVPLAYERMAINWINDOW_H
#define AVPLAYERMAINWINDOW_H

/*---------------------------------------------------------------------------*/

#include "AVPlayerDocument.h"

#include <ui_AVPlayerMainWindow.h>

#include <log/Logger.h>
#include <QtWidgets/QMainWindow>

/*---------------------------------------------------------------------------*/

class AVPlayerMainWindow : public QMainWindow
{
Q_OBJECT

private:
    static Oct::Log::Logger<AVPlayerMainWindow> Logger;

private:
    Ui::AVPlayerMainWindowClass m_ui;
    AVPlayerDocument* m_document;

public:
    AVPlayerMainWindow(QWidget *parent = 0);

private slots:
    void processListDoubleClick(QModelIndex index);
    void processModelChanged(QAbstractItemModel* model);
    void appAbout();
    void qtAbout();
};

/*---------------------------------------------------------------------------*/

#endif // AVPLAYERMAINWINDOW_H
