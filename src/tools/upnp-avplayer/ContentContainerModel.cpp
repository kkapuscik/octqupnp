/*
 * UpnpDeviceModel.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "ContentContainerModel.h"

#include "ReturnItem.h"
#include "ContentContainerItem.h"
#include "ContentItemItem.h"

#include <upnpcp/ActionExecutor.h>
#include <QtWidgets/QMessageBox>

Oct::Log::Logger<ContentContainerModel> ContentContainerModel::Logger("/oct/tools/avplayer/ContentContainerModel");

// TODO: move this
static const QString DIDL_LITE_NAMESPACE("urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/");
static const QString DIDL_LITE_LOCAL_NAME("DIDL-Lite");
static const QString CONTAINER_LOCAL_NAME("container");
static const QString ITEM_LOCAL_NAME("item");
static const QString RES_LOCAL_NAME("res");

static const QString DC_NAMESPACE("http://purl.org/dc/elements/1.1/");
static const QString TITLE_LOCAL_NAME("title");

//static const QString UPNP_NAMESPACE("urn:schemas-upnp-org:metadata-1-0/upnp/");

ContentContainerModel::ContentContainerModel(Oct::UpnpCp::ControlPoint* controlPoint, Oct::UpnpCp::Service cds, const QString& containerId, QObject* parent) :
    AVPlayerModel(parent), m_controlPoint(controlPoint), m_cds(cds), m_containerId(containerId), m_currentExecutor(NULL), m_currentStartingIndex(0)
{
    appendRow(new ReturnItem());

    if (m_cds.hasAction("Browse")) {
        scheduleNextBrowse();
    }
}

void ContentContainerModel::scheduleNextBrowse()
{
    if (!m_cds.hasAction("Browse")) {
        return;
    }

    Oct::UpnpCp::Action action = m_cds.action("Browse");

    Oct::UpnpCp::ActionRequest actionRequest(m_cds, m_cds.action("Browse"));

    actionRequest.setArgument("ObjectID", m_containerId);
    actionRequest.setArgument("BrowseFlag", "BrowseDirectChildren");
    actionRequest.setArgument("Filter", "*");
    actionRequest.setArgument("StartingIndex", m_currentStartingIndex);
    actionRequest.setArgument("RequestedCount", "0");
    actionRequest.setArgument("SortCriteria", "");

    m_currentExecutor = m_controlPoint->execute(actionRequest);
    if (m_currentExecutor != NULL) {
        connect(m_currentExecutor, SIGNAL( executionFinished(Oct::UpnpCp::ActionExecutor*) ),
                this, SLOT( onExecutionFinished(Oct::UpnpCp::ActionExecutor*) ));
    } else {
        // TODO: warning
    }
}

void ContentContainerModel::onExecutionFinished(Oct::UpnpCp::ActionExecutor* executor)
{
    Q_ASSERT(executor == m_currentExecutor);

    m_currentExecutor = NULL;

    if (executor->state() == Oct::UpnpCp::ActionExecutor::EXECUTION_STATE_SUCCEEDED) {
        QString result = executor->outputArgument("Result").toString();
        int numberReturned = executor->outputArgument("NumberReturned").toInt(); //TODO: ok
        int totalMatches = executor->outputArgument("TotalMatches").toInt(); //TODO: ok
        int updateID = executor->outputArgument("UpdateID").toInt(); //TODO: ok

        Logger.trace(this) << "Execution succeeded: " << numberReturned << " " << totalMatches << " " << updateID;

        if (totalMatches == 0) {
            Logger.trace(this) << "Browse finished. No items.";
        } else if (totalMatches > 0 && numberReturned > 0) {
            processDocument(result);

            int nextBrowseIndex = m_currentStartingIndex + numberReturned;

            if (nextBrowseIndex == totalMatches) {
                Logger.trace(this) << "Browse finished. Total matches: " << totalMatches;
            } else if (nextBrowseIndex > totalMatches) {
                // TODO: warning
            } else {
                m_currentStartingIndex = nextBrowseIndex;

                scheduleNextBrowse();
            }
        } else {
            // TODO: warning
        }
    } else if (executor->state() == Oct::UpnpCp::ActionExecutor::EXECUTION_STATE_FAILED) {
        QMessageBox::warning(NULL, tr("Browse failed"),
                tr("Browse failed.\n\nError code: %1\nError description: %2\n").
                arg(executor->errorCode()).arg(executor->errorDescription()));
    }

    executor->deleteLater();
}

void ContentContainerModel::processDocument(const QString& resultDocument)
{
    QDomDocument document;
    QString errorMsg;
    int errorLine;
    int errorColumn;

    if (! document.setContent(resultDocument, true, &errorMsg, &errorLine, &errorColumn)) {
        Logger.trace(this) << "DIDL-Lite processing failed. Error: " << errorMsg << " " << errorLine << ":" << errorColumn;
        return;
    }

    QDomElement didlElement = document.documentElement();
    if (didlElement.namespaceURI() != DIDL_LITE_NAMESPACE || didlElement.localName() != DIDL_LITE_LOCAL_NAME) {
        Logger.trace(this) << "DIDL-Lite processing failed. Error: Not a DIDL-Lite document";
        return;
    }

    for (QDomElement objectElement = didlElement.firstChildElement();
            !objectElement.isNull();
            objectElement = objectElement.nextSiblingElement()) {

        if (objectElement.namespaceURI() != DIDL_LITE_NAMESPACE) {
            Logger.trace(this) << "Invalid object: " << objectElement.namespaceURI() << " " << objectElement.localName();
            continue;
        }

        QString id = objectElement.attribute("id");
        if (id.isNull()) {
            Logger.trace(this) << "Invalid object: No ID attribute";
            continue;
        }

        QDomElement titleElement;
        for (titleElement = objectElement.firstChildElement();
                !titleElement.isNull();
                titleElement = titleElement.nextSiblingElement()) {
            if (titleElement.namespaceURI() == DC_NAMESPACE && titleElement.localName() == TITLE_LOCAL_NAME) {
                break;
            }
        }
        if (titleElement.isNull()) {
            Logger.trace(this) << "Invalid object: No title element";
            continue;
        }
        QString title = titleElement.text().trimmed();
        if (title.isEmpty()) {
            Logger.trace(this) << "Invalid object: Empty title element";
            continue;
        }

        if (objectElement.localName() == CONTAINER_LOCAL_NAME) {
            appendRow(new ContentContainerItem(m_cds, id, title));
        } else if (objectElement.localName() == ITEM_LOCAL_NAME) {
            QDomElement resElement;
            for (resElement = objectElement.firstChildElement();
                    !resElement.isNull();
                    resElement = resElement.nextSiblingElement()) {
                if (resElement.namespaceURI() == DIDL_LITE_NAMESPACE && resElement.localName() == RES_LOCAL_NAME) {
                    break;
                }
            }

            QString resURI;

            if (!resElement.isNull()) {
                resURI = resElement.text();
            }

            appendRow(new ContentItemItem(m_cds, id, title, resURI));
        } else {
            Logger.trace(this) << "Invalid object: " << objectElement.namespaceURI() << " " << objectElement.localName();
        }
    }
}

AVPlayerModel* ContentContainerModel::createChildView(QModelIndex index)
{
    AVPlayerItem* item = (AVPlayerItem*)itemFromIndex(index);
    if (item != NULL) {
        if (item->type() == AVPlayerItem::TYPE_CONTENT_CONTAINER) {
            ContentContainerItem* ccItem = (ContentContainerItem*)item;
            return new ContentContainerModel(m_controlPoint, m_cds, ccItem->containerId(), this);
        } else if (item->type() == AVPlayerItem::TYPE_CONTENT_ITEM) {
            // TODO
        }
    }
    return NULL;
}
