/*
 * MediaServerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef CONTENT_CONTAINER_ITEM_H_
#define CONTENT_CONTAINER_ITEM_H_

#include "AVPlayerItem.h"

#include <upnpcp/Device.h>

class ContentContainerItem : public AVPlayerItem
{
private:
    Oct::UpnpCp::Service m_cdService;
    QString m_containerId;

public:
    ContentContainerItem(Oct::UpnpCp::Service cdService, const QString& containerId, const QString& title);

    QString containerId() const
    {
        return m_containerId;
    }
};

#endif /* CONTENT_CONTAINER_ITEM_H_ */
