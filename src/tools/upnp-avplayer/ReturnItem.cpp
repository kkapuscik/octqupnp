/*
 * MediaServerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "ReturnItem.h"

ReturnItem::ReturnItem() :
    AVPlayerItem(TYPE_RETURN_ITEM, MAJOR_TYPE_RETURN_ITEM)
{
    setText("..");
}
