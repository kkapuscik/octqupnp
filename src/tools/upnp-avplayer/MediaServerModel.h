/*
 * UpnpDeviceModel.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef MEDIASERVERMODEL_H
#define MEDIASERVERMODEL_H

#include "ContentContainerModel.h"
#include "MediaServerItem.h"

#include <log/Logger.h>
#include <upnpcp/Device.h>

class MediaServerModel : public ContentContainerModel
{
    Q_OBJECT

private:
    static Oct::Log::Logger<MediaServerModel> Logger;

private:
    Oct::UpnpCp::Device m_device;

public:
    MediaServerModel(Oct::UpnpCp::ControlPoint* controlPoint, MediaServerItem* item, QObject* parent = 0);
};

#endif /* MEDIASERVERMODEL_H */
