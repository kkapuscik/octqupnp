/*
 * MediaStatusWidget.cpp
 *
 *  Created on: 02-03-2011
 *      Author: saveman
 */

#include "MediaStatusWidget.h"

#include <QtCore/QTimer>
#include <QtWidgets/QBoxLayout>

MediaStatusWidget::MediaStatusWidget(QWidget* parent) :
    QWidget(parent), m_mediaObject(NULL)
{
    QHBoxLayout* mainLayout = new QHBoxLayout(this);

    m_playbackTimeLabel = new QLabel(this);
    m_playbackStatusLabel = new QLabel(this);
    m_bufferStatusLabel = new QLabel(this);

    mainLayout->addWidget(m_playbackTimeLabel);
    mainLayout->addStretch(1);
    mainLayout->addWidget(m_playbackStatusLabel);
    mainLayout->addStretch(1);
    mainLayout->addWidget(m_bufferStatusLabel);

    QTimer* refreshTimer = new QTimer(this);
    refreshTimer->setSingleShot(false);
    refreshTimer->setInterval(500);

    connect(refreshTimer, SIGNAL( timeout() ), this, SLOT( onRefreshTimer() ));

    refreshTimer->start();
}

void MediaStatusWidget::setMediaObject(Phonon::MediaObject* mediaObject)
{
    /* disconnect old */
    if (m_mediaObject != NULL) {
        disconnect(m_mediaObject, SIGNAL( stateChanged(Phonon::State,Phonon::State) ),
                this, SLOT( mediaStateChanged(Phonon::State,Phonon::State) ));
        connect(m_mediaObject, SIGNAL( bufferStatus(int) ),
                this, SLOT( mediaBufferStatus(int) ));
    }

    // TODO: register to object delete

    /* store object */
    m_mediaObject = mediaObject;

    /* connect new */
    if (m_mediaObject != NULL) {
        connect(m_mediaObject, SIGNAL( stateChanged(Phonon::State,Phonon::State) ),
                this, SLOT( mediaStateChanged(Phonon::State,Phonon::State) ));
        connect(m_mediaObject, SIGNAL( bufferStatus(int) ),
                this, SLOT( mediaBufferStatus(int) ));
    }
}

QString MediaStatusWidget::stateToName(Phonon::State state) const
{
    switch (state) {
        case Phonon::LoadingState:
            return tr("Loading");
        case Phonon::StoppedState:
            return tr("Stopped");
        case Phonon::PlayingState:
            return tr("Playing");
        case Phonon::BufferingState:
            return tr("Buffering");
        case Phonon::PausedState:
            return tr("Paused");
        case Phonon::ErrorState:
            return tr("Error");
        default:
            return tr("Unknown");
    }
}

QString MediaStatusWidget::timeToString(qint64 time) const
{
    if (time >= 0) {
        qint64 hour = time / (1000*60*60) % 1;
        qint64 mins = time / (1000*60) % 60;
        qint64 secs = time / (1000) % 60;
        return QString("%1:%2:%3").
                arg(hour, 0, 10).
                arg(mins, 2, 10, QChar('0')).
                arg(secs, 2, 10, QChar('0'));
    } else {
        return QString("-:--:--");
    }
}

void MediaStatusWidget::onRefreshTimer()
{
    QString previousText = m_playbackTimeLabel->text();

    qint64 currTime;
    qint64 totalTime;

    if (m_mediaObject != NULL) {
        currTime = m_mediaObject->currentTime();
        totalTime = m_mediaObject->totalTime();
    } else {
        currTime = -1;
        totalTime = -1;
    }

    QString currTimeStr = timeToString(currTime);
    QString totalTimeStr = timeToString(totalTime);

    QString newText = QString("%1 / %2").arg(currTimeStr).arg(totalTimeStr);

    if (newText != previousText) {
        m_playbackTimeLabel->setText(newText);
    }
}

void MediaStatusWidget::mediaBufferStatus(int percentFilled)
{
    if (percentFilled < 0) {
        percentFilled = 0;
    } else if (percentFilled > 100) {
        percentFilled = 100;
    }

    m_bufferStatusLabel->setText(QString("%1%").arg(percentFilled));
}

void MediaStatusWidget::mediaStateChanged(Phonon::State newstate, Phonon::State oldstate)
{
    m_playbackStatusLabel->setText(stateToName(newstate));
}
