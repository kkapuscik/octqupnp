/*
 * MediaServerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef RETURNITEM_H_
#define RETURNITEM_H_

#include "AVPlayerItem.h"

class ReturnItem : public AVPlayerItem
{
public:
    ReturnItem(void);
};

#endif /* RETURNITEM_H_ */
