/*
 * MediaStatusWidget.h
 *
 *  Created on: 02-03-2011
 *      Author: saveman
 */

#ifndef MEDIASTATUSWIDGET_H_
#define MEDIASTATUSWIDGET_H_

#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>

#include <phonon/MediaObject>

class MediaStatusWidget : public QWidget
{
    Q_OBJECT

private:
    QLabel* m_playbackTimeLabel;
    QLabel* m_playbackStatusLabel;
    QLabel* m_bufferStatusLabel;

    Phonon::MediaObject* m_mediaObject;

public:
    MediaStatusWidget(QWidget* parent = 0);

public slots:
    void setMediaObject(Phonon::MediaObject* mediaObject);

private slots:
    void onRefreshTimer();
    void mediaBufferStatus(int percentFilled);
    void mediaStateChanged(Phonon::State newstate, Phonon::State oldstate);

private:
    QString stateToName(Phonon::State state) const;
    QString timeToString(qint64 time) const;

};

#endif /* MEDIASTATUSWIDGET_H_ */
