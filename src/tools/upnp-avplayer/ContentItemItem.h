/*
 * MediaServerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef CONTENT_ITEM_ITEM_H_
#define CONTENT_ITEM_ITEM_H_

#include "AVPlayerItem.h"

#include <upnpcp/Device.h>

class ContentItemItem : public AVPlayerItem
{
private:
    Oct::UpnpCp::Service m_cdService;
    QString m_itemId;

public:
    ContentItemItem(Oct::UpnpCp::Service cdService, const QString& itemId, const QString& title, const QString& uri = QString());

    QString itemId() const
    {
        return m_itemId;
    }
};

#endif /* CONTENT_ITEM_ITEM_H_ */
