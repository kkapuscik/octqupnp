/*
 * MediaVideoPlayerWidget.cpp
 *
 *  Created on: 05-03-2011
 *      Author: saveman
 */

#include "MediaVideoPlayerWidget.h"

#include <phonon/VideoWidget>

Oct::Log::Logger<MediaVideoPlayerWidget> MediaVideoPlayerWidget::Logger("/oct/tools/avplayer/MediaVideoPlayerWidget");

MediaVideoPlayerWidget::MediaVideoPlayerWidget(QWidget* parent) :
    Phonon::VideoPlayer(parent)
{
    /* create toggle full screen action */
    m_toggleFullScreenAction = new QAction(tr("Full Screen"), this);
    m_toggleFullScreenAction->setCheckable(true);
    m_toggleFullScreenAction->setChecked(false);

    /* create aspect ratio actions */
    m_aspectRatioGroup = new QActionGroup(this);
    addAspectRatioAction(tr("Auto"), Phonon::VideoWidget::AspectRatioAuto);
    addAspectRatioAction(tr("Window"), Phonon::VideoWidget::AspectRatioWidget);
    addAspectRatioAction(tr("4:3"), Phonon::VideoWidget::AspectRatio4_3);
    addAspectRatioAction(tr("16:9"), Phonon::VideoWidget::AspectRatio16_9);

    /* create scale mode actions */
    m_scaleModeGroup = new QActionGroup(this);
    addScaleModeAction(tr("Fit in View"), Phonon::VideoWidget::FitInView);
    addScaleModeAction(tr("Scale & Crop"), Phonon::VideoWidget::ScaleAndCrop);

    m_videoWidgetContextMenu = new QMenu(this);
    m_videoWidgetContextMenu->addAction(m_toggleFullScreenAction);
    m_videoWidgetContextMenu->addSeparator();
    QMenu* aspectRatioMenu = m_videoWidgetContextMenu->addMenu(tr("Aspect Ratio"));
    aspectRatioMenu->addActions(m_aspectRatioGroup->actions());
    QMenu* scaleModeMenu = m_videoWidgetContextMenu->addMenu(tr("Scale Mode"));
    scaleModeMenu->addActions(m_scaleModeGroup->actions());

    /* setup video widget context menu */
    videoWidget()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(videoWidget(), SIGNAL( customContextMenuRequested(const QPoint&) ), this,
            SLOT( showVideoPlayerContextMenu(const QPoint&) ));
    connect(m_aspectRatioGroup, SIGNAL( triggered(QAction*) ), this,
            SLOT( setAspectRatio(QAction*) ));
    connect(m_scaleModeGroup, SIGNAL( triggered(QAction*) ), this,
            SLOT( setScaleMode(QAction*) ));

    connect(m_toggleFullScreenAction, SIGNAL( triggered(bool) ), this, SLOT( setFullScreen(bool) ));
}

void MediaVideoPlayerWidget::addAspectRatioAction(const QString& text, Phonon::VideoWidget::AspectRatio ratio)
{
    Phonon::VideoWidget::AspectRatio currentAspectRatio = videoWidget()->aspectRatio();

    QAction* aspectAction = m_aspectRatioGroup->addAction(text);
    aspectAction->setCheckable(true);
    aspectAction->setData((int)ratio);
    aspectAction->setChecked(ratio == currentAspectRatio);
}

void MediaVideoPlayerWidget::addScaleModeAction(const QString& text, Phonon::VideoWidget::ScaleMode mode)
{
    Phonon::VideoWidget::ScaleMode currentScaleMOde = videoWidget()->scaleMode();

    QAction* aspectAction = m_scaleModeGroup->addAction(text);
    aspectAction->setCheckable(true);
    aspectAction->setData((int)mode);
    aspectAction->setChecked(mode == currentScaleMOde);
}

void MediaVideoPlayerWidget::setAspectRatio(QAction* action)
{
    Phonon::VideoWidget::AspectRatio ratio = (Phonon::VideoWidget::AspectRatio)action->data().toInt();
    videoWidget()->setAspectRatio(ratio);
}

void MediaVideoPlayerWidget::setScaleMode(QAction* action)
{
    Phonon::VideoWidget::ScaleMode mode = (Phonon::VideoWidget::ScaleMode)action->data().toInt();
    videoWidget()->setScaleMode(mode);
}

void MediaVideoPlayerWidget::setFullScreen(bool enable)
{
    Logger.trace(this) << "setFullScreen pos=" << enable;

    videoWidget()->setFullScreen(enable);

    m_toggleFullScreenAction->setChecked(videoWidget()->isFullScreen());
}

void MediaVideoPlayerWidget::showVideoPlayerContextMenu(const QPoint& pos)
{
    Logger.trace(this) << "showVideoPlayerContextMenu pos=" << pos.x() << " " << pos.y();

    m_videoWidgetContextMenu->exec(videoWidget()->mapToGlobal(pos));
}
