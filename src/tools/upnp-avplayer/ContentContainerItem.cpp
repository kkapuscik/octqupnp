/*
 * MediaServerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "ContentContainerItem.h"

ContentContainerItem::ContentContainerItem(Oct::UpnpCp::Service cdService, const QString& containerId,
        const QString& title) :
    AVPlayerItem(TYPE_CONTENT_CONTAINER, MAJOR_TYPE_CONTAINER), m_cdService(cdService), m_containerId(containerId)
{
    setText(title);
}
