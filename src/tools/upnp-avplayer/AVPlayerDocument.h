/*
 * AVPlayerDocument.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef AVPLAYERDOCUMENT_H_
#define AVPLAYERDOCUMENT_H_

/*---------------------------------------------------------------------------*/

#include <upnpcp/ControlPoint.h>
#include <QtCore/QObject>
#include <QtCore/QStack>
#include <QtCore/QAbstractItemModel>

/*---------------------------------------------------------------------------*/

class ControlPointModel;
class AVPlayerModel;

class AVPlayerDocument : public QObject
{
    Q_OBJECT

private:
    Oct::UpnpCp::ControlPoint* m_controlPoint;
    ControlPointModel* m_rootModel;
    QStack<AVPlayerModel*> m_modelStack;

public:
    AVPlayerDocument(QObject* parent = 0);
    virtual ~AVPlayerDocument();

    void start();

    QAbstractItemModel* currentModel() const;

    QVariant getMajorType(QModelIndex index);
    void enterContainer(QModelIndex index);
    void exitContainer(QModelIndex index);
    QString mediaURI(QModelIndex index);

signals:
    void modelChanged(QAbstractItemModel* model);
};

/*---------------------------------------------------------------------------*/

#endif /* AVPLAYERDOCUMENT_H_ */
