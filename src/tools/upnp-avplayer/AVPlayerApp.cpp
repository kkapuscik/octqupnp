/*
 * AVPlayerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "AVPlayerApp.h"

#include <QtCore/QResource>

AVPlayerApp::AVPlayerApp(int& argc, char** argv) :
    Oct::App::GuiApplication(argc, argv)
{
    // nothing to do
}

int AVPlayerApp::initResources()
{
    Q_INIT_RESOURCE(AVPlayer);

    return 0;
}

int AVPlayerApp::initWindows()
{
    m_mainWindow = new AVPlayerMainWindow();
    m_mainWindow->show();

    return 0;
}

QMainWindow* AVPlayerApp::mainWindow()
{
    return m_mainWindow;
}
