/*
 * AVPlayerDocument.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "AVPlayerDocument.h"

#include "ControlPointModel.h"
#include "AVPlayerModel.h"
#include "AVPlayerItem.h"

/*---------------------------------------------------------------------------*/

AVPlayerDocument::AVPlayerDocument(QObject* parent) :
    QObject(parent)
{
    m_controlPoint = new Oct::UpnpCp::ControlPoint(this);

    m_rootModel = new ControlPointModel(m_controlPoint, this);
    m_modelStack.push(m_rootModel);

    // TODO Auto-generated constructor stub
}

AVPlayerDocument::~AVPlayerDocument()
{
    // nothing to do
}

QAbstractItemModel* AVPlayerDocument::currentModel() const
{
    return m_modelStack.top();
}

void AVPlayerDocument::start()
{
    m_controlPoint->start();
}

void AVPlayerDocument::enterContainer(QModelIndex index)
{
    AVPlayerModel* model = m_modelStack.top();
    AVPlayerModel* childModel = model->createChildView(index);
    if (childModel != NULL) {
        m_modelStack.push(childModel);
        emit modelChanged(childModel);
    }
}

void AVPlayerDocument::exitContainer(QModelIndex index)
{
    if (m_modelStack.size() > 1) {
        AVPlayerModel* model = m_modelStack.pop();
        AVPlayerModel* topModel = m_modelStack.top();

        emit modelChanged(topModel);

        model->deleteLater();
    }
}

QString AVPlayerDocument::mediaURI(QModelIndex index)
{
    AVPlayerModel* model = m_modelStack.top();

    return model->data(index, AVPlayerItem::ROLE_MEDIA_URI).toString();
}

QVariant AVPlayerDocument::getMajorType(QModelIndex index)
{
    AVPlayerModel* model = m_modelStack.top();

    return model->data(index, AVPlayerItem::ROLE_MAJOR_TYPE);
}
