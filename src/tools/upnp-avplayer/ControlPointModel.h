/*
 * UpnpDeviceModel.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef CONTROLPOINTMODEL_H
#define CONTROLPOINTMODEL_H

#include "AVPlayerModel.h"

#include <upnpcp/ControlPoint.h>
#include <log/Logger.h>

class ControlPointModel : public AVPlayerModel
{
    Q_OBJECT

private:
    static Oct::Log::Logger<ControlPointModel> Logger;

private:
    Oct::UpnpCp::ControlPoint* m_controlPoint;

public:
    ControlPointModel(Oct::UpnpCp::ControlPoint* controlPoint, QObject* parent = 0);

    virtual AVPlayerModel* createChildView(QModelIndex index);

private slots:
    void processDeviceAdded(Oct::UpnpCp::Device device);
    void processDeviceRemoved(Oct::UpnpCp::Device device);

};

#endif /* CONTROLPOINTMODEL_H */
