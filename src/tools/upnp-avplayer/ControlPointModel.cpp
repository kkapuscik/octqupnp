/*
 * UpnpDeviceModel.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "ControlPointModel.h"
#include "MediaServerItem.h"
#include "MediaServerModel.h"

Oct::Log::Logger<ControlPointModel> ControlPointModel::Logger("/oct/tools/avplayer/ControlPointModel");

ControlPointModel::ControlPointModel(Oct::UpnpCp::ControlPoint* controlPoint, QObject* parent) :
    AVPlayerModel(parent), m_controlPoint(controlPoint)
{
    connect(m_controlPoint, SIGNAL( deviceAdded(Oct::UpnpCp::Device) ), this,
            SLOT( processDeviceAdded(Oct::UpnpCp::Device) ));
    connect(m_controlPoint, SIGNAL( deviceRemoved(Oct::UpnpCp::Device) ), this,
            SLOT( processDeviceRemoved(Oct::UpnpCp::Device) ));

    // TODO Auto-generated constructor stub

}

void ControlPointModel::processDeviceAdded(Oct::UpnpCp::Device device)
{
    Logger.trace(this) << "Device added: " << device.udn().toString() << " " << device.deviceType() << " "
            << device.friendlyName();

    if (! device.isCompatible("urn:schemas-upnp-org:device:MediaServer:1")) {
        return;
    }

    MediaServerItem* newItem = NULL;

    QList<Oct::UpnpCp::Service> services = device.services();
    for (int i = 0; i < services.size(); ++i) {
        Logger.trace(this) << "Service: " << i << " " << services[i].serviceType() << " " << services[i].serviceId();

        if (services[i].isCompatible("urn:schemas-upnp-org:service:ContentDirectory:1")) {
            newItem = new MediaServerItem(device, services[i]);
            break;
        }
    }

    if (newItem != NULL) {
        appendRow(newItem);
    }
}

void ControlPointModel::processDeviceRemoved(Oct::UpnpCp::Device device)
{
    Logger.trace(this) << "Device removed: " << device.udn().toString() << " " << device.deviceType() << " "
            << device.friendlyName();

    for (int i = 0; i < rowCount(); ++i) {
        MediaServerItem* item = (MediaServerItem*)itemFromIndex(index(i, 0));
        if (item->device() == device) {
            removeRow(i);
            break;
        }
    }
}

AVPlayerModel* ControlPointModel::createChildView(QModelIndex index)
{
    MediaServerItem* item = (MediaServerItem*)itemFromIndex(index);
    if (item != NULL) {
        return new MediaServerModel(m_controlPoint, item, this);
    } else {
        return NULL;
    }
}
