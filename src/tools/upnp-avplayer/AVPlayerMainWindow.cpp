#include "AVPlayerMainWindow.h"

#include "AVPlayerItem.h" // TODO: move major type somewhere else
#include "ViewImageDialog.h"

#include <app/GuiApplication.h>

/*---------------------------------------------------------------------------*/

Oct::Log::Logger<AVPlayerMainWindow> AVPlayerMainWindow::Logger("/oct/tools/avplayer/AVPlayerMainWindow");

/*---------------------------------------------------------------------------*/

AVPlayerMainWindow::AVPlayerMainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_ui.setupUi(this);

    m_document = new AVPlayerDocument(this);

    m_ui.m_browseListView->setModel(m_document->currentModel());

    connect(m_document, SIGNAL( modelChanged(QAbstractItemModel*) ),
            this, SLOT( processModelChanged(QAbstractItemModel*) ));

    m_document->start();
}

void AVPlayerMainWindow::processListDoubleClick(QModelIndex index)
{
    QVariant majorTypeVariant = m_document->getMajorType(index);
    bool ok;
    int majorType = majorTypeVariant.toInt(&ok);

    if (ok) {
        switch (majorType) {
            case AVPlayerItem::MAJOR_TYPE_CONTAINER:
                m_document->enterContainer(index);
                break;
            case AVPlayerItem::MAJOR_TYPE_MEDIA: {
                QString uri = m_document->mediaURI(index);

                // TODO:

                if (!uri.isEmpty()) {
                    ViewImageDialog* dialog = new ViewImageDialog(this);
                    dialog->setAttribute(Qt::WA_DeleteOnClose, true);
                    dialog->setURI(uri);

                    dialog->setVisible(true);
                }
                break;
            }
            case AVPlayerItem::MAJOR_TYPE_RETURN_ITEM:
                m_document->exitContainer(index);
                break;
            default:
                break;
        }
    }
}

void AVPlayerMainWindow::processModelChanged(QAbstractItemModel* model)
{
    m_ui.m_browseListView->setModel(model);
}

void AVPlayerMainWindow::appAbout()
{
    Oct::App::GuiApplication::showAboutDialog(this);
}

void AVPlayerMainWindow::qtAbout()
{
    Oct::App::GuiApplication::showAboutQtDialog(this);
}
