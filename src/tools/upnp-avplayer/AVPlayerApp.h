/*
 * AVPlayerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef _AV_PLAYER__AV_PLAYER_APP_H_
#define _AV_PLAYER__AV_PLAYER_APP_H_

#include "AVPlayerMainWindow.h"

#include <app/GuiApplication.h>

class AVPlayerApp : public Oct::App::GuiApplication
{
    Q_OBJECT

private:
    AVPlayerMainWindow* m_mainWindow;

public:
    /**
     * Constructs application.
     *
     * @param argc
     *      Arguments count (from main function).
     * @param argv
     *      Arguments array (from main function).
     */
    AVPlayerApp(int& argc, char** argv);

protected:
    virtual int initResources();

    virtual int initWindows();

    virtual QMainWindow* mainWindow();

};

#endif /* _AV_PLAYER__AV_PLAYER_APP_H_ */
