/*
 * MediaServerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "MediaServerItem.h"

MediaServerItem::MediaServerItem(Oct::UpnpCp::Device dmsDevice, Oct::UpnpCp::Service cdService) :
    AVPlayerItem(TYPE_MEDIA_SERVER, MAJOR_TYPE_CONTAINER), m_dmsDevice(dmsDevice), m_cdService(cdService)
{
    setText(m_dmsDevice.friendlyName());
}
