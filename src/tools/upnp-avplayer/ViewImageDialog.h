#ifndef VIEWIMAGEDIALOG_H
#define VIEWIMAGEDIALOG_H

#include "ui_ViewImageDialog.h"
#include "MediaVideoPlayerWidget.h"

#include <log/Logger.h>
#include <QtWidgets/QDialog>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

#include <phonon/VideoPlayer>
#include <phonon/MediaObject>

class ViewImageDialog : public QDialog
{
    Q_OBJECT

private:
    static Oct::Log::Logger<ViewImageDialog> Logger;

private:
    Ui::ViewImageDialogClass m_ui;
    QUrl m_url;
    QNetworkAccessManager* m_netManager;
    QNetworkReply* m_netReply;
    bool m_netReplyFinished;
    QByteArray m_imageData;

    MediaVideoPlayerWidget* m_videoPlayer;
    Phonon::VideoPlayer* m_audioPlayer;

public:
    ViewImageDialog(QWidget *parent = 0);

    void setURI(const QString& uri);

private:
    void getMediaInfo();
    void getImage();
    QString stateToName(Phonon::State state) const;

private slots:
    void onGetReadyRead();
    void onGetFinished();
    void onGetError(QNetworkReply::NetworkError code);

    void onHeadFinished();
    void onHeadError(QNetworkReply::NetworkError code);

    void videoStateChanged(Phonon::State newstate, Phonon::State oldstate);
    void audioStateChanged(Phonon::State newstate, Phonon::State oldstate);
};

#endif // VIEWIMAGEDIALOG_H
