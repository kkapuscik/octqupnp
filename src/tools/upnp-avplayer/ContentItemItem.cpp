/*
 * MediaServerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "ContentItemItem.h"

ContentItemItem::ContentItemItem(Oct::UpnpCp::Service cdService, const QString& itemId,
        const QString& title, const QString& resURI) :
    AVPlayerItem(TYPE_CONTENT_ITEM, MAJOR_TYPE_MEDIA), m_cdService(cdService), m_itemId(itemId)
{
    setText(title);
    if (!resURI.isEmpty()) {
        setData(resURI, ROLE_MEDIA_URI);
    }
}
