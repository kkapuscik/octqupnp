#include "ViewImageDialog.h"
#include "MediaControlWidget.h"
#include "MediaStatusWidget.h"

#include <QtNetwork/QNetworkReply>
#include <QtWidgets/QImageReader>
#include <QtWidgets/QMessageBox>
#include <QtCore/QBuffer>

#include <phonon/AudioOutput>
#include <phonon/VideoPlayer>

Oct::Log::Logger<ViewImageDialog> ViewImageDialog::Logger("/oct/tools/avplayer/ViewImageDialog");

ViewImageDialog::ViewImageDialog(QWidget *parent) :
    QDialog(parent)
{
    m_netManager = new QNetworkAccessManager(this);
    m_netReply = NULL;

    m_ui.setupUi(this);

    m_videoPlayer = new MediaVideoPlayerWidget(this);
    MediaControlWidget* m_videoMediaControlWidget = new MediaControlWidget(this);
    MediaStatusWidget* m_videoMediaStatusWidget = new MediaStatusWidget(this);

    m_videoMediaControlWidget->setMediaObject(m_videoPlayer->mediaObject());
    m_videoMediaControlWidget->setAudioOutput(m_videoPlayer->audioOutput());
    m_videoMediaStatusWidget->setMediaObject(m_videoPlayer->mediaObject());

    QVBoxLayout* videoLayout = new QVBoxLayout(m_ui.m_videoPage);
    videoLayout->addWidget(m_videoPlayer, 1);
    videoLayout->addSpacing(5);
    videoLayout->addWidget(m_videoMediaControlWidget, 0);
    videoLayout->addSpacing(5);
    videoLayout->addWidget(m_videoMediaStatusWidget, 0);

    m_audioPlayer = new Phonon::VideoPlayer(Phonon::MusicCategory, this);
    MediaControlWidget* m_audioMediaControlWidget = new MediaControlWidget(this);
    MediaStatusWidget* m_audioMediaStatusWidget = new MediaStatusWidget(this);

    m_audioMediaControlWidget->setMediaObject(m_audioPlayer->mediaObject());
    m_audioMediaControlWidget->setAudioOutput(m_audioPlayer->audioOutput());
    m_audioMediaStatusWidget->setMediaObject(m_audioPlayer->mediaObject());

    QVBoxLayout* audioLayout = new QVBoxLayout(m_ui.m_audioPage);
    audioLayout->addWidget(m_audioPlayer, 1);
    audioLayout->addSpacing(5);
    audioLayout->addWidget(m_audioMediaControlWidget, 0);
    audioLayout->addSpacing(5);
    audioLayout->addWidget(m_audioMediaStatusWidget, 0);

    Phonon::MediaObject* videoMediaObject = m_videoPlayer->mediaObject();
    Phonon::MediaObject* audioMediaObject = m_audioPlayer->mediaObject();

    connect(videoMediaObject, SIGNAL( stateChanged(Phonon::State,Phonon::State) ),
            this, SLOT( videoStateChanged(Phonon::State,Phonon::State) ));

    connect(audioMediaObject, SIGNAL( stateChanged(Phonon::State,Phonon::State) ),
            this, SLOT( audioStateChanged(Phonon::State,Phonon::State) ));

    m_ui.m_contentStackWidget->setCurrentWidget(m_ui.m_imagePage);
}

void ViewImageDialog::onGetReadyRead()
{
    Logger.trace(this) << "onReadyRead";

    if (m_netReply == NULL) {
        return;
    }

    QByteArray newData = m_netReply->readAll();

    Logger.trace(this) << "onReadyRead: " << newData.size();

    m_imageData.append(newData);

    if (m_netReplyFinished) {
        QBuffer buffer(&m_imageData);

        buffer.open(QBuffer::ReadOnly);

        QImageReader* imageReader = new QImageReader(&buffer);
        QImage image = imageReader->read();
        if (image.isNull()) {
            return;
        }

        m_ui.m_imageLabel->setText("");
        //        m_ui.label->setPixmap(QPixmap::fromImage(image));
        m_ui.m_imageLabel->setPixmap(QPixmap::fromImage(image).scaled(
                m_ui.m_imageLabel->width(), m_ui.m_imageLabel->height()));
    }
}

void ViewImageDialog::onGetError(QNetworkReply::NetworkError code)
{
    Logger.trace(this) << "onError: " << code << " " << m_netReply->errorString();

    m_netReply->deleteLater();
    m_netReply = NULL;
}

void ViewImageDialog::onGetFinished()
{
    Logger.trace(this) << "onFinished";

    m_netReplyFinished = true;
    onGetReadyRead();

    m_netReply->deleteLater();
    m_netReply = NULL;
}

void ViewImageDialog::onHeadError(QNetworkReply::NetworkError code)
{
    Logger.trace(this) << "onError: " << code << " " << m_netReply->errorString();

    m_netReply->deleteLater();
    m_netReply = NULL;
}

void ViewImageDialog::onHeadFinished()
{
    Logger.trace(this) << "onHeadFinished";

    if (m_netReply == NULL) {
        return;
    }

    m_netReplyFinished = true;

    // TODO:
#if 0
    enum KnownHeaders {
        ContentTypeHeader,
        ContentLengthHeader,
        LocationHeader,
        LastModifiedHeader,
        CookieHeader,
        SetCookieHeader
    };
#endif
    QString contentType = m_netReply->header(QNetworkRequest::ContentTypeHeader).toString().toLower();

    m_netReply->deleteLater();
    m_netReply = NULL;

    Logger.trace(this) << "onHeadFinished: " << contentType;

    if (contentType.startsWith("video/")) {
        m_audioPlayer->stop();

        m_ui.m_contentStackWidget->setCurrentWidget(m_ui.m_videoPage);

        m_videoPlayer->mediaObject()->setCurrentSource(m_url);
        m_videoPlayer->play();
    } else if (contentType.startsWith("audio/")) {
        m_videoPlayer->stop();

        m_ui.m_contentStackWidget->setCurrentWidget(m_ui.m_audioPage);

        m_audioPlayer->mediaObject()->setCurrentSource(m_url);
        m_audioPlayer->play();
    } else if (contentType.startsWith("image/")) {
        m_ui.m_contentStackWidget->setCurrentWidget(m_ui.m_imagePage);

        getImage();
    } else {
        QMessageBox::information(this, tr("Media Viewer"), tr("Unknown content type:\n- %1").arg(contentType));
    }
}

void ViewImageDialog::getImage()
{
    if (m_netReply != NULL) {
        m_netReply->deleteLater();
        m_netReply = NULL;
    }

    Logger.trace(this) << "URL: " << m_url.toString();

    QNetworkRequest request(m_url);

    m_netReply = m_netManager->get(request);
    if (m_netReply == NULL) {
        return;
    }

    m_netReplyFinished = false;
    m_imageData.clear();

    connect(m_netReply, SIGNAL( finished() ), this, SLOT( onGetFinished() ));
    connect(m_netReply, SIGNAL( readyRead() ), this, SLOT( onGetReadyRead() ));
    connect(m_netReply, SIGNAL( error(QNetworkReply::NetworkError) ), this,
            SLOT( onGetError(QNetworkReply::NetworkError) ));

    Logger.trace(this) << "GetImage: Net reply: " << m_netReply;
}

void ViewImageDialog::getMediaInfo()
{
    if (m_netReply) {
        m_netReply->deleteLater();
        m_netReply = NULL;
    }

    Logger.trace(this) << "URL: " << m_url.toString();

    QNetworkRequest request(m_url);

    m_netReply = m_netManager->head(request);
    if (m_netReply == NULL) {
        return;
    }

    m_netReplyFinished = false;
    m_imageData.clear();

    connect(m_netReply, SIGNAL( finished() ), this, SLOT( onHeadFinished() ));
    connect(m_netReply, SIGNAL( error(QNetworkReply::NetworkError) ), this,
            SLOT( onHeadError(QNetworkReply::NetworkError) ));

    Logger.trace(this) << "GetMediaInfo: Net reply: " << m_netReply;
}

void ViewImageDialog::setURI(const QString& uri)
{
    Logger.trace(this) << "Set URI: " << uri;

    if (uri.contains('%')) {
        m_url.setEncodedUrl(uri.toLatin1(), QUrl::TolerantMode);
    } else {
        m_url.setUrl(uri);
    }

    Logger.trace(this) << "URL: " << m_url.host() << " - " << m_url.port() << " - " << m_url.path() << " - "
            << m_url.hasQuery();

    getMediaInfo();
}

void ViewImageDialog::videoStateChanged(Phonon::State newstate, Phonon::State oldstate)
{
    Logger.trace(this) << "videoStateChanged() " << stateToName(oldstate) << " -> " << stateToName(newstate);
    if (newstate == Phonon::ErrorState) {
        Logger.trace(this) << "videoStateChanged() ERROR: " << m_videoPlayer->mediaObject()->errorType() << " " << m_videoPlayer->mediaObject()->errorString();
    }
}

void ViewImageDialog::audioStateChanged(Phonon::State newstate, Phonon::State oldstate)
{
    Logger.trace(this) << "audioStateChanged() " << stateToName(oldstate) << " -> " << stateToName(newstate);
    if (newstate == Phonon::ErrorState) {
        Logger.trace(this) << "audioStateChanged() ERROR: " << m_audioPlayer->mediaObject()->errorType() << " " << m_audioPlayer->mediaObject()->errorString();
    }
}

QString ViewImageDialog::stateToName(Phonon::State state) const
{
    switch (state) {
        case Phonon::LoadingState:
            return QString("Loading");
        case Phonon::StoppedState:
            return QString("Stopped");
        case Phonon::PlayingState:
            return QString("Playing");
        case Phonon::BufferingState:
            return QString("Buffering");
        case Phonon::PausedState:
            return QString("Paused");
        case Phonon::ErrorState:
            return QString("Error");
        default:
            return QString("Unknown");
    }
}
