/*
 * UpnpDeviceModel.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef CONTENTCONTEINERMODEL_H
#define CONTENTCONTEINERMODEL_H

#include "AVPlayerModel.h"

#include <log/Logger.h>
#include <upnpcp/ControlPoint.h>
#include <upnpcp/Service.h>

class ContentContainerModel : public AVPlayerModel
{
    Q_OBJECT

private:
    static Oct::Log::Logger<ContentContainerModel> Logger;

private:
    Oct::UpnpCp::ControlPoint* m_controlPoint;
    Oct::UpnpCp::Service m_cds;
    QString m_containerId;
    Oct::UpnpCp::ActionExecutor* m_currentExecutor;
    int m_currentStartingIndex;

public:
    ContentContainerModel(Oct::UpnpCp::ControlPoint* controlPoint, Oct::UpnpCp::Service cds, const QString& containerId, QObject* parent = 0);

    virtual AVPlayerModel* createChildView(QModelIndex index);

private:
    void scheduleNextBrowse();
    void processDocument(const QString& resultDocument);

private slots:
    void onExecutionFinished(Oct::UpnpCp::ActionExecutor* executor);

};

#endif /* CONTENTCONTEINERMODEL_H */
