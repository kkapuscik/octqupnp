/*
 * AVPlayerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "AVPlayerItem.h"

AVPlayerItem::IconsMap AVPlayerItem::sm_iconsMap;

AVPlayerItem::AVPlayerItem(int type, int majorType) :
    QStandardItem()
{
    setData(type, ROLE_TYPE);
    setData(majorType, ROLE_MAJOR_TYPE);
    setIcon(getIcon(type));
}

QIcon AVPlayerItem::getIcon(int type)
{
    if (sm_iconsMap.size() == 0) {
        sm_iconsMap.insert(TYPE_MEDIA_SERVER, QIcon(":/octupnp/avplayer/icons/device.svg"));
        sm_iconsMap.insert(TYPE_RETURN_ITEM, QIcon(":/octupnp/avplayer/icons/return.svg"));
        sm_iconsMap.insert(TYPE_CONTENT_CONTAINER, QIcon(":/octupnp/avplayer/icons/container.svg"));
        sm_iconsMap.insert(TYPE_CONTENT_ITEM, QIcon(":/octupnp/avplayer/icons/item.svg"));
    }

    if (sm_iconsMap.contains(type)) {
        return sm_iconsMap[type];
    } else {
        return sm_iconsMap[-1];
    }
}
