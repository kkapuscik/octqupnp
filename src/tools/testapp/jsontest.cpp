/*
 * jsontest.cpp
 *
 *  Created on: 17-04-2011
 *      Author: saveman
 */

#include <util/SharedDataPointer.h>
#include <json/JsonDecoder.h>
#include <json/JsonEncoder.h>

static const char object1[] = {
"{\n"
"\"img\": \"http://imgs.xkcd.com/comics/convincing_pickup_line.png\",\n"
"\"title\": \"Convincing Pickup Line\",\n"
"\"month\": \"3\",\n"
"\"num\": 403,\n"
"\"link\": \"\",\n"
"\"year\": \"2008\",\n"
"\"news\": \"\",\n"
"\"safe_title\": \"Convincing Pickup Line\",\n"
"\"transcript\": \"[[A couple sit at the small table of a cafe.  The woman holds up a graph.]]\\nWoman: We're a terrible match.  But if we sleep together, it'll make the local hookup network a symmetric graph.\\nMan: I can't argue with that.\\n{{Title text: Check it out; I've had sex with someone who's had sex with someone who's written a paper with Paul Erd&#337;s!}}\",\n"
"\"alt\": \"Check it out; I've had sex with someone who's had sex with someone who's written a paper with Paul Erd\\u00c5\\u0091s!\",\n"
"\"day\": \"31\"\n"
"}\n"
};

static const char object2[] = {
"{\n"
"\"img\": \"http://imgs.xkcd.com/comics/convincing_pickup_line.png\",\n"
"\"title\": \"Convincing Pickup Line\",\n"
"\"month\": \"3\",\n"
"\"num\": \"403\",\n"
"\"link\": \"\",\n"
"\"year\": \"2008\",\n"
"\"news\": \"\",\n"
"\"safe_title\": \"Convincing Pickup Line\",\n"
"\"transcript\": \"[[A couple sit at the small table of a cafe.  The woman holds up a graph.]]\\nWoman: We're a terrible match.  But if we sleep together, it'll make the local hookup network a symmetric graph.\\nMan: I can't argue with that.\\n{{Title text: Check it out; I've had sex with someone who's had sex with someone who's written a paper with Paul Erd&#337;s!}}\",\n"
"\"alt\": \"Check it out; I've had sex with someone who's had sex with someone who's written a paper with Paul Erd\\u00c5\\u0091s!\",\n"
"\"day\": \"31\"\n"
"}\n"
};

#include <QtCore/QDebug>

void test_json()
{
    qDebug() << "Input: " << object1;

    Oct::Json::JsonObject obj1 = Oct::Json::JsonDecoder::decode(QByteArray(object1));
    qDebug() << "Json object: isNull=" << obj1.isNull() << " memberCount=" << obj1.memberCount();

    if (!obj1.isNull()) {
        QByteArray data1 = Oct::Json::JsonEncoder::encode(obj1, true);
        qDebug() << "Json encoded: " << QString::fromUtf8(data1.data(), data1.size());
    }

    qDebug() << "Input: " << object2;

    Oct::Json::JsonObject obj2 = Oct::Json::JsonDecoder::decode(QByteArray(object2));
    qDebug() << "Json object: isNull=" << obj2.isNull() << " memberCount=" << obj2.memberCount();

    if (!obj2.isNull()) {
        QByteArray data2 = Oct::Json::JsonEncoder::encode(obj2, true);
        qDebug() << "Json encoded: " << QString::fromUtf8(data2.data(), data2.size());
    }
}
