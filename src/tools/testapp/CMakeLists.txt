cmake_minimum_required(VERSION 2.8.11)

project(oct-testapp)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

include_directories(${Qt5Core_INCLUDE_DIRS})
include_directories(${Qt5Widgets_INCLUDE_DIRS})

file(GLOB oct-testapp_SOURCES *.cpp)
file(GLOB oct-testapp_HEADERS *.h)
file(GLOB oct-testapp_UIS *.ui)

QT5_WRAP_CPP(oct-testapp_MOCSOURCES ${oct-testapp_HEADERS})
QT5_WRAP_UI(oct-testapp_UIHEADERS ${oct-testapp_UIS})
#macro QT5_ADD_RESOURCES(outfiles inputfile ... OPTIONS ...)

include_directories(${CMAKE_CURRENT_BINARY_DIR}) # for UIS

add_executable(oct-testapp ${oct-testapp_SOURCES} ${oct-testapp_MOCSOURCES} ${oct-testapp_UIHEADERS})
target_link_libraries(oct-testapp octlog octupnpcp octhttpserver octjson Qt5::Core Qt5::Widgets)
