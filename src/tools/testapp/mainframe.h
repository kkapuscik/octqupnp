/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup TestApp
 * @{
 */

#ifndef MAINFRAME_H
#define MAINFRAME_H

#include <ui_mainframe.h>

#include <upnpssdp/DiscoverySocket.h>
#include <upnpcp/ControlPoint.h>
#include <httpserver/Server.h>

#include <QtWidgets/QMainWindow>

class MainFrame : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainFrameClass m_ui;
    Oct::UpnpSsdp::DiscoverySocket* m_ssdpSocket;
    Oct::UpnpCp::ControlPoint* m_controlPoint;
    Oct::HttpServer::Server* m_server;

public:
    MainFrame(QWidget *parent = 0);
    ~MainFrame();

protected slots:
    void appAbout();
    void logClear();
    void sendSearch();
    void logAppendLine(const QString& message, bool addNewLine = true);

    void readyReadHandler();
    void aboutToCloseHandler();
    void readChannelFinishedHandler();

    void processSsdpSearchResponse(
            const QHostAddress& sender,
            quint16 senderPort,
            const Oct::UpnpSsdp::SearchResponseMessage& message);
    void readPendingDatagrams();
};

#endif // MAINFRAME_H

/**
 * @}
 * @}
 * @}
 */
