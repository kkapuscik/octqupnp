/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "mainframe.h"

#include <QtWidgets/QMessageBox>
#include <QtCore/QFile>

static QTcpSocket* theSocket;

void MainFrame::aboutToCloseHandler()
{
    logAppendLine("aboutToCloseHandler() called");
}

void MainFrame::readChannelFinishedHandler()
{
    logAppendLine("readChannelFinishedHandler() called");
}

void MainFrame::readyReadHandler()
{
    char buffer[100];

    for (;;) {
        char buffer[1024];

        qint64 avail = theSocket->bytesAvailable();
        bool atEnd = theSocket->atEnd();

        qint64 readRv = theSocket->read(buffer, sizeof(buffer));

        qint64 avail2 = theSocket->bytesAvailable();
        bool atEnd2 = theSocket->atEnd();

        bool isOpen = theSocket->isOpen();
        bool isReadable = theSocket->isReadable();

        logAppendLine(QString("readyReadHandler() avail1=%1 atEnd1=%2 readRv=%3 avail2=%4 atEnd2=%5 isOpen=%6 isReadable=%7")
                .arg(avail).arg(atEnd).arg(readRv).arg(avail2).arg(atEnd2).arg(isOpen).arg(isReadable));
        if (readRv > 0) {
            QString val = QString::fromLatin1(buffer, readRv);
            logAppendLine(QString("Bytes read: %1").arg(val));
        }

        if (readRv <= 0) {
            break;
        }
    }

    logAppendLine("readyReadHandler() exited");
}

MainFrame::MainFrame(QWidget *parent)
    : QMainWindow(parent)
{
    m_ui.setupUi(this);

    logAppendLine(tr("octUPnP started"));

    m_ssdpSocket = new Oct::UpnpSsdp::DiscoverySocket(this);
#if 0
    if (theSocket->joinSsdpGroup()) {
        logAppendLine(tr("Socket joined SSDP multicast group."));

        connect(theSocket, SIGNAL(searchResponseReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchResponseMessage&)),
                this,      SLOT(processSsdpSearchResponse(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchResponseMessage&)));
//        connect(theSocket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
    } else {
        logAppendLine(tr("Cannot join SSDP group."));
    }
#endif

    m_controlPoint = new Oct::UpnpCp::ControlPoint(this);
#if 0
    m_controlPoint->start();
#endif

    m_server = new Oct::HttpServer::Server(this);
    m_server->setPort(8000);
    m_server->start();

    theSocket = new QTcpSocket(this);
    connect(theSocket, SIGNAL( readyRead() ), this, SLOT( readyReadHandler() ));
    connect(theSocket, SIGNAL( aboutToClose() ), this, SLOT( aboutToCloseHandler() ));
    connect(theSocket, SIGNAL( readChannelFinished() ), this, SLOT( readChannelFinishedHandler() ));

    theSocket->connectToHost(QHostAddress("80.252.0.145"), 80);
    if (theSocket->waitForConnected()) {
        logAppendLine("Socket connected");

        theSocket->write(QByteArray("GET / HTTP/1.1\r\n"));
        theSocket->write(QByteArray("HOST: 80.252.0.145\r\n"));
//        theSocket->write(QByteArray("Connection: close\r\n"));
        theSocket->write(QByteArray("\r\n"));
    } else {
        logAppendLine("Socket not connected");
    }

}

MainFrame::~MainFrame()
{
}

void MainFrame::sendSearch()
{
    m_controlPoint->sendSearch();

    if (m_ssdpSocket != NULL && m_ssdpSocket->sendSearch()) {
        logAppendLine(tr("Search message sent."));
    } else {
        logAppendLine(tr("Cannot send search message."));
    }
}

void MainFrame::appAbout()
{
    QMessageBox::about(this, tr("&About"), tr("octUPnP 0.1\n\nCopyright (C) 2010 Krzysztof Kapuscik"));

    extern void test_json();
    test_json();

}

void MainFrame::logClear()
{
    m_ui.logTextEdit->setPlainText(QString::null);
}

void MainFrame::logAppendLine(const QString& message, bool addNewLine)
{
    m_ui.logTextEdit->appendPlainText(message);
    if (addNewLine) {
        m_ui.logTextEdit->appendPlainText(QString("\n"));
    }
}

void MainFrame::processSsdpSearchResponse(
        const QHostAddress& sender,
        quint16 senderPort,
        const Oct::UpnpSsdp::SearchResponseMessage& /*message*/)
{
    logAppendLine(tr("Search response message received from %1:%2.").arg(sender.toString()).arg(senderPort));
}

void MainFrame::readPendingDatagrams()
{
    while (m_ssdpSocket->hasPendingDatagrams()) {
        QHostAddress sender;
        quint16 senderPort;

        Oct::HttpUdp::Message message = m_ssdpSocket->readMessage(&sender, &senderPort);

        if (message.isValid()) {
            logAppendLine(tr("Received message from %1:%2.").arg(sender.toString()).arg(senderPort));
        }

#if 0
        QByteArray datagram;
        QHostAddress sender;
        quint16 senderPort;

        datagram.resize(theSocket->pendingDatagramSize());

        if (theSocket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort) >= 0) {
            logAppendLine(tr("Received packet from %1:%2. Length: %3.").arg(sender.toString()).arg(senderPort).arg(datagram.size()));
        }
#endif
    }
}
