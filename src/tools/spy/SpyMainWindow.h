/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYMAINWINDOW_H
#define SPYMAINWINDOW_H

#include "ui_SpyMainWindow.h"
#include "SpyDocument.h"

#include <log/Logger.h>

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>

class SpyMainWindow : public QMainWindow
{
    Q_OBJECT

private:
    static Oct::Log::Logger<SpyMainWindow> Logger;

private:
    typedef QMap<int,QMenu*> MenuMap;

private:
    Ui::SpyMainWindowClass m_ui;
    SpyDocument* m_spyDocument;
    MenuMap m_popupMenus;
    QAction* m_actionActionExecute;

public:
    SpyMainWindow(QWidget *parent = 0);

private slots:
    void about();
    void aboutQt();
    void currentTreeItemChanged(const QModelIndex& current, const QModelIndex& previous);
    void treeContextMenuRequested(QPoint point);
};

#endif // SPYMAINWINDOW_H

/**
 * @}
 * @}
 * @}
 */
