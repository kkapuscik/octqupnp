/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyDocument.h"

SpyDocument::SpyDocument(QObject* parent) :
    QObject(parent)
{
    m_controlPoint = new Oct::UpnpCp::ControlPoint(this);
    m_spyTreeModel = new SpyTreeModel(m_controlPoint, this);
    m_propertyTableModel = new PropertyTableModel(this);

    connect(m_controlPoint, SIGNAL( rootDeviceAdded(Oct::UpnpCp::Device) ),
            m_spyTreeModel, SLOT( addRootDevice(Oct::UpnpCp::Device) ));
    connect(m_controlPoint, SIGNAL( rootDeviceRemoved(Oct::UpnpCp::Device) ),
            m_spyTreeModel, SLOT( removeRootDevice(Oct::UpnpCp::Device) ));

    m_controlPoint->start();
}

SpyDocument::~SpyDocument()
{
    // nothing to do
}

QAbstractItemModel* SpyDocument::treeModel()
{
    return m_spyTreeModel;
}

QAbstractTableModel* SpyDocument::propertyModel()
{
    return m_propertyTableModel;
}

void SpyDocument::updateProperties(const QModelIndex& treeItemIndex)
{
    SpyTreeItem* item = (SpyTreeItem*)m_spyTreeModel->itemFromIndex(treeItemIndex);
    m_propertyTableModel->fillProperties(item);
}

SpyExecuteActionData* SpyDocument::executionData(const QModelIndex& treeModelIndex)
{
    return m_spyTreeModel->executionData(treeModelIndex);
}
