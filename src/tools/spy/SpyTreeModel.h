/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYTREEMODEL_H_
#define SPYTREEMODEL_H_

#include "SpyTreeItemControlPoint.h"
#include "SpyExecuteActionData.h"

#include <log/Logger.h>
#include <upnpcp/Device.h>

#include <QtGui/QStandardItemModel>

class SpyTreeModel : public QStandardItemModel
{
    Q_OBJECT

private:
    static Oct::Log::Logger<SpyTreeModel> Logger;

private:
    Oct::UpnpCp::ControlPoint* m_controlPoint;
    SpyTreeItemControlPoint* m_controlPointItem;

public:
    SpyTreeModel(Oct::UpnpCp::ControlPoint* controlPoint, QObject* parent = 0);
    virtual ~SpyTreeModel();

    SpyExecuteActionData* executionData(const QModelIndex& index);

public slots:
    void addRootDevice(Oct::UpnpCp::Device device);
    void removeRootDevice(Oct::UpnpCp::Device device);

};

#endif /* SPYTREEMODEL_H_ */

/**
 * @}
 * @}
 * @}
 */
