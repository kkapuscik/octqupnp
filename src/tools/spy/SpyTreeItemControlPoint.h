/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPY_TREE_ITEM_CONTROL_POINT_H
#define SPY_TREE_ITEM_CONTROL_POINT_H

#include "SpyTreeItem.h"
#include "SpyTreeItemDevice.h"

#include <upnpcp/Device.h>

class SpyTreeItemControlPoint : public SpyTreeItem
{
public:
    SpyTreeItemControlPoint();

    void addRootDevice(Oct::UpnpCp::Device device);
    void removeRootDevice(Oct::UpnpCp::Device device);

    virtual QList<ItemProperty> properties() const;

private:
    QStandardItem* rootDeviceItem(Oct::UpnpCp::Device device);
};

#endif /* SPY_TREE_ITEM_CONTROL_POINT_H */

/**
 * @}
 * @}
 * @}
 */
