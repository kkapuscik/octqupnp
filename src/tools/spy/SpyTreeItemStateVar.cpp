/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyTreeItemStateVar.h"

SpyTreeItemStateVar::SpyTreeItemStateVar(const Oct::UpnpCp::StateVariable& stateVar) :
    SpyTreeItem(StateVar),
    m_stateVar(stateVar)
{
    setText(m_stateVar.name());
}

QList<ItemProperty> SpyTreeItemStateVar::properties() const
{
    QList<ItemProperty> propList;

    m_stateVar.name();

    propList.append(ItemProperty("Name", m_stateVar.name()));
    propList.append(ItemProperty("Send events", m_stateVar.sendEvents() ? "Yes" : "No"));
    propList.append(ItemProperty("Data type", m_stateVar.dataType()));

    if (! m_stateVar.defaultValue().isNull()) {
        propList.append(ItemProperty("Default value", m_stateVar.defaultValue().toString()));
    }
    if (! m_stateVar.minimumValue().isNull()) {
        propList.append(ItemProperty("Minimum value", m_stateVar.minimumValue().toString()));
    }
    if (! m_stateVar.maximumValue().isNull()) {
        propList.append(ItemProperty("Maximum value", m_stateVar.maximumValue().toString()));
    }
    if (! m_stateVar.stepValue().isNull()) {
        propList.append(ItemProperty("Step value", m_stateVar.stepValue().toString()));
    }
    QStringList values = m_stateVar.allowedValues();
    for (int i = 0; i < values.size(); ++i) {
        propList.append(ItemProperty(
                QString("Allowed value %1").arg(i),
                values.at(i)));
    }

    return propList;
}
