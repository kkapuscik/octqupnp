/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyMainWindow.h"

#include "SpyExecuteActionDialog.h"

#include <app/GuiApplication.h>

Oct::Log::Logger<SpyMainWindow> SpyMainWindow::Logger("/oct/tools/spy/SpyMainWindow");

SpyMainWindow::SpyMainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_ui.setupUi(this);

    m_spyDocument = new SpyDocument(this);

    m_ui.m_spyTreeView->setModel(m_spyDocument->treeModel());
    m_ui.m_spyTreeView->expandToDepth(1);

    m_ui.m_propertyTableView->setModel(m_spyDocument->propertyModel());

    connect(m_ui.m_spyTreeView->selectionModel(),
            SIGNAL( currentChanged(const QModelIndex&, const QModelIndex&) ),
            this,
            SLOT( currentTreeItemChanged(const QModelIndex&, const QModelIndex&) ));

    QMenu* actionPopupMenu = new QMenu(this);
    m_actionActionExecute = actionPopupMenu->addAction("&Execute");

    m_popupMenus.insert(Action, actionPopupMenu);
}

void SpyMainWindow::about()
{
    Oct::App::GuiApplication::showAboutDialog(this);
}

void SpyMainWindow::aboutQt()
{
    Oct::App::GuiApplication::showAboutQtDialog(this);
}

void SpyMainWindow::currentTreeItemChanged(const QModelIndex& current, const QModelIndex& /*previous*/)
{
    m_spyDocument->updateProperties(current);
}

void SpyMainWindow::treeContextMenuRequested(QPoint point)
{
    QModelIndex index = m_ui.m_spyTreeView->indexAt(point);

    QAbstractItemModel* model = m_ui.m_spyTreeView->model();

    QVariant data = model->data(index, SpyDocument::ItemTypeRole);

    if (data.type() == QVariant::Int) {
        if (m_popupMenus.contains(data.toInt())) {
            QMenu* popupMenu = m_popupMenus[data.toInt()];

            QPoint globalPoint = m_ui.m_spyTreeView->mapToGlobal(point);

            QAction* action = popupMenu->exec(globalPoint);

            Logger.trace(this) << "Action selected: " << action;

            if (action == m_actionActionExecute) {
                SpyExecuteActionData* data = m_spyDocument->executionData(index);
                if (data != NULL) {
                    SpyExecuteActionDialog* dialog = new SpyExecuteActionDialog(this, data);
                    dialog->show();
                } else {
                    // TODO
                }
            }
        }
    }
}
