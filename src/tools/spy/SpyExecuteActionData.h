/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYEXECUTEACTIONDATA_H_
#define SPYEXECUTEACTIONDATA_H_

#include <upnpcp/ControlPoint.h>
#include <upnpcp/Service.h>
#include <upnpcp/Types.h>

class SpyExecuteActionData
{
private:
    Oct::UpnpCp::ControlPoint* m_controlPoint;
    Oct::UpnpCp::Service m_service;
    Oct::UpnpCp::Action m_action;

public:
    SpyExecuteActionData(Oct::UpnpCp::ControlPoint* controlPoint, const Oct::UpnpCp::Service& service, const Oct::UpnpCp::Action& action) :
        m_controlPoint(controlPoint), m_service(service), m_action(action)
    {
        // do nothing
    }

    Oct::UpnpCp::ControlPoint* controlPoint() const
    {
        return m_controlPoint;
    }

    const Oct::UpnpCp::Service& service() const
    {
        return m_service;
    }

    const Oct::UpnpCp::Action& action() const
    {
        return m_action;
    }
};

#endif /* SPYEXECUTEACTIONDATA_H_ */

/**
 * @}
 * @}
 * @}
 */
