/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYTREEITEMDEVICE_H_
#define SPYTREEITEMDEVICE_H_

#include "SpyTreeItem.h"

#include <upnpcp/Device.h>

class SpyTreeItemDevice : public SpyTreeItem
{
private:
    Oct::UpnpCp::Device m_device;

public:
    SpyTreeItemDevice(const Oct::UpnpCp::Device& device);

    const Oct::UpnpCp::Device& device() const
    {
        return m_device;
    }

    virtual QList<ItemProperty> properties() const;

};

#endif /* SPYTREEITEMDEVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
