/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyTreeItemService.h"

#include "SpyTreeItemStateVarTable.h"
#include "SpyTreeItemAction.h"

SpyTreeItemService::SpyTreeItemService(
        const Oct::UpnpCp::Service& service) :
    SpyTreeItem(Service), m_service(service)
{
    setText(service.serviceType());

    appendRow(new SpyTreeItemStateVarTable(service));

    QList<Oct::UpnpCp::Action> actions = service.actions();
    for (int i = 0; i < actions.size(); ++i) {
        appendRow(new SpyTreeItemAction(actions.at(i)));
    }
}

QList<ItemProperty> SpyTreeItemService::properties() const
{
    QList<ItemProperty> propList;

    propList.append(ItemProperty("Type", m_service.serviceType()));
    propList.append(ItemProperty("ID", m_service.serviceId()));
    propList.append(ItemProperty("SCPD URL", m_service.scpdURL().toString()));
    propList.append(ItemProperty("Control URL", m_service.controlURL().toString()));
    propList.append(ItemProperty("Event URL", m_service.eventURL().toString()));

    return propList;
}
