/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyTreeItemAction.h"

SpyTreeItemAction::SpyTreeItemAction(const Oct::UpnpCp::Action& action) :
    SpyTreeItem(Action),
    m_action(action)
{
    setText(action.name());
}

QList<ItemProperty> SpyTreeItemAction::properties() const
{
    QList<ItemProperty> propList;

    propList.append(ItemProperty("Name", m_action.name()));

    QList<Oct::UpnpCp::ActionArgument> args = m_action.arguments();
    for (int i = 0; i < args.size(); ++i) {
        QString typeString;

        switch (args.at(i).type()) {
            case Oct::UpnpCp::ActionArgument::ACTION_ARGUMENT_TYPE_INPUT:
                typeString = "Input";
                break;
            case Oct::UpnpCp::ActionArgument::ACTION_ARGUMENT_TYPE_OUTPUT:
                typeString = "Output";
                break;
            case Oct::UpnpCp::ActionArgument::ACTION_ARGUMENT_TYPE_RETVAL:
                typeString = "Output (RetVal)";
                break;
            default:
                Q_ASSERT(0);
                typeString = "Invalid";
                break;
        }

        propList.append(ItemProperty(
                args.at(i).name(),
                QString("%1 / %2").arg(typeString).arg(args.at(i).relatedStateVariable())));
    }

    return propList;
}
