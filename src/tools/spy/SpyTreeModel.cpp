/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyTreeModel.h"

#include "SpyTreeItemAction.h"
#include "SpyTreeItemService.h"

#include <upnpcp/ActionExecutor.h>

Oct::Log::Logger<SpyTreeModel> SpyTreeModel::Logger("/oct/tools/spy/SpyTreeModel");

SpyTreeModel::SpyTreeModel(Oct::UpnpCp::ControlPoint* controlPoint, QObject* parent) :
    QStandardItemModel(parent), m_controlPoint(controlPoint)
{
    m_controlPointItem = new SpyTreeItemControlPoint();

    setColumnCount(1);
    setHeaderData(0, Qt::Horizontal, "Device Spy Tree", Qt::DisplayRole);

    insertRow(0, m_controlPointItem);
}

SpyTreeModel::~SpyTreeModel()
{
    // nothing to do
}

void SpyTreeModel::addRootDevice(Oct::UpnpCp::Device device)
{
    Logger.trace(this) << "Add root device: " << device.udn().toString();

    m_controlPointItem->addRootDevice(device);
}

void SpyTreeModel::removeRootDevice(Oct::UpnpCp::Device device)
{
    Logger.trace(this) << "Remove root device: " << device.udn().toString();

    m_controlPointItem->removeRootDevice(device);
}

SpyExecuteActionData* SpyTreeModel::executionData(const QModelIndex& index)
{
    QStandardItem* item = itemFromIndex(index);
    if (item != NULL && item->type() == Action) {
        QStandardItem* parent = item->parent();
        if (parent != NULL && parent->type() == Service) {
            SpyTreeItemService* serviceItem = (SpyTreeItemService*)parent;
            SpyTreeItemAction* actionItem = (SpyTreeItemAction*)item;

            return new SpyExecuteActionData(m_controlPoint, serviceItem->service(), actionItem->action());
        }
    }

    return NULL;
}
