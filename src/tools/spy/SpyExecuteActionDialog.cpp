/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyExecuteActionDialog.h"

#include <QtWidgets/QSplitter>
#include <QtWidgets/QMessageBox>

Oct::Log::Logger<SpyExecuteActionDialog> SpyExecuteActionDialog::Logger("/oct/tools/spy/SpyExecuteActionDialog");

SpyExecuteActionDialog::SpyExecuteActionDialog(QWidget *parent, SpyExecuteActionData* data) :
    QDialog(parent), m_data(data), m_executor(NULL)
{
    Q_ASSERT(m_data != NULL);

    m_ui.setupUi(this);
    m_ui.m_deviceNameLabel->setText(m_data->service().device().friendlyName());
    m_ui.m_serviceNameLabel->setText(m_data->service().serviceType());
    m_ui.m_actionNameLabel->setText(m_data->action().name());

    QGridLayout* argsLayout = new QGridLayout(m_ui.m_argumentsScrollContents);
    m_ui.m_argumentsScrollContents->setLayout(argsLayout);

    QList<Oct::UpnpCp::ActionArgument> args = m_data->action().arguments();
    for (int i = 0; i < args.length(); ++i) {
        Oct::UpnpCp::StateVariable stateVar = m_data->service().stateVariable(args[i].relatedStateVariable());
        ArgumentEntry entry(args[i]);

        QLabel* label = new QLabel(args[i].name(), m_ui.m_argumentsScrollContents);

        QWidget* editWidget = NULL;
        int factor = 0;

        if (stateVar.dataType() == "string") {
            if (stateVar.allowedValues().size() == 0) {
                entry.m_textEdit = new QPlainTextEdit(m_ui.m_argumentsScrollContents);
                entry.m_textEdit->setReadOnly(!args[i].isInput());

                factor = 1;

                editWidget = entry.m_textEdit;
            } else {
                if (args[i].isInput()) {
                    entry.m_comboBox = new QComboBox(m_ui.m_argumentsScrollContents);
                    entry.m_comboBox->insertItems(0, stateVar.allowedValues());
                    entry.m_comboBox->setCurrentIndex(0);

                    editWidget = entry.m_comboBox;
                } else {
                    entry.m_lineEdit = new QLineEdit(m_ui.m_argumentsScrollContents);
                    entry.m_lineEdit->setValidator(new QIntValidator(entry.m_lineEdit));
                    entry.m_lineEdit->setReadOnly(true);

                    editWidget = entry.m_lineEdit;
                }
            }
        } else {
            entry.m_lineEdit = new QLineEdit(m_ui.m_argumentsScrollContents);
            entry.m_lineEdit->setValidator(new QIntValidator(entry.m_lineEdit));
            entry.m_lineEdit->setReadOnly(!args[i].isInput());

            editWidget = entry.m_lineEdit;
        }

        Q_ASSERT(editWidget != NULL);

        label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        label->setMinimumWidth(150);

        argsLayout->addWidget(label, i, 0);
        argsLayout->addWidget(editWidget, i, 1);

        m_arguments.append(entry);
    }
}

SpyExecuteActionDialog::~SpyExecuteActionDialog()
{
    delete m_data;
}

void SpyExecuteActionDialog::execute()
{
    if (m_executor != NULL) {
        return;
    }

    Logger.trace(this) << "Executing action";

    m_ui.m_executeButton->setEnabled(false);

    Oct::UpnpCp::ActionRequest actionRequest(m_data->service(), m_data->action());

    for (int i = 0; i < m_arguments.size(); ++i) {
        if (m_arguments[i].m_argument.isInput()) {
            QVariant value;

            if (m_arguments[i].m_comboBox != NULL) {
                value = m_arguments[i].m_comboBox->currentText();
            } else if (m_arguments[i].m_lineEdit != NULL) {
                value = m_arguments[i].m_lineEdit->text();
            } else if (m_arguments[i].m_textEdit != NULL) {
                value = m_arguments[i].m_textEdit->toPlainText();
            } else {
                Q_ASSERT(0);
            }

            actionRequest.setArgument(m_arguments[i].m_argument.name(), value);
        }
    }

    m_executor = m_data->controlPoint()->execute(actionRequest);
    if (m_executor != NULL) {
        connect(m_executor, SIGNAL( executionFinished(Oct::UpnpCp::ActionExecutor*) ), this,
                SLOT ( executionFinished(Oct::UpnpCp::ActionExecutor*) ));

        Logger.trace(this) << "Executor executed: " << m_executor;
    } else {
        QMessageBox::warning(this, tr("Execute Action Failed"), tr("Cannot execute action."));
    }
}

void SpyExecuteActionDialog::executionFinished(Oct::UpnpCp::ActionExecutor* executor)
{
    Q_ASSERT(executor == m_executor);

    Logger.trace(this) << "Executor state: " << ((int) executor->state());

    // TODO: clear all

    if (executor->state() == Oct::UpnpCp::ActionExecutor::EXECUTION_STATE_SUCCEEDED) {
        for (int i = 0; i < m_arguments.size(); ++i) {
            Logger.trace(this) << "Processing argument: " << m_arguments[i].m_argument.name();

            if (! m_arguments[i].m_argument.isInput()) {
                QVariant value = executor->outputArgument(m_arguments[i].m_argument.name());

                if (m_arguments[i].m_comboBox != NULL) {
                    for (int i = 0; i < m_arguments[i].m_comboBox->count(); ++i) {
                        if (value.toString() == m_arguments[i].m_comboBox->itemText(i)) {
                            m_arguments[i].m_comboBox->setCurrentIndex(i);
                            break;
                        }
                    }
                } else if (m_arguments[i].m_lineEdit != NULL) {
                    m_arguments[i].m_lineEdit->setText(value.toString());
                } else if (m_arguments[i].m_textEdit != NULL) {
                    m_arguments[i].m_textEdit->setPlainText(value.toString());
                } else {
                    Q_ASSERT(0);
                }
            }
        }
    } else if (executor->state() == Oct::UpnpCp::ActionExecutor::EXECUTION_STATE_FAILED) {
        QMessageBox::information(this, tr("Execute Action Failed"), tr(
                "Action execution failed.\n\nError: %1\nReason: %2\n").arg(executor->errorCode()).arg(
                executor->errorDescription()));
    } else {
        QMessageBox::critical(this, tr("Execute Action Failed"), tr("Error occured during action execution."));
    }

    m_executor = NULL;
    executor->deleteLater();

    m_ui.m_executeButton->setEnabled(true);
}
