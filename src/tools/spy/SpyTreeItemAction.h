/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYTREEITEMACTION_H_
#define SPYTREEITEMACTION_H_

#include "SpyTreeItem.h"

#include <upnpcp/Types.h>

class SpyTreeItemAction : public SpyTreeItem
{
private:
    Oct::UpnpCp::Action m_action;

public:
    SpyTreeItemAction(const Oct::UpnpCp::Action& action);

    virtual QList<ItemProperty> properties() const;

    const Oct::UpnpCp::Action& action() const
    {
        return m_action;
    }

};

#endif /* SPYTREEITEMACTION_H_ */

/**
 * @}
 * @}
 * @}
 */
