/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYEXECUTEACTIONDIALOG_H
#define SPYEXECUTEACTIONDIALOG_H

#include "ui_SpyExecuteActionDialog.h"

#include "SpyDocument.h"
#include "SpyExecuteActionData.h"

#include <log/Logger.h>

#include <QtWidgets/QDialog>
#include <QtCore/QList>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QComboBox>

class SpyExecuteActionDialog : public QDialog
{
    Q_OBJECT

private:
    static Oct::Log::Logger<SpyExecuteActionDialog> Logger;

private:
    struct ArgumentEntry {
        ArgumentEntry(const Oct::UpnpCp::ActionArgument& argument) :
            m_argument(argument), m_lineEdit(NULL), m_comboBox(NULL), m_textEdit(NULL)
        {
        }

        Oct::UpnpCp::ActionArgument m_argument;
        QLineEdit* m_lineEdit;
        QComboBox* m_comboBox;
        QPlainTextEdit* m_textEdit;
    };

    typedef QList<ArgumentEntry> ArgumentEntryList;

private:
    Ui::SpyExecuteActionDialogClass m_ui;
    SpyExecuteActionData* m_data;
    Oct::UpnpCp::ActionExecutor* m_executor;
    ArgumentEntryList m_arguments;

public:
    SpyExecuteActionDialog(QWidget *parent, SpyExecuteActionData* data);
    virtual ~SpyExecuteActionDialog();

private slots:
    void execute();
    void executionFinished(Oct::UpnpCp::ActionExecutor* executor);
};

#endif // SPYEXECUTEACTIONDIALOG_H
