/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyTreeItemDevice.h"

#include "SpyTreeItemService.h"

SpyTreeItemDevice::SpyTreeItemDevice(const Oct::UpnpCp::Device& device) :
    SpyTreeItem(Device), m_device(device)
{
    setText(device.property(Oct::UpnpCp::Device::PROPERTY_FRIENDLY_NAME));

    QList<Oct::UpnpCp::Device> subdevices = device.subdevices();
    for (int i = 0; i < subdevices.size(); ++i) {
        appendRow(new SpyTreeItemDevice(subdevices.at(i)));
    }

    QList<Oct::UpnpCp::Service> services = device.services();
    for (int i = 0; i < services.size(); ++i) {
        appendRow(new SpyTreeItemService(services.at(i)));
    }
}

QList<ItemProperty> SpyTreeItemDevice::properties() const
{
    QList<ItemProperty> propList;

    propList.append(ItemProperty("UDN", m_device.udn().toString()));
    propList.append(ItemProperty("Location", m_device.location().toString()));

    QList<QString> propKeys = m_device.propertyKeys();
    for (int i = 0; i < propKeys.size(); ++i) {
        propList.append(ItemProperty(propKeys.at(i), m_device.property(propKeys.at(i))));
    }

    return propList;
}
