/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYTREEITEM_H_
#define SPYTREEITEM_H_

#include "ItemProperty.h"
#include "SpyItemType.h"

#include <QtGui/QStandardItem>

class SpyTreeItem : public QStandardItem
{
public:
    SpyTreeItem(SpyItemType type);

    virtual int type() const;

    virtual QList<ItemProperty> properties() const
    {
        return QList<ItemProperty>();
    }

private:
    typedef QMap<int,QIcon> IconsMap;

private:
    static IconsMap sm_iconsMap;

protected:
    QIcon getIcon(int type);

};

#endif /* SPYTREEITEM_H_ */

/**
 * @}
 * @}
 * @}
 */
