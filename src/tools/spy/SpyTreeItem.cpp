/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyTreeItem.h"

#include "SpyDocument.h"

SpyTreeItem::IconsMap SpyTreeItem::sm_iconsMap;

SpyTreeItem::SpyTreeItem(SpyItemType type) :
    QStandardItem()
{
    setData((int)type, SpyDocument::ItemTypeRole);
    setIcon(getIcon(type));
}

int SpyTreeItem::type() const
{
    return data(SpyDocument::ItemTypeRole).toInt();
}

QIcon SpyTreeItem::getIcon(int type)
{
    if (sm_iconsMap.size() == 0) {
        sm_iconsMap.insert(ControlPoint, QIcon(":/octupnp/spy/icons/controlpoint.svg"));
        sm_iconsMap.insert(Device, QIcon(":/octupnp/spy/icons/device.svg"));
        sm_iconsMap.insert(Service, QIcon(":/octupnp/spy/icons/service.svg"));
        sm_iconsMap.insert(StateVarTable, QIcon(":/octupnp/spy/icons/statetable.svg"));
        sm_iconsMap.insert(StateVar, QIcon(":/octupnp/spy/icons/statevar.svg"));
        sm_iconsMap.insert(Action, QIcon(":/octupnp/spy/icons/action.svg"));
        sm_iconsMap.insert(-1, QIcon());
    }

    if (sm_iconsMap.contains(type)) {
        return sm_iconsMap[type];
    } else {
        return sm_iconsMap[-1];
    }
}
