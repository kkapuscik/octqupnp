/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef ITEMPROPERTY_H_
#define ITEMPROPERTY_H_

#include <QtCore/QString>

class ItemProperty
{
private:
    QString m_key;
    QString m_value;

public:
    ItemProperty()
    {
        // do nothing
    }

    ItemProperty(const QString key, const QString& value) :
        m_key(key), m_value(value)
    {
    }

    const QString& key() const
    {
        return m_key;
    }

    const QString& value() const
    {
        return m_value;
    }
};

#endif /* ITEMPROPERTY_H_ */

/**
 * @}
 * @}
 * @}
 */
