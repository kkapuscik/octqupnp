/*
 * AVPlayerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "SpyApp.h"

#include <QtCore/QResource>

SpyApp::SpyApp(int& argc, char** argv) :
    Oct::App::GuiApplication(argc, argv)
{
    // nothing to do
}

int SpyApp::initResources()
{
    Q_INIT_RESOURCE(Spy);

    return 0;
}

int SpyApp::initWindows()
{
    m_mainWindow = new SpyMainWindow();
    m_mainWindow->show();

    return 0;
}

QMainWindow* SpyApp::mainWindow()
{
    return m_mainWindow;
}
