/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyApp.h"

/*---------------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    return Oct::App::GuiApplication::startApp<SpyApp>(
            "UPnP-Spy", "0.1", argc, argv);
}

/*---------------------------------------------------------------------------*/
