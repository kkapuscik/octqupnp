/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYDOCUMENT_H_
#define SPYDOCUMENT_H_

#include "SpyTreeModel.h"
#include "PropertyTableModel.h"

#include <QtCore/QObject>
#include <upnpcp/ControlPoint.h>
#include <upnpcp/ActionExecutor.h>

class SpyDocument : public QObject
{
    Q_OBJECT

public:
    enum Role {
        ItemTypeRole = Qt::UserRole + 1,
        ActionExecutorRole
    };

protected:


private:
    Oct::UpnpCp::ControlPoint* m_controlPoint;
    SpyTreeModel* m_spyTreeModel;
    PropertyTableModel* m_propertyTableModel;

public:
    SpyDocument(QObject* parent = 0);
    virtual ~SpyDocument();

    QAbstractItemModel* treeModel();
    QAbstractTableModel* propertyModel();

    SpyExecuteActionData* executionData(const QModelIndex& treeModelIndex);

    void updateProperties(const QModelIndex& treeItemIndex);

};

#endif /* SPYDOCUMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
