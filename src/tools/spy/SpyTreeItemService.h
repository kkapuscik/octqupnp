/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYTREEITEMSERVICE_H_
#define SPYTREEITEMSERVICE_H_

#include "SpyTreeItem.h"

#include <upnpcp/Service.h>

class SpyTreeItemService : public SpyTreeItem
{
private:
    Oct::UpnpCp::Service m_service;

public:
    SpyTreeItemService(const Oct::UpnpCp::Service& service);

    virtual QList<ItemProperty> properties() const;

    const Oct::UpnpCp::Service& service() const
    {
        return m_service;
    }
};

#endif /* SPYTREEITEMSERVICE_H_ */

/**
 * @}
 * @}
 * @}
 */
