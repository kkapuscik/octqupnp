cmake_minimum_required(VERSION 2.8.11)

project(oct-upnp-spy)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

include_directories(${Qt5Core_INCLUDE_DIRS})
include_directories(${Qt5Widgets_INCLUDE_DIRS})

file(GLOB oct-upnp-spy_SOURCES *.cpp)
file(GLOB oct-upnp-spy_HEADERS *.h)
file(GLOB oct-upnp-spy_UIS *.ui)
file(GLOB oct-upnp-spy_RESOURCES *.qrc)

QT5_WRAP_CPP(oct-upnp-spy_MOCSOURCES ${oct-upnp-spy_HEADERS})
QT5_WRAP_UI(oct-upnp-spy_UIHEADERS ${oct-upnp-spy_UIS})
QT5_ADD_RESOURCES(oct-upnp-spy_RESSRC ${oct-upnp-spy_RESOURCES})

include_directories(${CMAKE_CURRENT_BINARY_DIR}) # for UIS

add_executable(oct-upnp-spy ${oct-upnp-spy_SOURCES} ${oct-upnp-spy_MOCSOURCES} ${oct-upnp-spy_UIHEADERS} ${oct-upnp-spy_RESSRC})
target_link_libraries(oct-upnp-spy octupnpcp octlog octapp Qt5::Core Qt5::Widgets)
