/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYITEMTYPE_H_
#define SPYITEMTYPE_H_

#include <QtGui/QStandardItem>

enum SpyItemType
{
    ControlPoint = QStandardItem::UserType + 1,
    Device,
    Service,
    StateVarTable,
    StateVar,
    Action
};

#endif /* SPYITEMTYPE_H_ */

/**
 * @}
 * @}
 * @}
 */
