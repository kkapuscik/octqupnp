/*
 * AVPlayerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef _SPY__SPY_APP_H_
#define _SPY__SPY_APP_H_

#include "SpyMainWindow.h"

#include <app/GuiApplication.h>

class SpyApp : public Oct::App::GuiApplication
{
    Q_OBJECT

private:
    SpyMainWindow* m_mainWindow;

public:
    /**
     * Constructs application.
     *
     * @param argc
     *      Arguments count (from main function).
     * @param argv
     *      Arguments array (from main function).
     */
    SpyApp(int& argc, char** argv);

protected:
    virtual int initResources();

    virtual int initWindows();

    virtual QMainWindow* mainWindow();

};

#endif /* _SPY__SPY_APP_H_ */
