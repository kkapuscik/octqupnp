/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyTreeItemControlPoint.h"

SpyTreeItemControlPoint::SpyTreeItemControlPoint() :
    SpyTreeItem(ControlPoint)
{
    setText("Control Point");
}

void SpyTreeItemControlPoint::addRootDevice(Oct::UpnpCp::Device device)
{
    Q_ASSERT(rootDeviceItem(device) == NULL);

    appendRow(new SpyTreeItemDevice(device));
}

void SpyTreeItemControlPoint::removeRootDevice(Oct::UpnpCp::Device device)
{
    QStandardItem* item = rootDeviceItem(device);
    if (item != NULL) {
        removeRow(item->row());
    }
}

QStandardItem* SpyTreeItemControlPoint::rootDeviceItem(Oct::UpnpCp::Device device)
{
    for (int i = 0; i < rowCount(); ++i) {
        QStandardItem* item = child(i);
        if (item != NULL && item->type() == Device) {
            SpyTreeItemDevice* deviceItem = (SpyTreeItemDevice*)item;
            if (deviceItem->device() == device) {
                return item;
            }
        }
    }

    return NULL;
}

QList<ItemProperty> SpyTreeItemControlPoint::properties() const
{
    QList<ItemProperty> propList;

    propList.append(ItemProperty("ControlPoint", "Oct UPnP"));

    return propList;
}
