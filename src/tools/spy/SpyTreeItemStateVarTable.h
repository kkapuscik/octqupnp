/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYTREEITEMSTATEVARTABLE_H_
#define SPYTREEITEMSTATEVARTABLE_H_

#include "SpyTreeItem.h"

#include <upnpcp/Service.h>

class SpyTreeItemStateVarTable : public SpyTreeItem
{
public:
    SpyTreeItemStateVarTable(const Oct::UpnpCp::Service& service);

    virtual QList<ItemProperty> properties() const;

};

#endif /* SPYTREEITEMSTATEVARTABLE_H_ */

/**
 * @}
 * @}
 * @}
 */
