/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef PROPERTYTABLEMODEL_H_
#define PROPERTYTABLEMODEL_H_

#include "ItemProperty.h"
#include "SpyTreeItem.h"

#include <log/Logger.h>

#include <QtCore/QAbstractTableModel>
#include <QtCore/QList>

class PropertyTableModel : public QAbstractTableModel
{
    Q_OBJECT

private:
    static Oct::Log::Logger<PropertyTableModel> Logger;

private:
    enum
    {
        COLUMN_KEY = 0,
        COLUMN_VALUE,

        TOTAL_COLUMNS
    };

    typedef QList<ItemProperty> PropertyList;

private:
    PropertyList m_propertyList;

public:
    PropertyTableModel(QObject* parent = 0);
    virtual ~PropertyTableModel();

    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex& index, int role) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    void fillProperties(SpyTreeItem* item);
};

#endif /* PROPERTYTABLEMODEL_H_ */

/**
 * @}
 * @}
 * @}
 */
