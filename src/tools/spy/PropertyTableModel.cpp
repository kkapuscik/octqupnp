/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "PropertyTableModel.h"

Oct::Log::Logger<PropertyTableModel> PropertyTableModel::Logger("/oct/tools/spy/PropertyTableModel");

PropertyTableModel::PropertyTableModel(QObject* parent) :
    QAbstractTableModel(parent)
{
}

PropertyTableModel::~PropertyTableModel()
{
}

int PropertyTableModel::rowCount(const QModelIndex& /*parent*/) const
{
    return m_propertyList.size();
}

int PropertyTableModel::columnCount(const QModelIndex& /*parent*/) const
{
    return TOTAL_COLUMNS;
}

QVariant PropertyTableModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        // TODO
        // Logger.trace(this) << "Invalid index" << index;
        return QVariant();
    }

    if (index.row() >= rowCount() || index.column() >= columnCount()) {
        // TODO
        // Logger.trace(this) << "Index outside range" << index;
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        switch (index.column()) {
            case COLUMN_KEY:
                return m_propertyList.at(index.row()).key();
            case COLUMN_VALUE:
                return m_propertyList.at(index.row()).value();
        }
    }

    return QVariant();
}

QVariant PropertyTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            switch (section) {
                case COLUMN_KEY:
                    return QVariant("Name");
                case COLUMN_VALUE:
                    return QVariant("Value");
            }
        }
    }

    return QVariant();
}

void PropertyTableModel::fillProperties(SpyTreeItem* item)
{
    beginResetModel();

    m_propertyList.clear();

    if (item != NULL) {
        m_propertyList = item->properties();
    }

    endResetModel();
}
