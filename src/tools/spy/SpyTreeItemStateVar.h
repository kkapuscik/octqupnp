/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Spy
 * @{
 */

#ifndef SPYTREEITEMSTATEVAR_H_
#define SPYTREEITEMSTATEVAR_H_

#include "SpyTreeItem.h"

#include <upnpcp/Types.h>

class SpyTreeItemStateVar : public SpyTreeItem
{
private:
    Oct::UpnpCp::StateVariable m_stateVar;

public:
    SpyTreeItemStateVar(const Oct::UpnpCp::StateVariable& stateVar);

    QList<ItemProperty> properties() const;
};

#endif /* SPYTREEITEMSTATEVAR_H_ */

/**
 * @}
 * @}
 * @}
 */
