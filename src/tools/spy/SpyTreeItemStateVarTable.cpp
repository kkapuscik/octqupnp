/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SpyTreeItemStateVarTable.h"

#include "SpyTreeItemStateVar.h"

SpyTreeItemStateVarTable::SpyTreeItemStateVarTable(const Oct::UpnpCp::Service& service) :
    SpyTreeItem(StateVarTable)
{
    setText("State Variables");

    QList<Oct::UpnpCp::StateVariable> variables = service.stateVariables();
    for (int i = 0; i < variables.size(); ++i) {
        appendRow(new SpyTreeItemStateVar(variables.at(i)));
    }
}

QList<ItemProperty> SpyTreeItemStateVarTable::properties() const
{
    QList<ItemProperty> propList;

    propList.append(ItemProperty("State variables", QString("Count: %1").arg(rowCount())));

    return propList;
}
