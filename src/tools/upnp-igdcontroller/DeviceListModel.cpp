/*
 * Copyright (C) 2012 Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "DeviceListModel.h"

/*-------------------------------------------------------------------------------------*/

const QString DeviceListModel::DEVICE_TYPE_IGD("urn:schemas-upnp-org:device:InternetGatewayDevice:1");

DeviceListModel::DeviceListModel(QObject* parent) :
        QAbstractListModel(parent) {
    // nothing
}

DeviceListModel::~DeviceListModel() {
    // nothing
}

void DeviceListModel::addRootDevice(Oct::UpnpCp::Device device) {
    if (device.isCompatible(DEVICE_TYPE_IGD)) {
        beginInsertRows(QModelIndex(), m_deviceList.size(), m_deviceList.size());
        m_deviceList.append(device);
        endInsertRows();
    }
}

void DeviceListModel::removeRootDevice(Oct::UpnpCp::Device device) {
    int index = m_deviceList.indexOf(device);
    if (index >= 0) {
        beginRemoveRows(QModelIndex(), index, index);
        m_deviceList.removeAt(index);
        endRemoveRows();
    }
}

int DeviceListModel::rowCount(const QModelIndex &parent) const {
    return m_deviceList.size();
}

QVariant DeviceListModel::data(const QModelIndex &index, int role) const {
    if (role == Qt::DisplayRole) {
        if (index.column() == 0) {
            int row = index.row();
            if (row >= 0 && row < m_deviceList.size()) {
                return m_deviceList[row].friendlyName() + " (" + m_deviceList[row].location().host() + ")";
            }
        }
    }
    return QVariant();
}

Oct::UpnpCp::Device DeviceListModel::device(int index) {
    if (index >= 0 && index < m_deviceList.size()) {
        return m_deviceList[index];
    } else {
        return Oct::UpnpCp::Device();
    }
}

/*-------------------------------------------------------------------------------------*/
