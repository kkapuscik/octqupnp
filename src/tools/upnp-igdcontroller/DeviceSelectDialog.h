#ifndef DEVICESELECTDIALOG_H
#define DEVICESELECTDIALOG_H

#include "ui_DeviceSelectDialog.h"
#include "Document.h"

#include <QtWidgets/QDialog>

class DeviceSelectDialog : public QDialog {
Q_OBJECT

private:
    Ui::DeviceSelectDialogClass m_ui;
    Document* m_document;

public:
    DeviceSelectDialog(QWidget *parent = 0);
    virtual ~DeviceSelectDialog();

    void setDocument(Document* document);

private:
    int getSelectedIndex() const;

private slots:
    void processSelectionChange(const QItemSelection & selected, const QItemSelection & deselected);
    void selectDevice();
};

#endif // DEVICESELECTDIALOG_H
