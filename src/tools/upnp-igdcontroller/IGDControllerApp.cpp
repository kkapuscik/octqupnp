#include "IGDControllerApp.h"

#include <QtCore/QResource>

IGDControllerApp::IGDControllerApp(int& argc, char** argv) :
    Oct::App::GuiApplication(argc, argv)
{
    // nothing to do
}

int IGDControllerApp::initResources()
{
    Q_INIT_RESOURCE(IGDController);

    return 0;
}

int IGDControllerApp::initWindows()
{
    m_mainWindow = new IGDControllerMainWindow();
    m_mainWindow->show();

    return 0;
}

QMainWindow* IGDControllerApp::mainWindow()
{
    return m_mainWindow;
}
