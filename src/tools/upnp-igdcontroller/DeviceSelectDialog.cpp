#include "DeviceSelectDialog.h"

DeviceSelectDialog::DeviceSelectDialog(QWidget *parent) :
        QDialog(parent), m_document(0) {
    m_ui.setupUi(this);
    m_ui.m_selectButton->setEnabled(false);
}

DeviceSelectDialog::~DeviceSelectDialog() {

}

void DeviceSelectDialog::setDocument(Document* document) {
    Q_ASSERT(!m_document);
    Q_ASSERT(document);

    m_document = document;

    m_ui.m_devicesList->setModel(m_document->getDeviceListModel());

    connect(m_ui.m_devicesList->selectionModel(),
            SIGNAL( selectionChanged(const QItemSelection&, const QItemSelection&) ), this,
            SLOT( processSelectionChange(const QItemSelection&, const QItemSelection&) ));

}

int DeviceSelectDialog::getSelectedIndex() const {
    QModelIndexList indexes = m_ui.m_devicesList->selectionModel()->selectedRows(0);
    if (indexes.size() == 1) {
        return indexes[0].row();
    }
    return -1;
}

void DeviceSelectDialog::processSelectionChange(const QItemSelection& selected, const QItemSelection& deselected) {
    int selectedIndex = getSelectedIndex();

    m_ui.m_selectButton->setEnabled(selectedIndex >= 0);
}

void DeviceSelectDialog::selectDevice() {
    int selectedIndex = getSelectedIndex();

    if (selectedIndex >= 0) {
        m_document->setActiveDevice(selectedIndex);
        accept();
    }
}
