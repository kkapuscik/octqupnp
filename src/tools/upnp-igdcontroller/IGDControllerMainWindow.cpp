#include "IGDControllerMainWindow.h"

#include <app/GuiApplication.h>

IGDControllerMainWindow::IGDControllerMainWindow(QWidget *parent)
    : QMainWindow(parent)
{
	m_ui.setupUi(this);

	m_deviceSelectDialog = new DeviceSelectDialog(this);
    m_deviceSelectDialog->setModal(false);

    m_document = new Document(this);

    m_deviceSelectDialog->setDocument(m_document);

    showDeviceSelectDialog();
}

IGDControllerMainWindow::~IGDControllerMainWindow()
{
    // nothing to do
}

void IGDControllerMainWindow::showDeviceSelectDialog()
{
    m_deviceSelectDialog->setVisible(true);
}

void IGDControllerMainWindow::appAbout()
{
    Oct::App::GuiApplication::showAboutDialog(this);
}

void IGDControllerMainWindow::qtAbout()
{
    Oct::App::GuiApplication::showAboutQtDialog(this);
}
