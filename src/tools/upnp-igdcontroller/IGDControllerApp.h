#ifndef _IGD_CONTROLLER__IGD_CONTROLLER_APP_H_
#define _IGD_CONTROLLER__IGD_CONTROLLER_APP_H_

#include "IGDControllerMainWindow.h"

#include <app/GuiApplication.h>

class IGDControllerApp : public Oct::App::GuiApplication
{
    Q_OBJECT

private:
    IGDControllerMainWindow* m_mainWindow;

public:
    /**
     * Constructs application.
     *
     * @param argc
     *      Arguments count (from main function).
     * @param argv
     *      Arguments array (from main function).
     */
    IGDControllerApp(int& argc, char** argv);

protected:
    virtual int initResources();

    virtual int initWindows();

    virtual QMainWindow* mainWindow();

};

#endif /* _IGD_CONTROLLER__IGD_CONTROLLER_APP_H_ */
