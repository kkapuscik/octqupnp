/*
 * Copyright (C) 2011 Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "Document.h"

/*-------------------------------------------------------------------------------------*/

Document::Document(QObject* parent) :
        QObject(parent) {
    m_controlPoint = new Oct::UpnpCp::ControlPoint(this);
    m_deviceListModel = new DeviceListModel(this);

    connect(m_controlPoint, SIGNAL( rootDeviceAdded(Oct::UpnpCp::Device) ), m_deviceListModel,
            SLOT( addRootDevice(Oct::UpnpCp::Device) ));
    connect(m_controlPoint, SIGNAL( rootDeviceRemoved(Oct::UpnpCp::Device) ), m_deviceListModel,
            SLOT( removeRootDevice(Oct::UpnpCp::Device) ));

    m_controlPoint->start();

}

Document::~Document() {
    // nothing to do
}

QAbstractListModel* Document::getDeviceListModel() {
    return m_deviceListModel;
}

void Document::setActiveDevice(int deviceIndex) {
    Oct::UpnpCp::Device device = m_deviceListModel->device(deviceIndex);
    if (device.isValid()) {
        // TODO: clean current device

        // TODO: set new device
    }
}

/*-------------------------------------------------------------------------------------*/
