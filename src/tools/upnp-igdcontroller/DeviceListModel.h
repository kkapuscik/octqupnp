/*
 * Copyright (C) 2012 Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef DEVICELISTMODEL_H_
#define DEVICELISTMODEL_H_

/*-------------------------------------------------------------------------------------*/

#include <upnpcp/Device.h>

#include <QtCore/QAbstractListModel>
#include <QtCore/QList>

/*-------------------------------------------------------------------------------------*/

class DeviceListModel : public QAbstractListModel {
    Q_OBJECT

private:
    static const QString DEVICE_TYPE_IGD;

private:
    QList<Oct::UpnpCp::Device> m_deviceList;

public:
    DeviceListModel(QObject* parent = 0);
    virtual ~DeviceListModel();

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    Oct::UpnpCp::Device device(int index);

public slots:
    void addRootDevice(Oct::UpnpCp::Device device);
    void removeRootDevice(Oct::UpnpCp::Device device);

};

/*-------------------------------------------------------------------------------------*/

#endif /* DEVICELISTMODEL_H_ */
