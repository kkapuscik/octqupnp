#ifndef IGDCONTROLLERMAINWINDOW_H
#define IGDCONTROLLERMAINWINDOW_H

#include "Document.h"
#include "DeviceSelectDialog.h"

#include "ui_IGDControllerMainWindow.h"

#include <QtWidgets/QMainWindow>

class IGDControllerMainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::IGDControllerMainWindowClass m_ui;
    DeviceSelectDialog* m_deviceSelectDialog;
    Document* m_document;

public:
    IGDControllerMainWindow(QWidget *parent = 0);
    virtual ~IGDControllerMainWindow();

public slots:
    void appAbout();
    void qtAbout();
    void showDeviceSelectDialog();

};

#endif // IGDCONTROLLERMAINWINDOW_H
