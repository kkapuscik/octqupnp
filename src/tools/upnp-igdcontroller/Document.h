/*
 * Copyright (C) 2012 Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef DOCUMENT_H_
#define DOCUMENT_H_

/*-------------------------------------------------------------------------------------*/

#include "DeviceListModel.h"

#include <upnpcp/ControlPoint.h>
#include <QtCore/QObject>
#include <QtCore/QAbstractListModel>

/*-------------------------------------------------------------------------------------*/

class Document : public QObject {
    Q_OBJECT

private:
    Oct::UpnpCp::ControlPoint* m_controlPoint;
    DeviceListModel* m_deviceListModel;

public:
    Document(QObject* parent = 0);
    virtual ~Document();

    QAbstractListModel* getDeviceListModel();
    void setActiveDevice(int deviceIndex);

};

/*-------------------------------------------------------------------------------------*/

#endif /* DOCUMENT_H_ */
