/*
 * $Id: main.cpp 182 2012-01-23 06:21:53Z saveman $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "IGDControllerApp.h"

/*---------------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    return Oct::App::GuiApplication::startApp<IGDControllerApp>(
            "UPnP-IGDController", "0.1", argc, argv);
}

/*---------------------------------------------------------------------------*/
