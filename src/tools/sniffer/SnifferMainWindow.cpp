/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SnifferMainWindow.h"

#include <app/GuiApplication.h>

Oct::Log::Logger<SnifferMainWindow> SnifferMainWindow::Logger("/oct/tools/sniffer/SnifferMainWindow");

SnifferMainWindow::SnifferMainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_ui.setupUi(this);
    m_document = new SnifferDocument(this);

    m_ui.m_packetTableView->setModel(m_document->packetTableModel());

    m_ui.m_actionShowPacketDetails->setChecked(true);

    m_ui.m_packetTableView->setColumnWidth(0, 150);
    m_ui.m_packetTableView->setColumnWidth(1, 150);
    m_ui.m_packetTableView->setColumnWidth(2, 120);
    m_ui.m_packetTableView->setColumnWidth(3, 760);

    connect(m_ui.m_packetTableView->selectionModel(),
            SIGNAL( currentChanged(const QModelIndex&, const QModelIndex&) ),
            this,
            SLOT( currentPacketChanged(const QModelIndex&, const QModelIndex&) ));

    /* enable capture */
    enableCapture(true);
    enableMulticasts(true);
}

SnifferMainWindow::~SnifferMainWindow()
{
    // do nothing
}

void SnifferMainWindow::about()
{
    Oct::App::GuiApplication::showAboutDialog(this);
}

void SnifferMainWindow::aboutQt()
{
    Oct::App::GuiApplication::showAboutQtDialog(this);
}

void SnifferMainWindow::clearCapture()
{
    m_document->clear();
}

void SnifferMainWindow::searchAllDevices()
{
    m_document->search(SnifferDocument::SEARCH_TARGET_ALL_DEVICES);
}

void SnifferMainWindow::searchRootDevices()
{
    m_document->search(SnifferDocument::SEARCH_TARGET_ROOT_DEVICES);
}

void SnifferMainWindow::searchMediaRenderers()
{
    m_document->search(SnifferDocument::SEARCH_TARGET_MEDIA_RENDERERS);
}

void SnifferMainWindow::searchMediaServers()
{
    m_document->search(SnifferDocument::SEARCH_TARGET_MEDIA_SERVERS);
}

void SnifferMainWindow::searchInternetGateways()
{
    m_document->search(SnifferDocument::SEARCH_TARGET_INTERNET_GATEWAYS);
}

#include <QtWidgets/QInputDialog>

void SnifferMainWindow::searchCustomType()
{
    QString customSearchType;
    bool ok;

    customSearchType = QInputDialog::getText(this,
            tr("Search Custom Type"),
            tr("Enter search target string:"),
            QLineEdit::Normal,
            m_customSearchType,
            &ok);
    if (ok) {
        m_customSearchType = customSearchType;
        m_document->search(m_customSearchType);
    }
}

void SnifferMainWindow::enableCapture(bool enabled)
{
    m_document->setCaptureEnabled(enabled);
    m_ui.m_actionEnableCapture->setChecked(m_document->captureEnabled());
}

void SnifferMainWindow::enableMulticasts(bool enabled)
{
    m_document->setMulticastsEnabled(enabled);
    m_ui.m_actionEnableMulticasts->setChecked(m_document->multicastsEnabled());
}

void SnifferMainWindow::setAddressFilter()
{
    QString addressFilter;
    bool ok;

    addressFilter = QInputDialog::getText(this,
            tr("Search Custom Type"),
            tr("Enter address to filter:"),
            QLineEdit::Normal,
            m_document->addressFilter(),
            &ok);
    if (ok) {
        m_document->setAddressFilter(addressFilter);
    }
}

void SnifferMainWindow::showPacketDetails(bool enabled)
{
    m_ui.m_dataTextEdit->setVisible(enabled);
}

void SnifferMainWindow::currentPacketChanged(const QModelIndex& current, const QModelIndex& /*previous*/)
{
    QVariant value = m_document->packetTableModel()->data(current, SnifferDocument::PACKET_USER_ROLE);

    // TODO
    // Logger.trace(this) << "CURRENT: " << current << " " << value.toString();

    if (!value.isNull() && value.type() == QVariant::String) {
        m_ui.m_dataTextEdit->setPlainText(value.toString());
    } else {
        m_ui.m_dataTextEdit->setPlainText(QString::null);
    }
}
