/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "SnifferDocument.h"

const QString SnifferDocument::SEARCH_TARGET_ALL_DEVICES("ssdp:all");
const QString SnifferDocument::SEARCH_TARGET_ROOT_DEVICES("upnp:rootdevice");
const QString SnifferDocument::SEARCH_TARGET_MEDIA_RENDERERS("urn:schemas-upnp-org:device:MediaRenderer:1");
const QString SnifferDocument::SEARCH_TARGET_MEDIA_SERVERS("urn:schemas-upnp-org:device:MediaServer:1");
const QString SnifferDocument::SEARCH_TARGET_INTERNET_GATEWAYS("urn:schemas-upnp-org:device:InternetGatewayDevice:1");

const int SnifferDocument::PACKET_USER_ROLE = PacketTableModel::PACKET_USER_ROLE;

SnifferDocument::SnifferDocument(QObject* parent) :
    QObject(parent),
    m_captureEnabled(false),
    m_multicastsEnabled(true)
{
    m_packetModel = new PacketTableModel(this);
    m_ssdpSocket = new Oct::UpnpSsdp::DiscoverySocket(this);
    m_searchSocket = new Oct::UpnpSsdp::DiscoverySocket(this);

    connect(m_ssdpSocket, SIGNAL( aliveMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::AliveMessage&) ),
            this, SLOT( addAliveMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::AliveMessage&) ));
    connect(m_ssdpSocket, SIGNAL( byeByeMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::ByeByeMessage&) ),
            this, SLOT( addByeByeMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::ByeByeMessage&) ));
    connect(m_ssdpSocket, SIGNAL( searchMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchMessage&) ),
            this, SLOT( addSearchMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchMessage&) ));
    connect(m_ssdpSocket, SIGNAL( searchResponseReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchResponseMessage&) ),
            this, SLOT( addSearchResponseMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchResponseMessage&) ));

    connect(m_searchSocket, SIGNAL( aliveMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::AliveMessage&) ),
            this, SLOT( addAliveMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::AliveMessage&) ));
    connect(m_searchSocket, SIGNAL( byeByeMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::ByeByeMessage&) ),
            this, SLOT( addByeByeMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::ByeByeMessage&) ));
    connect(m_searchSocket, SIGNAL( searchMessageReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchMessage&) ),
            this, SLOT( addSearchMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchMessage&) ));
    connect(m_searchSocket, SIGNAL( searchResponseReceived(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchResponseMessage&) ),
            this, SLOT( addSearchResponseMessage(const QHostAddress&, quint16, const Oct::UpnpSsdp::SearchResponseMessage&) ));

    m_ssdpSocket->joinSsdpGroup();
    m_searchSocket->bind(0);
}

SnifferDocument::~SnifferDocument()
{
}

void SnifferDocument::clear()
{
    m_packetModel->clear();
}

QAbstractTableModel* SnifferDocument::packetTableModel()
{
    return m_packetModel;
}

void SnifferDocument::addAliveMessage(
        const QHostAddress& sender,
        quint16 senderPort,
        const Oct::UpnpSsdp::AliveMessage& message)
{
    if (m_captureEnabled && m_multicastsEnabled) {
        if (m_addressFilter.isNull() || m_addressFilter == sender) {
            m_packetModel->appendPacket(PacketTableModel::PacketItem(sender, senderPort, message));
        }
    }
}

void SnifferDocument::addByeByeMessage(
        const QHostAddress& sender,
        quint16 senderPort,
        const Oct::UpnpSsdp::ByeByeMessage& message)
{
    if (m_captureEnabled && m_multicastsEnabled) {
        if (m_addressFilter.isNull() || m_addressFilter == sender) {
            m_packetModel->appendPacket(PacketTableModel::PacketItem(sender, senderPort, message));
        }
    }
}

void SnifferDocument::addSearchMessage(
        const QHostAddress& sender,
        quint16 senderPort,
        const Oct::UpnpSsdp::SearchMessage& message)
{
    if (m_captureEnabled && m_multicastsEnabled) {
        if (m_addressFilter.isNull() || m_addressFilter == sender) {
            m_packetModel->appendPacket(PacketTableModel::PacketItem(sender, senderPort, message));
        }
    }
}

void SnifferDocument::addSearchResponseMessage(
        const QHostAddress& sender,
        quint16 senderPort,
        const Oct::UpnpSsdp::SearchResponseMessage& message)
{
    if (m_captureEnabled) {
        if (m_addressFilter.isNull() || m_addressFilter == sender) {
            m_packetModel->appendPacket(PacketTableModel::PacketItem(sender, senderPort, message));
        }
    }
}

void SnifferDocument::search(const QString& searchTarget)
{
    m_searchSocket->sendSearch(Oct::UpnpSsdp::SearchMessage(searchTarget));
}
