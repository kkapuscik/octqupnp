/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Sniffer
 * @{
 */

#ifndef PACKETTABLEMODEL_H_
#define PACKETTABLEMODEL_H_

#include <log/Logger.h>
#include <upnpssdp/DiscoverySocket.h>

#include <QtCore/QAbstractTableModel>
#include <QtCore/QDateTime>

class PacketTableModel : public QAbstractTableModel
{
    Q_OBJECT

private:
    static Oct::Log::Logger<PacketTableModel> Logger;

public:
    static int PACKET_USER_ROLE;

private:
    enum {
        COLUMN_TIME = 0,
        COLUMN_SOURCE_ADDRESS,
        COLUMN_PACKET_TYPE,
        COLUMN_PACKET_INFO,

        TOTAL_COLUMNS
    };

public:
    class PacketItem {
    public:
        enum Type {
            TYPE_NOTIFY_ALIVE,
            TYPE_NOTIFY_BYEBYE,
            TYPE_SEARCH_REQUEST,
            TYPE_SEARCH_RESPONSE
        };

    private:
        Type m_type;
        QDateTime m_creationTime;
        QString m_source;
        Oct::HttpUdp::Message m_message;

    public:
        PacketItem(const QHostAddress& sender, quint16 senderPort, const Oct::UpnpSsdp::AliveMessage& message)
        {
            init(TYPE_NOTIFY_ALIVE, sender, senderPort, message);
        }

        PacketItem(const QHostAddress& sender, quint16 senderPort, const Oct::UpnpSsdp::ByeByeMessage& message)
        {
            init(TYPE_NOTIFY_BYEBYE, sender, senderPort, message);
        }

        PacketItem(const QHostAddress& sender, quint16 senderPort, const Oct::UpnpSsdp::SearchMessage& message)
        {
            init(TYPE_SEARCH_REQUEST, sender, senderPort, message);
        }

        PacketItem(const QHostAddress& sender, quint16 senderPort, const Oct::UpnpSsdp::SearchResponseMessage& message)
        {
            init(TYPE_SEARCH_RESPONSE, sender, senderPort, message);
        }

        QString type() const
        {
            switch (m_type) {
                case TYPE_NOTIFY_ALIVE:
                    return "NOTIFY-ALIVE";
                case TYPE_NOTIFY_BYEBYE:
                    return "NOTIFY-BYEBYE";
                case TYPE_SEARCH_REQUEST:
                    return "M-SEARCH";
                case TYPE_SEARCH_RESPONSE:
                    return "NOTIFY-SEARCH";
            }
            return QString::null;
        }

        QString source() const
        {
            return m_source;
        }

        QString time() const
        {
            return m_creationTime.toString("hh:ss:mm.zzz");
        }

        QString info() const
        {
            switch (m_type) {
                case TYPE_NOTIFY_ALIVE:
                    return m_message.headers().value("USN");
                case TYPE_NOTIFY_BYEBYE:
                    return m_message.headers().value("USN");
                case TYPE_SEARCH_REQUEST:
                    return m_message.headers().value("ST");
                case TYPE_SEARCH_RESPONSE:
                    return m_message.headers().value("USN");
            }
            return QString();
        }

        QString message() const
        {
            QBuffer messageBuffer;
            bool rv;

            messageBuffer.open(QBuffer::WriteOnly);

            rv = m_message.write(messageBuffer);
            messageBuffer.close();

            if (rv) {
                return QString::fromUtf8(messageBuffer.data(), messageBuffer.size());
            } else {
                return QString("Error");
            }
        }

    private:
        void init(Type type, const QHostAddress& sender, quint16 senderPort, Oct::HttpUdp::Message message)
        {
            m_type = type;
            m_creationTime = QDateTime::currentDateTime();
            m_source = QString("%1:%2").arg(sender.toString()).arg(senderPort);
            m_message = message;
        }

    };

private:
    QList<PacketItem> m_packets;

public:
    PacketTableModel(QObject *parent = 0);

    /*
     * When subclassing QAbstractTableModel, you must implement rowCount(), columnCount(), and data().
     * Default implementations of the index() and parent() functions are provided by QAbstractTableModel.
     * Well behaved models will also implement headerData().
     */

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);

    void clear();
    void appendPacket(const PacketItem& item);
};

#endif /* PACKETTABLEMODEL_H_ */

/**
 * @}
 * @}
 * @}
 */
