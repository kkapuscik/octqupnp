/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Sniffer
 * @{
 */

#ifndef SNIFFERMAINWINDOW_H
#define SNIFFERMAINWINDOW_H

#include "SnifferDocument.h"
#include "ui_SnifferMainWindow.h"

#include <log/Logger.h>

#include <QtWidgets/QMainWindow>

class SnifferMainWindow : public QMainWindow
{
    Q_OBJECT

private:
    static Oct::Log::Logger<SnifferMainWindow> Logger;

private:
    Ui::SnifferMainWindowClass m_ui;
    SnifferDocument* m_document;
    QString m_customSearchType;

public:
    SnifferMainWindow(QWidget *parent = 0);
    ~SnifferMainWindow();

private slots:
    void about();
    void aboutQt();
    void clearCapture();
    void searchAllDevices();
    void searchRootDevices();
    void searchMediaRenderers();
    void searchMediaServers();
    void searchInternetGateways();
    void searchCustomType();
    void enableCapture(bool enabled);
    void enableMulticasts(bool enabled);
    void setAddressFilter();
    void showPacketDetails(bool enabled);
    void currentPacketChanged(const QModelIndex& current, const QModelIndex& previous);
};

#endif // SNIFFERMAINWINDOW_H

/**
 * @}
 * @}
 * @}
 */
