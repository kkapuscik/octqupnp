/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup UPnP_Sniffer
 * @{
 */

#ifndef SNIFFERDOCUMENT_H_
#define SNIFFERDOCUMENT_H_

#include <QtCore/QObject>

#include "PacketTableModel.h"

#include <upnpssdp/DiscoverySocket.h>

class SnifferDocument: public QObject
{
Q_OBJECT

public:
    static const QString SEARCH_TARGET_ALL_DEVICES;
    static const QString SEARCH_TARGET_ROOT_DEVICES;
    static const QString SEARCH_TARGET_MEDIA_RENDERERS;
    static const QString SEARCH_TARGET_MEDIA_SERVERS;
    static const QString SEARCH_TARGET_INTERNET_GATEWAYS;

    static const int PACKET_USER_ROLE;

private:
    PacketTableModel* m_packetModel;
    Oct::UpnpSsdp::DiscoverySocket* m_ssdpSocket;
    Oct::UpnpSsdp::DiscoverySocket* m_searchSocket;
    bool m_captureEnabled;
    bool m_multicastsEnabled;
    QHostAddress m_addressFilter;

public:
    SnifferDocument(QObject* parent = 0);
    virtual ~SnifferDocument();

    QAbstractTableModel* packetTableModel();

    bool captureEnabled() const
    {
        return m_captureEnabled;
    }

    bool multicastsEnabled() const
    {
        return m_multicastsEnabled;
    }

    QString addressFilter() const
    {
        return m_addressFilter.toString();
    }

    void setCaptureEnabled(bool enabled)
    {
        m_captureEnabled = enabled;
    }

    void setMulticastsEnabled(bool enabled)
    {
        m_multicastsEnabled = enabled;
    }

    void setAddressFilter(const QString& filter)
    {
        QString tmp = filter.trimmed();

        if (filter.isNull() || filter.length() == 0) {
            m_addressFilter = QHostAddress::Null;
        } else {
            m_addressFilter = QHostAddress(filter);
        }
    }

    void search(const QString& searchTarget);

    void clear();

private slots:
    void addAliveMessage(const QHostAddress& sender, quint16 senderPort, const Oct::UpnpSsdp::AliveMessage& message);

    void addByeByeMessage(const QHostAddress& sender, quint16 senderPort, const Oct::UpnpSsdp::ByeByeMessage& message);

    void addSearchMessage(const QHostAddress& sender, quint16 senderPort, const Oct::UpnpSsdp::SearchMessage& message);

    void addSearchResponseMessage(const QHostAddress& sender, quint16 senderPort,
            const Oct::UpnpSsdp::SearchResponseMessage& message);
};

#endif /* SNIFFERDOCUMENT_H_ */

/**
 * @}
 * @}
 * @}
 */
