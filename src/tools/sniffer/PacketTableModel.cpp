/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "PacketTableModel.h"

Oct::Log::Logger<PacketTableModel> PacketTableModel::Logger("/oct/tools/sniffer/PacketTableModel");

int PacketTableModel::PACKET_USER_ROLE = Qt::UserRole + 1;

PacketTableModel::PacketTableModel(QObject* parent) :
    QAbstractTableModel(parent)
{
}

void PacketTableModel::clear()
{
    beginResetModel();
    m_packets.clear();
    endResetModel();
}

int PacketTableModel::rowCount(const QModelIndex& /*parent*/) const
{
    return m_packets.size();
}

int PacketTableModel::columnCount(const QModelIndex& /*parent*/) const
{
    return TOTAL_COLUMNS;
}

QVariant PacketTableModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        // TODO
        // Logger.trace(this) << "Invalid index" << index;
        return QVariant();
    }

    if (index.row() >= rowCount() || index.column() >= columnCount()) {
        // TODO
        // Logger.trace(this) << "Index outside range" << index;
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        switch (index.column()) {
            case COLUMN_TIME:
                return m_packets[index.row()].time();
            case COLUMN_SOURCE_ADDRESS:
                return m_packets[index.row()].source();
            case COLUMN_PACKET_TYPE:
                return m_packets[index.row()].type();
            case COLUMN_PACKET_INFO:
                return m_packets[index.row()].info();
        }
    } else if (role == PACKET_USER_ROLE) {
        return m_packets[index.row()].message();
    }

    return QVariant();

}

QVariant PacketTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            switch (section) {
                case COLUMN_TIME:
                    return QVariant("Time");
                case COLUMN_SOURCE_ADDRESS:
                    return QVariant("Source Address");
                case COLUMN_PACKET_TYPE:
                    return QVariant("Packet Type");
                case COLUMN_PACKET_INFO:
                    return QVariant("Packet Information");
            }
        }
    }

    return QVariant();
}

void PacketTableModel::appendPacket(const PacketItem& item)
{
    // TODO: with sorting

    beginInsertRows(QModelIndex(), m_packets.size(), m_packets.size());
    m_packets.append(item);
    endInsertRows();
}

void PacketTableModel::sort(int column, Qt::SortOrder order)
{
    Logger.trace(this) << "SORT REQUESTED: " << column << order;
}
