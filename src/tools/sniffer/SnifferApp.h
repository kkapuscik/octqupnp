/*
 * AVPlayerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef _SNIFFER__SNIFFER_APP_H_
#define _SNIFFER__SNIFFER_APP_H_

#include "SnifferMainWindow.h"

#include <app/GuiApplication.h>

class SnifferApp : public Oct::App::GuiApplication
{
    Q_OBJECT

private:
    SnifferMainWindow* m_mainWindow;

public:
    /**
     * Constructs application.
     *
     * @param argc
     *      Arguments count (from main function).
     * @param argv
     *      Arguments array (from main function).
     */
    SnifferApp(int& argc, char** argv);

protected:
    virtual int initResources();

    virtual int initWindows();

    virtual QMainWindow* mainWindow();

};

#endif /* _SNIFFER__SNIFFER_APP_H_ */
