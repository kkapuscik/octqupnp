/*
 * AVPlayerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "SnifferApp.h"

#include <QtCore/QResource>

SnifferApp::SnifferApp(int& argc, char** argv) :
    Oct::App::GuiApplication(argc, argv)
{
    // nothing to do
}

int SnifferApp::initResources()
{
    Q_INIT_RESOURCE(Sniffer);

    return 0;
}

int SnifferApp::initWindows()
{
    m_mainWindow = new SnifferMainWindow();
    m_mainWindow->show();

    return 0;
}

QMainWindow* SnifferApp::mainWindow()
{
    return m_mainWindow;
}
