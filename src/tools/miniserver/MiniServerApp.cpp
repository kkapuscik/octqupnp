/*
 * AVPlayerItem.cpp
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#include "MiniServerApp.h"

#include <QtCore/QResource>

MiniServerApp::MiniServerApp(int& argc, char** argv) :
    Oct::App::GuiApplication(argc, argv)
{
    // nothing to do
}

int MiniServerApp::initWindows()
{
    m_mainWindow = new MiniServerMainWindow();
    m_mainWindow->show();

    return 0;
}

QMainWindow* MiniServerApp::mainWindow()
{
    return m_mainWindow;
}
