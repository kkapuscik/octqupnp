/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

#include "MiniServerMainWindow.h"

#include <QtWidgets/QMessageBox>

#include <httpserver/StandardFS.h>
#include <httpserver/VfsRequestHandlerProvider.h>
#include <app/GuiApplication.h>

MiniServerMainWindow::MiniServerMainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    m_ui.setupUi(this);

    logAppendLine(tr("MiniServer started"));

    m_server = new Oct::HttpServer::Server(this);
    m_server->requestHandlerManager().registerHandlerProvider("/",
            new Oct::HttpServer::VfsRequestHandlerProvider(new Oct::HttpServer::StandardFS(QDir::homePath(), this)));
    m_server->requestHandlerManager().registerHandlerProvider("/test/",
            new Oct::HttpServer::VfsRequestHandlerProvider(new Oct::HttpServer::StandardFS(QDir::homePath(), this)));
    m_server->requestHandlerManager().registerHandlerProvider("/test/test/",
            new Oct::HttpServer::VfsRequestHandlerProvider(new Oct::HttpServer::StandardFS(QDir::homePath(), this)));

    // m_server->setPort(8000);
    m_server->start();

    logAppendLine(tr("Server listening at port: %1").arg(m_server->port()));
    logAppendLine(tr("Server address: http://localhost:%1/").arg(m_server->port()));
}

void MiniServerMainWindow::appAbout()
{
    Oct::App::GuiApplication::showAboutDialog(this);
}

void MiniServerMainWindow::logClear()
{
    m_ui.logTextEdit->setPlainText(QString::null);
}

void MiniServerMainWindow::logAppendLine(const QString& message, bool addNewLine)
{
    m_ui.logTextEdit->appendPlainText(message);
    if (addNewLine) {
        m_ui.logTextEdit->appendPlainText(QString("\n"));
    }
}
