/*
 * $Id: $
 *
 * Part of OCTaedr libraries.
 * Copyright (C) 2010-2011 Krzysztof Kapuscik (k.kapuscik@gmail.com)
 */

/**
 * \addtogroup OCT
 * @{
 * \addtogroup Tools
 * @{
 * \addtogroup MiniServer
 * @{
 */

#ifndef _MINI_SERVER__MINI_SERVER_MAIN_WINDOW_H_
#define _MINI_SERVER__MINI_SERVER_MAIN_WINDOW_H_

#include <QtWidgets/QMainWindow>
#include "ui_MiniServerMainWindow.h"
#include "httpserver/Server.h"

class MiniServerMainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MiniServerMainWindowClass m_ui;
    Oct::HttpServer::Server* m_server;

public:
    MiniServerMainWindow(QWidget *parent = 0);

protected slots:
    void appAbout();
    void logClear();
    void logAppendLine(const QString& message, bool addNewLine = true);
};

#endif // _MINI_SERVER__MINI_SERVER_MAIN_WINDOW_H_

/**
 * @}
 * @}
 * @}
 */
