/*
 * AVPlayerItem.h
 *
 *  Created on: 27-02-2011
 *      Author: saveman
 */

#ifndef _MINI_SERVER__MINI_SERVER_APP_H_
#define _MINI_SERVER__MINI_SERVER_APP_H_

#include "MiniServerMainWindow.h"

#include <app/GuiApplication.h>

class MiniServerApp : public Oct::App::GuiApplication
{
    Q_OBJECT

private:
    MiniServerMainWindow* m_mainWindow;

public:
    /**
     * Constructs application.
     *
     * @param argc
     *      Arguments count (from main function).
     * @param argv
     *      Arguments array (from main function).
     */
    MiniServerApp(int& argc, char** argv);

protected:
    virtual int initWindows();

    virtual QMainWindow* mainWindow();
};

#endif /* _MINI_SERVER__MINI_SERVER_APP_H_ */
