/**
\todo Short list:
- UI polishing
- Add classes documentation
- Cleanup of the documentation
- Cleanup of the source code (app part)
- Clean library dependencies in makefiles

GENERAL:
- multithread support (http handler, object deletion etc.)

SOAP:
versioning - checking and errors
checking for actors and mustUnderstand
types

UPNPDEV:
- should depend on soapserver only? to be checked

*/
