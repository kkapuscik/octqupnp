#
# Main makefile
#

default: build-debug

all: build build-doc

build: build-debug build-release

distclean: clean clean-doc

build-debug build-release build-doc clean clean-debug clean-release clean-doc:
	$(MAKE) -f makefile-linux.mk $@
